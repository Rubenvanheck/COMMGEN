function location = COMMGEN_location
%this function is used as a reference point in Matlab for the other functions.
location = mfilename('fullpath');
location = regexprep(location,'/COMMGEN_location','');
end

