[metanetx]: www.metanetx.org
[mnxref]: http://nar.oxfordjournals.org/content/early/2015/11/01/nar.gkv1117.full
[SBML]: http://sbml.org/Main_Page
[COBRA]: https://opencobra.github.io/cobratoolbox/

<h1>COMMGEN</H1>
COMMGEN is a tool for COnsensus Metabolic Model GENeration in the MATLAB environment. 
It facilitates matching and comparing the contents of independently generated genome-scale constraint-based metabolic models for the same organism.
These models aim to comprehensively represent the knowledge on the metabolism of an organism, yet different models often differ substantially
due to differences in the construction process; consulted databases, construction goals, expert knowledge, incorporated literature.
COMMGEN guides the user in reconciling such indepdently generated models in order to obtain a more comprehensive consensus model.
As COMMGEN does not involve any optimization for model performance, the resulting consensus model does not necessarily perform better on 
'model validation' tests such as the prediction of phenotypes for different media or deletion strains.

![Alt text](COMMGEN/Images/Fig2a.png)

Please check out the [paper](http://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1005085) for more information.

<h2>Installation instructions</h2>
Necessary for basic use:
0. Add the COMMGEN folder to the MATLAB path
0. Adapt the file [perlloc](https://gitlab.com/Rubenvanheck/COMMGEN/blob/master/COMMGEN/perlloc) in the COMMGEN folder such that it points to the Perl location on your computer.

Necessary for automatic conversion into MNXref namespace:
0. Install the Perl library [Mechanize](http://cpansearch.perl.org/src/PETDANCE/WWW-Mechanize-Cached-1.32/INSTALL)
0. Adapt the file [Mechloc](https://gitlab.com/Rubenvanheck/COMMGEN/blob/master/COMMGEN/MechLoc) in the COMMGEN folder such that it points to the Mechanize location on your computer. For example: /opt/local/lib/perl5/site_perl/5.12.4

<h2>User manual - Short</h2>
Here we cover the following topics:
0. Convert models to [MNXref] namespace
0. Load models into COMMGEN
0. COMMGEN methods
0. COMMGEN options
0. Re-running COMMGEN procedure

<h4>Conversion to MNXref namespace</h4>
COMMGEN uses the overlap between two (or more) metabolic models to identify more overlap and inconsistencies. 
Therefore, we use the standardised [MNXref] namespace to identify the initial overlap in metabolites between the models.
Models in SBML format can be converted into this namespace either via [MetaNetX] or via COMMGEN scripts (recommended).

<h6>MNXref namespace via MetaNetX</h6>
0. Go to [MetaNetX] and upload the model via the 'Upload model' tab. 
[MetaNetX] provides a mapping overview that indicates which MNXref metabolite ID has been assigned to each metabolite in the original SBML file. 
It is important to thoroughly check this overview as erroneous mappings can have a strong detrimental effect on the model. 
If any mappings are incorrect the id of these metabolites have to be appended with 'my_' or 'user_' in the original SBML file before uploading it.
If the model upload at [MetaNetX] fails due to 'illegal characters' use the Perl function [RemIllegalChars](https://gitlab.com/Rubenvanheck/COMMGEN/blob/master/COMMGEN/MetaNetXInteraction/RemIllegalChars.pl) to remove any illegal characters from the SBML file.
0. At [MetaNetX] go to the 'Summary' tab, click on the name of the model, and download and unpack the tarball ('*.tar.gz'). 
Alternatively, download the individual 'Model parts' .tsv files and place them within a single folder.
0. Use the perl script [c2m](https://gitlab.com/Rubenvanheck/COMMGEN/blob/master/COMMGEN/MetaNetXInteraction/m2c.pl) to convert these .tsv files to the COMMGEN format. Syntax: 'perl m2c.pl <oldfolder> <newfolder>'
0. In Matlab, run the function [tsv2mat](https://gitlab.com/Rubenvanheck/COMMGEN/blob/master/COMMGEN/deploy_oo/mscripts/tsv2mat.m) in order to create a MATLAB file that is ready to be loaded into COMMGEN. Syntax: "tsv2mat('-d',path')"

<h6>MNXref namespace via COMMGEN</h6>
NOTE: this step requires the Perl Mechanize library to be available. See Installation instructions.

All steps under 'MNXref namespace via MetaNetX' can also be directly performed in Matlab using the [SBMLToCOMMGEN](https://gitlab.com/Rubenvanheck/COMMGEN/blob/master/COMMGEN/Functions/SBMLToCOMMGEN.m) function.

<h4>Loading models into COMMGEN</h4>
Models are loaded into COMMGEN by calling the [COMMGEN class](https://gitlab.com/Rubenvanheck/COMMGEN/blob/master/COMMGEN/Classes/COMMGEN.m) in Matlab.
The COMMGEN class takes three arguments to initialise:
0. modelNames - A cell array containing the names of the model folders to load into COMMGEN.
0. options (optional) - an [MergeOptions class](https://gitlab.com/Rubenvanheck/COMMGEN/blob/master/COMMGEN/Classes/MergeOptions.m) object containing pre-set options. See [COMMGEN options]() for details.
0. LoadPrevious (optional) - the name of a folder containing a previously saved COMMGEN procedure. See 'Re-running COMMGEN procedure' for details.

<h4>COMMGEN methods</h4>
COMMGEN comes with a variety of methods to find match parts between the models, to find inconsistencies, and ultimately to export the consensus model.
Here follows an overview of the methods to use. See the [COMMGEN class](https://gitlab.com/Rubenvanheck/COMMGEN/blob/master/COMMGEN/Classes/COMMGEN.m) documentation for more details.

| Method name                        | Purpose                          | User input required | Short description |
| --------                           | --------                         | ------- | -------- |
| SimilarSpecies                     | Metabolite matching 1:1          | yes     | Matches metabolite instances for the same metabolite. Limited to matching 1 metabolite instance to 1 metabolite instance |
| SimilarSpeciesPairs                | Metabolite matching 2:2          | yes     | Matches instances of pairs of metabolites that correspond to the same pair. For example, two alternative representations of the same redox pair.|
| DuplicateRxns                      | Identical net reactions          | partial | Identifies and merges reactions with the same net reaction. Note that reaction directionalities and GPR rules may still differ. |
| SameMetsDiffStoich                 | Alternative stoichiometries      | partial | Identifies and merges reactions involving the same metabolites but in different stoichiometries. |
| LumpedReactions                    | Lumped reactions                 | yes     | Identifies potential cases of lumped reactions. |
| AltRedoxPairs                      | Alternative redox                | yes     | Identifies reactions that only differ in the involved redox pairs. There is often uncertainty in which redox pair is involved in which reaction during model construction, as this differs between organisms and databases. As a result, reactions involving redox metabolites often differ between models for the same organism. |
| NestedReactions                    | Nested reactions                 | partial | Identifies sets of reactions where one reaction is a perfect subset of the other. This is especially relevant for models where some metabolites are ignored.|
| SimilarReactions                   | Partially overlapping reactions  | yes     | Identifies sets of reactions that share metabolites and gene associations, but are not identical.|
| MultipleTransportSameSpecSameComps | Alternative transport reactions  | yes     | Identifies sets of reactions that transport the same metabolites across the same membrane. Transporters and corresponding reactions are often unknown and thus differ substantially between independently generated models. |
| InvalidTransport                   | Invalid transport reactions      | no      | Identifies reactions that transport metabolites between compartments that are not connected. These reactions are then split in multiple. |
| SameRxnDiffComp                    | Alternative compartmentalisation | partial | Identifies reactions that occur in different compartments according to different models. |
| ResolveUnknownComp                 | Unknown compartment              | partial | Identifies reactions involving metabolites with unknown compartmentalisation. This compartmentalisation is often a result of an error in the original SBML file. Attempts to automatically determine the compartmentalisation based on other reactions involving the same metabolites, otherwise requires user input. |
| InvalidExternalRxns                | Invalid boundary reactions       | no      | Identifies and splits boundary reactions that involve non-extracellular metabolites.         |
| RemoveCompartment                  | Remove compartment               | no      | Removes a compartment from the model. The reactions involving that metabolite are moved/changed, or removed depending on the exact reaction. |
| Export                             | Export                           | no      | Exports the consensus model in various formats. |

<h4>COMMGEN options</h4>
The inconsistencies that are identified by COMMGEN can be handled by the user on a case-by-case basis, 
but can also partially be handled automatically based on pre-set options.
Here follows an overview of the options in the [MergeOptions](https://gitlab.com/Rubenvanheck/COMMGEN/blob/master/COMMGEN/Classes/MergeOptions.m) class that can be set before initialising COMMGEN.
See the documentation of [MergeOptions](https://gitlab.com/Rubenvanheck/COMMGEN/blob/master/COMMGEN/Classes/MergeOptions.m) for more details.

| Option name                      | Short description |
| --------                        | -------- |
| BufSize                         | Increase this value if Matlab textscan throws a buffer size error. |
| RDPredict                       | Whether to perform the reaction directionality prediction. |
| databaseNames                   | Names of the model folders on which to base the reaction directionality prediction. |
| ValidExchange                   | Lists compartments from which exchange reactions are accepted. By default only the extracellular compartment. |
| CompartmentNames                | Links compartment identifiers and compartment names. |
| CompartmentConnections          | Indicates which compartments are directly connected and can have transport reactions between them. |
| ResolveInvalidTransport         | Indicates whether invalid transport reactions should be resolved automatically. |
| InvTransportSolution            | Indicates how to automatically resolve invalid transport reactions. |
| MergeArrows                     | Indicates how to automatically resolve inconsistencies in reaction directionality between matched reactions. |
| DirectModel                     | Indicates from which model the reaction directionalities will be taken if two matched reactions have different directionalities. Dependent on MergeArrows.  |
| GeneOR                          | Indicates whether differing GPR rules from matched reactions should be automatically resolved. |
| GeneORStrict                    | If differing GPR rules are automatically resolved, this indicates whether this is in a 'strict' or 'flexible' manner. |
| PermanentReject                 | Indicates whether user-rejected matches between metabolites or reactions may be proposed again later. |
| SameRxnDiffComp_RemoveDead      | Indicates whether a reaction that is inactive in a compartment may be removed if the same reaction is active in another compartment. 'Active' indicates whether the reaction is capable of carrying flux. |
| SameRxnDiffComp_AutoGPR         | Indicates whether GPR rules between matched reactions may still be processed automatically if the reactions originate from different compartments. |
| SameRxnDiffComp_AllActSameModel | Indicates whether sets of identical reactions in different compartments should be ignored if they all occur in the same model. |
| SameRxnDiffComp_IgnoreAllInact  | Indicates whether sets of identical reactions in different compartments should be ignored if they are all inactive. |
| HubMets                         | Lists metabolites that are involved in too many reactions to be relevant for the 'SimilarReactions' method. Other metabolites may be added as well. |
| HubComplete                     | Indicates whether 'HubMets' is considered to be complete. |
| HubThreshold                    | Indicates the percentage of top connected metabolites that are considered for the hub. |
| IgnoredSpec                     | Indicates which metabolites are ignored when comparing reactions. |
| RedoxPairs                      | Lists redox pairs. |
| Lumped_Threshold                | Indicates the maximum number of reactions that is considered for reaction lumping. |
| Lumped_MinGeneFrac              | Indicates the minimal fraction of overlap in genes between lumped and non-lumped reaction representations. |
| AllAuto                         | Indicates whether everything has to be automatic; skip all user input steps. |
| ExactlyIdentical                | Indicates whether two reactions may only be combined if they are exactly identical. |


<h4>Re-running COMMGEN procedure</h4>
The COMMGEN method SaveChoices saves all the changes that were made during the COMMGEN procedure in a specified target folder. 
This folder can then be referenced when initialising COMMGEN in order to repeat all the steps that were made. 
In addition, if you want to undo any of the changes that were made, you can remove these changes from the files in that folder.
COMMGEN will then not repeat those steps and will also not perform any of the later steps that relied on that step being performed.


For example: you discover that two metabolite instances that were matched some time ago are actually different metabolites.
You use COMMGEN_model.SaveChoices('ref_dir') to save all the made changes. 
In the directory ref_dir you remove the line in 'TrackSpec.tsv' where these metabolites were matched.
When now using this folder in the initialisation of COMMGEN, these metabolites are no longer matched. 
Any reactions that were subsequently matched because of the metabolite matching are also no longer matched. 