function [SpecMap] = importMapping(filename)
%IMPORTFILE Import numeric data from a text file as column vectors.

%% Initialize variables.
delimiter = '\t';
startRow = 1;
endRow = inf;

%% Format string for each line of text:
formatSpec = '%s%s%s%s%s%s%*s%[^\n\r]';

%% Open the text file.
fileID = fopen(filename,'r');

%% Read columns of data according to format string.
dataArray = textscan(fileID, formatSpec, endRow(1)-startRow(1)+1, 'Delimiter', delimiter, 'HeaderLines', startRow(1)-1, 'ReturnOnError', false);
for block=2:length(startRow)
    frewind(fileID);
    dataArrayBlock = textscan(fileID, formatSpec, endRow(block)-startRow(block)+1, 'Delimiter', delimiter, 'HeaderLines', startRow(block)-1, 'ReturnOnError', false);
    for col=1:length(dataArray)
        dataArray{col} = [dataArray{col};dataArrayBlock{col}];
    end
end

%% Close the text file.
fclose(fileID);

%% Allocate imported array to column variable names
VarName1 = dataArray{:, 1};
VarName2 = dataArray{:, 2};
VarName3 = dataArray{:, 3};
VarName4 = dataArray{:, 4};
VarName5 = dataArray{:, 5};
VarName6 = dataArray{:, 6};
SpecMap = [VarName1 VarName2 VarName3 VarName4 VarName5 VarName6];

%% trim SpecMap to requires parts
%isolate rows corresponding to species conversions
mapped = ~cellfun(@isempty,regexp(SpecMap(:,5),'^MNXM[0-9]*$'));
SpecMap(~mapped,:) = [];

%remove non-required columns
SpecMap(:,[1 2]) = [];

end

