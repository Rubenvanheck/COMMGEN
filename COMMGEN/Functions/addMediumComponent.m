function model = addMediumComponent(model,Compound,Stoich)

if nargin < 3
    Stoich = -10;
end

er = findER(model);

ExcRxn = intersect(findRxnsFromMets(model,Compound),model.rxns(er));

if ~isempty(ExcRxn)
    model = changeRxnBounds(model,ExcRxn,Stoich,'l');
end