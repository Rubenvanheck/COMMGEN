function model = ResolveNaNs(model)
%%% PURPOSE
% This function is meant to be run before converting COMMGEN models
% into COBRA models. In COBRA, NaN values for lower and upper
% bounds of reactions is not allowed. What this function does is to
% automatically set the lower and upper bounds of these reactions
% as inferred from the reaction arrows.
%%% EXAMPLE CALL
%   COMMGEN_model = ResolveNaNs(COMMGEN_model);
%%% INPUT
%   model: COMMGEN model structure.
%%% OPTIONAL INPUT
%%% OUTPUT
%   model: Updated COMMGEN model structure

% Obtain default (max/min) values for upper and lower bounds
defLB = 0;
defUB = 0;
for i=1:numel(model.reactions)
    if model.reactions{i}.lb < defLB
        defLB = model.reactions{i}.lb;
    end
    
    if model.reactions{i}.ub > defUB
        defUB = model.reactions{i}.ub;
    end
end

% Replace NaNs by default values depending on reaction
% arrows
for i=1:numel(model.reactions)
    if isnan(model.reactions{i}.lb);
        if regexp(model.reactions{i}.equation,'<==>|<--')
            model.reactions{i}.lb = defLB;
        else
            model.reactions{i}.lb = 0;
        end
    end
    
    if isnan(model.reactions{i}.ub);
        if regexp(model.reactions{i}.equation,'<==>|-->')
            model.reactions{i}.ub = defUB;
        else
            model.reactions{i}.ub = 0;
        end
    end
end
end