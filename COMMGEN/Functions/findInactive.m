function [M,R] = findInactive(model)
% This function removes Reactions that can not carry flux, metabolites that
% can either not be produced or consumed and genes that were only associated
% with reactions that have been removed.

changeCobraSolver('gurobi');

AllMets = model.mets;
AllRxns = model.rxns;

%Enable all exchange reactions
er = findER(model);
model.lb(er) = -1000;
model.ub(er) = 1000;
model.ub(isnan(model.ub)) = 1000;

% Find the maximal and minimal possible flux for each reaction
minflux = RxnFluxfun(model,'min');
maxflux = RxnFluxfun(model,'max');
maxflux(minflux>maxflux) = 0; % fixing numerical issues where both ~~ 0
minflux(minflux>maxflux) = 0; % fixing numerical issues where both ~~ 0

% If it is not possible to have flux through the reaction (under the
% current model conditions!) The reactions are removed
model = removeRxns(model,model.rxns(minflux==0 & maxflux==0));

% Remove all dead end (metabolites)
model = removeDeadEnds(model);

M = setdiff(AllMets,model.mets);
R = setdiff(AllRxns,model.rxns);

end

function [RxnFlux] = RxnFluxfun(model,minmax)
% This function returns either the minimally achievable or maximally
% achievable (default) flux for each reaction in the model.

if nargin<2
    minmax = 'max';
    disp('No directionality for the optimization given, defaulted to maximization')
end

RxnFlux = zeros(length(model.rxns),1); %Pre-allocation of variable
for i=1:length(model.rxns) % for each reaction
    tempmodel = changeObjective(model,model.rxns(i),1); % turn it into the objective
    Solution = optimizeCbModel(tempmodel,minmax);
    RxnFlux(i) = Solution.f; % and save the objective value.
end
end