function model = SwitchMedium(model,Medium,mediumFile)
%This function is used to switch between different media compositions. 
% INPUT
% model     Cobra model structure
% Medium    string corresponding to a known medium

%% Obtain medium composition

%Load available compositions
[~, ~, raw] = xlsread(mediumFile,'Media');

%identify medium of interest
medCol = find(strcmp(raw(1,:),Medium));
if isempty(medCol)
    error('Medium unknown!')
end

raw(1,:) = [];
Cpds = raw(:,2); %all possible medium constituents
Medium = cell2mat(raw(:,medCol)); %Boolean indicator 

Cpds = Cpds(logical(Medium));

Cpds = strcat(Cpds,'[MNXC2]');%Medium components are in the extracellular space (MNXC2)

%% Adjust medium composition in model
%Identify all boundary reactions
er = findER(model);

%Reset medium composition
model = changeRxnBounds(model,model.rxns(er),0,'l');

%Obtain reactions corresponding to medium components
MedRxns = intersect(findRxnsFromMets(model,Cpds),model.rxns(er));

%Set medium
model = changeRxnBounds(model,MedRxns,min(model.lb),'l');

% Find reactions which have to be allowed in order to obtain biomass
% (irrespective of medium composition)
tmpmodel = model;
er = find(findER(tmpmodel));
tmpmodel.lb(er) = min(tmpmodel.lb);
for i=1:numel(er)
    tmpmodel.lb(er(i)) = 0;
    sol = optimizeCbModel(tmpmodel);
    if sol.f<10^-6
        model.lb(er(i)) = min(model.lb);
    end
    tmpmodel.lb(er(i)) = min(model.lb);
end
end