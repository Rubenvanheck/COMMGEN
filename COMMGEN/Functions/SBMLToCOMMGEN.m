function dirname = SBMLToCOMMGEN(filename,dirname)
%%% PURPOSE 
% Converts an SBML file into the COMMGEN format. This involves converting
% the model into the MnXRef namespace. In order to run this function, perl
% and mechanize need to be set up.
%%% EXAMPLE CALL
%   COMMGEN_model = SBMLToCOMMGEN('Models/Model1');
%%% INPUT
%   filename: path to the SBML model
%%% OPTIONAL INPUT
%   dirname: desired name of COMMGEN model folder
%   Default value: [regexprep(filename,'\..*','') '_COMMGEN']
%%% OUTPUT
%   dirname: name of the COMMGEN model. This is subsequently used when
%   loading the model into COMMGEN.

if nargin<2
    dirname = [regexprep(filename,'\..*','') '_COMMGEN'];
end

try 
    mkdir(dirname);
catch
    error('Failed at creating the directory. Please check the input argument(s) for invalid characters.')
end

%% Conversion to MnXRef
%variables
tmpfilename = ['tmp_' filename];
mapfile = [tmpfilename '.mapping'];
MechanizeLocation = '/Users/rubenvanheck/perl5/perlbrew/perls/perl-5.22.1/lib/site_perl/5.22.1'; %This may vary between systems. Enter the correct folder for your computer here. (omit the /WWW at the end)
path = COMMGEN_location;

% convert GSM
%remove illegal characters
%currently MetaNetX cannot handle a range of special characters in metabolite names..
dos(['perl ' path '/COMMGEN/MetaNetXInteraction/RemIllegalChars.pl ' filename ' ' tmpfilename]);

%remove mapping file if already present
if exist(mapfile,'file')
    delete(mapfile)
end

%connect to MetaNetX to convert GSM to MnXRef namespace
dos(['perl -I' MechanizeLocation ' ' path '/COMMGEN/MetaNetXInteraction/upload_model.pl ' tmpfilename])
delete(tmpfilename)

%identify current files in folder. The name of the converted model
%is currently independent of the uploaded file's name.
files = dir;
files = {files.name}; 

%untar file
dos(['tar -xvf ' tmpfilename '.imported.tar.gz']) 

%identify folder name corresponding to converted GSM
files2 = dir;
files2 = {files2.name};
folder = setdiff(files2,files);
folder = folder{1}; %convert to string

%% inspect mapping file
%load file
SpecMap = importMapping(mapfile);

%Check mapping
Nel = size(SpecMap,1);
NR = 1:Nel;
Present = true(Nel,1);
while true; %Stop if all accepted
    %Present mapping
    disp([{'','Old ID','Old Description','New ID','New Description'} ; num2cell(NR(Present))' SpecMap(Present,:) ; {'','Old ID','Old Description','New ID','New Description'}])
    reply = input('Please check the list of species mapping.\nEnter the indices of any mapping which is incorrect or enter 0 to continue.\nFor example [1 2 5 6].\n');
    
    %Input processing
    if numel(reply)==1 && reply ==0
        break;
    end
    if any(reply<1 | reply>Nel)
        fprintf('\n\n\n\n\nPlease only include indices that are in the list\n\n')
    end
    
    % Remove wrongly mapped species
    TBR = ismember(NR,reply);
    Present(TBR) = false;
end

%% Remove incorrect mappings
if any(~Present)
   % One or more of the species was wrongly mapped. Change the species back
   % to the original.
   species = SpecMap(~Present,1:3);
   ChangeReactions;
   ChangeSpecies;
end

%% Restore original reaction names
RestoreRxnNames;

%% Convert tsv files to COMMGEN format
dos(['perl ' path '/COMMGEN/MetaNetXInteraction/c2m.pl ' folder ' ' dirname]);
tsv2mat('-d',dirname);
rmdir(folder,'s');
fprintf('Model saved in folder %s. Use the name of this folder as input argument for COMMGEN.\n',dirname);

%% Additional functions

    function ChangeReactions
        %Read in and modify the reactions.tsv file such that any incorrect
        %species conversions are reverted.
        formatSpec = '%s%s%s%s%s%s%s%[^\n\r]';
        
        % Open the text file.
        rxnfile = [folder '/reactions.tsv'];
        fileID = fopen(rxnfile,'r');
        data = textscan(fileID, formatSpec, 'Delimiter', '\t',  'ReturnOnError', false);
        fclose(fileID);
        
        % assign variables
        Var1 = data{:, 1};
        Var2 = data{:, 2};
        Var3 = data{:, 3};
        Var4 = data{:, 4};
        Var5 = data{:, 5};
        Var6 = data{:, 6};
        Var7 = data{:, 7};
        
        % modify reactions
        Var2 = regexprep(Var2,strcat(species(:,3),'@'),strcat(species(:,2),'@'));
        
        % write new reactions file
        fid = fopen(rxnfile, 'w+');
        for i=1:numel(Var1);
            fprintf(fid,'%s\t%s\t%s\t%s\t%s\t%s\t%s\n',Var1{i},Var2{i},Var3{i},Var4{i},Var5{i},Var6{i},Var7{i});
        end
        fclose(fid);
    end

    function ChangeSpecies
        %Read in and modify the chemicals.tsv file such that any incorrect
        %matches are reverted
        formatSpec = '%s%s%s%s%s%s%s%[^\n\r]';
        
        % Open the text file.
        specfile = [folder '/chemicals.tsv'];
        fileID = fopen(specfile,'r');
        data = textscan(fileID, formatSpec, 'Delimiter', '\t',  'ReturnOnError', false);
        fclose(fileID);
        
        % Remove species
        %Identify locations of wrongly mapped species
        Var1 = data{:, 1};
        Var2 = data{:, 2};
        Var3 = data{:, 3};
        Var4 = data{:, 4};
        Var5 = data{:, 5};
        Var6 = data{:, 6};
        Var7 = data{:, 7};
        loc = ismember(Var1,species(:,3));
        
        %Remove species
        Var1(loc) = [];
        Var2(loc) = [];
        Var3(loc) = [];
        Var4(loc) = [];
        Var5(loc) = [];
        Var6(loc) = [];
        Var7(loc) = [];
        
        %Write new species file
        %write mapped species
        fid = fopen(specfile, 'w+');
        for i=1:numel(Var1);
            fprintf(fid,'%s\t%s\t%s\t%s\t%s\t%s\t%s\n',Var1{i},Var2{i},Var3{i},Var4{i},Var5{i},Var6{i},Var7{i});
        end
        %write corrected species
        for i=1:size(species,1);
            fprintf(fid,'%s\t%s\t%s\t%s\t%s\t%s\t%s\n',species{i,2},species{i,2},species{i,1},'','','','');
        end
        fclose(fid);
    end

    function RestoreRxnNames
        %Read in and modify the reactions.tsv file such that the reaction
        %names presented in COMMGEN match those from the original models
        
        %% Reactions file
        
        formatSpec = '%s%s%s%s%s%s%s%[^\n\r]';
        
        % Open the text file.
        rxnfile = [folder '/reactions.tsv'];
        fileID = fopen(rxnfile,'r');
        data = textscan(fileID, formatSpec, 'Delimiter', '\t',  'ReturnOnError', false);
        fclose(fileID);
        
        % assign variables
        Var1 = data{:, 1};
        Var2 = data{:, 2};
        Var3 = data{:, 3};
        Var4 = data{:, 4};
        Var5 = data{:, 5};
        Var6 = data{:, 6};
        Var7 = data{:, 7};
        
        % write new reactions file
        fid = fopen(rxnfile, 'w+');
        for i=1:numel(Var1);
            if isempty(Var3{i})
                fprintf(fid,'%s\t%s\t%s\t%s\t%s\t%s\t%s\n',Var1{i},Var2{i},Var3{i},Var4{i},Var5{i},Var6{i},Var7{i});
            else
                fprintf(fid,'%s\t%s\t%s\t%s\t%s\t%s\t%s\n',Var3{i},Var2{i},Var1{i},Var4{i},Var5{i},Var6{i},Var7{i});
            end
        end
        fclose(fid);
        
        %% Enzymes file
        
        formatSpec = '%s%s%s%s%[^\n\r]';
        
        % Open the text file.
        enzfile = [folder '/enzymes.tsv'];
        fileID = fopen(enzfile,'r');
        data = textscan(fileID, formatSpec, 'Delimiter', '\t',  'ReturnOnError', false);
        fclose(fileID);
        
        % assign variables
        EVar1 = data{:, 1};
        EVar2 = data{:, 2};
        EVar3 = data{:, 3};
        EVar4 = data{:, 4};
        
        % find corresponding reaction
        [~,IA] = ismember(EVar1,Var1);
        
        % write new enzymes file
        fid = fopen(enzfile, 'w+');
        for i=1:numel(Var1);
            if isempty(Var3{IA(i)})
                fprintf(fid,'%s\t%s\t%s\t%s\n',EVar1{i},EVar2{i},EVar3{i},EVar4{i});
            else
                fprintf(fid,'%s\t%s\t%s\t%s\n',Var3{IA(i)},EVar2{i},EVar3{i},EVar4{i});
            end
        end
        fclose(fid);
    end
    
    function ResolveNaNs
        %Read in and modify the reactions.tsv file such that the reaction
        %names presented in COMMGEN match those from the original models
        
        %% Reactions file
        
        formatSpec = '%s%s%s%s%s%s%s%[^\n\r]';
        
        % Open the text file.
        rxnfile = [folder '/reactions.tsv'];
        fileID = fopen(rxnfile,'r');
        data = textscan(fileID, formatSpec, 'Delimiter', '\t',  'ReturnOnError', false);
        fclose(fileID);
        
        % assign variables
        Var1 = data{:, 1};
        Var2 = data{:, 2};
        Var3 = data{:, 3};
        Var4 = data{:, 4};
        Var5 = data{:, 5};
        Var6 = data{:, 6};
        Var7 = data{:, 7};
        
        % write new reactions file
        fid = fopen(rxnfile, 'w+');
        for i=1:numel(Var1);
            if isempty(Var3{i})
                fprintf(fid,'%s\t%s\t%s\t%s\t%s\t%s\t%s\n',Var1{i},Var2{i},Var3{i},Var4{i},Var5{i},Var6{i},Var7{i});
            else
                fprintf(fid,'%s\t%s\t%s\t%s\t%s\t%s\t%s\n',Var3{i},Var2{i},Var1{i},Var4{i},Var5{i},Var6{i},Var7{i});
            end
        end
        fclose(fid);
        
        %% Enzymes file
        
        formatSpec = '%s%s%s%s%[^\n\r]';
        
        % Open the text file.
        enzfile = [folder '/enzymes.tsv'];
        fileID = fopen(enzfile,'r');
        data = textscan(fileID, formatSpec, 'Delimiter', '\t',  'ReturnOnError', false);
        fclose(fileID);
        
        % assign variables
        EVar1 = data{:, 1};
        EVar2 = data{:, 2};
        EVar3 = data{:, 3};
        EVar4 = data{:, 4};
        
        % find corresponding reaction
        [~,IA] = ismember(EVar1,Var1);
        
        % write new enzymes file
        fid = fopen(enzfile, 'w+');
        for i=1:numel(Var1);
            if isempty(Var3{IA(i)})
                fprintf(fid,'%s\t%s\t%s\t%s\n',EVar1{i},EVar2{i},EVar3{i},EVar4{i});
            else
                fprintf(fid,'%s\t%s\t%s\t%s\n',Var3{IA(i)},EVar2{i},EVar3{i},EVar4{i});
            end
        end
        fclose(fid);
    end

end

