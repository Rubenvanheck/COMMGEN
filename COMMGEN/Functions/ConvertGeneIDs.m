function ConvertGeneIDs(folder,Old,New)
%This function uses two input vectors to change the genes from one naming
%system into a different one. This is recommended before merging the
%models.

%% Enzymes file

formatSpec = '%s%s%s%s%[^\n\r]';

% Open the text file.
enzfile = [folder '/enzymes.tsv'];
fileID = fopen(enzfile,'r');
data = textscan(fileID, formatSpec, 'Delimiter', '\t',  'ReturnOnError', false);
fclose(fileID);

% assign variables
EVar1 = data{:, 1};
EVar2 = data{:, 2};
EVar3 = data{:, 3};
EVar4 = data{:, 4};

%change gene naming
for i=1:numel(Old)
    if ~isempty(Old{i})
        Old_IDs = strsplit(Old{i},' ');
        for j=1:numel(Old_IDs)
            EVar2 = regexprep(EVar2,{[':' Old_IDs{j} ';'],[':' Old_IDs{j} '\+'],[':' Old_IDs{j} '$']},{[':' New{i} ';'],[':' New{i} '+'],[':' New{i}]});
        end
    end
end

% write new enzymes file
fid = fopen(enzfile, 'w+');
for i=1:numel(EVar1);
    fprintf(fid,'%s\t%s\t%s\t%s\n',EVar1{i},EVar2{i},EVar3{i},EVar4{i});
end
fclose(fid);
end

