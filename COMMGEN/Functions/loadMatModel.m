function model = loadMatModel(dir)
%%% PURPOSE
% This function loads a model from the COMMGEN tsv file format. This model
% can subsequently be loaded into the cobra toolbox using LoadMatModel
%%% EXAMPLE CALL
%   COMMGEN_model = loadMatModel('Models/Model1')
%%% INPUT
%   dir: name of the directory containing the tsv files in COMMGEN format
%%% OPTIONAL INPUT
%%% OUTPUT

dir = regexprep(dir,'/$','');

if exist([dir '/model.mat'],'file')
    model = load([dir '/model.mat']);
    model = model.model;
else
    tsv2mat('-d',dir);
    model = load([dir '/model.mat']);
    model = model.model;
    delete([dir '/model.mat']);
end
end

