function ExcRxns = findER(model)
%Finds all boundary reactions in a cobra model.
% The original Cobra findExcRxns does not find all exchange reactions as it 
% requires the stoichiometric value in the S matrix to be 1
% or -1.

ExcRxns = full(sum(logical(model.S))==1);

end

