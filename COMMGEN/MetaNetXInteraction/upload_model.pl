#!/usr/bin/env perl

# Perl embedded modules
use strict;
use warnings;
use diagnostics;
use Carp;

use WWW::Mechanize;
my $base_url = 'http://metanetx.org';


# Input model file
my $input = $ARGV[0]  or die "\n\tMissing input model file (MetaNetX XLS, MetaNetX tarball of TSVs, or SBML2/3)\n\t$0 input_model_file\n\n";

# Init
my $mech = WWW::Mechanize->new();


# Reset any previous uploads
$mech->get("$base_url/cgi-bin/mnxweb/reset");
# Send request, upload model
my $mimetype = $input =~ /\.xls$/  ? 'application/vnd.ms-excel'
             : $input =~ /gz$/     ? 'application/x-zip'
             : $input =~ /\.xml$/  ? 'text/xml'
             : $input =~ /\.sbml$/ ? 'text/xml'
             :                       '';
my $file = ["$input",
            "$input",
            'Content-type' => $mimetype,
           ];
$mech->post("$base_url/cgi-bin/mnxweb",
                'Content_Type' => 'form-data',
                'Content'      => 
                    {'action'       => 'mnet_upload',
                     'model_name'   => 'user_MNET'.$$, # For a more random model name
                     'map_opt'      => 0,              # 1 = no mapping to MnxRef;   2 = deep mapping; 0 = medium mapping (default)
                     'sbml_opt'     => 0,              # 0 = generic SBML (default); 1 = BiGG SBML;    2 = the SEED SBML
                     'model_file'   => $file,
                    }
           );


# Check request completion
if ( $mech->success() && $mech->content =~ /mapped to the MNXref namespace with / ){
#    print $mech->content, "\n\n";
    # Save "Download mapping information" file
    $mech->follow_link( text => 'Mapping summary' );
    $mech->save_content("$input.mapping");
    $mech->back();
    print "\tModel uploaded! Mapping saved in [$input.mapping]\n";

    # Get imported model
    $mech->submit();
    # If model has multiple biomass reactions, keep them all
    if ( $mech->content() =~ /No, don't update/ ){
        $mech->click_button(name => 'skip_update');
    }
    $mech->follow_link( text => 'user_MNET'.$$ );
    $mech->follow_link( text => 'user_MNET'.$$.'.tar.gz' );
    $mech->save_content("$input.imported.tar.gz");
    print "\tModel imported in MetaNetX and saved in [$input.imported.tar.gz]\n";
}
else {
#    print $mech->content, "\n\n";
    die "\n\tModel upload failed!\n\n";
}

exit 0;

