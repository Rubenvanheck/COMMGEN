#!/usr/bin/env perl

use strict;

use Digest::CRC;

my $DEBUG = 0;

my $usage = "./mathias2tsv.pl <source dir> <dest dir> [<model_ID>]\n";

my( $source_path, $dest_path, $user_model_name ) = @ARGV;

die "Usage: $usage\n" unless 2 <= @ARGV and @ARGV <= 3;

die "Source directory does not exist: $source_path\n"    unless -d $source_path;
die "Destination directory does not exist: $dest_path\n" unless -d $dest_path;

my $filename_in  = "$source_path/model.tsv";
my $filename_out = "$dest_path/model.tsv";
open(IN,$filename_in)       or die "Cannot read from file: $filename_in\n";
warn "# read    $filename_in\n" if $DEBUG;
open(OUT,"> $filename_out") or die "Cannot write to file: $filename_out\n";
warn "# write   $filename_out\n" if $DEBUG;
while(<IN>){
    next if /^\#/;
    chop;
    my( $key, $value ) = split /\t/;
    if ( $key eq "ID" and $user_model_name ){
        $value = $user_model_name;
    }
	print OUT join( "\t",
                    $key,
                    $value). "\n";
    last; # FIXME: a little bit rough, mmmh??
}
close IN;
close OUT;

my $filename_in   = "$source_path/reactions.tsv";
my $filename_out  = "$dest_path/reactions.tsv";
my $filename_out2 = "$dest_path/enzymes.tsv";

#Lit le fichier reaction une premiere fois pour determiner, pour chaque reaction, dans quel(s) sens elle se passe
my %orig_to_undir_reacIds = {};
my %undir_reacIDs_to_dir  = {};

open(IN,$filename_in)       or die "Cannot read from file: $filename_in\n";
while(<IN>){
    next if /^\#/;
    chop;
    my( $reac_id, $equation, $source, $prots, $cheq_id, @more ) = split /\t/;
	my $equa   = $equation;
	my $is_bio = $more[-2];
	my $is_ext = $more[-1];
	if ( $is_bio ){
        $equa = correctBiomassReac($equa);
    }
	if ( $is_ext ){
        $equa = correctBoundaryReac($equa);
    }
	my $undir_reac_id = computeUndirReacID($equa); # Corrige pour avoir des reacID qui correspondent a nos specs
	$orig_to_undir_reacIds{$reac_id} = $undir_reac_id;
	if ( $more[-6] < 0 and  $more[-5] <= 0 ){  # reaction RL
		if ( $undir_reacIDs_to_dir{$undir_reac_id} eq "B"){
        }
		elsif ( $undir_reacIDs_to_dir{$undir_reac_id} eq "LR"){
            $undir_reacIDs_to_dir{$undir_reac_id}="B";
        }
		else {
            $undir_reacIDs_to_dir{$undir_reac_id}="RL";
        }
	}
	elsif ( $more[-6] >= 0 and $more[-5] > 0 ){  #reaction LR
		if ( $undir_reacIDs_to_dir{$undir_reac_id} eq "B" ){
        }
		elsif ( $undir_reacIDs_to_dir{$undir_reac_id} eq "RL" ){
            $undir_reacIDs_to_dir{$undir_reac_id}="B";
        }
		else {
            $undir_reacIDs_to_dir{$undir_reac_id}="LR";
        }
	}
	else{ #reaction reversible
		$undir_reacIDs_to_dir{$undir_reac_id}="B";
	}
}
close IN;

##seconde passe pour ecrire les fichier reaction et enzyme cette fois-ci
open(IN,$filename_in)       or die "Cannot read from file: $filename_in\n";
warn "# read    $filename_in\n" if $DEBUG;
open(OUT,"> $filename_out") or die "Cannot write to file: $filename_out\n";
warn "# write   $filename_out\n" if $DEBUG;
open(OUT2,"> $filename_out2") or die "Cannot write to file: $filename_out2\n";
warn "# write   $filename_out2\n" if $DEBUG;
my %seen_reacIDs = {};
while(<IN>){
    next if /^\#/;
    chop;
    my( $reac_id, $equation, $source, $prots, $cheq_id, @more ) = split /\t/;
    my $equa = $equation;
	my $is_bio = $more[-2];
	my $is_ext = $more[-1];
	if ( $is_bio ){
        $equa = correctBiomassReac($equa);
    }
	if ( $is_ext ){
        $equa = correctBoundaryReac($equa);
    }
	my $undir_reac_id = $orig_to_undir_reacIds{$reac_id};
	my $dir = $undir_reacIDs_to_dir{$undir_reac_id};
	$reac_id = $undir_reac_id.'_'.$dir;
	
	my $arrow = '<==>';
	if ($dir eq 'LR'){
        $arrow='-->';
    }
	elsif ($dir eq 'RL'){
        $arrow='<--';
    }
	$equa =~ s/\s\<?[\=\-]+\>?\s/ $arrow /;

	if ( !defined $seen_reacIDs{$reac_id} ){
		$seen_reacIDs{$reac_id} = 1;
    	die "TOTO\n" unless $reac_id;
        print OUT join( "\t",
         $reac_id,  # IRId     = 1;  % reaction/enzyme unique identifier
         $equa,		# IREq     = 2;  % reaction equation
         $source,   # IRSource = 3;  % source
         $cheq_id,  # IRxn     = 5;  % reaction
         $more[1],  # IEC      = 7;  % EC number
         $more[2],  # IRName   = 11; % pathways
         $more[-4], # IRefs    = 12; % Xrefs
        ) . "\n";
	 }
	 print OUT2 join ("\t", 
		$reac_id,
		$prots,
		$more[-6],	#LB
		$more[-5])	#UB
	. "\n";	
    	
}
close IN;
close OUT;
close OUT2;


my $filename_in  = "$source_path/chemicals.tsv";
my $filename_out = "$dest_path/chemicals.tsv";
open(IN,$filename_in)       or die "Cannot read from file: $filename_in\n";
warn "# read    $filename_in\n" if $DEBUG;
open(OUT,"> $filename_out") or die "Cannot write to file: $filename_out\n";
warn "# write   $filename_out\n" if $DEBUG;
my $line;
while( $line = <IN> ){
    next if /^\#/;
    print OUT $line;
}

#TODO: rajouter le composé de BIOMASS?
close IN;
close OUT;


my $filename_in  = "$source_path/compartments.tsv";
my $filename_out = "$dest_path/compartments.tsv";
open(IN,$filename_in)      or die "Cannot read from file: $filename_in\n";
warn "# read    $filename_in\n" if $DEBUG;
open(OUT,"> $filename_out") or die "Cannot write to file: $filename_out\n";
warn "# write   $filename_out\n" if $DEBUG;
while(<IN>){
    next if /^\#/;
    chop;
    my($comp_id,$name,$source) = split /\t/;
    print OUT join( "\t",
                    $comp_id,
                    $name,
                    $source),"\n";
}    
close IN;
close OUT;

sub correctBiomassReac {
	my $equa = shift;
	#Si pas de compose BIOMASS dans la reaction
    if ( $equa !~  /\sBIOMASS@/ ){
		if ($equa =~  /\s[\-\=]+>\s/){
			$equa = $equa . ' + 1 BIOMASS@BOUNDARY';
		}
		elsif ($equa =~  /\s<[\-\=]+\s/){
			$equa = '1 BIOMASS@BOUNDARY + ' . $equa;
		}
		else{	
			#Le rajoute du cote de l'equation ou le moins de termes
			my ($left, $right) = split(/\s<?[\-\=]+>?\s/, $equa);
			my $lcount = ($left =~ tr/\+/\+/);
			my $rcount = ($right =~ tr/\+/\+/);
			if ($lcount > $rcount){
				$equa = $equa . ' + 1 BIOMASS@BOUNDARY';
			}
			elsif ($rcount > $lcount){
				$equa = '1 BIOMASS@BOUNDARY + '.$equa;
			}
		}
		#warn "BIOMASS $equa\n";
	}
	return $equa;
}

sub correctBoundaryReac {
	my $equa = shift;
    return $equa if /\@BOUNDARY/;
    my( $left, $arrow, $right ) = ( '', '', '' );
    if( $equa =~ /^(<--|<==>|-->) (.+)/ ){
        $arrow = $1;
        $right = $2;
        $left = $right;
        $left =~ s/\@\S+/\@BOUNDARY/g;
    }
    elsif( $equa =~ /(.+) (<--|<==>|-->)$/ ){
        $left  = $1;
        $arrow = $2;
        $right = $left;
        $right =~ s/\@\S+/\@BOUNDARY/g;
    }
    else{
        die "Don't know how to fix bounadry reaction: $equa\n";
    }
    return "$left $arrow $right";
}

sub computeUndirReacID {
	my $equa = shift;
	my $IDequa = $equa;
	$IDequa =~ s/\s\<?[\=\-]+\>?\s/ <?> /;
	my $ctx = Digest::CRC->new( type=>"crc32", poly=>0x2D );
	$ctx->add($IDequa);
    return 'R' . uc( $ctx->hexdigest );
}



