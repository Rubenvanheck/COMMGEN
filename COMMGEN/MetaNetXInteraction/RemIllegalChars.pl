#!/usr/bin/perl

use strict;
use warnings;

my $inFile = $ARGV[0];
my $outFile = $ARGV[1];
my $line;
my @elems;

#Open input file in read mode
open(READ,"<", $inFile) or die "Cannot open file $inFile for reading!\n";
#Open output file in write mode
open(OUT,">",$outFile) or die $!;

#Read input file line by line
while($line=<READ>) {
	chomp $line;
	# Replace illegal characters in model ids
	if ($line =~ /<model id=/) {
		$line =~ s/\[|\]|\,|-|:|\(|\)/__/g;
	}
	# Replace illegal characters in species ids - but avoid messing up names
	if ($line =~ /<species id=/) {
		@elems = split(/name=\"/, $line);
		$elems[0] =~ s/\[|\]|\,|-|:|\(|\)/__/g;
		$line = $elems[0] . "name=\"" . $elems[1];
	}
	# Replace illegal characters in species instances
	if ($line =~ /<speciesReference species=/) {
		$line =~ s/\[|\]|\,|-|:|\(|\)/__/g;
	}
	# Replace illegal characters in reaction ids
	if ($line =~ /<reaction id=/) {
		$line =~ s/\[|\]|\,|-|:|\(|\)/__/g;
	}
	# Print lines to output file
	print OUT $line, "\n";
}
close(OUT);
close(READ);
exit(0);
