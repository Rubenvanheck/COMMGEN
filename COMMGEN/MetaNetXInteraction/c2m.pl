#!/usr/bin/env perl

use strict;

my $DEBUG = 0;

my $usage = "./tsv2mathias.pl <source dir> <dest dir>\n";

my($source_path,$dest_path) = @ARGV;
die "Usage: $usage\n" unless $source_path and $dest_path;

die "Destination directory does not exist: $dest_path\n" unless -d $dest_path;

my $filename_in  = "$source_path/model.tsv";
my $filename_out = "$dest_path/model.tsv";
open(IN,$filename_in)       or die "Cannot read from file: $filename_in\n";
warn "# read    $filename_in\n" if $DEBUG;
open(OUT,"> $filename_out") or die "Cannot write to file: $filename_out\n";
warn "# write   $filename_out\n" if $DEBUG;
while(<IN>){
    next if /^\#/;
    chop;
    my($key,$value) = split /\t/;
    print OUT join( "\t",
                    $key,
                    $value) . "\n";
    last;
}
close IN;
close OUT;

# As far as I know, the enzymes.tsv file is not part of the "mathias" specifications 
my %enzymes;
my $count = 0;
my $filename_in  = "$source_path/enzymes.tsv";
# my $filename_out = "$dest_path/enzymes.tsv";
open(IN,$filename_in)       or die "Cannot read from file: $filename_in\n";
warn "# read    $filename_in\n" if $DEBUG;
# open(OUT,"> $filename_out") or die "Cannot write to file: $filename_out\n";
# warn "# write   $filename_out\n" if $DEBUG;
while(<IN>){
    next if /^\#/;
    chop;
    my($reac_id,$proteins,$lower_bound,$upper_bound) = split /\t/;
    $count++;
    my %data;
    # $proteins = ''; # FIXME: need to fix protein syntax!!!
    push @{$enzymes{$reac_id}}, { num  => $count, # some unique internal identifier
                                  prot => $proteins,
		                  LB   => $lower_bound,
                                  UB   => $upper_bound };
    # print OUT join("\t",  $reac_id. '-' .$count, $proteins, $lower_bound, $upper_bound) . "\n";
}
close IN;
# close OUT;


my $filename_in  = "$source_path/reactions.tsv";
my $filename_out = "$dest_path/reactions.tsv";
open(IN,$filename_in)       or die "Cannot read from file: $filename_in\n";
warn "# read    $filename_in\n" if $DEBUG;
open(OUT,"> $filename_out") or die "Cannot write to file: $filename_out\n";
warn "# write   $filename_out\n" if $DEBUG;
while(<IN>){
    next if /^\#/;
    chop;
    my($reac_id,$equation,$source,$cheq_id,@more) = split /\t/;
    my($is_bio,$is_ext) = (0,0);
    if($equation =~ /BIOMASS/){
        if(is_biomass_reac_eq($equation))
		{
			$is_bio = 1;
        	#$equation =~ s/1 BIOMASS\@\w+\s*\+*\s*//;
        	#$equation =~ s/\+ +\+/\+/;
        	#$equation =~ s/^\s*\+\s*//;
        	#$equation =~ s/\s*\+\s*$//;
		}
	}
    if($equation =~ /BOUNDARY/){
        $is_ext = 1;
    }
    while($equation =~ /\(([\w\+\-]+)\)/g){
        my $token = $1;
        my $token2 = $token;
        $token2 =~ s/[a-zA-Z]+/1000/g;
        my $coef = eval $token2;
        warn "TOKEN: $token\t$token2\t$coef\t $equation\n" if $DEBUG;
        $equation =~ s/\([\w\+\-]+\)/$coef/;
        warn "=>     $equation\n" if $DEBUG;
    }
    next if $equation =~ /^\s*<==>\s*$/;


    if ($enzymes{$reac_id}){	
      foreach( @{$enzymes{$reac_id}} ){
         my $id = $reac_id . '-' .  $_->{num};
         print OUT join( "\t",
         $id,  # IRId     = 1;  % reaction/enzyme unique identifier
         $equation, # IREq     = 2;  % reaction equation
         $source,   # IRSource = 3;  % source
         $_->{prot},        # $_->{prot},# IProts   = 4;  % proteins
         $cheq_id,  # IRxn     = 5;  % reaction
         $more[0],  # IComp    = 6;  % compartment
         $more[1],  # IEC      = 7;  % EC number
         $more[2],  # IOp      = 8;  % operator
         $_->{LB},  # ILow     = 9;  % lower_bound
         $_->{UB},  # IUpp     = 10; % upper_bound
         $more[3],  # IRName   = 11; % name
         $more[4],  # IRefs    = 12; % Xrefs
         $is_bio,   # IMue     = 13; % entry indicating the biomass equation (0 or 1)
         $is_ext    # 14; % column indicating the external reactions (0 or 1)
        ) . "\n";
      }
    }
    else
    {
     print OUT join( "\t",
         $reac_id,  # IRId     = 1;  % reaction identifier
         $equation, # IREq     = 2;  % reaction equation
         $source,   # IRSource = 3;  % source
         "",        # IProts   = 4;  % proteins
         $cheq_id,  # IRxn     = 5;  % reaction
         $more[0],  # IComp    = 6;  % compartment
         $more[1],  # IEC      = 7;  % EC number
         $more[2],  # IOp      = 8;  % operator
         "-99999",  # ILow     = 9;  % lower_bound
         "99999",   # IUpp     = 10; % upper_bound
         $more[3],  # IRName   = 11; % name
         $more[4],  # IRefs    = 12; % Xrefs
         $is_bio,   # IMue     = 13; % entry indicating the biomass equation (0 or 1)
         $is_ext    # 14; % column indicating the external reactions (0 or 1)
       ) . "\n";

    }	
}
close IN;
close OUT;

my $filename_in  = "$source_path/chemicals.tsv";
my $filename_out = "$dest_path/chemicals.tsv";
open(IN,$filename_in)       or die "Cannot read from file: $filename_in\n";
warn "# read    $filename_in\n" if $DEBUG;
open(OUT,"> $filename_out") or die "Cannot write to file: $filename_out\n";
warn "# write   $filename_out\n" if $DEBUG;
my $line;
while($line=<IN>){
    next if /^\#/;
    #if ($line !~ /^\#|^BIOMASS/ || $has_biomass_cmpd) {
	print OUT $line;
   #}
}
close IN;
close OUT;


my $filename_in  = "$source_path/compartments.tsv";
my $filename_out = "$dest_path/compartments.tsv";
open(IN,$filename_in)      or die "Cannot read from file: $filename_in\n";
warn "# read    $filename_in\n" if $DEBUG;
open(OUT,"> $filename_out") or die "Cannot write to file: $filename_out\n";
warn "# write   $filename_out\n" if $DEBUG;
while(<IN>){
    next if /^\#/;
    chop;
    my($comp_id,$name,$source) = split /\t/;
    my $flag = '';
    if($comp_id eq 'UNK_COMP'){
        $flag = 'unknown';
    }    
    elsif($comp_id eq 'go_0005576'){
        $flag = 'external';
    }
    elsif($comp_id eq 'UNK_Extraorganism'){
        $flag = 'external';
    }
    elsif($comp_id eq 'BOUNDARY'){
        $flag = 'boundary';
    }
    elsif($comp_id eq 'go_0005737'){
        $flag = 'default';
    }
    print OUT join( "\t",
                    $comp_id,
                    $name,
                    $source,
                    $flag ),"\n";
}    
close IN;
close OUT;



sub is_biomass_reac_eq{
	my $equa = shift;
	if ($equa !~  /\sBIOMASS@/){return 0;}
	#verifie que pas reaction de "transport" de biomasse
	while ($equa =~ m/\s(\S+)@/g) {
		if ($1 ne "BIOMASS"){return 1;}
	}
	return 0;
}
