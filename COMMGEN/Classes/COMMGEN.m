classdef COMMGEN < handle
    %COMMGEN
    % 
    
    properties %(GetAccess = private)
        % General information
        DEL = '%';
        Models
        ModelNames = {}
        NumModels
        LBdef = []
        UBdef = []
        % Species information
        RedoxPairsAccepted ={'',''}
        RedoxPairsRejected ={'',''}
        species_IDs
        SpeciesIDToName
        FormInspected
        % Metabolite information
        MetChemForm
        MetToSpecies
        % Reaction information
        Lb
        Ub
        rxnGeneMat % rows: genes, cols: rxns
        pDir %Prediction of reaction directionality
        % Compartment information
        CompartmentIDToName
        % Tracking progress
        RejectedMerge = {}
        TempRejectedMerge = {}
        CurrentFunction = ''
        ToBeRemovedRxns % Placeholder for reactions to be removed later in the process (for speed).
        TrackReactions = {'Relation','OldRxns','NewID'  ,'Comment','NewForm' ,'NewGPR','Lb','Ub','Phase','Tool','External'}
        TrackSpecies   = {'Relation','OldSpecID','OldSpecName','NewSpecID','NewSpecID','Comment','ChemForm','mass','charge','refs','Phase','Tool'};
        TrackSpecStats = {}
        TrackRxnStats = {}
        TrackDead = {'DeadReaction','InactiveReaction','DeadMetabolite','InactiveMetabolite'}
        Phase = 1 % Keep track of the round of model condensation
        MetFirstSeen
        MetLastSeen
        GenesFirstSeen
        GenesLastSeen
    end
    properties (Dependent) %, GetAccess = private)
        % Reaction info
        is_transport
        is_biomass
        is_external
        is_reversible
        FastRxnLookup
        reaction_IDs
        RxnComp
        RxnOrigin
        
        % Metabolite info
        HubMetabolites
        FastMetLookup
        MetCompartment
        MetFormula
        
        % Gene info
        FastGeneLookup
        
        % Species info
        SpecFormula
        SpecOrigin
        SpecComps
        
        % Consensus model info
        Consistent % Check whether or not the various matrix elements are consistent with one another
        
    end
    properties
        species
        metabolites
        reactions
        Stoich
        genes
        options
        RemovedRxns = {}
    end
    properties (Dependent)
        NumSpecies
        NumMetabolites
        NumGenes
        NumReactions
        NumCompartments
    end
    
    methods
        % Dependent properties
        %consensus model
        function NumReactions       = get.NumReactions(this)
            NumReactions = numel(this.reactions);
        end
        function NumSpecies         = get.NumSpecies(this)
            NumSpecies = numel(this.species);
        end
        function NumMetabolites     = get.NumMetabolites(this)
            NumMetabolites = numel(this.metabolites);
        end
        function NumGenes           = get.NumGenes(this)
            NumGenes = numel(this.genes);
        end
        function NumCompartments    = get.NumCompartments(this)
            NumCompartments = size(this.CompartmentIDToName,1);
        end
        function Consistent         = get.Consistent(this)
            Consistent = true;
            % If there are reactions to be removed.
            if any(this.ToBeRemovedRxns)
                Consistent = false;
                return
            end
            % If the number of metabolites or reactions has changed
            if ~all([this.NumMetabolites this.NumReactions]==size(this.Stoich))
                Consistent = false;
                return
            end
            % If the number of reactions or genes has changed
            if ~all([this.NumGenes this.NumReactions]==size(this.rxnGeneMat))
                Consistent = false;
                return
            end
            % If not all metabolites are associated with a reaction
            if ~all(any(this.Stoich,2))
                Consistent = false;
                return
            end
        end
        %reactions
        function reaction_IDs       = get.reaction_IDs(this)
            reaction_IDs = cell(this.NumReactions,1);
            for i=1:this.NumReactions
                reaction_IDs{i} = this.reactions{i}.id;
            end
        end
        function FastRxnLookup      = get.FastRxnLookup(this)
            FastRxnLookup = cell(this.NumReactions,1);
            for i=1:this.NumReactions
                FastRxnLookup{i} = [this.DEL this.reactions{i}.id this.DEL];
            end
            FastRxnLookup = char(FastRxnLookup);
        end
        function is_transport       = get.is_transport(this)
            is_transport = false(this.NumReactions,1);
            for i=1:this.NumReactions
                is_transport(i) = this.reactions{i}.transport;
            end
        end
        function is_biomass         = get.is_biomass(this)
            is_biomass = false(this.NumReactions,1);
            for i=1:this.NumReactions
                is_biomass(i) = this.reactions{i}.biomass > 0;
            end
        end
        function is_external        = get.is_external(this)
            is_external = zeros(this.NumReactions,1);
            for i=1:this.NumReactions
                is_external(i,1) = this.reactions{i}.external;
            end
            is_external = logical(is_external);
        end
        function is_reversible      = get.is_reversible(this)
            is_reversible = this.Lb<0 & this.Ub>0;
        end
        function RxnComp            = get.RxnComp(this)
            RxnComp = cell(this.NumReactions,1);
            for i=1:this.NumReactions
                RxnComp{i} = this.reactions{i}.compartment;
            end
        end
        function RxnOrigin          = get.RxnOrigin(this)
            RxnOrigin = false(this.NumReactions,this.NumModels);
            for i=1:this.NumReactions
                RxnOrigin(i,:) = this.reactions{i}.Origin;
            end
        end
        %metabolites
        function MetCompartment     = get.MetCompartment(this)
            MetCompartment = regexprep(this.metabolites,'.*@','');
        end
        function FastMetLookup      = get.FastMetLookup(this)
            FastMetLookup = cell(this.NumMetabolites,1);
            for i=1:this.NumMetabolites
                FastMetLookup{i} = [this.DEL this.metabolites{i} this.DEL];
            end
            FastMetLookup = char(FastMetLookup{:});
        end
        function HubMetabolites     = get.HubMetabolites(this)
            % Find metabolites which are associated with so many reactions
            % that they are not of interest when trying to match other
            % metabolites / reactions. For example, when searching for an
            % alternative for a transport reaction you don't want to search
            % for alternative transport reactions using protons.
            HubMetabolites = false(this.NumMetabolites,1);
            FastMetLookup  = this.FastMetLookup;
            
            if ~isempty(this.options.HubMets)
                InitList            = this.options.HubMets;
                Spec_IDs            = this.species_IDs;
                NoCompInfo          = cellfun(@isempty,regexp(InitList,'@'));
                MetToSpec           = this.MetToSpecies;
                for i=1:numel(InitList)
                    if NoCompInfo(i)
                        Match = find(strcmp(this.NameToID(InitList{i}),Spec_IDs));
                        Matches = MetToSpec == Match;
                        if ~any(Matches)
                            fprintf('WARNING: Species %s not found in the models, please check!\n',InitList{i})
                        else
                            HubMetabolites(Matches) = true;
                        end
                    else
                        Match = mfindstr(FastMetLookup,this.NameToID(InitList{i}));
                        if Match==0
                            fprintf('WARNING: Metabolite %s not found in the models, please check!\n',InitList{i})
                        else
                            HubMetabolites(Match) = true;
                        end
                    end
                end
            end
        end
        function MetFormula         = get.MetFormula(this)
            SpecForm = this.SpecFormula;
            MTS = this.MetToSpecies;
            MetFormula = SpecForm(MTS,:);
        end
        %genes
        function FastGeneLookup     = get.FastGeneLookup(this)
            FastGeneLookup = cell(this.NumGenes,1);
            for i=1:this.NumGenes
                FastGeneLookup{i} = [this.DEL this.genes{i} this.DEL];
            end
            FastGeneLookup = char(FastGeneLookup);
        end
        %species
        function SpecFormula        = get.SpecFormula(this)
            SpecFormula = zeros(this.NumSpecies,this.species{1}.NC);
            for i=1:this.NumSpecies
                SpecFormula(i,:) = this.species{i}.formula_as_vector;
            end
        end
        function SpecOrigin         = get.SpecOrigin(this)
            MetToSpec = this.MetToSpecies;
            MetOrigin = this.MetOrigin;
            SpecOrigin = false(this.NumSpecies,this.NumModels);
            for i=1:this.NumSpecies
                MetIDx = MetToSpec==i;
                SpecOrigin(i,:) = any(MetOrigin(MetIDx,:),1);
            end
        end
        function SpecComps          = get.SpecComps(this)
            MetToSpec = this.MetToSpecies;
            MetComp   = this.MetCompartment;
            Comps     = this.CompartmentIDToName(:,1);
            SpecComps = false(this.NumSpecies,this.NumModels);
            for j=1:this.NumCompartments
                MetToComp = ismember(MetComp,Comps(j));
                SpecComps(MetToSpec(MetToComp),j) = true;
            end
        end
        
        % Initialization methods
        function this = COMMGEN(modelNames,options,LoadPrevious)
            %%% PURPOSE
            % Initiates the COMMGEN procedure. One or more models are loaded and an
            % initial combined model is created (if multiple models were provided)
            %%% EXAMPLE CALL
            %   Combined_model = COMMGEN({'Model1','Model2','Model3'},opts,'run1');
            %%% INPUT
            %   modelNames: Cell array containing the names of the model folders to be
            %   loaded into COMMGEN.
            %%% OPTIONAL INPUT
            %   options: If any options were pre-set, these are provided here.
            %   LoadPrevious: The name of a folder in which previously
            %   saved changes can be found (Output of SaveChoices
            %   function). Alternatively, enter a 0 to indicate no previous
            %   choices should be loaded.
            %%% OUTPUT
            %   this: COMMGEN model structure on which all subsequent
            %   functions are run.
            
            fprintf('Welcome to COMMGEN.\nPlease mail any questions, suggestions, remarks or bug reports to ruben.vanheck@wur.nl\n\n')
            
            models = cell(numel(modelNames),1);
            for i=1:numel(modelNames)
                models{i} = loadMatModel(modelNames{i});
            end
            
            % Keep track of number of original models
            
            this.Models = models;
            this.NumModels = numel(models);
            for i=1:numel(models)
                this.ModelNames{i} = regexprep(models{i}.dir,{'/$','.*/'},{'',''});
            end
            
            % Current process
            this.Phase = 0;
            this.CurrentFunction = 'Initializing';
            
            % Load preset options for merging if given
            if nargin>=2 && ~isempty(options)
                this.options = MergeOptions(options);
            else
                this.options = MergeOptions();
            end
            
            % Check whether the input models share any genes
            for i=1:numel(models)-1
                for j=1+1:numel(models)
                    if isempty(intersect(models{i}.locus.unique_loci,models{j}.locus.unique_loci))
                        fprintf('model %d (%s) and model %d (%s) don''t overlap in their gene use.\n',i,models{i}.dir,j,models{j}.dir)
                    end
                end
            end
            
            % Collect all species from the models
            this = this.InitSpecies(models);
            
            % Collect all reactions from the different models
            for i=1:length(models)
                Origin    = false(1,this.NumModels);
                Origin(i) = true;
                this = InitReactions(this,models{i},Origin);
            end
            
            % Extract gene and Metabolite information from the reactions
            this = addMetabolites(this);
            this = InitGenes(this);
            
            % Create stoichiometric matrix
            this = addStoichMatrix(this);
            
            % Create matrix mapping genes to reactions (not a knockout
            % matrix, purely associations!)
            this = addRxnGeneMat(this);
            
            % Load previous settings
            if nargin<3
                this.LoadSettings;
            else
                this.LoadSettings(LoadPrevious);
            end
            
            % Obtaining information on the compartments.
            this.InitCompartments;
            
            % If not yet set, set hub metabolites
            if ~this.options.HubComplete && ~this.options.AllAuto
                this.SetHubMets;
            end
            
            % Prepare stucture to track future changes
            this.InitializeTracking;
            
            % Process the options set by the user to assure proper
            % functionality
            this.ProcessOptions;
            
            % Initialize variables
            this.ToBeRemovedRxns = false(this.NumReactions,1);
            this.Phase = 1;
            
            % Obtain default values for the lower and upper bounds
            this.LBdef = min(this.Lb);
            this.UBdef = max(this.Ub);
            
            % Load previously made changes
            if nargin<3
                this.LoadChoices;
            else
                this.LoadChoices(LoadPrevious);
            end
            
            if this.options.RDPredict
                this.RDPredict;
            end
            
            fprintf('The default lower and upper bounds are set to %d and %d. Please adjust this.LBdef and this.UBdef if other values are preferred\n',this.LBdef,this.UBdef)
            
        end
        function this = InitSpecies(this, models)
            % Obtain all species IDs
            
            AllSpecs = {''};
            for i=1:numel(models)
                model = models{i};
                speciesID = cell(length(model.species),1);
                
                for j=1:length(model.species);
                    speciesID{j}   = model.species{j}.id;
                end
                % Find species not yet present in merged model and appends
                [~,IA] = setdiff(speciesID,AllSpecs);
                AllSpecs = [AllSpecs;speciesID(IA)]; %#ok<AGROW>
                this.species = [this.species;model.species(IA)];
            end
            
            % Remove initial value
            AllSpecs(1) = [];
            
            % Sort the species list for future convenience
            [this.species_IDs,I] = sort(AllSpecs);
            this.species = this.species(I);
            
            % Obtain mapping of species ID to Name and of species IDx to
            % formula
            this.SpeciesIDToName = cell(this.NumSpecies,2);
            this.SpeciesIDToName(:,1) = this.species_IDs;
            for i=1:this.NumSpecies
                this.SpeciesIDToName{i,2} = this.species{i}.name;
                this.TrackSpec('Initial',this.species{i});
            end
            this.SpeciesIDToName(:,2) = regexprep(this.SpeciesIDToName(:,2),' ','_');
            
            % Remove Molecular formulas of polymers
            this.identifyPolymers;
            
            % Initiate FormInspected
            this.FormInspected = zeros(this.NumSpecies,1);
            
        end
        function this = InitReactions(this, model, Origin)
            % Go through the supplied models and load all non-empty
            % reactions
            NewReactions = cell(model.stoich.r,1);
            Discard = false(model.stoich.r,1);
            for i=1:model.stoich.r
                NewReactions{i} = MergedReaction(model.reactions{i},Origin);
                if NewReactions{i}.size==0 % Empty reaction
                    Discard(i) = true;
                end
            end
            NewReactions(Discard) = [];
            this.reactions    = [this.reactions;NewReactions];
        end
        function this = InitGenes(this)
            list = {};
            for i=1:this.NumReactions
                str = this.reactions{i}.proteins;
                str = regexprep(str,'\.',''); % Remove dots from gene names
                this.reactions{i}.proteins = str;
                % remove whitespace
                str = regexprep(str, '\s+', '');
                if ~isempty(str)
                    genelist = regexp(str, '[+;]', 'split')';
                    list = [list;genelist]; %#ok<AGROW>
                end
            end
            this.genes = unique(list);
        end
        function this = InitCompartments(this)
            % Check / obtain information on how the compartments are
            % connected. e.g., is transport from extracellular to cytosol
            % possible or does it have to go through the periplasm?
            
            if isempty(this.CompartmentIDToName)
                comps = unique(this.MetCompartment);
                NumComp = length(comps);
                
                % Initialize CompartmentIDToName. Take name if provided by
                % user, otherwise use ID.
                this.CompartmentIDToName = cell(NumComp,2);
                this.CompartmentIDToName(:,1) = comps;
                for i=1:NumComp
                    if ismember(comps(i),this.options.CompartmentNames(:,1))
                        this.CompartmentIDToName(i,2) = this.options.CompartmentNames(strcmp(comps(i),this.options.CompartmentNames(:,1)),2);
                    else
                        this.CompartmentIDToName(i,2) = this.CompartmentIDToName(i,1);
                    end
                end
                
                % Check if UnkComp is included in one of the models
                if any(strcmp(comps,'UNKCOMP')) && ~this.options.AllAuto
                    fprintf('One of the compartments included is ''UNKCOMP'', this may mean that a compartment has been lost either before or during the conversion to the MnXRef namespace.\nPleace check if any compartments are missing from the list:\n')
                    disp(this.CompartmentIDToName)
                    while true
                        fprintf('If any compartment is missing, please enter a desired compartment ID (recommended:use MNXC ID if available!) or just press enter to continue.\n')
                        reply = input('','s');
                        if isempty(reply)
                            break
                        else
                            reply2 = input('Please enter the corresponding compartment name (recommended: short abbreviation! or just press enter to use the same Name as ID.)\n','s');
                            if isempty(reply2)
                                reply2=reply;
                            end
                            if any(ismember({reply,reply2},this.CompartmentIDToName))
                                fprintf('IDs and names which are already in use cannot be used. Please refrain from using the following:')
                                disp(this.CompartmentIDToName)
                            else
                                this.CompartmentIDToName(end+1,:) = {reply,reply2};
                            end
                        end
                    end
                end
            else
                comps = this.CompartmentIDToName(:,1);
                NumComp = length(comps);
            end
            
            if isempty(this.options.CompartmentConnections)
                if NumComp>2
                    fprintf('%d compartments found, please indicate whether any connections are invalid\n',NumComp)
                    NotUnk = cellfun(@isempty,regexp(this.RxnComp,'.*UNKCOMP.*'));
                    Conn = unique(this.RxnComp(this.is_transport&NotUnk));
                    Rep1 = strcat(this.options.CompartmentNames,'%');
                    Rep2 = strcat(this.options.CompartmentNames(:,1),'$');
                    ConnNames = regexprep(Conn,Rep1(:,1),Rep1(:,2));
                    ConnNames = regexprep(ConnNames,Rep2,this.options.CompartmentNames(:,2));
                    while true && ~this.options.AllAuto
                        disp([num2cell(1:numel(ConnNames))' ConnNames])
                        reply = input('Enter the corresponding number to indicate an invalid connection or enter 0 to continue. You can use [1 2] to remove both connection 1 and 2 simultaneously.\n');
                        if any(reply~=0)
                            Conn(reply) = [];
                            ConnNames(reply) = [];
                        else
                            break
                        end
                    end
                else
                    Conn = unique(this.RxnComp(this.is_transport));
                end
                this.options.CompartmentConnections = Conn;
            end
        end
        function this = InitializeTracking(this)
            % Prepare the structures to track the changes made to the consensus
            % model
            
            % Load info
            NumRxns    = this.NumReactions;
            NumSpecies = this.NumSpecies;
            NumGenes   = this.NumGenes;
            MetCompartment = this.MetCompartment;
            CompToName     = this.CompartmentIDToName;
            MetToSpec      = this.MetToSpecies;
            
            % Prepare
            % Tracking Reactions
            for i=1:NumRxns
                this = TrackRxn(this,'Initial Reaction','',this.reactions{i});
            end
            
            % Tracking Metabolites
            this.MetFirstSeen = cell(NumSpecies+1,numel(CompToName(:,1))+1);
            this.MetFirstSeen(:,:) = {-1};
            this.MetFirstSeen(1,:) = ['Chemical';CompToName(:,2)];
            this.MetFirstSeen(2:end,1) = this.species_IDs;
            for i=1:this.NumMetabolites
                CompLoc = find(strcmp(MetCompartment(i),CompToName(:,1)))+1;
                this.MetFirstSeen(MetToSpec(i)+1,CompLoc) = {0};
            end
            this.MetLastSeen = this.MetFirstSeen;
            
            % Tracking Genes
            this.GenesFirstSeen = zeros(NumGenes,1);
            this.GenesLastSeen  = zeros(NumGenes,1);
            
            % Tracking Species
            %Structure already prepared in calling the class
            
            % Summary Statistics
            Groups = 0:2^this.NumModels-1;
            Groups = dec2bin(Groups,numel(this.Models));
            Groups = cellstr(Groups);
            
            this.TrackSpecStats(1,:) = Groups;
            this.TrackRxnStats(1,:)  = Groups;
            
            this.SummaryStatistics;
            
            % Track dead/inactive reactions/metabolites
            [dR,dM,iR,iM] = this.findDeadEnds;
            this.TrackDead(end+1,:) = num2cell([count(dR) count(iR) count(dM) count(iM)]);
            
        end
        %load previously made changes
        function this = LoadSettings(this,LoadPrevious)
            %%% PURPOSE
            % Loads the options file from a previous COMMGEN run.
            %%% EXAMPLE CALL
            %   Not meant to be called directly.
            %%% INPUT
            %   LoadPrevious: Either a zero to indicate no previous options
            %   are to be loaded or the name of the folder where the
            %   previous options can be found.
            %%% OPTIONAL INPUT
            %%% OUTPUT
            %   this: COMMGEN model structure on which all subsequent functions are run.
            
            fprintf('Checking if any previous setings can and should be loaded\n')
            
            if nargin==2
                %Check correctness input argument
                if isnumeric(LoadPrevious) && LoadPrevious==0
                    return
                elseif isnumeric(LoadPrevious)
                    error('Please enter either a zero or a string in the LoadPrevious argument.')
                elseif ~exist(LoadPrevious,'dir')
                    error(['The folder ' LoadPrevious ' does not exist. Please check for spelling errors.'])
                end
            else
                %Check if default folder is present
                LoadPrevious = [this.ModelNames{:}]; %Default folder name
                if ~exist(LoadPrevious,'dir')
                    return
                elseif ~exist([LoadPrevious '/options.mat'],'file')
                    return
                end
            end
            
            fprintf('Should any previous choices saved in %s be loaded?\n[Y/N]\n',[LoadPrevious '/options.mat'])
            reply = COMMGEN.InputProcessing({'Y','N'});
            if strcmp(reply,'N')
                return
            end
            
            %Load Settings
            load([LoadPrevious '/options.mat']);
            this.options = opts;
            this.options.HubComplete = true;
            this.CompartmentIDToName = CompIDToName;
        end
        function this = LoadChoices(this,LoadPrevious)
            %%% PURPOSE
            % Loads previously made changes from a previous COMMGEN run and reapplies them.
            %%% EXAMPLE CALL
            %   Not meant to be called directly.
            %%% INPUT
            %   LoadPrevious: Either a zero to indicate no previous options
            %   are to be loaded or the name of the folder where the
            %   previous options can be found.
            %%% OPTIONAL INPUT
            %%% OUTPUT
            %   this: COMMGEN model structure on which all subsequent functions are run.
            
            fprintf('Checking if any previous setings can and should be loaded\n')
            
            if nargin==2
                %Check correctness input argument
                if isnumeric(LoadPrevious) && LoadPrevious==0
                    return
                elseif isnumeric(LoadPrevious)
                    error('Please enter either a zero or a string in the LoadPrevious argument.')
                elseif ~exist(LoadPrevious,'dir')
                    error(['The folder ' LoadPrevious ' does not exist. Please check for spelling errors.'])
                end
            else
                %Check if default folder is present
                LoadPrevious = [this.ModelNames{:}]; %Default folder name
                if ~exist(LoadPrevious,'dir')
                    return
                end
            end
            
            %TrackRxns
            if exist([LoadPrevious '/TrackRxns.tsv'],'file')
                fprintf('Should any previous choices saved in %s be loaded?\n[Y/N]\n',[LoadPrevious '/TrackRxns.tsv'])
                reply = COMMGEN.InputProcessing({'Y','N'});
                if strcmp(reply,'Y')
                    TrackRxns = [LoadPrevious '/TrackRxns.tsv'];
                else
                    TrackRxns = '';
                end
            else
                TrackRxns = '';
            end
            
            %TrackSpec
            if exist([LoadPrevious '/TrackSpec.tsv'],'file')
                fprintf('Should any previous choices saved in %s be loaded?\n[Y/N]\n',[LoadPrevious '/TrackSpec.tsv'])
                reply = COMMGEN.InputProcessing({'Y','N'});
                if strcmp(reply,'Y')
                    TrackSpec = [LoadPrevious '/TrackSpec.tsv'];
                else
                    TrackSpec = '';
                end
            else
                TrackSpec = '';
            end
            
            %TrackSpec
            if exist([LoadPrevious '/RejMerge.tsv'],'file')
                fprintf('Should any previous choices saved in %s be loaded?\n[Y/N]\n',[LoadPrevious '/RejMerge.tsv'])
                reply = COMMGEN.InputProcessing({'Y','N'});
                if strcmp(reply,'Y')
                    RejMerge = [LoadPrevious '/RejMerge.tsv'];
                else
                    RejMerge = '';
                end
            else
                RejMerge = '';
            end
            
            % Return if no previous choices to be loaded
            if isempty(TrackRxns) && isempty(TrackSpec) && isempty(RejMerge)
                return %No previous choices to be loaded.
            end
            
            % Load previous choices
            delimiter = '\t';
            startRow = 2;
            
            if ~isempty(TrackRxns)
                formatSpec = '%s%s%s%s%s%s%f%f%f%s%f%[^\n\r]';
                fileID = fopen(TrackRxns,'r');
                dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'EmptyValue' ,NaN,'HeaderLines' ,startRow-1, 'ReturnOnError', false);
                fclose(fileID);
                dataArray([7, 8, 9, 11]) = cellfun(@(x) num2cell(x), dataArray([7, 8, 9, 11]), 'UniformOutput', false);
                TrackRxns = [dataArray{1:end-1}];
            end
            
            if ~isempty(TrackSpec)
                formatSpec = '%s%s%s%s%s%s%s%s%s%s%f%s%[^\n\r]';
                fileID = fopen(TrackSpec,'r');
                dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'EmptyValue' ,NaN,'HeaderLines' ,startRow-1, 'ReturnOnError', false);
                fclose(fileID);
                dataArray(11) = cellfun(@(x) num2cell(x), dataArray(11), 'UniformOutput', false);
                TrackSpec = [dataArray{1:end-1}];
            end
            
            if ~isempty(RejMerge)
                formatSpec = '%s%[^\n\r]';
                fileID = fopen(RejMerge,'r');
                dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'EmptyValue' ,NaN, 'ReturnOnError', false);
                fclose(fileID);
                RejMerge = [dataArray{1:end-1}];
            end
            
            % Find the Phases corresponding to each row in either cell matrix
            RxnPhase  = cell2mat(TrackRxns(:,9));
            SpecPhase = cell2mat(TrackSpec(:,11));
            Phases = unique([RxnPhase ; SpecPhase]);
            
            % Go through the modifications in the original order
            for i=1:numel(Phases)
                Ph = Phases(i);
                % Set Phase and current function
                this.Phase=Ph;
                if any(SpecPhase==Ph)% Species Changes
                    this.ChangeFunction(TrackSpec{find(SpecPhase==Ph,1),11});
                    this.ProcessPreviousSpecChoices(TrackSpec(SpecPhase==Ph,:));
                end
                if i==24
                    1 %TEMP
                end
                if any(RxnPhase==Ph) % Reaction changes
                    this.ChangeFunction(TrackRxns{find(RxnPhase ==Ph,1),9});
                    this.ProcessPreviousRxnChoices( TrackRxns(RxnPhase ==Ph,:));
                end
                this.UpdateAll;
            end
            
            this.RejectedMerge = RejMerge;
            
        end
        function ProcessPreviousSpecChoices(this,SpecCh)
            % Process previously made changes in species
            % Format in SpecCh: {'Relation','OldSpecID','OldSpecName','NewSpecID','NewSpecID','Comment','ChemForm','mass','charge','refs','Phase','Tool'};
            
            % Confirm there are any changes in the species
            NumCh = size(SpecCh,1);
            if NumCh==0
                return
            end
            
            for i=1:NumCh
                switch(SpecCh{i,1})
                    case('Merged')
                        % Species merging does not actually alter anything on the
                        % species level. Update this.TrackSpecies in current run.
                        KIDx = find(ismember(this.species_IDs,SpecCh{i,4}));
                        DIDx = find(ismember(this.species_IDs,SpecCh{i,2}));
                        this.MergeTwoSpecies([KIDx DIDx],KIDx);
                    case('New')
                        this.species{end+1} = MnXChemicalSpecies(SpecCh{i,4},SpecCh{i,5},'',SpecCh{i,7},SpecCh{i,8},SpecCh{i,9},SpecCh{i,10});
                        this.species_IDs(end+1) = SpecCh{i,4};
                        this.species_IDToName(end+1,:) = [SpecCh{i,4} SpecCh{i,5}];
                        this.TrackSpec('New',this.species{end},'','','Added by user');
                    case('Initial')
                        %Nothing to be done
                    case('ChangedForm')
                        %Formula changed
                        IDx = ismember(this.species_IDs,SpecCh{i,4});
                        this.SetSpecFormula(IDx,SpecCh{i,7})
                    otherwise
                        fprintf('Relation ''%s'' in species changes file not recognised. This row has been ignored.\n',SpecCh{i,1})
                end
            end
            
        end
        function ProcessPreviousRxnChoices(this,RxnsCh)
            % Process previously made changes in reactions
            % Format in SpecCh: {'Relation','OldSpecID','OldSpecName','NewSpecID','NewSpecID','Comment','ChemForm','mass','charge','refs','Phase','Tool'};
            
            % Confirm there are any changes in the reactions
            NumCh = size(RxnsCh,1);
            if NumCh==0
                return
            end
            
            % Identify reactions which may be removed (in case no inconsistencies are
            % found)
            RemoveIDx = strcmp('Removed',RxnsCh(:,1));
            RemRxns   = ismember(this.reaction_IDs,RxnsCh(RemoveIDx,2));
            
            for i=1:NumCh
                switch(RxnsCh{i,1})
                    case('Removed')
                        % Nothing to be done, all reactions to be removed will be
                        % removed in the end. First CMMG will check whether some should
                        % be kept due to inconsistencies between the current run and
                        % the past run.
                        
                    case('SpecModified')
                        %Nothing to be done, these cases are resolved by
                        %ProcessPreviousSpecChoices
                        
                    case('Merged')
                        % Find the reactions which were merged
                        RxnIDs = splitstr(RxnsCh{i,2},';');
                        RemRxns(ismember(this.reaction_IDs,RxnIDs))=false; % These reactions are already marked for removal via this.MergeReactions
                        if ~all(ismember(RxnIDs,this.reaction_IDs))
                            % Not all reactions which are supposed to be merged are
                            % present in the current model. Ignore this row and the
                            % corresponding to be removed reactions
                            fprintf('One or more to-be merged reactions not found, the reaction ''%s'' has not been created and original reactions have not been removed.\n',RxnsCh{i,3})
                            for j=1:numel(RxnIDs)
                                fprintf('The reaction %s has not been removed\n',RxnIDs{j})
                            end
                        else
                            group = ismember(this.reaction_IDs,RxnIDs)';
                            this.MergeReactions(group,{RxnsCh{i,3},RxnsCh{i,5},RxnsCh{i,6},RxnsCh{i,4},RxnsCh{i,7},RxnsCh{i,8}});
                        end
                        
                    case('NewRxn')
                        % Create new rxn
                        origin = regexp(RxnsCh{i,4},'orig:%([^%]*)%','tokens','once');
                        origin = logical(str2num(origin{1})); %#ok<ST2NM>
                        Rxn = MergedReaction([],origin);
                        Rxn.id       = RxnsCh{i,3};
                        Rxn.equation = RxnsCh{i,5};
                        Rxn.biomass  = 0;
                        Rxn.comment = RxnsCh{i,4};
                        Rxn.external = RxnsCh{i,11};
                        Rxn = Rxn.getReactants;
                        Rxn.lb = RxnsCh{i,7};
                        Rxn.lb = RxnsCh{i,8};
                        
                        % Adding the new reaction to the model
                        this.addReaction(Rxn);
                        this = TrackRxn(this,'NewRxn','',Rxn);
                        
                    case {'ChangedComp','SpecModified','ExtChanged'}
                        if ismember(RxnsCh(i,2),this.reaction_IDs)
                            RxnIDx = ismember(this.reaction_IDs,RxnsCh(i,2));
                            Rxn = this.reactions{RxnIDx};
                            Rxn.SrcRxns  = {Rxn};
                            Rxn.id       = RxnsCh{i,3};
                            Rxn.equation = RxnsCh{i,5};
                            Rxn.lb = RxnsCh{i,7};
                            Rxn.lb = RxnsCh{i,8};
                            Rxn.external = RxnsCh{i,11};
                            Rxn.comment  = RxnsCh{i,4};
                            Rxn          = Rxn.getReactants;
                            this.reactions{RxnIDx} = Rxn;
                            this.TrackRxn(RxnsCh{i,1},RxnsCh{i,2},Rxn);
                        else
                            fprintf('Original reaction %s not found, reaction %s not added!',RxnsCh{i,2},RxnsCh{i,3})
                        end
                    case {'RxnChanged'}
                        [~,IA] = ismember(RxnsCh(i,2),this.reaction_IDs);
                        if IA
                            this.reactions{IA}.equation = RxnsCh{i,5};
                            this.reactions{IA}.proteins = RxnsCh{i,6};
                            this.TrackRxn('RxnChanged',this.reactions{IA}.id,this.reactions{IA})
                        else
                            fprintf('Reaction %s not found and thus not modified',RxnsCh{i,2})
                        end
                    case('Initial Reaction')
                        %Nothing to be done.
                    case('EqChanged')
                        RxnIDx = ismember(this.reaction_IDs,RxnsCh(i,2));
                        NewEq = RxnsCh{i,5};
                        this.ChangeEq(RxnIDx,NewEq);
                    otherwise
                        fprintf('Relation ''%s'' in reaction changes file not recognised. This row has been ignored.\n',RxnsCh{i,1})
                end
            end
            this.MarkForRemoval(RemRxns);
        end
        %options
        function this = ProcessOptions(this)
            %This function checks whether or not the set options are
            %correct and alarms the user if not.
            
            fprintf('Processing given options.\n')
            
            % Compartments
            %Compartment connections
            fprintf('Checking compartment relations\n')
            
            % Assert that at least one of the compartments in the models is
            % to be kept
            assert(~isempty(intersect(this.options.Compartments(:,1),this.CompartmentIDToName(:,1))),'None of the compartments in the model is to be kept according to the options file. Check proper spelling')
            
            % Check that there is a 1:1 relation between compartment IDs
            % and compartment names
            assert(numel(this.options.CompartmentNames(:,1))==numel(unique(this.options.CompartmentNames(:,1))),'Duplicate value in Column 1 of CompartmentNames (options) found')
            assert(numel(this.options.CompartmentNames(:,2))==numel(unique(this.options.CompartmentNames(:,2))),'Duplicate value in Column 2 of CompartmentNames (options) found')
            
            % Compartment connections, check whether all names are
            % recognised and put in correct format (alphabetic).
            for i=1:numel(this.options.CompartmentConnections)
                cmps = splitstr(this.options.CompartmentConnections{i},'%');
                cmps = this.NameToID(cmps);
                assert(all(ismember(cmps,this.CompartmentIDToName(:,1))),'Not all compartments in ''CompartmentConnections'' are present in the models, check proper spelling')
                cmps = sort(cmps);
                conn = cmps{1};
                for j=2:numel(cmps)
                    conn = [conn this.DEL cmps{j}]; %#ok<AGROW>
                end
                this.options.CompartmentConnections{i} = conn;
            end
            
            % Invalid transport reactions
            fprintf('Checking for invalid transport reactions.\n')
            this.checkInvalidTransportSettings;
            
            % Reaction directionality
            fprintf('Checking reaction directionality settings.\n')
            % Reaction direction prediction
            this.options.databaseNames = reshape(this.options.databaseNames,1,numel(this.options.databaseNames));
            if isempty(strjoin(this.options.databaseNames,''))
                fprintf('Defaulted to using the input models for reaction direction prediction.\n')
                this.options.databaseNames = this.ModelNames;
            end
            
            if ~ismember(this.options.MergeArrows,-1:4)
                fprintf('Invalid value found for ResolveInvalidTransport (options), reset.\n')
                this.options.MergeArrows = [];
            end
            if isempty(this.options.MergeArrows) && this.options.AllAuto
                this.options.MergeArrows = -1;
            elseif isempty(this.options.MergeArrows)
                disp(['When two or more reactions are being merged which occur in opposite' char(10)...
                    'directions, but are identical otherwise, may bidirectionality be assumed?' char(10)...
                    'example1:' char(10)...
                    'A --> B' char(10)...
                    'A <-- B' char(10)...
                    'Assumption:' char(10)...
                    '(Y1 & Y2) A <==> B' char(10) char(10)...
                    'example2:' char(10)...
                    'A --> B' char(10)...
                    'A <==> B' char(10)...
                    'Assumption:' char(10)...
                    '(Y1) A <==> B' char(10) ...
                    '(Y2) A --> B' char(10) char(10)...
                    'Choose:' char(10)...
                    'Y1: Always assume <==> when in disagreement' char(10)...
                    'Y2: If choosing between one type of unidirectional and bidirectional choose unidirectional.' char(10)...
                    'Y3: Automatically predict reaction directionality' char(10) ...
                    'Y4: Take the directionality from a particular model' char(10) ...
                    'N: Never choose automatically' char(10)...
                    'P: Always Prompt' char(10)])
                reply = COMMGEN.InputProcessing({'Y1','Y2','Y3','N','P'});
                switch(reply)
                    case('Y1')
                        this.options.MergeArrows = 1;
                    case('Y2')
                        this.options.MergeArrows = 2;
                    case('Y3')
                        this.options.MergeArrows = 3;
                    case('Y4')
                        this.options.MergeArrows = 4;
                    case('N')
                        this.options.MergeArrows = -1;
                    case('P')
                        this.options.MergeArrows = 0;
                end
            end
            if this.options.MergeArrows==4 && isempty(this.options.DirectModel)
                %Choose the model to use:
                disp('Choose the index of the model of which to take reaction directionalities')
                disp([num2cell((1:this.NumModels)') reshape(this.ModelNames,this.NumModels,1)])
                reply = COMMGEN.InputProcessing(strread(num2str(1:this.NumModels),'%s'));
                this.options.DirectModel = str2double(reply);
            end
            
            % GPR rules
            fprintf('Checking GPR settings.\n')
            if ~ismember(this.options.GeneOR,[0 1])
                disp('Invalid value found for GeneOR (options), reset.')
                this.options.GeneOR = [];
            end
            if isempty(this.options.GeneOR)
                if this.options.AllAuto
                    fprintf('As the option has not been pre-set and the option AllAuto is true, gene rule merging has been defaulted to automatic and strict.\n please modify this.options.GeneOR and this.options.GeneORStrict to change this.\n\n')
                    this.options.GeneOR = 1;
                    this.options.GeneORStrict = 1;
                else
                    disp(['When reactions having different GPRs (gene rules) are' char(10)...
                        'being merged, may they be automatically connected via an ''OR''?' char(10)...
                        'Y: Always' char(10)...
                        'N: Prompt' char(10)...
                        ])
                    reply = COMMGEN.InputProcessing({'Y','N'});
                    if strcmp(reply,'Y')
                        this.options.GeneOR = 1;
                        if this.options.GeneORStrict ==0
                            disp(['Would you like to employ a strict or flexible OR merging for GPR rules?'             char(10) ...
                                'Example: ''(A AND B) OR (A AND C) OR A'''                                              char(10) ...
                                'Y: Strict: ''(A AND B) OR (A AND C)'''                                                 char(10) ...
                                'N: Flexible: ''(A AND B) OR (A AND C) OR A''; effectively the rule resorts to ''A'''   char(10) ...
                                '[Y/N]'])
                            reply = COMMGEN.InputProcessing({'Y','N'});
                            if strcmpi(reply,'Y')
                                this.options.GeneORStrict=1;
                            else
                                this.options.GeneORStrict=-1;
                            end
                        end
                    else
                        this.options.GeneOR = 0;
                    end
                end
            end
            
            if ~ismember(this.options.GeneORStrict,[-1 0 1])
                disp('Invalid value found for GeneORStrict (options), reset.')
                this.options.GeneORStrict = 0;
            end
            
            % Rejected merges
            fprintf('Checking settings for rejections\n')
            if ~ismember(this.options.PermanentReject,[-1 0 1])
                fprintf('Invalid value found for PermanentReject (options), reset.\n')
                this.options.GeneOR = [];
            end
            
            % Hub metabolites
            fprintf('Checking settings for hub metabolites.\n')
            if ~isempty(this.options.HubMets)
                disp('Checking provided list of Hub metabolites\n')
                FastMetLookup  = this.FastMetLookup;
                InitList            = this.options.HubMets;
                Spec_IDs            = this.species_IDs;
                NoCompInfo          = cellfun(@isempty,regexp(InitList,'@'));
                for i=1:numel(InitList)
                    if NoCompInfo(i)
                        Match = strcmp(this.NameToID(InitList{i}),Spec_IDs);
                        if ~any(Match)
                            fprintf('Species %s not found in the models, please check!\n',InitList{i})
                        end
                    else
                        Match = mfindstr(FastMetLookup,this.NameToID(InitList{i}));
                        if isempty(Match)
                            fprintf('Metabolite %s not found in the models, please check!\n',InitList{i})
                        end
                    end
                end
            end
            
            % Redox pairs
            fprintf('Checking settings for redox pairs\n')
            this.RedoxPairsAccepted = this.options.RedoxPairs;
        end
        function this = checkInvalidTransportSettings(this)
            %Checks whether the settings provided are in the correct format
            %and otherwise (or if none were provided) obtains the settings.
            
            % Finding whether there are any Invalid Transport reactions
            % Ignore the unknown compartment in finding invalid compartment
            % connections
            CompConn = unique(regexprep(this.RxnComp,{'^UNK_COMP%','%UNK_COMP'},{'',''}));
            InvCompConn = setdiff(CompConn,[this.options.CompartmentConnections;this.CompartmentIDToName(:,1);'boundary']);
            
            if isempty(InvCompConn) % No invalid connections found
                return
            end
            % Check whether any can be solved automatically
            % Invalid transport reactions involving more than 2 compartments
            % cannot be solved automatically.
            autoSolve = true(numel(InvCompConn),1);
            for i=1:numel(InvCompConn)
                if length(findstr(InvCompConn{i},'%'))>1 %#ok<FSTR>
                    %More than two compartments
                    autoSolve(i) = false;
                end
            end
            if ~any(autoSolve)
                return
            else
                InvCompConn(~autoSolve) = [];
            end
            % Checking properness of settings and altering if required
            if ~ismember(this.options.ResolveInvalidTransport,[-1 0 1])
                disp('Invalid value found for ResolveInvalidTransport (options), reset.')
                this.options.ResolveInvalidTransport = 0;
            end
            if this.options.ResolveInvalidTransport == 0
                clc
                disp([ ...
                    'If a reaction transports a species A from x to z, where x and z are' char(10)...
                    'not connected, this can be automatically resolved via' char(10)...
                    'an intermediate compartment y IF the species A is not yet present' char(10)...
                    'in compartment y. The original reaction is then split into 2, where' char(10)...
                    'one is the original reaction with altered compartmentalization and' char(10)...
                    'the other corresponds to a ''free'' reaction.' char(10)...
                    char(10)...
                    'In the following examples, reactions from x to z are changed into' char(10)...
                    '2 reactions, x->y & y->z. In the original reaction x is changed into y.:' char(10)...
                    char(10)...
                    'Example 1: Active transport.' char(10)...
                    'cpd1@x + ATP@z + H2O@z --> cpd1@z + ADP@z + Pi@z + H@z' char(10)...
                    'Becomes:' char(10)...
                    'cpd1@x <==> cpd1@y' char(10)...
                    'cpd1Zy + ATP@z + H2O@z --> cpd1@z + ADP@z + Pi@z + H@z' char(10)...
                    char(10) ...
                    'Example 2: Symport.' char(10)...
                    'cpd1@x + H@x --> cpd1@z + H@z' char(10)...
                    'Becomes:' char(10)...
                    'cpd1@x <==> cpd1@y' char(10)...
                    'cpd1@y + H@y --> cpd1@z + H@z' char(10)...
                    char(10) ...
                    'Example 3: Antiport.' char(10)...
                    'cpd1@x + H@z --> cpd1@z + H@x' char(10)...
                    'Becomes:' char(10)...
                    'cpd1@x <==> cpd1@y' char(10)...
                    'cpd1@y + H@z --> cpd1@z + H@y' char(10)...
                    char(10) ...
                    'Example 4: Free diffusion' char(10)...
                    'cpd1@x --> cpd1@z' char(10)...
                    'Becomes:' char(10)...
                    'cpd1@x <==> cpd1@y' char(10)...
                    'cpd1@y --> cpd1@z' char(10)...
                    char(10)...
                    'Do you want to use this automatic resolving where possible?' char(10)...
                    'The exact transitions will be asked afterwards.' char(10) ...
                    '[Y/N]'...
                    ])
                reply = COMMGEN.InputProcessing({'Y','N'});
                if strcmp(reply,'Y')
                    this.options.ResolveInvalidTransport = 1;
                else
                    this.options.ResolveInvalidTransport = -1;
                    return
                end
            elseif this.options.ResolveInvalidTransport == -1;
                return
            end
            % Check provided solutions (if any)
            InvSol = false(size(this.options.InvTransportSolution,1));
            for i=1:size(this.options.InvTransportSolution,1)
                % Invalid transport
                cmps = splitstr(this.options.InvTransportSolution{i,1},'%');
                if length(cmps)>2
                    fprintf('Only Invalid transport reactions involving two compartments may be solved automatically, %s is ignored',this.options.InvTransportSolution{i,1})
                    InvSol(i) = true;
                end
                % Put compartments in right order and convert to IDs
                cmps = this.NameToID(cmps);
                cmps = sort(cmps);
                this.options.InvTransportSolution{i,1} = [cmps{1} this.DEL cmps{2}];
            end
            this.options.InvTransportSolution(InvSol,:) = [];
            if ~isempty(this.options.InvTransportSolution)
                this.options.InvTransportSolution(:,2:3) = this.NameToID(this.options.InvTransportSolution(:,2:3));
            end
            
            for i=1:numel(InvCompConn)
                % Check whether a solution for this case has already been
                % provided
                if isempty(this.options.InvTransportSolution) || ~ismember(InvCompConn(i),this.options.InvTransportSolution(:,1))
                    % if not:
                    comps = splitstr(InvCompConn{i},this.DEL);
                    compNames = this.IDToName(comps);
                    fprintf('May transport reactions between compartment ''%s'' and ''%s'' be automatically resolved? [Y/N]',compNames{1},compNames{2})
                    reply = COMMGEN.InputProcessing({'Y','N'});
                    if strcmp(reply,'Y')
                        % Finding intermediate compartment
                        disp(['via which compartment would they be connected?' char(10) ... ...
                            'Options:'])
                        disp(setdiff(this.CompartmentIDToName(:,2),compNames))
                        ViaCompName = COMMGEN.InputProcessing(setdiff(this.CompartmentIDToName(:,2),compNames));
                        ViaComp = this.NameToID(ViaCompName);
                        % Finding the compartment between which
                        % transport will be 'free'
                        fprintf('\nWhich compartment will have ''free'' transport of the species to and from compartment %s?\nOptions:\n',ViaCompName)
                        disp(compNames)
                        reply = COMMGEN.InputProcessing(compNames);
                        Free = this.NameToID(reply);
                        this.options.InvTransportSolution(end+1,:) = {InvCompConn{i},ViaComp,Free};
                    end
                end
            end
        end
        
        % Updating
        function this = addReaction(this,Rxn)
            % this function adds the reaction to the model and enters the
            % corresponding information in the stoichiometric matrix and in
            % the rxnGeneMat. If there are new species, genes and/or
            % metabolites this is not yet processed.
            this.reactions{end+1} = Rxn;
            FastMets  = this.FastMetLookup;
            FastGenes = this.FastGeneLookup;
            Spec_IDs  = this.species_IDs;
            
            % Update bounds
            this.Lb(end+1) = Rxn.lb;
            this.Ub(end+1) = Rxn.ub;
            
            % Update Stoichiometric matrix
            this.Stoich(:,end+1) = 0;
            for i=1:numel(Rxn.reactants)
                MetIDx = mfindstr(FastMets,Rxn.reactants{i}.id);
                if MetIDx == 0; % Metabolite doesn't exist yet
                    % add Metabolite and connect to species
                    this.metabolites{end+1} = Rxn.reactants{i}.id;
                    spec = this.MetToSpecAndComp(Rxn.reactants{i}.id);
                    this.MetToSpecies(end+1) = find(strcmp(spec,Spec_IDs));
                    % update stoichiometric matrix
                    this.Stoich(end+1,:) = 0;
                    this.Stoich(end,end) = Rxn.reactants{i}.stoichiometry;
                    % obtain chemical formula for metabolite
                    this.MetChemForm(:,end+1) = this.species{this.MetToSpecies(end)}.formula_as_vector;
                else
                    % this.Stoich also on right side in case that
                    % the same metabolite is present
                    % on both sides of the equation.
                    this.Stoich(MetIDx,end) = this.Stoich(MetIDx,end) + Rxn.reactants{i}.stoichiometry;
                end
            end
            
            % Update rxnGeneMat
            this.rxnGeneMat(:,end+1) = false;
            str = this.reactions{end}.proteins;
            % remove whitespace
            str = regexprep(str, {'\s+','[()]'}, {'',''});
            if ~isempty(str)
                genelist = regexp(str, '[+;]', 'split');
                for j=1:numel(genelist)
                    IDx = mfindstr(FastGenes,genelist{j});
                    this.rxnGeneMat(IDx,end)=true;
                end
            end
            
        end
        function this = addMetabolites(this)
            
            % Load Data
            SpecChemForm = this.SpecFormula;
            
            % creates the metabolite-list based on the reactions
            list = cell(0);
            cnt = 0;
            
            % create list of species
            for i=1:this.NumReactions
                for j=1:this.reactions{i}.size
                    cnt = cnt + 1;
                    list{cnt,1} = this.reactions{i}.reactants{j}.id;
                end
            end
            % make list unique
            this.metabolites    = unique(list);
            
            % Store relation between metabolites and species
            MetIDs  = this.MetToSpecAndComp(this.metabolites);
            for i=1:this.NumMetabolites
                MetIDs{i} = [this.DEL MetIDs{i} this.DEL];
            end
            MetIDs = char(MetIDs{:});
            
            SpecIDs = this.species_IDs;
            for i=1:this.NumSpecies
                SpecIDs{i} = [this.DEL SpecIDs{i} this.DEL];
            end
            SpecIDs = char(SpecIDs{:});
            
            this.MetToSpecies = nan(this.NumMetabolites,1);
            this.MetChemForm  = nan(size(SpecChemForm,2),this.NumMetabolites);
            for i=1:this.NumMetabolites
                IDx = mfindstr(SpecIDs,MetIDs(i,:));
                this.MetToSpecies(i) = IDx;
                this.MetChemForm(:,i) = SpecChemForm(IDx,:);
            end
        end
        function this = addStoichMatrix(this)
            % get the number of reactions
            rr   = this.NumReactions;
            rxns = this.reactions;
            Sneg = sparse(this.NumMetabolites, rr);
            Spos = sparse(this.NumMetabolites, rr);
            Mets = this.FastMetLookup;
            this.Lb = nan(this.NumReactions,1);
            this.Ub = nan(this.NumReactions,1);
            
            for i=1:rr
                this.Lb(i) = rxns{i}.lb;
                this.Ub(i) = rxns{i}.ub;
                for j=1:rxns{i}.size
                    idx = mfindstr(Mets, [this.DEL rxns{i}.reactants{j}.id this.DEL]);
                    coef = rxns{i}.reactants{j}.stoichiometry;
                    if coef < 0
                        Sneg(idx, i) = coef; %#ok<SPRIX>
                    else
                        Spos(idx, i) = coef; %#ok<SPRIX>
                    end
                end
            end
            
            % create stoichiometric matrix
            S = Sneg + Spos;
            
            this.Stoich = S;
        end
        function this = addRxnGeneMat(this)
            Genes = this.FastGeneLookup;
            RxnGeneMat = false(this.NumGenes,this.NumReactions);
            for i=1:this.NumReactions
                str = this.reactions{i}.proteins;
                % remove whitespace
                str = regexprep(str, {'\s+','[()]'}, {'',''});
                if ~isempty(str)
                    genelist = regexp(str, '[+;]', 'split');
                    for j=1:numel(genelist)
                        IDx = mfindstr(Genes,genelist{j});
                        try
                            RxnGeneMat(IDx,i)=true;
                        catch
                            disp('try_catch loop in addRxnGeneMat failed!')
                        end
                    end
                end
            end
            
            
            this.rxnGeneMat = sparse(RxnGeneMat);
        end
        function this = SetHubMets(this,reset)
            % This function proposes (additional) hub metabolites. Provide
            % true as second argument if the current hub list is to be
            % reset.
            
            if nargin < 2
                reset = false;
            end
            if reset
                this.options.HubMets = [];
            end
            
            % load info
            HubMets  = this.HubMetabolites;
            MetList  = this.metabolites;
            MetNames = this.IDToName(MetList);
            
            % Print current hub list
            FastMetLookup  = this.FastMetLookup;
            
            if ~isempty(this.options.HubMets)
                InitList            = this.options.HubMets;
                Spec_IDs            = this.species_IDs;
                NoCompInfo          = cellfun(@isempty,regexp(InitList,'@'));
                MetToSpec           = this.MetToSpecies;
                for i=1:numel(InitList)
                    if NoCompInfo(i)
                        Match = find(strcmp(InitList{i},Spec_IDs));
                        Matches = MetToSpec == Match;
                        if ~any(Matches)
                            fprintf('WARNING: Species %s not found in the models, please check!\n',InitList{i})
                        else
                            HubMets(Matches) = true;
                        end
                    else
                        Match = mfindstr(FastMetLookup,InitList{i});
                        if Match==0
                            fprintf('WARNING: Metabolite %s not found in the models, please check!\n',InitList{i})
                        else
                            HubMets(Match) = true;
                        end
                    end
                end
            end
            if any(HubMets)
                disp(['The list of hub metabolites already set:' char(10)])
                disp([MetList(HubMets) MetNames(HubMets)])
            end
            
            % Present additional
            RxnsPerMet = sum(logical(this.Stoich),2);
            RxnsPerMet(HubMets) = 0;
            [~,I] = sort(RxnsPerMet,'descend');
            NumAddHubs = ceil(this.NumMetabolites/(100/this.options.HubThreshold));
            CandidateHub = false(this.NumMetabolites,1);
            CandidateHub(I(1:NumAddHubs)) = true;
            CandidateHub = find(CandidateHub);
            
            Present = (1:NumAddHubs)';
            CandHub = CandidateHub(Present);
            metID = MetList(CandHub);
            metName  = MetNames(CandHub);
            NumbRxns = RxnsPerMet(CandHub);
            [~,order]= sort(NumbRxns,'descend');
            metID = metID(order);
            metName = metName(order);
            NumbRxns = NumbRxns(order);
            
            while true
                %Stop if no remaining proposals
                if isempty(Present)
                    break
                end
                if any(CandidateHub)
                    clc
                    disp([num2cell(Present),metID(Present),metName(Present),num2cell(NumbRxns(Present))])
                    fprintf('The %d percent most connected metabolites are currently considered for the hub\n\n',this.options.HubThreshold)
                    disp(['The list above shows candidate hub metabolites.' char(10) ...
                        'Please choose how to continue:' char(10) ...
                        'R: Reject one or more of the metabolites as hub metabolites' char(10) ...
                        'A: Adjust the threshold for when to consider a metabolites as hub metabolite' char(10) ...
                        'C: Continue (Accept all remaining proposals)' char(10) ...
                        'Number / ID / Name / Number of connections' char(10)])
                    reply = COMMGEN.InputProcessing({'R';'C';'A';'H'});
                    
                    switch(reply)
                        case('R')
                            I = input('Please enter the indices of pairs which are to be rejected\nFor example: 1 or [1 2 3]\nAlternatively, enter 0 to cancel\n');
                            if ~(numel(I)==1 && I==0) && ~isempty(Present)
                                Present = setdiff(Present,I);
                            end
                            clc
                        case('C')
                            break;
                        case('A')
                            disp('Choose the new threshold (between 0 and 100 %)')
                            reply = COMMGEN.InputProcessing('realnumber');
                            if reply<0
                                reply = 0;
                            elseif reply>100
                                reply = 100;
                            end
                            this.options.HubThreshold = reply;
                            this.SetHubMets;
                            return
                    end
                else
                    fprintf('No additional hub metabolites found based on the threshold of %s reactions per met\n',this.options.HubThreshold)
                    disp(['Choose how to continue:' char(10) ...
                        'C: Continue' char(10) ...
                        'A: Adjust threshold' char(10)])
                    COMMGEN.InputProcessing({'C';'A'});
                    switch(reply)
                        case('C')
                            break;
                        case('A')
                            disp('Choose the new threshold (between 0 and 100 %)')
                            reply = COMMGEN.InputProcessing('realnumber');
                            reply = str2double(reply);
                            if reply<0
                                reply = 0;
                            elseif reply>100
                                reply = 100;
                            end
                            this.options.HubThreshold = reply;
                            this.SetHubMets;
                            return
                    end
                end
            end
            HubMets(ismember(this.metabolites,metID)) = true;
            this.options.HubMets = this.metabolites(HubMets);
        end
        function this = EnterNewReaction(this,Rxn)
            % Add a new reaction manually, this may be based on an already
            % existing reaction.
            if exist('Rxn','var')
                disp('The original reaction:')
                Rxn.printfields
                NewRxn = MergedReaction('',Rxn.Origin);
                NewRxn.source = [Rxn.source ';' Rxn.id];
            else
                NewRxn = MergedReaction('',false(this.NumModels));
            end
            
            while true
                reply = input('Choose an identifier for the new reaction\n','s');
                if any(strcmp(reply,[this.reaction_IDs ; this.TrackReactions(:,1)]))
                    disp('The chosen identifier corresponds to a different reaction, please choose a new one')
                else
                    NewRxn.id = reply;
                    break
                end
            end
            
            if exist('Rxn','var')
                if Rxn.biomass
                    disp('Is the new reaction a biomass reaction? [[y/n]')
                    reply = COMMGEN.InputProcessing({'Y','N'});
                    if strcmp('Y',reply)
                        NewRxn.biomass = 1;
                    end
                end
            end
            
            disp('Is the new reaction a boundary reaction? [Y/N]')
            reply = COMMGEN.InputProcessing({'Y','N'});
            if strcmp(reply,'Y')
                NewRxn.external = true;
            else
                NewRxn.external = false;
            end
            
            % If available these fields will be automatically set upon re-upload to
            % MetaNetX
            NewRxn.alternatives = '';
            NewRxn.ec_number = '';
            NewRxn.operator = '';
            NewRxn.name = '';
            NewRxn.references = '';
            NewRxn.compartment = '';
            
            % Entering a new reaction equation
            equation = EnterEquation(this,'');
            
            NewRxn.equation = equation;
            NewRxn          = NewRxn.getReactants;
            
            [NewRxn.lb,NewRxn.ub] = SetLbUb(this,nan,nan,NewRxn.equation);
            NewRxn.proteins = this.MakeGeneRules;
            
            if exist('Rxn','var')
                disp(['Do you want to add a comment to the new reaction?'  char(10) ...
                    'Y: Add new comment'                                   char(10) ...
                    'N: No comment at all'                                 char(10) ...
                    'K: Keep comments from original reaction'              char(10) ...
                    'B: Keep comments from original and add new'           char(10) ...
                    ]);
                reply = COMMGEN.InputProcessing({'Y','N','K','B'});
            else
                disp(['Do you want to add a comment to the new reaction?'  char(10) ...
                    'Y: Add new comment'                                   char(10) ...
                    'N: No comment at all'                                 char(10) ...
                    ]);
                reply = COMMGEN.InputProcessing({'Y','N'});
            end
            switch reply
                case('Y')
                    comment = input('Enter comment','s');
                    NewRxn.comment = comment;
                case('N')
                    comment = '';
                    NewRxn.comment = comment;
            end
            NewRxn.comment  = [NewRxn.comment '%%%orig:%' num2str(Rxn.Origin) '%'];
            
            this.addReaction(NewRxn);
            this = TrackRxn(this,'NewRxn','',NewRxn);
        end
        function this = addNewSpecies(this,NewSpecies)
            % Add a new species to the model. The species ID is passed via
            % NewSpecies.
            
            % Obtain information
            id = NewSpecies{1};
            while true
                reply = input(['Choose an ID for the new species ' id '\n'],'s');
                if any(strcmp(reply,this.SpeciesIDToName(:,2)))
                    disp('The chosen name corresponds to a different species, please choose a different one')
                else
                    name = reply;
                    break
                end
            end
            source  = '';
            formula = input('Enter the corresponding chemical formula\n','s');
            mass    = input('Enter the corresponding molecular mass\n');
            charge  = input('Enter the charge of the molecule\n');
            ref     = input('Enter any references\n','s');
            
            this.species{end+1} = MnXChemicalSpecies(id,name,source,formula,mass,charge,ref);
            this.species_IDs(end+1) = {id};
            this.SpeciesIDToName(end+1,:) = {id,name};
            this.TrackSpec('New',this.species{end},'','','Added by user');
        end
        function this = UpdateAll(this)
            % This function processes all changes and updates the required
            % fields.
            RxnIDx = this.ToBeRemovedRxns;
            
            this.RemovedRxns(end+1:end+count(RxnIDx)) = this.reactions(RxnIDx);
            this.reactions(RxnIDx) = [];
            
            % Check Metabolites
            this.addMetabolites;
            % Recreate stoichiometric matrix
            this.addStoichMatrix;
            % Recreate RxnGeneMat
            this.addRxnGeneMat;
            % Restore this.ToBeRemovedRxns to empty state
            this.ToBeRemovedRxns = false(this.NumReactions,1);
            % Track Changes in metabolites and genes
            MetToSpec = this.MetToSpecies;
            MetCompartment = regexprep(this.MetCompartment,strcat('^',this.CompartmentIDToName(:,1),'$'),this.CompartmentIDToName(:,2));
            for i=1:this.NumMetabolites
                this.MetLastSeen(MetToSpec(i)+1,strcmp(MetCompartment(i),this.MetLastSeen(1,:))) = {this.Phase};
                if this.MetFirstSeen{MetToSpec(i)+1,strcmp(MetCompartment(i),this.MetFirstSeen(1,:))} == -1
                    this.MetFirstSeen(MetToSpec(i)+1,strcmp(MetCompartment(i),this.MetFirstSeen(1,:))) = {this.Phase};
                end
            end
            % Save choices such that they can be remade automatically
            % if CMMG is restarted.
            
            % Save Summary statistics
            this.SummaryStatistics;
            % Track dead/inactive reactions/metabolites
            [dR,dM,iR,iM] = this.findDeadEnds;
            this.TrackDead(end+1,:) = num2cell([count(dR) count(iR) count(dM) count(iM)]);
            % Increase Phase by 1
            this.Phase = this.Phase+1;
        end
        
        % Merging
        %reactions
        function Cancel = MergeReactions(this, group, options)
            %%% INPUT
            %   group   Logical vector containing true on the positions
            %           corresponding to the reactions to be merged.
            %%% OPTIONAL INPUT
            %   options If some choices are already made when this function is called
            %           they can be entered here in two formats:
            %           CELL:   {'NEWID','RXNFORM','GPR','comment','lb,'ub'}
            %           Double: Indices of the reaction from which to take the id,
            %                   equation and/or GPR. [2 3 1] would thus mean to take
            %                   the id from reaction 2, the equation from reaction 3
            %                   and the GPR from reaction 1. Enter 0 at a
            %                   position if that choice is not yet made.
            %               NOTE: The id will be appended with _upd as IDs
            %               are not to be re-used.
            
            if ~islogical(group)
                RxnIDx = group;
                group = false(1,this.NumReactions);
                group(RxnIDx) = true;
            else
                RxnIDx = find(group);
            end
            
            % Initialize
            InitTotNumRxns = this.NumReactions;
            NumRxns = numel(RxnIDx);
            if NumRxns < 2
                disp('Need at least two reactions in order to merge')
                return
            end
            Cancel = false;
            
            % Check in history if and how merge was processed before
            GroupName = COMMGEN.GetGroupName(this.reactions(group));
            
            % check if options have been provided
            if ~exist('options','var')
                options = zeros(3,1);
            elseif ~((isnumeric(options) && max(options)<=NumRxns) || (iscell(options) && numel(options)>=3))
                disp('Input format for ''options'' invalid. Choices may have to be made manually.')
                options = zeros(3,1);
            end
            
            Origin = false(NumRxns,numel(this.Models));
            comment   = ''; %For new comments
            oldcomment= ''; %To keep track of comments of "parent" reactions
            source = '';
            ec_number = '';
            
            for i=1:numel(RxnIDx)
                Rxn = this.reactions{RxnIDx(i)};
                id{i}           = Rxn.id; %#ok<AGROW>
                source          = [source ';' Rxn.source]; %#ok<AGROW>
                if ~isempty(this.reactions{i}.ec_number)
                    ec_number   = [ec_number ';' Rxn.ec_number]; %#ok<AGROW>
                end
                lb(i)           = Rxn.lb; %#ok<AGROW>
                ub(i)           = Rxn.ub; %#ok<AGROW>
                biomass(i)      = Rxn.biomass; %#ok<AGROW>
                if ~isempty(Rxn.comment)
                    oldcomment         = [oldcomment '(parent' Rxn.id ': ' Rxn.comment ');']; %#ok<AGROW>
                end
                Origin(i,Rxn.Origin) = true;
            end
            
            CombOrigin = any(Origin,1);
            NewRxn = MergedReaction('',CombOrigin);
            % id;
            if iscell(options)
                NewRxn.id = options{1};
            elseif options(1)>0
                NewRxn.id = [id{options(1)} '_upd'];
            else
                UniqID = unique(id);
                if numel(UniqID)==1
                    NewRxn.id = UniqID{1};
                else
                    NewRxn.id = regexprep(GroupName,';','___');
                end
            end
            % equation
            if ~Cancel
                NewRxn.external = any(this.is_external(group));
                if iscell(options)
                    NewRxn.equation = options{2};
                    NewRxn = getReactants(NewRxn);
                elseif options(2)>0
                    NewRxn.equation = this.reactions{RxnIDx(options(2))}.equation;
                    NewRxn = getReactants(NewRxn);
                else
                    [NewRxn.equation,newcomment,Cancel] = MergeEquation(this,group);
                    if ~Cancel
                        if ~isempty(newcomment)
                            if isempty(comment)
                                comment = newcomment;
                            else
                                comment = [comment ';' newcomment];
                            end
                        end
                        NewRxn = getReactants(NewRxn);
                    end
                end
            end
            % source
            if ~Cancel
                source(1)=[]; % Remove initial ';'
                source = srgetargs(strtrim(source),';');
                source = unique(source);
                for i = 1:length(source)
                    NewRxn.source = [NewRxn.source ';' source{i}];
                end
                NewRxn.source(1) = []; % Remove initial ';'
            end
            % ec_number(s)
            if ~Cancel
                if isempty(ec_number)
                    NewRxn.ec_number = '';
                else
                    ec_number = srgetargs(strtrim(ec_number),';');
                    ec_number = unique(ec_number);
                    NewRxn.ec_number = ec_number{1};
                    for i = 2:length(ec_number)
                        NewRxn.ec_number = [NewRxn.ec_number ';' ec_number{i}];
                    end
                end
            end
            % genes
            if iscell(options)
                NewRxn.proteins = options{3};
            elseif options(3)>0
                NewRxn.proteins = this.reactions{RxnIDx(options(3))}.proteins;
            else
                if ~Cancel
                    [CNF,newcomment,Cancel] = MergeGPR(this,group);
                    NewRxn.proteins = CNF;
                    if ~isempty(newcomment)
                        if isempty(comment)
                            comment = newcomment;
                        else
                            comment = [comment ';' newcomment];
                        end
                    end
                end
            end
            % biomass & external
            if ~Cancel
                %biomass
                NewRxn.biomass = max(biomass);
                %external
                if any(~cellfun(@isempty,regexp(NewRxn.equation,{'^[<>-]|[-<>]$'})))
                    NewRxn.external = 1;
                else
                    NewRxn.external = 0;
                end
            end
            % lb / ub
            if ~Cancel
                if iscell(options) && numel(options)==6
                    NewRxn.lb = options{5};
                    NewRxn.ub = options{6};
                elseif  ~iscell(options) && options(2)>0
                    NewRxn.lb = lb(options(2));
                    NewRxn.ub = ub(options(2));
                elseif NewRxn.external
                    NewRxn.lb = 0;
                    NewRxn.ub = 0;
                else
                    [NewRxn.lb,NewRxn.ub,newcomment] = SetLbUb(this,lb,ub,NewRxn.equation);
                    if ~isempty(newcomment)
                        if isempty(comment)
                            comment = newcomment;
                        else
                            comment = [comment ';' newcomment];
                        end
                    end
                end
            end
            if nargin == 3 && numel(options)>4
                comment = options{4};
            end
            % Create new reaction & Prepare removal of original reactions
            if ~Cancel
                % Final reaction updates
                
                %  This is performed here and not when they are initiated
                %  in order to prevent them from being added if generation
                %  of the reaction is cancelled.
                
                if ~isempty(oldcomment)
                    NewRxn.comment = [comment ';' oldcomment];
                else
                    NewRxn.comment = comment;
                end
                NewRxn.SrcRxns  = this.reactions(group);
                
                this.addReaction(NewRxn);
                this = this.MarkForRemoval(group);
                
                % Track changes
                this.TrackRxn('Merged',GroupName,NewRxn);
                
            else % Track rejected merges
                % Track rejection. First check whether really no merge
                % occurred. It is possible that initially three reactions
                % were proposed to merge and only 2 were merged. In that
                % case the rejection does not need to be remembered.
                if this.NumReactions ~= InitTotNumRxns
                    return
                else % Track
                    this.RejectMerge(group);
                end
            end
        end
        function [NewLb,NewUb,flag] = SetLbUb(this,lb,ub,equation)
            % Sets the lower and upper bounds of the reaction based on the
            % type of arrow in the reaction equation. In case it is unclear
            % or if the original reactions had lower bounds larger than
            % zero or upper bounds smaller than 0 the user gets prompted.
            arrow = regexp(equation,'[<\->=?]*','match','once');
            flag = '';
            while true
                % lb==ub, if the reaction was practically disabled in one of the original models, the bounds can not be automatically inferred from the arrow
                if this.options.AllAuto
                    [NewLb,NewUb] = this.EqToBounds(arrow);
                elseif any(lb==ub)
                    fprintf('For %d of the reactions the original lower and upper bounds are identical.\n',sum(lb==ub))
                    disp(equation)
                    disp(['The lower and upper bounds are:' char(10)])
                    disp(dataset(lb',ub','VarNames',{'Lower_Bound','Upper_Bound'}))
                    reply = input('Please select the lower bound for the reaction.\nEnter * to enter a comment\n','s');
                    while strcmpi('*',reply)
                        comment = input('Please enter your comment\n','s');
                        flag = [flag ';Lb:' comment]; %#ok<AGROW>
                        reply = input('Please select the lower bound for the reaction.\nEnter * to enter a comment\n','s');
                    end
                    NewLb = str2double(reply);
                    reply = input('Please select the upper bound for the reaction.\nEnter * to enter a comment\n','s');
                    while strcmpi('*',reply)
                        comment = input('Please enter your comment\n','s');
                        flag = [flag ';Ub:' comment]; %#ok<AGROW>
                        reply = input('Please select the upper bound for the reaction.\nEnter * to enter a comment\n','s');
                    end
                    NewUb = str2double(reply);
                    % If the reaction in the original models carried a minimal flux (in either direction), the bounds cannot be automatically inferred.
                elseif any(lb>0) || any(ub<0)
                    disp(['An upper bound below zero or lower bound above zero has been detected.' char(10) ...
                        'The lower and upper bounds have to be set manually.' char(10) ...
                        'The original lower and upper bounds for the to-be merged reactions:'])
                    disp(dataset(lb,ub))
                    disp(['Choose a lower bound for this reaction.' char(10) 'Enter * to enter a comment' char(10)]);
                    reply = COMMGEN.InputProcessing('realnumber');
                    while strcmp('*',reply)
                        comment = input('Please enter your comment');
                        flag = [flag ';Lb:' comment]; %#ok<AGROW>
                        disp(['Choose a lower bound for this reaction.' char(10) 'Enter * to enter a comment' char(10)]);
                        reply = COMMGEN.InputProcessing('realnumber');
                    end
                    NewLb = reply;
                    disp(['Choose an upper bound for this reaction.' char(10) 'Enter * to enter a comment' char(10)]);
                    reply = COMMGEN.InputProcessing('realnumber');
                    while strcmp('*',reply)
                        comment = input('Please enter your comment');
                        flag = [flag ';Ub:' comment]; %#ok<AGROW>
                        disp(['Choose an upper bound for this reaction.' char(10) 'Enter * to enter a comment' char(10)]);
                        reply = COMMGEN.InputProcessing('realnumber');
                    end
                    NewUb = reply;
                else
                    % Choose default values based on the arrow in the
                    % equation
                    [NewLb,NewUb] = this.EqToBounds(arrow);
                end
                if NewUb>=NewLb
                    return;
                else
                    disp(['The provided lower bound is larged than the upper bound or non-numeric values have been provided, please enter new values.' char(10)])
                end
            end
        end
        function [CNF,flag,Cancel] = MergeGPR(this,group)
            flag = '';
            Cancel = 0;
            RxnIDx = find(group);
            
            proteins = cell(numel(RxnIDx),1);
            for i=1:numel(RxnIDx)
                proteins{i}     = this.reactions{RxnIDx(i)}.proteins;
            end
            
            if this.options.ExactlyIdentical && numel(unique(proteins))>1
                CNF = '';
                Cancel = 1;
            end
            
            proteins(cellfun(@isempty,proteins)) = [];
            if isempty(proteins)
                CNF = '';
                return
            else
                for i=1:numel(proteins)
                    CNF{i} = COMMGEN.StandardGPR(proteins{i});
                end
            end
            
            CNF = unique(CNF);
            if length(CNF)>1
                if this.options.GeneOR ~=1 && this.options.AllAuto
                    Cancel = 1;
                    return
                end
                
                if this.options.GeneOR ~=1
                    clc
                    disp(['The reactions differ in the gene rules' char(10)])
                    for i=1:length(CNF)
                        fprintf('Rule %d: %s\n',i,CNF{i})
                    end
                    disp('Additional reaction information')
                    this.PrintEquations(RxnIDx);
                    
                else
                    reply = 'O';
                end
                while true
                    if ~exist('reply','var') || ~strcmp(reply,'O')
                        disp([char(10) 'How do you want to proceed?' char(10)                               ...
                            'R: Keep one of the two gene rules' char(10)                                    ...
                            'C: Cancel merge' char(10)                                                      ...
                            'A: Enter alternative gene rule' char(10)                                       ...
                            'O: Place an OR clause between the two separate rules' char(10)                 ...
                            'M: Modify the name of a gene' char(10)                                         ...
                            '*: Enter comment for later inspection' char(10)                                ...
                            '[R/C/A/O/M/*]']);
                        reply = COMMGEN.InputProcessing({'R','C','A','O','M','*'});
                    end
                    if strcmpi('*',reply)
                        comment = input('Enter comment','s');
                        if isempty(flag)
                            flag = ['Protein:' comment];
                        else
                            flag = [flag ';Protein' comment]; %#ok<AGROW>
                        end
                        clc, disp('Comment added, return to question')
                    elseif strcmpi('R',reply)
                        AllInputs = ['C' cellstr(num2str((1:numel(CNF))'))'];
                        disp([char(10) 'Choose number corresponding to gene rule to keep' char(10)       ...
                            'Or enter C to change previous input' char(10)]);
                        CNFChoice = COMMGEN.InputProcessing(AllInputs);
                        if ~strcmpi('C',CNFChoice)
                            CNFChoice = uint8(str2double(CNFChoice));
                            break
                        end
                    else
                        break
                    end
                end
                switch reply
                    case('R')
                        CNF = CNF{CNFChoice};
                    case('C')
                        disp('Merge cancelled')
                        CNF = '';
                        Cancel = 1;
                    case('A')
                        clc, disp(['Original gene rules' char(10)])
                        for i=1:length(CNF)
                            fprintf('Rule %d: %s\n',i,CNF{i})
                        end
                        CNF = this.MakeGeneRules;
                    case('O')
                        rule = '';
                        for i=1:length(CNF)
                            rule = [rule CNF{i}]; %#ok<AGROW>
                            if i<length(CNF)
                                rule = [rule ' ; ']; %#ok<AGROW>
                            end
                        end
                        CNF = COMMGEN.StandardGPR(rule);
                        if this.options.GeneORStrict ==0
                            disp(['Would you like to employ a strict or flexible OR merging for GPR rules?'             char(10) ...
                                'Example: ''(A AND B) OR (A AND C) OR A'''                                              char(10) ...
                                'Y: Strict: ''(A AND B) OR (A AND C)'''                                                 char(10) ...
                                'N: Flexible: ''(A AND B) OR (A AND C) OR A''; effectively the rule resorts to ''A'''   char(10) ...
                                '[Y/N]'])
                            reply = COMMGEN.InputProcessing({'Y','N'});
                            if strcmpi(reply,'Y')
                                this.options.GeneORStrict=1;
                            else
                                this.options.GeneORStrict=-1;
                            end
                        end
                        if this.options.GeneORStrict==1
                            CNF = this.StrictRules(CNF);
                        end
                        if isempty(flag)
                            flag = 'Protein:ORmerge';
                        else
                            flag = [flag ';Protein:ORmerge'];
                        end
                    case('M') %Change the name of a gene
                        OrigName = input('Of which gene do you want to change the name?','s');
                        TargetName = input('What should it be changed to?','s');
                        this.ChangeGeneName(OrigName,TargetName);
                        [CNF,flag,Cancel] = MergeGPR(this,group);
                end
            end
            if ~ischar(CNF)
                CNF = char(CNF);
            end
        end
        function rule = StrictRules(this,rule)
            %This function creates a more strict version of the provided
            %rule, assuming that the original modellers had good reason to
            %assume an AND relation over an OR relation.
            if ~exist('rule','var')
                for i=1:this.NumReactions
                    this.reactions{i}.proteins = this.StrictRules(this.reactions{i}.proteins);
                end
            else
                Complexes    = regexp(rule,' ; ','split');
                Genes        = unique(regexp(rule,'[ ;+]*','split'));
                NumGenes     = length(Genes);
                NumComplex   = length(Complexes);
                GenesComplex = zeros(NumGenes,NumComplex);
                for i=1:length(Genes)
                    GenesComplex(i,:) = ~cellfun(@isempty,regexp(Complexes,Genes{i}));
                end
                KeepComplex  = true(NumComplex,1);
                for i=1:NumComplex-1
                    for j=i+1:NumComplex
                        if all(GenesComplex(:,i)>=GenesComplex(:,j))
                            KeepComplex(j) = true;
                        elseif all(GenesComplex(:,j)>=GenesComplex(:,j))
                            KeepComplex(i) = true;
                        end
                    end
                end
                Complexes = Complexes(KeepComplex);
                rule = [sprintf('%s;',Complexes{1:end-1}), Complexes{end}];
            end
            
        end
        %species
        function MergeTwoSpecies(this,group,CollapseTo)
            % this function merges the species presented in group
            
            if islogical(group)
                speciesIDx = find(group);
            else
                speciesIDx = group;
            end
            
            % Find species to merge towards
            if nargin<3
                disp([char(10) 'Merging two species.' char(10) ...
                    'Select the species which will be kept as is.' char(10) ...
                    'The other species will be made identical' char(10) ...
                    'Or enter C to cancel merge']);
                this.PrintSpeciesInformation(speciesIDx,true)
                reply = COMMGEN.InputProcessing(['C';this.species_IDs(speciesIDx);this.IDToName(this.species_IDs(speciesIDx))]);
                if strcmp(reply,'C')
                    return
                end
                CollapseTo = find(strcmp(this.NameToID(reply),this.species_IDs));
            else
                assert(ismember(CollapseTo,speciesIDx),'The species to be merged towards is not present in the species list');
            end
            
            % Obtaining required info
            %Find IDs of species to be merged
            FormerSpecID = setdiff(speciesIDx,CollapseTo);
            
            % Find IDx of corresponding metabolites
            MetIDx = find(ismember(this.MetToSpecies,FormerSpecID));
            
            % Adjust the reactions - Alter Stoichiometric matrix, gene reaction matrix and equation
            RxnIDx = any(this.Stoich(MetIDx,:),1);
            RxnIDx = find(RxnIDx);
            for i=1:numel(RxnIDx);
                this.ChangeSpecInRxn(RxnIDx(i),FormerSpecID,CollapseTo);
            end
            
            % Track the merged species
            this.TrackSpec('Merged',this.species{CollapseTo},this.species_IDs{FormerSpecID},char(this.IDToName(this.species_IDs(FormerSpecID)))); %allow commenting
            
            % Remove no longer existing species from the hub metabolites
            this.options.HubMets(ismember(this.options.HubMets,this.metabolites(MetIDx))) = [];
            
        end
        function SpecMultToOne(this,SpecIDx,One)
            % This function converts all reactions for the species in
            % SpecIDx to reactions for the species in One
            if islogical(SpecIDx)
                SpecIDx = find(SpecIDx);
            end
            if islogical(One)
                One = find(One);
            end
            Rest = setdiff(SpecIDx,One);
            
            for i=1:numel(Rest)
                this.MergeTwoSpecies([Rest(i) One],One);
            end
        end
        function SpecOneToMult(this,SpecIDx,One)
            % This function divides the reactions corresponding to one
            % species over two or more other species. e.g., D-glucose
            % towards alpha-D-glucose & beta-D-glucose
            if islogical(SpecIDx)
                SpecIDx = find(SpecIDx);
            end
            if islogical(One)
                One = find(One);
            end
            Rest = setdiff(SpecIDx,One);
            
            % Assigning all reactions to all subgroup species
            fprintf('Should all subgroup-species receive all reactions from %s?\nAlternatively reactions will be processed on a by-reaction basis[Y/N]\n',this.SpeciesIDToName{One,2})
            reply = COMMGEN.InputProcessing({'Y','N'});
            if strcmp(reply,'Y')
                for i=1:numel(Rest)
                    group = false(1,this.NumSpecies);
                    group([One Rest(i)]) = true;
                    this.MergeTwoSpecies(group,Rest(i));
                end
            end
            
            % Finding reactions corresponding to species
            MetIDx = this.MetToSpecies==One;
            RxnIDx = any(this.Stoich(MetIDx,:),1);
            RxnIDx = find(RxnIDx);
            
            % Processing each reaction
            for i=1:numel(RxnIDx)
                while true
                    fprintf('Please enter the indices of the species to which the reaction should be assigned.\nFormat: numerical values.\nFor example: [1 2 3], 1:3, Or 4.\nEnter 0 if the reaction should just be removed.\nOr enter -1 to change the reaction instead.\n')
                    this.PrintEquations(RxnIDx(i));
                    dataset(this.SpeciesIDToName(Rest,1),this.SpeciesIDToName(Rest,2),'VarNames',{'ID','Name'})
                    reply = input('');
                    if isnumeric(reply) && all(reply>0)
                        for j=1:numel(reply)
                            SpecIDx = Rest(reply(j));
                            this.ChangeSpecInRxn(RxnIDx(i),One,SpecIDx);
                        end
                        break
                    elseif isnumeric(reply) && numel(reply)==1 && reply==0
                        this.MarkForRemoval(RxnIDx(i));
                        break
                    elseif isnumeric(reply) && numel(reply)==1 && reply==-1
                        this.adjustReaction(RxnIDx(i));
                        this.reactions{RxnIDx(i)} = this.reactions{RxnIDx(i)}.getReactants;
                        break
                    else
                        fprintf('Input not recognised. please try again.')
                    end
                end
            end
        end
        function ChangeSpecInRxn(this,RxnIDx,oldSpec,NewSpec)
            % Replace oldSpec by Newspec in the reaction included in RxnIDx
            % - Alter Stoichiometric matrix, gene reaction matrix equation
            % & id
            
            % Create new reaction and delete the old reaction.
            NewRxn = this.reactions{RxnIDx};
            NewRxn.SrcRxns = {NewRxn};
            this.MarkForRemoval(RxnIDx);
            
            % Alter the equation and reload the reactants
            NewRxn.equation = regexprep(NewRxn.equation,[' ' this.species_IDs{oldSpec} '@'],[' ' this.species_IDs{NewSpec} '@']);
            NewRxn.equation = regexprep(NewRxn.equation,['^' this.species_IDs{oldSpec} '@'],['^' this.species_IDs{NewSpec} '@']);
            NewRxn.equation = this.SortEquation(NewRxn.equation);
            NewRxn = NewRxn.getReactants;
            
            % Adjust the reaction ID
            NewRxn.id = [NewRxn.id '_Spec' this.species_IDs{oldSpec} '_ChangedTo_' this.species_IDs{NewSpec}];
            
            % Add the new reaction to the model - if the reaction still
            % involves any metabolites
            if ~isempty(NewRxn.reactants)
                this.TrackRxn('SpecModified',this.reactions{RxnIDx}.id,NewRxn);
                this.addReaction(NewRxn);
            end
        end
        
        % Main functions
        %reaction-related
        function DuplicateRxns(this)
            %%% PURPOSE
            % Identifies duplicate reactions in the model structure.Here, duplicate
            % reactions are defined only with respect to their stoichiometries with
            % respect to substrates and products. Differences in reaction bounds,
            % directionalities, and gene rules are considered as discrepancies between
            % the reactions and are to be resolved.
            %%% EXAMPLE CALL
            %   DuplicateRxns(Combined_model;
            %%% INPUT
            %   this: The model structure
            %%% OPTIONAL INPUT
            %%% OUTPUT
            %   this: An updated model structure
            
            % Keep track of which function is being used to condense the
            % model
            this.ChangeFunction('Duplicate reactions');
            
            % Initial number of reactions
            NumRxns = this.NumReactions;
            
            % Finding identical reactions
            groups = this.findDuplicateRxns;
            
            % Trimming the groups by removing permanently rejected combinations
            groups = this.FilterGroups(groups);
            
            % Confirm that there are still reactions to be merged
            NumGroups = size(groups,1);
            if NumGroups==0
                return
            end
            % Attempt to Merge the remainder
            
            fprintf('%d groups of "duplicate" reactions found.\n',NumGroups)
            for i=1:NumGroups
                clc, fprintf('Merging group %d out of the %d groups of duplicate reactions\n\n',i,NumGroups)
                this.MergeReactions(groups(i,:));
            end
            
            % Update consensus model structure
            this.UpdateAll;
            if NumRxns ~=this.NumReactions
                clc,fprintf('By merging duplicate reactions, the total number of reactions was changed from %d to %d\n',NumRxns,this.NumReactions)
            end
        end
        function SameMetsDiffStoich(this)
            %%% PURPOSE
            % Identifies reactions in the model structure that only differ with respect
            % to the stoichiometries of their substrates and products. Differences in
            % reaction bounds, directionalities, and gene rules are considered as
            % discrepancies between the reactions and are to be resolved.
            %%% EXAMPLE CALL
            %   SameMetsDiffStoich(Combined_model);
            %%% INPUT
            %   this: The model structure
            %%% OPTIONAL INPUT
            %%% OUTPUT
            %   this: An updated model structure
            
            % Get original number of reactions
            NumRxns = this.NumReactions;
            
            % Keep track of which function is being used to condense the
            % model
            this.ChangeFunction('SameMetsDiffStoich');
            
            % Obtaining the groups
            S = logical(this.Stoich);
            S = this.RemIgnored(S);
            groups = this.findDuplicateRxns(S);
            
            % Remove earlier proposed and permanently rejected merges
            groups = this.FilterGroups(groups);
            
            NumGroups = size(groups,1);
            if NumGroups==0
                %this.Done(2) =1;
                return
            end
            
            fprintf('%d groups of reactions with identical species but different stoichiometry found.\n',NumGroups)
            for i=1:size(groups,1)
                clc, fprintf('Merging group %d out of the %d groups with identical participants\n',i,NumGroups)
                this.MergeReactions(groups(i,:));
            end
            
            % Update consensus model structure
            this = this.UpdateAll;
            if NumRxns ~=this.NumReactions
                clc,fprintf('By merging reactions with the same metabolites but different stoichiometry, the total number of reactions was changed from %d to %d\n',NumRxns,this.NumReactions)
            end
        end
        function NestedReactions(this)
            %%% PURPOSE
            % Identifies pairs of reactions in the model structure where one reaction
            % is a perfect subset of the other with respect to involved substrates and
            % products. This mainly occurs due to different models not modeling all
            % processes in the same level of detail. For example, some models omit the
            % inclusion of protons and water altogether. Differences in reaction
            % bounds, directionalities, and gene rules are considered as discrepancies
            % between the reactions and are to be resolved.
            %%% EXAMPLE CALL
            %   NestedReactions(Combined_model);
            %%% INPUT
            %   this: The model structure
            %%% OPTIONAL INPUT
            %%% OUTPUT
            %   this: An updated model structure
            
            % Keep track of which function is being used to condense the
            % model
            this.ChangeFunction('Nested reactions');
            
            % Initial number of reactions
            NumRxns = this.NumReactions;
            
            % Finding nested reactions
            groups = this.findNestedReactions;
            
            %Check if any reactions occur very often, these are not of
            %interest.
            if any(sum(groups,1)>10)
                FrqRxns = sum(groups,1)>10;
                CorrGroups = any(groups(:,FrqRxns),2);
                groups(CorrGroups,:) = [];
            end
            
            % Trimming the groups by removing permanently rejected combinations
            groups = this.FilterGroups(groups);
            
            % Confirm that there are still reactions to be merged
            NumGroups = size(groups,1);
            if NumGroups==0
                return
            end
            
            % Attempt to Merge the remainder
            fprintf('%d groups of nested reactions found.\n',NumGroups)
            for i=1:NumGroups
                clc, fprintf('Processing group %d out of the %d groups of nested reactions',i,NumGroups)
                this.MergeReactions(groups(i,:));
            end
            
            % Update consensus model structure
            this.UpdateAll;
            if NumRxns ~=this.NumReactions
                clc,fprintf('By processing nested reactions, the total number of reactions was changed from %d to %d\n',NumRxns,this.NumReactions)
            end
        end
        function AltRedoxPairs(this)
            %%% PURPOSE
            % Identifies reactions in the model structure that only differ with respect
            % to the included redox metabolites. As assigning the right redox pair to a
            % reaction remains challenging, generally many inconsistencies herein are
            % identified between models. Differences in reaction bounds,
            % directionalities, and gene rules are considered as discrepancies
            % between the reactions and are to be resolved.
            %%% EXAMPLE CALL
            %   AltRedoxPairs(Combined_model)
            %%% INPUT
            %   this: The model structure
            %%% OPTIONAL INPUT
            %%% OUTPUT
            %   this: An updated model structure
            
            % Update Redox pairs
            this.updateRedoxPairs;
            
            % Keep track of which function is being used to condense the
            % model
            this.ChangeFunction('Alt. redox pairs');
            
            % Initial number of reactions
            NumRxns = this.NumReactions;
            
            % Finding reaction pairs
            groups = this.findAltDonorAcceptorPairs;
            
            % Trimming the groups by removing permanently rejected combinations
            groups = this.FilterGroups(groups);
            
            % Confirm that there are still reactions to be merged
            NumGroups = size(groups,1);
            if NumGroups==0
                return
            end
            % Present the remainder
            
            fprintf('%d groups of reactions only differing in redox pairs found.\n',NumGroups)
            for i=1:NumGroups
                clc, fprintf('Merging group %d out of the %d groups only differing in redox pairs\n',i,NumGroups)
                this.MergeReactions(groups(i,:));
            end
            
            % Update consensus model structure
            this.UpdateAll;
            if NumRxns ~=this.NumReactions
                clc,fprintf('By processing similar reactions involving alternative redox pairs, the total number of reactions was changed from %d to %d\n',NumRxns,this.NumReactions)
            end
        end
        function SimilarReactions(this,N1,N2,NG)
            %%% PURPOSE
            % Identifies reactions in the model structure that are not identical or
            % nested but are still highly similar. This identified cases where
            % biological processes are modelled differently between models, for example
            % due to differences in the included metabolites:
            % model1: ATP + H + H20 + A --> B + PPPi
            % model2: ATP + H + H20 + A --> B + PPi + Pi
            % where model2 does not contain the compound PPPi.
            % Differences in reaction bounds, directionalities, and gene rules are
            % considered as discrepancies between the reactions and are to be resolved.
            %%% EXAMPLE CALL
            %   SimilarReactions(Combined_model,1,1,0);
            %%% INPUT
            %   this: The model structure
            %%% OPTIONAL INPUT
            %   N1: Minimal number of shared compounds at one side of the equation.
            %   Default value: 1
            %   N2: Minimal number of shared compounds at the other side of the equation.
            %   Default value: 1
            %   NG:  Minimal number of shared genes.
            %   Default value: 1
            %%% OUTPUT
            %   this: An updated model structure
            
            % Input processing
            if nargin<2
                N1=1;
            end
            if nargin<3
                N2=1;
            end
            if nargin<4
                NG=1;
            end
            
            % Altering Hub Metabolites
            fprintf([...
                'This function attempts to find reactions which share\n'...
                'At least %d non-hub compound(s) at one side of the equation\n'...
                'At least %d non-hub compound(s) at the other side of the equation\n'...
                'No model origin\n'...
                'At least %d gene(s)' char(10) char(10)...
                'Do you want to inspect the set Hub Metabolites before running this function?' char(10)...
                '[Y/N]'],N1,N2,NG)
            reply = COMMGEN.InputProcessing({'Y','N'});
            if strcmp(reply,'Y')
                this.ChangeHubMets;
            end
            
            % Tracking
            % Keep track of which function is being used to condense the
            % model
            this.ChangeFunction('Similar reactions');
            
            % Initial number of reactions
            NumRxns = this.NumReactions;
            
            % Finding similar reactions
            groups = this.findSimilarReactions(N1,N2,NG);
            
            % Trimming the groups by removing permanently rejected combinations
            groups = this.FilterGroups(groups);
            
            % Confirm that there are still reactions to be merged
            NumGroups = size(groups,1);
            if NumGroups==0
                return
            end
            % Attempt to Merge the remainder
            
            fprintf('%d groups of corresponding reactions found.\n',NumGroups)
            for i=1:NumGroups
                clc, fprintf('Merging group %d out of the %d groups of corresponding reactions',i,NumGroups)
                this.MergeReactions(groups(i,:));
            end
            
            % Update consensus model structure
            this.UpdateAll;
            if NumRxns ~=this.NumReactions
                clc,fprintf('By merging highly similar reactions, the number of reactions was changed from %d to %d\n',NumRxns,this.NumReactions)
            end
        end
        function LumpedReactions(this,reactions)
            %%% PURPOSE
            % Identifies reactions in the model structure that are potentially lumped
            % reactions and the corresponding non-lumped representations. For example:
            % Rxn 1: A -> B
            % Rxn 2: B -> C
            % Rxn 3: C -> D
            % Rxn x: A -> D
            % Rxn x is potentially a lumped representation of reactions 1-3
            %%% EXAMPLE CALL
            %   LumpedReactions(Combined_model,[1:100 103 106]);
            %%% INPUT
            %   this: The model structure
            %%% OPTIONAL INPUT
            %   reactions: Indices of the reactions for which to search candidate
            %   non-lumped representations.
            %   Default value: all reactions in the model
            %%% OUTPUT
            %   this: An updated model structure
            
            % Keep track of which function is being used to condense the
            % model
            this.ChangeFunction('Lumped reactions');
            
            % Initial number of reactions
            NumRxns = this.NumReactions;
            
            % Finding lumped reactions
            if exist('reactions','var')
                [groups,cands] = this.findLumpedReactions(reactions);
            else
                [groups,cands] = this.findLumpedReactions;
            end
            
            % Filter lumped reactions
            %Earlier rejected
            %Large size
            
            % Confirm that there are any candidate lumped reactions to be
            % considered
            NumGroups = size(groups,1);
            if NumGroups==0,return,end
            
            % Process the identified cases
            this.ProcessLumpedReactions(groups,cands);
            
            % Update consensus model structure
            this.UpdateAll;
            if NumRxns ~=this.NumReactions
                clc,fprintf('By processing lumped reactions the number of reactions was changed from %d to %d\n',NumRxns,this.NumReactions)
            end
        end
        %compartment-related (also reaction)
        function InvalidTransport(this)
            %%% PURPOSE
            % To identify faulty transport reactions; transport reactions between
            % cellular compartments that are not connected.
            %%% EXAMPLE CALL
            %   InvalidTransport(Combined_model);
            %%% INPUT
            %   this: The model structure
            %%% OPTIONAL INPUT
            %%% OUTPUT
            %   this: An updated model structure
            
            % Keep track of which function is being used to condense the
            % model
            this.ChangeFunction('Invalid transport');
            
            % Initial number of reactions
            NumRxns = this.NumReactions;
            
            % Finding invalid transport reactions
            InvTrans = findInvalidTransport(this);
            InvTrans = find(InvTrans);
            
            % Confirm that there are still reactions to be merged
            NumInvTrans = numel(InvTrans);
            if NumInvTrans==0
                disp('No invalid transport reactions found')
                return
            end
            
            % Process the invalid transport reactions, either automatically or manually
            
            RxnComp   = this.RxnComp;
            fprintf('%d Invalid Transport reactions found.\n',NumInvTrans)
            
            groups = [];
            for i=1:numel(InvTrans);
                clc, fprintf('Processing #%d out of the %d Invalid Transport reactions',i,NumInvTrans)
                if this.options.ResolveInvalidTransport==1 && ismember(RxnComp(InvTrans(i)),this.options.InvTransportSolution(:,1));
                    group = AutoResolveInvTrans(this,InvTrans(i));
                else
                    group = ResolveInvTrans(this,InvTrans(i));
                end
                if ~isempty(group)
                    groups(end+1,:) = group; %#ok<AGROW>
                end
            end
            
            % Trimming the groups by removing permanently rejected combinations
            groups = this.FilterGroups(groups);
            
            % Attempt to Merge the remainder
            if ~isempty(groups)
                NumGroups = size(groups,1);
                fprintf('%d groups of to-be-merged reactions found.\n',NumGroups)
                for i=1:NumGroups
                    clc, fprintf('Merging group %d out of the %d groups of duplicate reactions',i,NumGroups)
                    this.MergeReactions(groups(i,:));
                end
            end
            
            this.UpdateAll;
            if NumRxns ~=this.NumReactions
                clc,fprintf('By processing invalid transport reactions, the number of reactions was changed from %d to %d\n',NumRxns,this.NumReactions)
            end
        end
        function SameRxnDiffComp(this)
            %%% PURPOSE
            % To identify sets of compartmentalised reactions (no transport reactions)
            % that exist in multiple compartments. Compartmentalization of reactions
            % is one of the main remaining problems in metabolic model construction. As
            % a result, large differences exist herein in different models.
            %%% EXAMPLE CALL
            %   SameRxnDiffComp(Combined_model);
            %%% INPUT
            %   this: The model structure
            %%% OPTIONAL INPUT
            %%% OUTPUT
            %   this: An updated model structure
            
            % Tracking used function
            this.ChangeFunction('Nested reactions');
            
            % Initial number of reactions
            NumR = this.NumReactions;
            
            % Obtaining preferred settings
            if this.options.SameRxnDiffComp_RemoveDead==0
                disp(['If two or more identical chemical conversions are found in different compartments,' char(10) ...
                    'may the ones corresponding to dead end / inactive reactions automatically be removed?' char(10) ...
                    'This will only be done in case there is at least one active reaction remaining.' char(10) ...
                    'Y: Yes, both for dead and inactive reactions' char(10) ...
                    'D: Yes, but only for dead-end reactions' char(10) ...
                    'N: No, always prompt.'...
                    ])
                reply = COMMGEN.InputProcessing({'Y','D','N'});
                if strcmp('Y',reply)
                    this.options.SameRxnDiffComp_RemoveDead = 2;
                elseif strcmp('D',reply)
                    this.options.SameRxnDiffComp_RemoveDead = 1;
                else
                    this.options.SameRxnDiffComp_RemoveDead = -1;
                end
            end
            
            if this.options.SameRxnDiffComp_AutoGPR==0
                disp([ char(10) 'If one of the reactions differs in gene rules with respect to the other, may they be resolved automatically?' char(10) ...
                    'Automatic: Either keeping the gene rule corresponding to the to-be-kept reaction or automatic merging according to the' char(10) ...
                    'default settings (Make choice afterwards)' char(10) ...
                    '[Y/N]'])
                reply = COMMGEN.InputProcessing({'Y','N'});
                if strcmp('Y',reply)
                    disp([char(10) 'Which type of automatic processing is preferred?' char(10) ...
                        'K: Keep the gene rule corresponding to the to-be-kept reaction.' char(10) ...
                        'M: Merge the gene rules of the to-be-kept and to-be-discarded reactions.'])
                    reply = COMMGEN.InputProcessing({'K','M'});
                    if strcmp('M',reply)
                        this.options.SameRxnDiffComp_AutoGPR = 1;
                    else
                        this.options.SameRxnDiffComp_AutoGPR = 2;
                    end
                else
                    this.options.SameRxnDiffComp_AutoGPR = -1;
                end
            end
            
            if this.options.SameRxnDiffComp_AllActSameModel == 0
                disp(['If identical chemical conversions in different compartments where all reactions are active and' char(10) ...
                    'originating from the same model, should they automatically be kept?' char(10) ...
                    '[Y/N]' char(10)])
                reply = COMMGEN.InputProcessing({'Y','N'});
                if strcmp('Y',reply)
                    this.options.SameRxnDiffComp_AllActSameModel = 1;
                else
                    this.options.SameRxnDiffComp_AllActSameModel = -1;
                end
            end
            
            if this.options.SameRxnDiffComp_IgnoreAllInact == 0;
                disp(['If all reactions in the set of identical chemical conversions in different compartments' char(10) ...
                    'are inactive, should the set be ignored?' char(10) ...
                    '[Y/N]' char(10)])
                reply = COMMGEN.InputProcessing({'Y','N'});
                if strcmp('Y',reply)
                    this.options.SameRxnDiffComp_IgnoreAllInact = 1;
                else
                    this.options.SameRxnDiffComp_IgnoreAllInact = -1;
                end
            end
            
            % Adjusting Settings if needed
            if this.options.SameRxnDiffComp_AutoGPR == -1;
                GeneOR = this.options.GeneOR;
                this.options.GeneOR = 0;
            end
            
            if this.options.SameRxnDiffComp_AutoGPR == 1;
                % MergeOptions: By default keep the equation and id of the
                % to be kept reaction. Whether the GPR rule should be kept
                % as it is or changed depends on AutoGPR settings.
                this.options.SameRxnDiffComp_Merge = [1 1 0];
            else
                this.options.SameRxnDiffComp_Merge = [1 1 1];
            end
            
            % Finding and processing Identical reactions in different compartments until all have been processed
            while true
                % Finding identical reactions in different compartments
                groups   = this.findSameRxnDiffComp;
                InAGroup = any(groups); % All reactions which are yet to be proposed for deletion.
                
                % Getting rid of groups which were proposed before
                groups = this.FilterGroups(groups);
                
                % If none remain, stop.
                NumGroups = size(groups,1);
                if NumGroups==0
                    break
                end
                
                fprintf('%d groups of identical chemical conversions in different compartments found.\n',NumGroups)
                
                % Process the found groups of reactions.
                this.ProcessSameRxnDiffComp(groups,InAGroup);
                
                % Process the removal and addition of reactions.
                this.UpdateAll;
            end
            
            % Restoring original options
            % Processing of GPR rules
            if this.options.SameRxnDiffComp_AutoGPR == -1;
                this.options.GeneOR = GeneOR;
            end
            if NumR~=this.NumReactions
                clc,fprintf('By processing identical reactions in different compartments, the total number of reactions was changed from %d to %d\n',NumR,this.NumReactions)
            end
        end
        function MultipleTransportSameSpecSameComps(this)
            %%% PURPOSE
            % To identify sets of transport reactions between the same cellular
            % compartments for the same chemical species. The identification of
            % transporters and transport mechanisms remains difficult, resulting in
            % alternative modeling thereof.
            %%% EXAMPLE CALL
            %   MultipleTransportSameSpecSameComps(Combined_model);
            %%% INPUT
            %   this: The model structure
            %%% OPTIONAL INPUT
            %%% OUTPUT
            %   this: An updated model structure
            
            % Keep track of which function is being used to condense the
            % model
            this.ChangeFunction('MultTransSameSpecSameComps');
            
            % Initial number of reactions
            NumRxns = this.NumReactions;
            
            % Find groups of reactions transporting the same species between the same compartments
            % The search ignored protons, for they are supposed to be
            % involved in many transport reactions (proton pumps).
            groups = this.findMultipleTransportSameSpecSameComps;
            groups = this.FilterGroups(groups);
            
            % Confirm that there are still reactions to be merged
            NumGroups = size(groups,1);
            if NumGroups==0
                return
            end
            
            fprintf('%d groups of alternative transport reactions for the same species over the same membrane found.\n',NumGroups)
            for i=1:NumGroups
                clc, fprintf('Merging group %d out of the %d groups of duplicate reactions\n',i,NumGroups)
                this.MergeReactions(groups(i,:));
            end
            
            % Update consensus model structure
            this.UpdateAll;
            if NumRxns ~=this.NumReactions
                clc,fprintf('By processing alternative transport reactions for the same species, the total number of reactions was changed from %d to %d\n',NumRxns,this.NumReactions)
            end
        end
        function InvalidExternalRxns(this)
            %%% PURPOSE
            % To identify exchange/external/boundary reactions that do not connect to
            % the extracellular compartment. External reactions indicate exchange with
            % the medium, therefore, the exchanged chemical species should end up (or
            % originate from) the external compartment in the model.
            %%% EXAMPLE CALL
            %   InvalidExternalRxns(Combined_model);
            %%% INPUT
            %   this: The model structure
            %%% OPTIONAL INPUT
            %%% OUTPUT
            %   this: An updated model structure
            
            % Keep track of which function is being used to condense the
            % model
            this.ChangeFunction('External reactions');
            
            % Initial number of reactions
            NumRxns = this.NumReactions;
            
            % Finding invalid external reactions
            reactionIDx = findInvalidExternalRxns(this);
            
            % Confirm that there are still reactions to process
            NumInv = numel(reactionIDx);
            if NumInv==0
                return
            end
            
            % Attempt to Resolve the remainder
            fprintf('%d invalid external reactions found.\n',NumInv)
            this.ResolveInvExt(reactionIDx);
            
            % Update consensus model structure
            this.UpdateAll;
            if NumRxns ~=this.NumReactions
                clc,fprintf('The splitting of invalid external reactions has changed the number of reactions from %d to %d\n',NumRxns,this.NumReactions)
            end
        end
        function ResolveUnknownComp(this)
            %%% PURPOSE
            % To identify reactions where one or multiple metabolites have an unknown
            % compartmentalization. Other reactions corresponding to the same chemical
            % conversion are then identified to assist with the assignment of a
            % compartment for these metabolites.
            %%% EXAMPLE CALL
            %   ResolveUnknownComp(Combined_model);
            %%% INPUT
            %   this: The model structure
            %%% OPTIONAL INPUT
            %%% OUTPUT
            %   this: An updated model structure
            
            RxnCmp = this.RxnComp;
            
            % Check if there are any reactions in the unknown compartment
            if ~ismember('UNKCOMP',this.CompartmentIDToName)
                fprintf('For all metabolites the compartment was already set.\n')
                return
            elseif all(cellfun(@isempty,regexp(this.RxnComp,'UNKCOMP')))
                fprintf('There are no longer any metabolites for which the compartment is unknown\n')
                return
            end
            
            % Keep track of which function is being used
            this.ChangeFunction('UnkComp');
            
            % Initial number of reactions
            NumRxns = this.NumReactions;
            
            % Finding reactions containing metabolites in the unknown
            % compartment and highly similar reactions.
            groups = this.findUnkCompRxns;
            
            % Attempt to solve cases automatically
            OneMatch = find(sum(groups,2)==2);
            NumOneM  = numel(OneMatch);
            if NumOneM % If nonzero
                fprintf('For %d reactions having a metabolite in the unknown compartment exactly one alternative reaction was found.\n Attempting to resolve automatically\n',NumOneM)
                for i=1:numel(NumOneM)
                    clc, fprintf('Processing group %d out of the %d groups\n\n',i,NumOneM)
                    NonAutoGroups = this.AutoUnkCompRes(groups(OneMatch(i),:));
                end
                fprintf('%d out of %d cases solved automatically\n',NumOneM-size(NonAutoGroups,1),NumOneM)
                groups(OneMatch,:) = [];
                groups = [groups ; NonAutoGroups];
            end
            
            % Present cases where the groups contain more than 1 reaction
            % (An alternative reaction was found)
            AltFound = find(sum(groups,2)>1);
            NumAlt = numel(AltFound);
            if NumAlt % If nonzero
                fprintf('For %d reactions having a metabolite in the unknown compartment alternative reactions were found.\n',NumAlt)
                for i=1:numel(AltFound)
                    clc, fprintf('Processing group %d out of the %d groups\n\n',i,NumAlt)
                    this.MergeReactions(groups(AltFound(i),:));
                end
            end
            groups(AltFound,:) = [];
            
            NumGroups = size(groups,1);
            if NumGroups % If there are any groups remaining
                %Find back the individual reactions which corresponded to
                %the unknown compartment
                UnkCompRxns = ~cellfun(@isempty,regexp(RxnCmp,'UNKCOMP'));
                RemRxns = any(groups,1)';
                RemUnkCompRxns = find(UnkCompRxns & RemRxns);
            end
            
            % Present the remaining cases to solve manually
            NumGroups = numel(RemUnkCompRxns);
            for i=1:NumGroups
                clc, fprintf('%d reactions remaining which have a metabolite in the unknown compartment. For these reactions no alternative was found.\n',NumGroups)
                fprintf('Processing number %d\n',i)
                this.ManUnkCompRes(RemUnkCompRxns(i));
            end
            
            % Update consensus model structure
            this.UpdateAll;
            if NumRxns ~=this.NumReactions
                clc,fprintf('Processing reactions with metabolites in unknown compartments got the total number of reactions to change from %d to %d\n',NumRxns,this.NumReactions)
            end
        end
        function RemoveCompartment(this,RC,TC)
            %%% PURPOSE
            % to remove a cellular compartment.
            %%% EXAMPLE CALL
            %   RemoveCompartment(Combined_model,'MNXC19','MNXC3');
            %%% INPUT
            %   this: The model structure
            %   RC: The compartment which is to be removed.
            %%% OPTIONAL INPUT
            %   TC: the compartment where the reactions from compartment RC should be
            %   moved to.
            %   Default values:
            %             RC        RC name                         TC
            %             MNXC0     cellular_component              No default - prompt
            %             MNXC1     cell                            No default - prompt
            %             MNXC2     extracellular region            No default - prompt
            %             MNXC3     cytoplasm                       No default - prompt
            %             MNXC4     mitochondrion                   cytoplasm
            %             MNXC5     mitochondrial membrane          cytoplasm
            %             MNXC6     nucleus                         cytoplasm
            %             MNXC7     nuclear membrane                cytoplasm
            %             MNXC8     chloroplast                     cytoplasm
            %             MNXC9     vacuole                         cytoplasm
            %             MNXC10    vacuolar membrane               cytoplasm
            %             MNXC11    endoplasmic reticulum           cytoplasm
            %             MNXC12    endoplasmic reticulum membrane  cytoplasm
            %             MNXC13    peroxisome                      cytoplasm
            %             MNXC14    peroxisomal membrane            cytoplasm
            %             MNXC15    Golgi apparatus                 cytoplasm
            %             MNXC16    Golgi membrane                  cytoplasm
            %             MNXC17    lysosome                        cytoplasm
            %             MNXC18    plastid                         cytoplasm
            %             MNXC19    periplasmic space               cytoplasm
            %             MNXC20    plasma membrane                 cytoplasm
            %             MNXC21    outer membrane                  cytoplasm
            %             MNXC22    cell wall                       cytoplasm
            %             MNXC23    glycosome                       cytoplasm
            %             MNXC24    glyoxysome                      cytoplasm
            %             MNXC25    apicoplast                      cytoplasm
            %             MNXC26    provirus                        No default - prompt
            %             MNXC27    virion                          No default - prompt
            %             MNXC28    flagellum                       cytoplasm
            %             MNXC29    acidocalcisome                  cytoplasm
            %%% OUTPUT
            %   this: An updated model structure
            
            % Check input arguments
            assert(numel(RC)==1,'Only one compartment can be removed at a time')
            assert(ismember(RC,this.CompIDToName),'Only a compartment which is in the model can be removed')
            RC = this.NameToID(RC);
            
            %Find the target compartment
            if nargin < 3
                DefRC = ['MNXC4','MNXC5','MNXC6','MNXC7','MNXC8','MNXC9','MNXC10','MNXC11','MNXC12','MNXC13','MNXC14','MNXC15','MNXC16','MNXC17','MNXC18','MNXC19','MNXC20','MNXC21','MNXC22','MNXC23','MNXC24','MNXC25','MNXC28','MNXC29'];
                DefTC = ['MNXC3','MNXC3','MNXC3','MNXC3','MNXC3','MNXC3', 'MNXC3', 'MNXC3', 'MNXC3', 'MNXC3', 'MNXC3', 'MNXC3', 'MNXC3', 'MNXC3', 'MNXC3', 'MNXC3', 'MNXC3', 'MNXC3', 'MNXC3', 'MNXC3', 'MNXC3', 'MNXC3', 'MNXC3', 'MNXC3'];
                [~,IA] = ismember(RC,DefRC);
                if IA
                    TC = DefTC(IA);
                    fprintf('The default target compartment for the reactions from %s is %s.\nPlease confirm:\nY: Agreed, proceed\nC: Cancel, do not remove %s\nA: Choose an alternative target compartment\n',RC,TC,RC)
                    reply = COMMGEN.InputProcessing({'Y','C','N'});
                    if strcmp(reply,'C')
                        return
                    elseif strcmp(reply,'N')
                        fprintf('Please choose the target compartment for the reactions coming from %s\nThe compartments in the model are:\n',RC)
                        disp(this.CompartmentIDToName)
                        TC = COMMGEN.InputProcessing(this.CompartmentIDToName);
                    end
                else
                    fprintf('Please choose the target compartment for the reactions coming from %s\nThe compartments in the model are:\n',RC)
                    disp(this.CompartmentIDToName)
                    TC = COMMGEN.InputProcessing(this.CompartmentIDToName);
                end
            else
                assert(numel(TC)==1,'Only one compartment can be the target holder of the reactions')
                assert(ismember(TC,this.CompartmentIDToName),'Only a compartment which is in the model can be the target holder for the reactions')
            end
            TC = this.NameToID(TC);
            
            % Load info
            RxnCmp = this.RxnComp;
            
            % Sort the reactions involving RC into different groups to be
            % processed individually.
            tr = this.identifyRealTransport;
            InComp = strcmp(this.RxnComp,RC);
            RCRxns = ~cellfun(@isempty,regexp(RxnCmp,[RC{1} '$'])) | ~cellfun(@isempty,regexp(RxnCmp,[RC{1} '%']));%
            TCRxns = ~cellfun(@isempty,regexp(RxnCmp,[TC{1} '$'])) | ~cellfun(@isempty,regexp(RxnCmp,[TC{1} '%']));%
            TwoCmp = ~cellfun(@isempty,regexp(Merged.RxnComp,'^[^%]*%[^%]*$'));
            
            % Move all non-transport reactions from compartment RC to TC
            IDx1 = InComp;
            this.ChangeCmp(IDx1,RC,TC);
            
            % Discard any transport reactions between RC & TC (except if
            % they correspond to a chemical conversion as well or a third compartment is involved)
            IDx2 = tr & RCRxns & TCRxns & TwoCmp; %Transport reactions involving both the original and the target compartment and no other compartments
            this.MarkForRemoval(IDx2);
            
            % Move those transport reactions corresponding to a chemical
            % conversion
            IDx3 = RCRxns & TCRxns & ~IDx2;
            this.ChangeCmp(IDx3,RC,TC);
            
            % Move all transport reactions between RC & compx to TC & compx
            IDx4 = RCRxns & ~InComp & ~TCRxns;
            this.ChangeCmp(IDx4,RC,TC);
        end
        %species-related
        function SimilarSpecies(this,optionset)
            %%% PURPOSE
            % To identify different chemical species which are identical in the network
            % context. This is a result of either a failed mapping to the MnXRef
            % namespace or modelling of metabolites at different levels of detail. For
            % example: glucose versus alpha-D-glucose
            %%% EXAMPLE CALL
            %   SimilarSpecies(Combined_model,3)
            %%% INPUT
            %   this: The model structure
            %%% OPTIONAL INPUT
            %   optionset: predefined settings for the identification of similar
            %   species:
            %       1       Find very similar species which do not
            %               necessarily have to overlap in reactions
            %       2       Find less similar species which have identical
            %               reactions
            %       3       Manually set the values
            %   Default value: 3
            %%% OUTPUT
            %   this: An updated model structure
            
            if nargin==1
                fprintf('Please select the criteria for identification of similar species\n')
                fprintf('1: very similar species, not necessarily sharing reactions\n')
                fprintf('2: less similar species that have identical reactions\n')
                fprintf('3: manually enter threshold values\n')
                reply = COMMGEN.InputProcessing({'1','2','3'});
                optionset = str2double(reply);
            end
            
            switch optionset
                case 1
                    this.options.SimilarSpecies_OneReaction = 0;
                    this.options.findBaseCorrScore_quantile = 70;
                case 2
                    this.options.SimilarSpecies_OneReaction = 1;
                    this.options.findBaseCorrScore_quantile = 5;
                case 3
                    fprintf('Manual selection of settings\n')
                    fprintf('In order for two species to be considered for matching, should they have an identical reaction?[Y/N]\n')
                    reply = COMMGEN.InputProcessing({'Y','N'});
                    switch reply
                        case 'Y'
                            this.options.SimilarSpecies_OneReaction = 1;
                        case 'N'
                            this.options.SimilarSpecies_OneReaction = 0;
                    end
                    fprintf('How similar should the two species be? This value is on a scale of 0 to 100, where 0 means everything suffices and 100 means exactly identical.\n The default values for options 1 & 2 are 70 and 5 respectively.\n')
                    reply = COMMGEN.InputProcessing('realnumber');
                    this.options.findBaseCorrScore_quantile = reply;
            end
            
            % Keep track of which function is being used to condense the
            % model
            this.ChangeFunction('Similar species');
            
            % Initial number of reactions
            NumRxns = this.NumReactions;
            
            % Find highly similar species groups
            groups = this.findSpeciesMatches;
            
            % Confirm that there are still reactions to be merged
            NumGroups = size(groups,1);
            if NumGroups == 0
                fprintf('No new matches have been found. Consider using less strict criteria.\n')
                return
            elseif NumGroups > 100
                fprintf('%d matches have been found. It''s advisable to use stricter criteria. Do you want to change the criteria? [Y/N]\n',NumGroups)
                reply = COMMGEN.InputProcessing({'Y','N'});
                if strcmp(reply,'Y')
                    this.SimilarSpecies(3);
                    return
                end
            end
            
            % Divide the groups in two sets, one where species are
            % allowed to have multiple matches and one where not.
            SMM  = sum(groups,1)>1; %Species with multiple matches
            GSMM = any(groups(:,SMM),2); %Groups with species with multiple matches
            
            % Propose to merge the species
            this.ProposeSpeciesMergeSingleMatch(groups(~GSMM,:))
            this.ProposeSpeciesMergeMultMatch(groups(GSMM,:))
            
            % If any merges were accepted, iterate.
            if ~this.Consistent
                this.DuplicateRxns;
                this.SimilarSpecies;
            end
            
            % Print changes
            if NumRxns ~=this.NumReactions
                clc,fprintf('By merging highly similar species, the total number of reactions was changed from %d to %d\n',NumRxns,this.NumReactions)
            end
        end
        function SimilarSpeciesPairs(this)
            %%% PURPOSE
            % To identify different pairs of chemical species which are identical in
            % the network context. This is a result of either a failed mapping to the
            % MnXRef namespace or modelling of metabolites at different levels of
            % detail. For example: donor/acceptor versus nadh/nad
            %%% EXAMPLE CALL
            %   SimilarSpeciesPairs(Combined_model);
            %%% INPUT
            %   this: The model structure
            %%% OPTIONAL INPUT
            %%% OUTPUT
            %   this: An updated model structure
            
            % Keep track of which function is being used to condense the
            % model
            this.ChangeFunction('Similar species pairs');
            
            % Initial number of reactions
            NumRxns = this.NumReactions;
            
            fprintf('Searching for highly similar sets of species pairs of species.\n A pair is a set of two species that nearly always occur together.')
            % Obtain possible pairs
            [groups,SpecCoOccur] = this.findSimilarSpeciesPairs;
            
            NumGroups = size(groups,1);
            if NumGroups==0
                return
            end
            
            % Propose to merge the species
            fprintf('\n%d sets of to be potentially merged species pairs found\n',NumGroups)
            
            this.ProposeSpeciesPairsMerge(groups,SpecCoOccur);
            
            if ~this.Consistent
                % If any merges were accepted
                this.DuplicateRxns;
            end
            
            if NumRxns ~=this.NumReactions
                clc,fprintf('By merging pairs of highly similar species, the total number of reactions was changed from %d to %d\n',NumRxns,this.NumReactions)
            end
        end
        function InspectUnkForm(this)
            %%% PURPOSE
            % To assign chemical sum formulae to chemical species for which these are
            % not available.
            %%% EXAMPLE CALL
            %   InspectUnkForm(Combined_model);
            %%% INPUT
            %   this: The model structure
            %%% OPTIONAL INPUT
            %%% OUTPUT
            %   this: An updated model structure
            
            
            % Load information
            SF = this.SpecFormula;
            RxnIDs = this.reaction_IDs;
            MTS = this.MetToSpecies;
            
            %Determine number of reactions for each species
            NRPM = sum(logical(this.Stoich),2); %Number of reactions per metabolite
            NRPS = zeros(this.NumSpecies,1);
            for i=1:this.NumSpecies
                mets = MTS==i;
                NRPS(i) = sum(NRPM(mets));
            end
            this.FormInspected(NRPS==0) = 1; % if the species no longer has any reactions, the formula does no longer need to be determined
            FI = this.FormInspected;
            
            % Pre-process information
            
            if ~any(FI==0)
                FI(FI==-1)=0;
            end
            
            % Select species to inspect
            %Identify species needing inspection
            UF = ~any(SF,2) & FI==0; %Unknown formulae and not previously tried to determine
            if ~any(UF)
                UF = FI==0;
            end
            UF = find(UF);
            
            %Select current candidates
            if numel(UF)>20
                order = NRPS(UF);
                [~,I] = sort(order);
                UF = UF(I(1:20));
            end
            % Check if task completed
            if isempty(UF)
                disp('All species have a confirmed formula')
                return
            end
            
            %Find formula per reaction
            FormPerRxn = findFormulaFromReactions(this,UF);
            
            % Check if any formulae can be unambiguously assigned
            %Criteria:
            %All formulas are in agreement
            %No formulas are 'NA'
            %A minimum of two reactions
            assigned = false;
            for i=1:numel(UF)
                if ~ismember('NA',FormPerRxn(:,i)) && numel(setdiff(FormPerRxn(:,i),''))==1 && sum(~ismember(FormPerRxn(:,i),''))>1
                    Number = sum(~ismember(FormPerRxn(:,i),''));
                    Name = this.IDToName(this.species_IDs(UF(i)));
                    newForm = setdiff(unique(FormPerRxn(:,i)),'');
                    fprintf('Based on %d reactions, the formula of %s has been changed to %s',Number,Name{1},newForm{1})
                    this.SetSpecFormula(UF(i),newForm);
                    assigned = true;
                    this.FormInspected(UF(i))=1;
                end
            end
            if assigned
                this.InspectUnkForm;
                return
            end
            % Else, inspect all species
            for i=1:numel(UF)
                id = UF(i);
                Name = this.IDToName(this.species_IDs(id));
                clc
                fprintf('Currently inspecting the species %s\n',Name{1});
                Categories = setdiff(unique(FormPerRxn(:,i)),'');
                if numel(Categories)==1
                    fprintf('Formula for %s unknown in all reactions \n\n\n',Name{1})
                    this.FormInspected(id)=-1;
                else
                    fprintf('%d different categories (molecular formulae) have been identified:\nNote that if different formulae for the same metabolite effectively exist in the same GSM, it is possible that atoms are spontaneously generated in the cell\n',numel(Categories))
                    disp(Categories)
                    
                    rxnlist = {}; %placeholder for reaction names
                    for j=1:numel(Categories)
                        %find reaction names corresponding to the category
                        rxns = RxnIDs(ismember(FormPerRxn(:,i),Categories{j}));
                        rxnlist(end+1:end+numel(rxns)) = rxns;
                        fprintf('\n\nReactions in category %s:\n',Categories{j})
                        this.PrintEquations(ismember(RxnIDs,rxns))
                    end
                    
                    while true
                        fprintf('The full list of reactions, with indices to refer to them:\n')
                        disp([num2cell(1:numel(rxnlist))' rxnlist'])
                        disp([char(10) 'Please choose how to proceed.'                                char(10)...
                            'R: Remove reactions'                                                     char(10)...
                            'M: Modify reactions (GPR / equation)'                                    char(10)...
                            'A: Assign chemical sum formula to the currently inspected species'       char(10)...
                            'C: Continue'                                                             char(10)...
                            ])
                        reply = COMMGEN.InputProcessing({'R','A','C','M'});
                        switch reply
                            case 'R' % Remove reactions
                                I = input('Please enter the indices of reactions to be removed \nFor example: 1 or [1 2 3]\nAlternatively, enter 0 to cancel\n');
                                if ~(numel(I)==1 && I==0)
                                    %Remove reactions
                                    this.MarkForRemoval(ismember(RxnIDs,rxnlist(I)));
                                    rxnlist(I) = []; %#ok<AGROW>
                                end
                                fprintf('\n\n\n')
                            case 'M' % Modify reactions
                                I = input('Please enter the indices of reactions to be changed \nFor example: 1 or [1 2 3]\nAlternatively, enter 0 to cancel\n');
                                if ~(numel(I)==1 && I==0)
                                    idx = find(ismember(RxnIDs,rxnlist(I)));
                                    for j=1:numel(idx)
                                        this.adjustReaction(idx(j));
                                    end
                                end
                                fprintf('\n\n\n')
                            case 'A' % Assign chemical formula
                                fprintf('Assigning chemical formula for %s\n',Name{1})
                                fprintf('The usable atoms:\n')
                                disp(this.species{1}.CEL)
                                reply = input('What is the desired chemical formula?\n','s');
                                this.SetSpecFormula(id,reply);
                                this.FormInspected(UF(i)) = 1;
                            case 'C' % Continue
                                if this.FormInspected(UF(i))~=1
                                    this.FormInspected(UF(i))=-1;
                                end
                                break
                        end
                    end
                    
                end
            end
            %Check whether to rerun or reset
            if any(this.FormInspected==0)
                this.InspectUnkForm
            else
                this.FormInspected(this.FormInspected==-1)=0;
            end
        end
        
        % Finding groups
        %reaction groups
        function groups = findDuplicateRxns(this,S)
            % Search for reactions which are identical concerning
            % metabolites and stoichiometric coefficients. Not necessarily
            % for directionality and GPR.
            %%% Optional Input
            % S     Alternative Stoichiometric matrix
            
            % Load Data
            
            if ~exist('S','var')
                S = this.Stoich;
            end
            
            NumRxns = size(S,2);
            
            % Adding reverse reactions
            if ~islogical(S)
                S(:,end+1:end+NumRxns) = -1*S;
                NumRxns = NumRxns*2;
            end
            
            % Finding duplicate reactions
            
            % Obtaining Hash values for each non-empty reaction
            S = full(sparse(S)); % Required for data hashing
            NonEmpty = any(S,1);
            HashVals = cell(NumRxns,1);
            for i=1:NumRxns
                %if NonEmpty(i)
                HashVals{i} = DataHash(S(:,i));
                %end
            end
            
            % Finding the unique values
            UniqHash  = unique(HashVals(NonEmpty));
            
            % Pre-allocating space
            NumGroups = numel(UniqHash);
            groups = false(NumGroups,NumRxns);
            
            % Finding the reactions belonging to each group
            for i=1:NumGroups
                groups(i,:) = strcmp(UniqHash(i),HashVals);
            end
            
            % Removing groups with less than 2 reactions
            groups(sum(groups,2)==1,:) = [];
            
            if ~islogical(S)
                % Mapping reverse reactions back to original reactions
                groups = groups(:,1:NumRxns/2) | groups(:,NumRxns/2+1:end);
                
                % Remove duplicate rows
                groups = unique(groups,'rows');
            end
        end
        function groups = findAltDonorAcceptorPairs(this)
            % Find reactions which would be the same if not for the
            % different redox pairs
            
            % Load data and pre-allocate variables
            logS = logical(this.Stoich);
            
            %Ignore certain metabolites
            logS = this.RemIgnored(logS);
            
            Pairs = this.RedoxPairsAccepted; % All the currently accepted redox pairs
            
            % Add a new row to logS which corresponds to a redox pair being
            % involved in the reaction.
            logS(end+1,:) = false;
            
            for i=1:size(Pairs,1)
                % Identify metabolite indices corresponding to the redox pairs
                SpecIDx = find(ismember(this.species_IDs,Pairs(i,:)));
                if numel(SpecIDx)==2 %In case species passed in the options file are not present in the models.
                    MetIDx1 = this.MetToSpecies==SpecIDx(1);
                    MetIDx2 = this.MetToSpecies==SpecIDx(2);
                    
                    % Identify reactions in which both species are involved and
                    % add the 'redox pair' metabolite there. Also remove the
                    % original metabolites from the reaction.
                    RxnIDx  = any(logS(MetIDx1,:),1) & any(logS(MetIDx2,:),1);
                    logS(end,RxnIDx) = true;
                    logS(MetIDx1|MetIDx2,RxnIDx) = false;
                end
            end
            
            % Filter reactions and metabolites. Only reactions involving a
            % redox pair are to be considered.
            logS(:,~logS(end,:)) = false;
            
            % Reactions only involving redox pairs can be ignored
            logS(:,~any(logS(1:end-1,:),1))=false;
            
            % Find the groups
            groups = findDuplicateRxns(this,logS);
        end
        function groups = findNestedReactions(this)
            % this function finds nested reactions, where one reaction is a
            % subset of the other
            % A + B -> C Vs A + B + D -> C + E
            
            % Load stoichiometric matrix, remove ignored metabolites
            S = this.Stoich;
            S = this.RemIgnored(S);
            LogStoich = logical(S);
            PosS = S>0;
            NegS = S<0;
            
            % Pre-allocate variable
            RxnVsRxn = false(this.NumReactions);
            
            % For each reaction, find other reactions which could
            % potentially be "SuperReaction"
            
            for i=1:this.NumReactions
                MetIDx = LogStoich(:,i);
                RxnVsRxn(i,:) = all(LogStoich(MetIDx,:),1); % all metabolites found in superset
            end
            
            RxnVsRxn = RxnVsRxn - diag(diag(RxnVsRxn)); % Reactions cannot match themselves.
            
            % Filter reaction sets
            
            % Ignore external and transport reactions
            RxnVsRxn(this.is_external|this.is_transport,:) = 0;
            RxnVsRxn(:,this.is_external|this.is_transport) = 0;
            
            % Ignore reactions only containing hub metabolites
            HubRxns = ~any(LogStoich(~this.HubMetabolites,:),1);
            RxnVsRxn(HubRxns,:) = 0;
            RxnVsRxn(:,HubRxns) = 0;
            
            % Assert that matching metabolites are on the same side of the
            % reaction
            Num = count(RxnVsRxn);
            IDx = zeros(Num,2);
            [IDx(:,1),IDx(:,2)] = find(RxnVsRxn);
            Keep = true(Num,1);
            
            for i=1:Num
                SubR = IDx(i,1);
                SupR = IDx(i,2);
                ed = NegS(:,SubR); % educts in the subreaction
                pr = PosS(:,SubR); % products in the subreaction
                % In the superreaction the educts and products of the
                % subreaction need to be on opposite sides of each other.
                if ~(all(NegS(ed,SupR)) && all(PosS(pr,SupR)) || all(NegS(pr,SupR)) && all(PosS(ed,SupR)));
                    Keep(i) = false;
                end
            end
            IDx(~Keep,:) = [];
            
            groups = false(size(IDx,1),this.NumReactions);
            for i=1:size(IDx,1)
                groups(i,IDx(i,:)) = true;
            end
        end
        function groups = findSimilarReactions(this,N1,N2,NG)
            % Search for shared (non-hub) species which have reactions which are not
            % shared. Then try whether any of these reactions correspond to
            % the same process.
            
            % Load info
            RxnOrig   = this.RxnOrigin*1; %Convert to double for later convenience
            PosS = (this.Stoich>0)*1;
            NegS = (this.Stoich<0)*1;
            RxnGene = this.rxnGeneMat*1;
            
            % Disregard hubmetabolites
            PosS(this.HubMetabolites,:) = 0;
            NegS(this.HubMetabolites,:) = 0;
            
            % Reactions sharing at least one educt and product
            % (disregarding hub metabolites)
            SharedEdAndPr = (PosS'*PosS >=N1)&(NegS'*NegS >=N2) | (PosS'*PosS >=N2)&(NegS'*NegS >=N1) |...
                (PosS'*NegS >=N1)&(NegS'*PosS >=N2) | (PosS'*NegS >=N2)&(NegS'*PosS >=N1);
            
            % Reactions not originating from the same model
            NoSharedOrig = (RxnOrig*RxnOrig')==0;
            
            % Gene sharing
            SharedGene = RxnGene'*RxnGene >=NG;
            
            [IDx(:,1),IDx(:,2)] = find(SharedEdAndPr&NoSharedOrig&SharedGene);
            IDx = unique(sort(IDx,2),'rows');
            groups = false(size(IDx,1),this.NumReactions);
            for i=1:size(IDx,1)
                groups(i,IDx(i,:)) = true;
            end
            
        end
        function [groups,cands] = findLumpedReactions(this,S,reactions)
            % This function finds lumped reactions by trying to obtain a
            % flux in the direction opposite to the normal direction for each
            % reaction. If this is possible, without any exchange
            % reactions, this implies that another set of reactions exists
            % which can together carry out exactly the same chemical
            % conversion as the first reaction.
            %%% OPTIONAL Input
            % S         Modified stoichiometric matrix to be used. Default:
            %           this.Stoich, after removal of ignored metabolites
            % reactions indices of the reactions for which non-lumped
            %           representations are to be searched for. Default:
            %           all
            %%% OUTPUT
            % groups    A logical matrix where every row corresponds to a
            %           set of reactions containing one or more candidate
            %           lumped reactions (The remaining reactions can
            %           together perform exactly the same net conversion of
            %           chemical compounds).
            % cands     A logical matrix with the same size as groups,
            %           containing the information for each group which
            %           reactions are candidate lumped representations.
            
            % Load information
            if nargin<2
                S = this.RemIgnored(this.Stoich);
            end
            if nargin>=3 && islogical(reactions)
                reactions = find(reactions);
            end
            
            Ext = this.is_external;
            org = this.RxnOrigin;
            Threshold   = this.options.Lumped_Threshold;   %The maximum number of reactions (and total flux) to be considered for lumping.
            MinGeneFrac = this.options.Lumped_MinGeneFrac; %The minimal fraction of the shared genes between lumped and non-lumped reaction sets for them to be considered.
            RGM = this.rxnGeneMat;
            
            % Alter S;
            %Ignore external reations
            S(:,Ext) = 0;
            
            %Alter S to correspond to irreversible reactions only.
            [Sirr,OrigIDx] = this.IrrS(S);
            
            %Remove empty reactions (only ignored metabolites)
            ERxn = ~any(Sirr,1);
            Sirr(:,ERxn)=[];
            OrigIDx(ERxn)=[];
            
            % Pre-allocate variables
            [NumMets,NumRxns] = size(Sirr);
            groups  = false(0,this.NumReactions); % Placeholder
            cands   = false(0,this.NumReactions); % Placeholder
            
            % Prepare LP
            %Solver options
            opt = struct( ...
                'Algorithm','interior-point', ...
                'Diagnostics','off', ...
                'Display','off', ...
                'LargeScale','on', ...
                'MaxIter',[], ...
                'Simplex','off', ...
                'TolFun',[]);
            
            %Set reaction bounds
            LB = zeros(NumRxns,1);
            UB =  ones(NumRxns,1) * 1000;
            
            %prepare objective and nulvector
            %Linprog optimizes with minimization criteria. In order to find
            %solutions involving a minimal number of reactions, a
            %positive objective value is assigned to every reaction in the
            %network. Only the reaction which is to be tried will receive a
            %negative objective value, which is on a different order of
            %magnitude to assure that the main criterium is to obtain a
            %flux through this reaction.
            c   = ones(NumRxns,1); % Effectively a penalty for flux through any other reaction in order to minimize futile cycles and such.
            beq = zeros(NumMets,1);
            
            % Select reactions for which to identify non-lumped
            % representations
            if nargin >=3
                Select = find(ismember(OrigIDx,reactions));
            else
                Select = 1:numel(OrigIDx);
            end
            NumCases = numel(Select);
            
            % Try to maximize every reaction in the opposite direction
            h = waitbar(0,'Searching for lumped reactions');
            for i=1:NumCases
                idx = Select(i);
                waitbar(i/NumCases)
                newGroups = false(1,this.NumReactions); % Initially fill with zeros for compatibility with ismember and to stop if no solution is found
                rejGroups = false(1,this.NumReactions); % Initially fill with zeros for compatibility with ismember and to stop if no solution is found
                f = c;
                f(idx) = 1000; % Objective function, linprog minimizes the objective, thus a positive objective value to maximize negative flux.
                lb = LB;
                lb(idx) = -1;
                while true % As soon as no new pathway is found, all elements in x will be zero, which corresponds to the empty row, which is already present in newGroups by default, thus triggering the break
                    %Find the reactions which together carry out the same
                    %net conversion
                    x = linprog(f,[],[],Sirr,beq,lb,UB,[],opt);
                    x(abs(x)<10^-6)=0; % Filter out reactions without significant flux
                    UsedRxns = x~=0;
                    newGroup = false(1,this.NumReactions);
                    newGroup(OrigIDx(UsedRxns)) = true; % Find all remaining reactions (using the original reaction indices)
                    if ismember(newGroup,[newGroups;rejGroups],'rows') || sum(x)>Threshold;
                        % This case was already found so no new similarly
                        % good case was identified. OR the group is too
                        % large to be considered
                        break
                    end
                    
                    if count(newGroup)<Threshold
                        %Check if all required reactions are present in a
                        %single model.
                        NonLumped = newGroup; NonLumped(OrigIDx(idx)) = false; %find those reactions which are not the candidate lumped reaction
                        if any(all(org(NonLumped,:),1)) && count(NonLumped)>=2 %All reactions share origin in a model and at least two other reactions are involved
                            newGroups(end+1,:) = newGroup; %#ok<AGROW> % Save group of candidate reactions
                        end
                    else
                        rejGroups(end+1,:) = newGroup; %#ok<AGROW>
                    end
                    
                    % Increase the penalty for all currently involved
                    % reactions. Hereby other potential unlumped
                    % representations of the reaction can be identified.
                    UsedRxns(idx) = false;
                    f(UsedRxns) = f(UsedRxns)*3;
                end
                newGroups(1,:) = []; % Remove first row as it was only a placeholder for compatibility with ismember and to stop if no solution was found
                
                %Check if the candidate lumped reactions corresponds to any
                %genes any of the other reactions corresponds to.
                keep=false(size(newGroups,1),1);
                for j=1:size(newGroups,1)
                    GroupExcRxn = newGroups(j,:);
                    GroupExcRxn(OrigIDx(idx)) = false;
                    frac = count(RGM(:,OrigIDx(idx))&any(RGM(:,GroupExcRxn),2))/count(RGM(:,OrigIDx(idx))|any(RGM(:,GroupExcRxn),2));
                    if frac>=MinGeneFrac || ~any(RGM(:,OrigIDx(idx))) || ~any(any(RGM(:,GroupExcRxn)))
                        keep(j)=true;
                    end
                    
                end
                newGroups(~keep,:) = [];
                
                if size(newGroups,1)>0
                    [~,IB] = ismember(newGroups,groups,'rows'); % See whether any groups were previously identified
                    cands(IB(IB~=0),OrigIDx(idx))=true; % If the group was already found, note that the currentl examined reaction is a candidate lumped representation
                    newGroups(IB~=0,:) = []; % Remove the previously identified groups
                    if size(newGroups)>0
                        groups = [groups ; newGroups]; %#ok<AGROW> % Add the new groups
                        newCands = false(size(newGroups));
                        newCands(:,OrigIDx(idx)) = true;
                        cands  = [cands  ; newCands]; %#ok<AGROW> % Expand the candidate lumped matrix to the right size
                    end
                end
            end
        end
        %compartment groups
        function groups = findUnkCompRxns(this)
            % Find reactions where for one or more of the metabolites the
            % compartment is unknown and find other reactions which are
            % highly similar (same metabolites but with known compartments)
            
            % Load Data
            LogS      = logical(this.Stoich);
            UnkMets   = strcmp(this.MetCompartment,'UNKCOMP');
            KnownMets = ~UnkMets;
            MetToSpec = this.MetToSpecies;
            
            % Get all rxns where one or more metabolites is unknown
            UnkCompRxns = find(~cellfun(@isempty,regexp(this.RxnComp,'UNKCOMP')));
            
            % For each of these reactions find whether there are other
            % reactions which use exactly the same metabolites (ignoring
            % stoichiometry)
            groups = false(numel(UnkCompRxns),this.NumReactions);
            for i=1:numel(UnkCompRxns)
                UnkRxnIDx = UnkCompRxns(i);
                groups(i,UnkRxnIDx) = true;
                KnownPart = LogS(:,UnkRxnIDx) & KnownMets;
                UnkPart   = LogS(:,UnkRxnIDx) & UnkMets;
                UnkPart   = MetToSpec(UnkPart);
                
                PotAltRxns = all(LogS(KnownPart,:),1); % Reactions which share all metabolites for which the compartment is known.
                PotAltRxns = PotAltRxns & ~any(this.RxnOrigin(:,this.RxnOrigin(UnkRxnIDx,:)),2)'; % Remove reactions originating from the same model
                PotAltRxns = find(PotAltRxns);
                
                for j=1:numel(PotAltRxns)
                    PotAltRxnIDx = PotAltRxns(j);
                    % Check whether the remainder of the metabolites match
                    % on the species Level
                    RemMets = LogS(:,PotAltRxnIDx) & ~KnownPart;
                    RemMets = MetToSpec(RemMets);
                    if all(ismember(RemMets,UnkPart)) && all(ismember(UnkPart,RemMets))
                        groups(i,PotAltRxnIDx) = true;
                    end
                end
            end
            groups = unique(groups,'rows');
        end
        function [InvTrans,InvCompConn] = findInvalidTransport(this)
            % This function finds all transport reactions which involve
            % metabolites in compartments which are not actually connected.
            
            % Load information
            Transport = this.is_transport;
            CompConn  = this.RxnComp;
            ValidConn = this.options.CompartmentConnections;
            
            InvTrans    = Transport & ~ismember(CompConn,ValidConn);
            InvCompConn = setdiff(CompConn(Transport),ValidConn);
        end
        function groups = findSameRxnDiffComp(this,S)
            % Find identical chemical conversions in different compartments. If S is
            % provided separately it needs to have the same number of metabolites as
            % this.Stoich.
            %%% OPTIONAL INPUT
            % S     Stoichiometric matrix to be used
            
            if nargin<2
                S = this.Stoich;
            else
                assert(size(S,1)==this.NumMetabolites,'Number of rows in S inconsistent with the number of metabolites in the model')
            end
            
            % Load information
            Skip  = this.is_external | this.is_transport;
            NumS = this.NumSpecies;
            NumR = this.NumReactions;
            NumC = this.NumCompartments;
            
            % Modify S
            S = this.RemIgnored(S); % Remove to-be ignored metabolites
            S(:,Skip) = 0;          % Ignore external/transport reactions
            BigS = this.BigS(S);    % Create extended stoichiometric matrix
            
            % Pre-allocate variables
            HashVals = cell(NumR,1);
            groups = false(0,NumR);
            
            % Generate hash values for reactions based on chemical conversion.
            Pos = [(1:NumS:(1+NumS*(NumC-1)))' (NumS:NumS:NumS*NumC)'];
            for i=1:NumR
                %Find compartment in which reaction is
                firstMet = find(BigS(:,i),1);
                MetComp  = ceil(firstMet/NumS);
                HashVals{i} = DataHash(BigS(Pos(MetComp,1):Pos(MetComp,2),i));
            end
            
            % Find identical reactions in different compartments
            [~,idx] = unique(HashVals);
            MultIDx = setdiff(1:NumR,idx); % For these reactions there are others with the same Hash Value;
            for i=1:1:numel(MultIDx);
                idx = MultIDx(i);
                if ~Skip(idx)
                    Set = ismember(HashVals,HashVals(idx));
                    if ~any(Skip(Set));
                        groups(end+1,:) = Set; %#ok<AGROW>
                    end
                    Skip(Set) = true; % Prevent a group from being added multiple times.
                end
            end
            
            if isempty(groups)
                return
            end
            
            % Filter groups that are not in different compartments
            groups2 = this.findDuplicateRxns(S);
            groups = setdiff(groups,groups2,'rows');
        end
        function groups = findMultipleTransportSameSpecSameComps(this)
            % Search for alternative transport reactions over the same
            % membrane for the same species.
            
            % Prepare empty output
            groups = false(0);
            
            % Load required information
            MetToSpec = this.MetToSpecies;
            LogStoich = logical(this.Stoich);
            MetComp   = this.MetCompartment;
            
            % For each species except H(+) find all reactions transporting it from one
            % compartment to another
            for i=1:this.NumSpecies
                % Find reactions involving more than one instance of the species
                MetIDx = MetToSpec==i;
                if count(MetIDx)>1 && ~ismember(this.species{i}.id,this.options.IgnoredSpec)
                    TransForSpec = sum(LogStoich(MetIDx,:))>1;
                    reactionIDx = find(TransForSpec);
                    
                    % find which compartment transition is made in each of
                    % these reactions. NOTE: cannot use RxnComp as the reaction
                    % could involve more than 2 compartments whereas the
                    % species of interest is only passed between two.
                    TransComps = cell(numel(reactionIDx),1);
                    for j=1:numel(reactionIDx)
                        comps = MetComp(LogStoich(:,reactionIDx(j)) & MetIDx);
                        comps = sort(comps);
                        compstr = comps{1};
                        for k=2:numel(comps)
                            compstr = [compstr '%' comps{k}]; %#ok<AGROW>
                        end
                        TransComps{j} = compstr;
                    end
                    
                    % Count how many reactions exist which transfer the
                    % metabolite between two compartments.
                    [uniqTransComps,~,idx] = unique(TransComps);
                    N = accumarray(idx(:),1,[],@sum);
                    
                    if any(N>1)
                        MultTrans     = find(N>1);
                        for j=1:numel(MultTrans)
                            MultTransRxns = reactionIDx(strcmp(uniqTransComps(MultTrans(j)),TransComps));
                            group = false(1,this.NumReactions);
                            group(MultTransRxns) = true;
                            groups(end+1,:) = group; %#ok<AGROW>
                        end
                    end
                end
            end
            groups = unique(groups,'rows'); % it is possible that a group would be found twice
        end
        function reactionIDx = findInvalidExternalRxns(this)
            % Finds invalid external reactions
            
            stoich = this.Stoich;
            NonExtMets   = ~ismember(this.MetCompartment,this.options.ValidExchange);
            NonExtMetRxns= any(stoich(NonExtMets,:),1);
            
            ExternalRxns = this.is_external';
            
            InvExcRxns     = NonExtMetRxns & ExternalRxns & ~this.is_biomass';
            reactionIDx   = find(InvExcRxns);
            
        end
        %species groups
        function groups = findSpeciesMatches(this)
            %This function searches for highly similar species which may
            %actually correspond to the same compound.
            
            disp('Searching for highly similar chemical species')
            
            % Load info
            FormMatch = this.matchSpecForm;
            CorrMatch = this.matchSpecCorr;
            
            % Find candidate matches
            %Exclude matches between same species
            SameSpec = diag(true(this.NumSpecies,1));
            PotentialMatch = FormMatch & CorrMatch & ~SameSpec;
            
            %Optional: Exclude matches between species from the same model
            if this.options.findSpeciesMatches_SameModel==0
                fprintf(['Do you want to include potential matches between species from the same model?\n' ...
                    'y: Yes, for this time\n' ...
                    'n: No, not now\n' ...
                    'Y: Yes, always\n' ...
                    'N: No, never\n']);
                reply = COMMGEN.InputProcessing({'y','n','Y','N'});
                switch reply
                    case('y')
                        ConsiderSameModel = 1;
                    case('n')
                        ConsiderSameModel = 0;
                    case('Y')
                        ConsiderSameModel = 1;
                        this.options.findSpeciesMatches_SameModel=1;
                    case('N')
                        ConsiderSameModel = 0;
                        this.options.findSpeciesMatches_SameModel=-1;
                end
            elseif this.options.findSpeciesMatches_SameModel ==-1
                ConsiderSameModel = 0;
            elseif this.options.findSpeciesMatches_SameModel ==1
                ConsiderSameModel = 1;
            end
            
            if ~ConsiderSameModel
                for i=1:this.NumModels
                    SpecSameModel = this.SpecOrigin(:,i);
                    PotentialMatch(SpecSameModel,SpecSameModel)=false;
                end
            end
            
            % Obtain possible pairs
            %Find indices
            IDx = zeros(count(PotentialMatch),2); %Pre-allocate variable
            [IDx(:,1),IDx(:,2)] = find(PotentialMatch);
            %Remove duplicate matches
            IDx = sort(IDx,2);
            IDx = unique(IDx,'rows');
            
            %Create groups
            groups = false(size(IDx,1),this.NumSpecies); %Pre-allocate variable
            for i=1:size(IDx,1)
                groups(i,IDx(i,:)) = true;
            end
            
            % Trim the groups by removing permanently rejected combinations
            groups = this.FilterGroups(groups,'species');
            
            if this.options.SimilarSpecies_OneReaction
                % Filter based on reactions
                groups = this.matchSpecRxns(groups);
            end
            
        end
        function [groups,SpcCoOcc,S] = findSimilarSpeciesPairs(this)
            %This function searches for pairs of species which tend to occur together
            %and for those instances of pairs which might correspond to the same
            %molecular species. (e.g., NAD / NADH vs. NAD+ & NADH(2-).
            %%% OUTPUT
            % groups    logical matrix corresponding to species pairs potentially
            %           corresponding to the same pair.
            % SpcCoOcc  Link to the original species for each group.
            % S         A modified stoichiometric matrix where each pair has been
            %           assigned a new row.
            
            % Load info
            SpcCoOcc = this.findSpecCoOccur;
            NumPairs = size(SpcCoOcc,1);
            S = logical(this.Stoich);
            MetOrg = this.MetOrigin;
            NMet = this.NumMetabolites;
            MTS = this.MetToSpecies;
            
            % Obtain modified version of the stoichiometric matrix
            S = [S ; false(NumPairs,this.NumReactions)];
            MetOrg = [MetOrg;false(NumPairs,size(MetOrg,2))];
            for i=1:size(SpcCoOcc)
                % find metabolite indices corresponding to species
                MetIDx1  = MTS==SpcCoOcc(i,1);
                MetIDx2  = MTS==SpcCoOcc(i,2);
                % find reactions corresponding to both pairs of the species
                SpecRxns = any(S(MetIDx1,:),1) & any(S(MetIDx2,:));
                % Add a new row to the matrix for each pair meaning: "involved in the reaction"
                S(NMet+i,SpecRxns)=true;
                MetOrg(NMet+i,:)= any(MetOrg(MetIDx1 | MetIDx2,:),1);
                % Remove the original metabolites from the matrix (in those reactions
                % where both species were found)
                S(MetIDx1|MetIDx2,SpecRxns) = false;
            end
            
            % Remove the part of the stoichiometric matrix which is not involved with
            % any species pairs.
            RxnsWithPairs = any(S(this.NumMetabolites+1:end,:),1);
            S(:,~RxnsWithPairs) = false;
            
            % Potential Matches
            Matches = true(NumPairs);
            
            % Filter species matches
            %Pairs can't match themselves
            Matches(1:NumPairs+1:NumPairs*NumPairs) = false;
            %Species pairs taking part in the same reaction can't match
            RxnsTwoPairs = sum(S(this.NumMetabolites+1:end,:),1)>1;
            RxnsTwoPairs = find(RxnsTwoPairs);
            for i=1:numel(RxnsTwoPairs)
                NoMatch = S(this.NumMetabolites+1:end,RxnsTwoPairs(i));
                Matches(NoMatch,NoMatch) = false;
            end
            %There should be a sufficiently high correlation between the
            %pairs
            CorrMatch = this.matchSpecCorrPair(S,MetOrg);
            CorrMatch = CorrMatch(NMet+1:end,NMet+1:end);
            Matches = Matches & CorrMatch;
            %There should be matching reactions between the pairs
            [IA(:,1),IA(:,2)] = find(Matches);
            IA = unique(sort(IA,2),'rows');
            IA(IA(:,1)==IA(:,2),:) = [];
            groups = false(size(IA,1),NMet+size(Matches,1));
            for i=1:size(IA,1)
                groups(i,NMet+IA(i,:)) = true;
            end
            groups = this.matchSpecRxns(groups,S);
            groups(:,1:NMet) = [];
        end
        
        % Processing groups
        %compartment groups
        function group = AutoResolveInvTrans(this,reactionIDx)
            
            % Loading information
            LogStoich = logical(this.Stoich);
            
            % Load how to automatically solve this case
            compstr = this.reactions{reactionIDx}.compartment;
            SolIDx  = strcmp(compstr,this.options.InvTransportSolution(:,1));
            ViaComp = this.options.InvTransportSolution(SolIDx,2);
            FreeCmp = this.options.InvTransportSolution(SolIDx,3);
            
            % Find the species for which the corresponding metabolites will be assigned to a different compartment
            MetToSpec = this.MetToSpecies;
            SpecIDs   = unique(MetToSpec(strcmp(FreeCmp,this.MetCompartment) & LogStoich(:,reactionIDx)));
            
            % Find out whether this particular case can be solved automatically
            % Find whether each species for which the
            % compartment would be altered is either still
            % absent in the intermediate compartment or already
            % has a 'free' reaction between the two
            % compartments for which this would otherwise be
            % added.
            FreeTransComps = sort([ViaComp FreeCmp]);
            FreeTrans = this.FreeTransport([FreeTransComps{1} this.DEL FreeTransComps{2}]);
            
            AutoSolve = false(numel(SpecIDs));
            for i=1:numel(SpecIDs)
                Mets = MetToSpec==SpecIDs(i);
                InComp = any(strcmp(ViaComp,this.MetCompartment(Mets)));
                AutoSolve = ~InComp || FreeTrans(SpecIDs(i));
            end
            
            if all(AutoSolve)
                % If yes, solve
                for i=1:numel(SpecIDs)
                    
                    if ~FreeTrans(SpecIDs(i))
                        this.addFreeTrans(SpecIDs(i),FreeCmp,ViaComp,this.reactions{reactionIDx});
                    else
                        % Else: adjust reaction origin
                        % Find the metabolites between which free transport
                        % is desired
                        LogStoich = logical(this.Stoich); % Has to be newly obtained in case a metabolite was added.
                        Mets = this.MetToSpecies==SpecIDs(i); % MetToSpecies has to be recalculated in case a metabolite was added.
                        FreeMetIDs = Mets & ismember(this.MetCompartment,FreeTransComps);
                        % The free reaction has only these two metabolites
                        FreeRxnID  = all(LogStoich(FreeMetIDs,:),1) & ~any(LogStoich(~FreeMetIDs,:),1);
                        % Indicate source rxns
                        this.reactions{find(FreeRxnID)}.SrcRxns = {this.reactions{find(FreeRxnID)} this.reactions{reactionIDx}}; %#ok<FNDSB>
                        this.reactions{find(FreeRxnID)}.Origin = any([this.reactions{FreeRxnID}.Origin;this.reactions{reactionIDx}.Origin]); %#ok<FNDSB>
                    end
                end
                
                % Create Alternative reaction
                this.ChangeCmp(reactionIDx,FreeCmp,ViaComp);
                
                % Create empty output variable
                group = [];
            else
                % If not: pass to other function
                group = this.ResolveInvTrans(reactionIDx);
            end
        end
        function group = ResolveInvTrans(this,reactionIDx)
            % Process the invalid transport reactions
            %%% Input
            % reactionIDx:     Index corresponding to invalid transport
            %                  reaction.
            
            % Default empty output
            group = false(this.NumReactions,1);
            group(reactionIDx) = true;
            
            % Load required information
            
            % Load information
            LogStoich       = logical(this.Stoich);
            HubMets         = this.HubMetabolites;
            MetToSpec       = this.MetToSpecies;
            
            HubSpec = false(this.NumSpecies,1);
            HubSpec(MetToSpec(HubMets)) = true;
            
            % Find which metabolite(s) is/are transported.
            RxnMets  = LogStoich(:,reactionIDx);
            RxnSpec  = MetToSpec(RxnMets);
            uniqSpec = unique(RxnSpec);
            if numel(uniqSpec)>1
                N        = hist(RxnSpec,uniqSpec);
                TransSpec= uniqSpec(N>1);
            else
                TransSpec = uniqSpec;
            end
            
            Transported = false(this.NumSpecies,1);
            Transported(TransSpec) = true;
            
            % Find metabolites for which alternative transport reactions
            % are to be displayed. If there is a non-hub metabolite which
            % is transported, ignore the hub metabolites.
            if count(Transported) > 1 && count(Transported & ~HubSpec) >0
                ShowSpec = Transported & ~HubSpec;
            else
                ShowSpec = Transported;
            end
            
            % Present the invalid reactions and decide how to proceed
            while true
                
                disp([char(10) 'Processing the invalid transport reaction:'])
                this.PrintEquations(group)
                
                % Find the species to search for
                ShowSpecIDx = find(ShowSpec);
                
                % Print all other transport reactions for the same species
                for i=1:numel(ShowSpecIDx)
                    this.PrintAllRxnsForSpec(this.species_IDs{ShowSpecIDx(i)},true);
                end
                
                disp([char(10) 'Please choose how to proceed.'                                char(10)...
                    'Recommended is either to merge the invalid reaction to an existing reaction, directly remove it or' char(10)...
                    'to first add (a) new reaction(s) which replaces the invalid reaction.'   char(10)...
                    ''                                                                        char(10)...
                    'Obtain additional information'                                           char(10)...
                    'D: Display the currently searched species'                               char(10)...
                    ''                                                                        char(10)...
                    'Change search parameters'                                                char(10)...
                    'E: Expand search to also show reactions for an additional species'       char(10)...
                    'S: Shrink search by removing a species from the search list'             char(10)...
                    'H: Change the hub metabolite list (hub metabolites ignored by default)'  char(10)...
                    ''                                                                        char(10)...
                    'Process reaction'                                                        char(10)...
                    'R: Remove the invalid reaction'                                          char(10)...
                    'M: Merge the invalid reaction with a different reaction'                 char(10)...
                    'A: Add new reaction (Will return here)'                                  char(10)...
                    'C: Change the reaction equation'                                         char(10)...
                    'K: Keep the invalid reaction as it is.'                                  char(10)...
                    ])
                
                reply = COMMGEN.InputProcessing({'D','E','S','H','R','M','A','K','C'});
                clc
                switch reply
                    case('D') %Display currently searched  species
                        disp(['The chemicals displayed on the left are used in the search for alternative transport reactions.' char(10)...
                            'A zero in any of the compartment columns means that this chemical is not present in this compartment'])
                        SearchedSpecies     = this.species_IDs(ShowSpecIDx);
                        SearchedSpeciesNames= this.IDToName(SearchedSpecies);
                        
                        MetOverview = cell(1+numel(SearchedSpecies),2+size(this.CompartmentIDToName,1));
                        MetOverview(1,:)     = ['ID','Name',this.CompartmentIDToName(:,1)'];
                        MetOverview(2:end,1:2) = [SearchedSpecies SearchedSpeciesNames];
                        MetOverview(2:end,3:end) = {false};
                        
                        for i=1:numel(ShowSpecIDx)
                            comps = this.MetCompartment(ismember(MetToSpec,ShowSpecIDx(i)));
                            MetOverview(i+1,3:end) = num2cell(ismember(this.CompartmentIDToName(:,1),comps));
                        end
                        disp(MetOverview)
                    case('E') %Expand search to additional species
                        disp('The invalid reaction:')
                        this.PrintEquations(this.reactions(reactionIDx))
                        disp('The current search list:')
                        this.species_IDs(ShowSpec);
                        disp('Which species would you like to include in the search?')
                        reply = COMMGEN.InputProcessing([this.species_IDs(~ShowSpec);this.IDToName(this.species_IDs(~ShowSpec))]);
                        IDx   = strcmp(this.species_IDs,this.NameToID(reply));
                        ShowSpec(IDx) = true;
                    case('S') %Shrink search by removing a species
                        disp('The current search list:')
                        this.species_IDs(ShowSpec);
                        disp(SearchSpecies)
                        disp('Which metabolite would you like to remove from the search?')
                        reply = COMMGEN.InputProcessing([this.species_IDs(ShowSpec);this.IDToName(this.species_IDs(ShowSpec))]);
                        IDx   = strcmp(this.species_IDs,this.NameToID(reply));
                        ShowSpec(IDx) = false;
                    case('H') %Change the hub metabolites
                        this.ChangeHubMets;
                    case('R') %Remove invalid reaction
                        this = this.MarkForRemoval(reactionIDx);
                        group = [];
                        break;
                    case('M') %Merge reactions
                        disp(['With which reaction(s) would you like to merge the invalid reaction? (enter reaction_id)' char(10)
                            'C: Cancel' char(10)
                            'M: All reactions selected, merge!'])
                        group = false(this.NumReactions,1);
                        group(reactionIDx) = true;
                        while true
                            reply = COMMGEN.InputProcessing([this.reaction_IDs;'C';'M']);
                            if strcmp(reply,'C')
                                break;
                            elseif strcmp(reply,'M')
                                if sum(group)>1
                                    break;
                                else
                                    disp('First select at least one other reaction')
                                end
                            else
                                IDx   = strcmp(reply,this.reaction_IDs);
                                group(IDx) = true;
                            end
                        end
                        if strcmp(reply,'M')
                            group = [];
                            break
                        end
                    case('A') %Add new reaction
                        this = this.EnterNewReaction(this.reactions{reactionIDx});
                    case('C') %Change equation
                        disp([char(10) 'Entering a new equation to replace:'])
                        this.PrintEquations(reactionIDx)
                        this.reactions{reactionIDx}.equation = this.EnterEquation();
                        this.reactions{reactionx} = this.reactions{reactionIDx}.getReactants;
                    case('K') %Keep reaction as is
                        group = [];
                        break;
                end
            end
        end
        function ProcessSameRxnDiffComp(this,groups,InAGroup)
            % Iteratively processing groups which contain dead-end reactions, then all with inactive reactions then the rest.
            grouptype=1;
            % grouptype=1: Process groups containing dead-end reactions
            % grouptype=2: Process groups containing inactive reactions
            % grouptype=3: Process the remaining groups
            
            % Load info
            RxnOrigin = this.RxnOrigin;
            
            % Finding out which kind of groups are currently being processed
            [dR,~,iR] = findDeadEnds(this);
            
            if this.options.SameRxnDiffComp_IgnoreAllInact ==1 % if groups where every reaction is inactive should be ignored
                RemoveGroup = false(size(groups,1),1);
                NumGroups = size(groups,1);
                for j=1:size(groups,1)
                    if all(iR(groups(j,:)))
                        % All reactions in the group are inactive, the
                        % group can be ignored.
                        RemoveGroup(j) = true;
                        this.RejectMerge(groups(j,:));
                    end
                end
                groups(RemoveGroup,:) = [];
                if size(groups,1) < NumGroups
                    fprintf('%d out of %d remaining groups were removed due to all reactions being inactive\n',NumGroups-size(groups,1),NumGroups)
                end
                if isempty(groups)
                    return
                end
            end
            
            if this.options.SameRxnDiffComp_AllActSameModel == 1 % Are there any groups which should not be merged as all reactions come from the same model?
                RemoveGroup = false(size(groups,1));
                NumGroups = size(groups,1);
                for j=1:size(groups,1)
                    Origin = this.RxnOrigin(groups(j,:));
                    if any(all(Origin))
                        % All reactions have an origin in a common
                        % model. The merge will be automatically
                        % cancelled.
                        RemoveGroup(j) = true;
                        this.RejectMerge(groups(j,:));
                    end
                end
                groups(RemoveGroup,:) = [];
                if size(groups,1) < NumGroups
                    fprintf('%d out of %d remaining groups were removed due to all reactions originating from the same model\n',NumGroups-size(groups,1),NumGroups)
                end
                if isempty(groups)
                    return
                end
            end
            
            if grouptype==1 % Are there any groups with dead-end reactions?
                disp('Searching for groups involving dead end reactions')
                SelGroups = find(any(groups(:,dR),2));
                if isempty(SelGroups)
                    disp('None Found')
                    grouptype = 2;
                end
            end
            
            if grouptype==2 % Are there any groups with inactive reactions?
                disp('Searching for groups involving inactive reactions')
                SelGroups = find(any(groups(:,iR),2));
                if isempty(SelGroups)
                    disp('None Found')
                    grouptype = 3;
                end
            end
            
            if grouptype==3
                if ~isempty(groups)
                    disp('Processing the remainder of the groups')
                    SelRxns = true(this.NumReactions,1);
                    SelGroups = find(any(groups(:,SelRxns),2));
                else
                    return
                end
            end
            
            % Processing the groups
            for i=1:numel(SelGroups)
                % Find reactions corresponding to the group
                reactionIDx = find(groups(SelGroups(i),:));
                
                % If dead end reactions may automatically be removed,
                % remove before printing any information. If the set of
                % reactions can be reduced to 1 by removing dead ends
                % there need be no user interaction.
                if grouptype <= this.options.SameRxnDiffComp_RemoveDead && ... % The type of group should be processed automatically if
                        ~all(iR(reactionIDx)) && ... % There is at least one active reaction in the group
                        any(iR(reactionIDx)) % There is at least one dead-end / inactive reaction in the group
                    Alive = find(~iR(reactionIDx));
                    Dead  = iR(reactionIDx);
                    for j=1:numel(Alive)
                        group = false(1,this.NumReactions);
                        group(reactionIDx(Dead))  = true;
                        group(reactionIDx(Alive(j))) = true;
                        AliveIDx = count(group(1:reactionIDx(Alive(j))));
                        % Merge where the id and equation of the functional
                        % reaction are kept. Whether the GPR rule is kept
                        % as the original or whether the different ones are
                        % merged depends on
                        % this.options.SameRxnDiffComp_AutoGPR
                        this.MergeReactions(group,AliveIDx*this.options.SameRxnDiffComp_Merge);
                    end
                else
                    % Display reaction information
                    ReactionDeletionConsequence(this,groups(SelGroups(i),:),InAGroup)
                    
                    disp(['Should any of the reactions be removed or merged with an existing reaction?' char(10) ...
                        'R: Select a reaction for removal' char(10) ...
                        'M: Select two or more reactions for merging (If only two reactions available, automatically chosen)' char(10) ...
                        'C: Continue. Do not merge these reactions' char(10) ...
                        ])
                    reply = COMMGEN.InputProcessing({'R','M','C'});
                    
                    switch reply
                        case('R')
                            disp('Choose which reaction to remove (enter reaction ID) or enter C to cancel')
                            AllInputs = [this.reaction_IDs;'C'];
                            RxnChoice = COMMGEN.InputProcessing(AllInputs);
                            if ~strcmp('C',RxnChoice)
                                RxnChoice = strcmp(RxnChoice,this.reaction_IDs);
                                this.MarkForRemoval(RxnChoice);
                                UpdRxns = setdiff(reactionIDx,RxnChoice);
                                for j=1:numel(UpdRxns)
                                    this.reactions{UpdRxns(j)}.Origin = any([RxnOrigin(RxnChoice,:);RxnOrigin(UpdRxns(j),:)],1);
                                end
                            end
                        case('M')
                            if numel(reactionIDx)==2
                                this.MergeReactions(groups(SelGroups(i),:));
                            else
                                %Find reaction indices
                                CG = groups(SelGroups(i),:);
                                opts = find(CG);
                                this.PrintEquations(CG);
                                %Select to-be merged reactions
                                RxnChoice = input('\nChoose numbers corresponding to to-be merged reactions\n');
                                %merge
                                this.MergeReactions(opts(RxnChoice));
                            end
                        case('C')
                            this.RejectMerge(groups(SelGroups(i),:));
                    end
                    clc
                end
            end
        end
        function ResolveInvExt(this,reactionIDx)
            %Propose to split invalid external reactions in two. The
            %original reaction is turned into a transport reaction and a
            %new reaction X@ext <==> X@bound is added. If only a single
            %metabolite is involved in a reaction this is performed
            %automatically.
            
            % Loading information
            MTS = this.MetToSpecies;
            S = this.RemIgnored(this.Stoich);
            logS = logical(S);
            NewExt = {};
            
            for i=1:numel(reactionIDx)
                % Find reaction
                idx = reactionIDx(i);
                
                % If single metabolite in reaction, automatic
                if count(logS(:,idx))==1
                    spec = this.species_IDs{MTS(logS(:,idx))};
                    eq = this.reactions{idx}.equation;
                    % change reaction
                    %check on which side of the equation the species is
                    if isempty(regexp(eq,[spec '.*[<>]'],'once')) %productside
                        Neq = ['1 ' spec '@MNXC2 ' eq];
                    else %substrate side
                        Neq = [eq ' 1 ' spec '@MNXC2'];
                    end
                    disp(Neq)
                    this.reactions{idx}.external = 0;
                    [this.reactions{idx}.lb,this.reactions{idx}.ub] = this.EqToBounds(this.reactions{idx}.equation);
                    this.ChangeEq(idx,Neq);
                    
                    %add reaction
                    Rxn = MergedReaction([],this.reactions{idx}.Origin);
                    Rxn.SrcRxns = this.reactions(idx);
                    Rxn.id       = [spec 'EXCHANGE'];
                    Rxn.equation = ['1 ' spec '@MNXC2 <==>'];
                    Rxn.biomass  = 0;
                    Rxn.comment  = ['orig:%' num2str(Rxn.Origin) '%'];
                    Rxn.external = 1;
                    Rxn = Rxn.getReactants;
                    Rxn.lb = this.LBdef;
                    Rxn.ub = this.UBdef;
                    
                    % Adding the new reaction to the model
                    this.addReaction(Rxn);
                    this = TrackRxn(this,'NewRxn','',Rxn);
                    % Else, ask user for input, if manual resolving is desired.
                elseif count(logS(:,idx))==0 && count(S(:,idx))==1
                    spec = this.species_IDs{MTS(S(:,idx))};
                    eq = this.reactions{idx}.equation;
                    % change reaction
                    %check on which side of the equation the species is
                    if isempty(regexp(eq,[spec '.*[<>]'],'once')) %productside
                        Neq = ['1 ' spec '@MNXC2 ' eq];
                    else %substrate side
                        Neq = [eq ' 1 ' spec '@MNXC2'];
                    end
                    disp(Neq)
                    this.reactions{idx}.external = 0;
                    this.ChangeEq(idx,Neq);
                    
                    %add reaction
                    Rxn = MergedReaction([],this.reactions{idx}.Origin);
                    Rxn.id       = [spec 'EXCHANGE'];
                    Rxn.equation = ['1 ' spec '@MNXC2 <==>'];
                    Rxn.biomass  = 0;
                    Rxn.comment  = ['orig:%' num2str(Rxn.Origin) '%'];
                    Rxn.external = 1;
                    Rxn = Rxn.getReactants;
                    Rxn.lb = this.LBdef;
                    Rxn.ub = this.UBdef;
                    
                    % Adding the new reaction to the model
                    this.addReaction(Rxn);
                    this = TrackRxn(this,'NewRxn','',Rxn);
                elseif ~this.options.AllAuto
                    clc
                    fprintf('Processing %d of %d invalid external reactions\n\n',i,numel(reactionIDx));
                    fprintf(['The following reaction is indicated to be a boundary/external reaction, which corresponds to exchange with the medium.\n' ...
                        'However, some of the involved metabolites are in cellular compartments, rather than extracellular.\n' ...
                        'Please choose how to continue:\n' ...
                        'C: Continue without changing anything\n' ...
                        'A: Split the reaction in a transport reaction + a boundary reaction (recommended)\n' ...
                        'N: Indicate that the reaction is not a boundary/external reaction\n\n'])
                    this.PrintEquations(idx,false);
                    reply = COMMGEN.InputProcessing({'C','A','N'});
                    switch reply
                        case 'N'
                            this.reactions{idx}.external = 0;
                            id = this.reactions{idx}.id;
                            this.reactions{idx}.id = [id '_NoExt'];
                            this.TrackRxn('ExtChanged',id,this.reactions{i})
                        case 'A'
                            clc
                            fprintf(['First, the original external reaction will be changed into a transport reaction between the extracellular compartment and the compartment the reaction is in now.\n' ...
                                'For the species newly added to the extracellular compartment a boundary reaction will be automatically created.' ...
                                'For example:\n' ...
                                '--> A@cyt + B@cyt\n' ...
                                'Could become:\n' ...
                                'A@ext + B@ext --> A@cyt + B@cyt\n' ...
                                'However, in other cases not all species can be added to the other side of the equation, for example:\n' ...
                                'ATP@cyt + H2O@cyt --> ADP@cyt + H@cyt + PI@cyt + X@cyt\n' ...
                                'Should become:\n' ...
                                'ATP@cyt + H2O@cyt + X@cyt--> ADP@cyt + H@cyt + PI@cyt + X@cyt\n\n' ...
                                'Please enter an alternative equation for:\n\n'])
                            this.PrintEquations(idx,false);
                            Neq = this.EnterEquation('',false);
                            this.ChangeEq(idx,Neq);
                            this.reactions{idx}.external = 0;
                            %C Check if any species weren't found earlier
                            %in the extracellular environment
                            for j=1:this.reactions{idx}.size
                                Reactant = {this.reactions{idx}.reactants{j}.id};
                                if ~ismember(Reactant,[this.metabolites;NewExt])
                                    spec = this.reactions{idx}.reactants{j}.chemical_id;
                                    %add reaction
                                    Rxn = MergedReaction([],this.reactions{idx}.Origin);
                                    Rxn.id       = [spec 'EXCHANGE'];
                                    Rxn.equation = ['1 ' spec '@MNXC2 <==>'];
                                    Rxn.biomass  = 0;
                                    Rxn.external = 1;
                                    Rxn.comment  = ['orig:%' num2str(Rxn.Origin) '%'];
                                    Rxn = Rxn.getReactants;
                                    Rxn.lb = this.LBdef;
                                    Rxn.ub = this.UBdef;
                                    
                                    % Adding the new reaction to the model
                                    this.addReaction(Rxn);
                                    this = TrackRxn(this,'NewRxn','',Rxn);
                                end
                            end
                    end
                    
                end
            end
        end
        function groups = AutoUnkCompRes(this,groups)
            %This function attempts to automatically resolve cases where
            %exactly one alternative is found for a reaction with an
            %unknown compartment. The condition for this is that either the
            %gene associations match or are a perfect subset of the other
            %or are both empty. If matched, the reaction equation is first
            %adjusted and only then passed to MergeReactions such that the
            %default settings concerning the arrows can still be used.
            %%% INPUT
            % groups    A logical matrix where each row corresponds to a
            %           pair of two reactions: one with a metabolite in an
            %           unknown compartment and the other corresponding to
            %           the exact same chemical conversion but not having a
            %           metabolite in the unknown compartment.
            %%% OUTPUT
            % groups    The groups which could not be solved in this
            %           automatic manner.
            
            NumGroups = size(groups,1);
            KeepGroup = false(NumGroups,1);
            
            for i=1:NumGroups;
                IDx = find(groups(i,:));
                Rxn1 = this.reactions{IDx(1)};
                Rxn2 = this.reactions{IDx(2)};
                
                if Rxn1.size ~=Rxn2.size
                    KeepGroup(i) = true; % The reaction is not a perfect match even though it is related to exactly the same species
                else
                    if strcmp(Rxn1.proteins,Rxn2.proteins) % Either the GPR matches exactly or both are empty
                        Success = true;
                        GPRChoice = 1; % as both are identical, the choice doesn't matter.
                    else % Check if the GPR from one is a perfect subset of the other
                        GPR1 = regexprep(Rxn1.proteins,{'\s+','[()]'},{'',''}); % Removing brackets is alright here as we are only checking whether the reactions are associated with the same genes (though one may be a subset of the other)
                        GPR2 = regexprep(Rxn2.proteins,{'\s+','[()]'},{'',''});
                        GeneList1 = regexp(GPR1,'[+;]','split');
                        GeneList2 = regexp(GPR2,'[+;]','split');
                        if all(ismember(GeneList1,GeneList2))
                            Success = true;
                            GPRChoice = 2; % Genelist 1 is a perfect subset of 2, we thus keep 2
                        elseif all(ismember(GeneList2,GeneList1));
                            Success = true;
                            GPRChoice = 1; % Genelist 2 is a perfect subset of 1, we thus keep 1
                        else % Neither of the gene lists are a perfect subset of the other.
                            Success = false;
                            KeepGroup(i) = true;
                        end
                    end
                    if Success
                        % Find out which reaction contains the unknown
                        % compartment
                        if strfind(this.RxnComp{IDx(1)},'UNKCOMP');
                            this.MergeReactions([IDx(1) IDx(2)],[0 2 GPRChoice])
                        else
                            this.MergeReactions([IDx(1) IDx(2)],[0 1 GPRChoice])
                        end
                        this.MergeReactions(groups(i,:),[0 0 GPRChoice]);
                    end
                end
            end
        end
        function ManUnkCompRes(this,IDx)
            % This function presents the remaining reactions which have at
            % least one metabolite in the unknown compartment. Also all
            % other reactions for the same (non-hub) species are printed
            % as well as all the compartments which are present in the model.
            
            fprintf('The following reaction has a metabolite in the unknown compartment:\n')
            this.PrintEquations(IDx)
            
            fprintf('Overview of all compartments in the model:\n')
            disp(this.CompartmentIDToName)
            while true
                fprintf('Choose how to proceed\n')
                fprintf('K: Keep reaction as it is\n')
                fprintf('A: Enter alternative reaction equation.\n')
                fprintf('M: Merge reaction with one of the other reactions\n')
                fprintf('P: Print all reactions for a certain species\n')
                fprintf('C: Print all compartment-spanning reaction for a certain species\n')
                reply = COMMGEN.InputProcessing({'K','A','M','P','C'});
                switch reply
                    case 'K'
                        return
                    case 'A'
                        equation = '';
                        this.PrintEquations(IDx,false)
                        equation = EnterEquation(this,equation);
                        this.reactions{IDx}.equation = equation;
                        return
                    case 'M'
                        fprintf('Please enter the reaction ID of the reaction with which to merge\n')
                        RxnChoice = COMMGEN.InputProcessing(reaction_IDs);
                        RxnIDx = find(strcmp(this.reaction_IDs,RxnChoice));
                        if IDx == RxnIDx
                            fprintf('Cannot merge reaction with itself\n')
                        else
                            group = false(1,this.NumReactions);
                            group([IDx RxnIDx]) = true;
                            this.MergeReactions(group);
                            return
                        end
                    case 'P'
                        fprintf('Choose the species for which to print all reactions\n')
                        SpecChoice = COMMGEN.InputProcessing(this.SpeciesIDToName);
                        this.PrintAllRxnsForSpec(SpecChoice)
                    case 'C'
                        fprintf('Choose the species for which to print all reactions\n')
                        SpecChoice = COMMGEN.InputProcessing(this.SpeciesIDToName);
                        this.PrintAllRxnsForSpec(SpecChoice,true)
                end
            end
        end
        %species groups
        function ProposeSpeciesMergeSingleMatch(this,groups)
            % This function proposes to merge the species for which only a single match
            % has been found.
            
            % Load info
            NG = size(groups,1);
            
            % Pre-allocate variables
            
            % Find indices of matches pairs
            Pairs = zeros(NG,2);
            for i=1:NG
                Pairs(i,:) = find(groups(i,:));
            end
            
            % Propose matches
            Present = 1:NG;
            while true;
                %Stop if no remaining proposals
                if isempty(Present)
                    break
                end
                
                fprintf('Candidate identical species:\n')
                IDs = this.IDToName(this.species_IDs(Pairs(Present,:)));
                if numel(IDs)==2
                    IDs = IDs';
                end
                disp([num2cell(Present)' IDs])
                disp(['Please choose how to proceed:' char(10)...
                    'R: Reject one or more of the merges' char(10)...
                    'I: Obtain more information on a particular match' char(10)...
                    'P: Print all reactions corresponding to one of the matches' char(10)...
                    'C: Continue (Accept all remaining matches)' char(10)...
                    ])
                reply = COMMGEN.InputProcessing({'R','I','P','C'});
                
                switch(reply)
                    case('R') % Reject merges
                        I = input('Please enter the indices of pairs which are to be rejected\nFor example: 1 or [1 2 3]\nAlternatively, enter 0 to cancel\n');
                        if ~(numel(I)==1 && I==0) && ~isempty(Present)
                            Present = setdiff(Present,I);
                            for i=1:numel(I)
                                this.RejectSpeciesMerge(Pairs(I(i),:));
                            end
                        end
                        clc
                    case('I') % More information on match
                        I = input('Please select the group(s) on which to get more information\nFor example: 1 or [1 2 3]\nAlternatively, enter 0 to cancel\n');
                        if ~(numel(I)==1 && I==0) && ~isempty(Present)
                            for i=1:numel(I)
                                this.PrintSpeciesInformation(Pairs(I(i),:),true)
                            end
                        end
                    case('P') % Print all reactions for species
                        disp('Please enter the ID/name of the species for which all reactions should be printed')
                        reply = COMMGEN.InputProcessing(this.SpeciesIDToName);
                        this.PrintAllRxnsForSpec(reply);
                    case('C')
                        for i=1:numel(Present)
                            this.MergeTwoSpecies(Pairs(Present(i),:));
                        end
                        break
                end
            end
        end
        function ProposeSpeciesMergeMultMatch(this,groups)
            % This function proposes to merge the species for which multiple matches
            % were found.
            
            % Load info
            NumGroups = size(groups,1);
            I = [];
            while any(any(groups));
                
                % find the species with the most matches
                if isempty(I);
                    [~,I] = max(sum(groups,1));
                end
                
                fprintf('\n%d out of %d original matches still to be processed\n',count(groups)/2,NumGroups);
                fprintf('\nFound potential alternatives for \n\n%s\n\n',this.species{I}.name);
                
                % Identify all groups the species occurs in
                GroupIDx = find(groups(:,I));
                AltSpec1 = cell(numel(GroupIDx),2);
                AltSpec2 = zeros(numel(GroupIDx),1);
                
                SpecIDx = find(any(groups(GroupIDx,:),1));
                SpecIDx = setdiff(SpecIDx,I);
                for i=1:numel(SpecIDx)
                    % Text information
                    AltSpec1{i,1} = this.species{SpecIDx(i)}.id;
                    AltSpec1{i,2} = this.species{SpecIDx(i)}.name;
                    % Numeric information
                    AltSpec2(i,1) = count(groups(:,SpecIDx(i)));
                end
                
                % Present all matches for a species
                AltProp = dataset(SpecIDx',AltSpec1(:,1),AltSpec1(:,2),AltSpec2(:,1),'VarNames',{'index','ID','Species','NumOwnMatches'});
                disp(AltProp)
                
                disp(['Please choose how to proceed:' char(10)...
                    'M: Select one of the species to merge with' char(10)...
                    'D: Select multiple of the species to merge with' char(10)...
                    '   In this case the merge will be on single a reaction basis.' char(10) ...
                    '   One reaction may correspond to several others' char(10) ...
                    'F: Inspect the alternative matches for one of the other species' char(10)...
                    'R: Reject (some) merges for this species' char(10)...
                    'I: Obtain more information on a particular match' char(10)...
                    'P: Print all reactions corresponding to one of the species char(10)' char(10)...
                    ])
                reply = COMMGEN.InputProcessing({'M','D','F','R','I','P'});
                
                switch(reply)
                    case('M') % Merge to one
                        if count(SpecIDx)==1
                            this.MergeTwoSpecies(SpecIDx);
                            groups(:,SpecIDx) = false;
                            I = [];
                        else
                            reply = input('Please enter the index of the species to match with or enter C to cancel');
                            if isstr(reply) && strcmp(reply,'C')
                                fprintf('Merge cancelled.')
                            elseif isnumeric(reply) && numel(reply)==1 && ismember(reply,SpecIDx)
                                group = false(this.NumSpecies,1);
                                group([I reply]) = true;
                                this.MergeTwoSpecies(group);
                                groups(:,[I SpecIDx]) = false;
                                I = [];
                            else
                                fprintf('Input not recognised. Merge cancelled.')
                            end
                        end
                    case('D') % Merge to multiple
                        reply = input('Please enter the indices of the species to match with, or enter ''C'' to cancel, or ''A'' to match with all suggested species.','s');
                        if strcmp(reply,'C')
                            fprintf('Merge cancelled.')
                        elseif strcmp(reply,'A')
                            fprintf('Should %s or the other compounds be retained after merging?\nK: Keep %s\nD: Keep the rest',this.SpeciesIDToName{I,2},this.SpeciesIDToName{I,2})
                            reply = COMMGEN.InputProcessing({'K','D'});
                            if strcmp(reply,'K')
                                this.SpecMultToOne(SpecIDx,I);
                            else
                                this.SpecOneToMult(SpecIDx,I);
                                return %Prevent multiple compounds from being split simultaneously, this could otherwise result in an explosion in the number of reactions.
                            end
                            I = [];
                            groups(:,SpecIDx) = false;
                        elseif ~isempty(str2num(reply)) && all(ismember(str2num(reply),SpecIDx)) %#ok<ST2NM>
                            SpecIDx = str2num(reply); %#ok<ST2NM>
                            fprintf('Should %s or the other compounds be retained after merging?\nK: Keep %s\nD: Keep the rest',this.SpeciesIDToName{I,2},this.SpeciesIDToName{I,2})
                            reply = COMMGEN.InputProcessing({'K','D'});
                            if strcmp(reply,'K')
                                this.SpecMultToOne(SpecIDx,I);
                            else
                                this.SpecOneToMult(SpecIDx,I);
                                return %Prevent multiple compounds from being split simultaneously, this could otherwise result in an explosion in the number of reactions.
                            end
                            I = [];
                            groups(:,SpecIDx) = false;
                        else
                            fprintf('Input not recognised. Merge cancelled.')
                        end
                    case('F') % Change focus
                        disp('Choose species (Enter name or ID)');
                        SpecIDx = any(groups,1);
                        reply = COMMGEN.InputProcessing(this.SpeciesIDToName(SpecIDx,:));
                        reply = this.NameToID(reply);
                        I = strcmp(this.SpeciesIDToName(:,1),reply);
                    case('R') % Reject merges
                        fprintf('Please enter the names corresponding to the to-be rejected proposals.\nFormat: ID1%sID2%s... / Name1%sName2%s...\n',this.DEL,this.DEL,this.DEL,this.DEL)
                        disp('Or enter C to cancel')
                        disp('Or enter A to reject all proposals')
                        reply = input('','s');
                        if ~strcmp(reply,'C')
                            if strcmp(reply,'A')
                                SpecIDx = find(any(groups(GroupIDx,:),1));
                                RejGroups = groups(:,I) & any(groups(:,SpecIDx),2);
                                groups(RejGroups,:) = false;
                                for i=1:numel(SpecIDx)
                                    this.RejectSpeciesMerge([I,SpecIDx(i)]);
                                end
                                I = [];
                            else
                                Names = splitstr(reply,this.DEL);
                                Names = this.NameToID(Names);
                                Found = ismember(Names,this.SpeciesIDToName(:,1));
                                if all(Found)
                                    SpecIDx = find(ismember(this.SpeciesIDToName(:,1),Names));
                                    RejGroups = groups(:,I) & any(groups(:,SpecIDx),2);
                                    groups(RejGroups,:) = false;
                                    for i=1:numel(SpecIDx)
                                        this.RejectSpeciesMerge([I,SpecIDx(i)]);
                                    end
                                    I = [];
                                else
                                    disp('The following names were not recognized. Please try again.')
                                    disp(Names(~Found))
                                end
                            end
                        end
                        clc
                    case('I') % More information on match
                        SpecIDx = any(groups(GroupIDx,:),1);
                        if count(SpecIDx)==2
                            this.PrintSpeciesInformation(SpecIDx,true)
                        else
                            disp('Please enter the species name corresponding to the match')
                            SpecIDx(I) = false;
                            reply = COMMGEN.InputProcessing(this.SpeciesIDToName(SpecIDx,:));
                            reply = this.NameToID(reply);
                            SpecIDx = find(strcmp(this.SpeciesIDToName(:,1),reply));
                            this.PrintSpeciesInformation([I SpecIDx],true)
                        end
                    case('P') % Print all reactions for species
                        disp('Please enter the ID/name of the species for which all reactions should be printed')
                        reply = COMMGEN.InputProcessing(this.SpeciesIDToName);
                        this.PrintAllRxnsForSpec(reply);
                end
                groups(sum(groups,2)==1,:)=false;
            end
        end
        function ProposeSpeciesPairsMerge(this,groups,Sets)
            % This function prints some general information about the
            % input species.
            %%% INPUT
            % Sets  Matrix with 4 columns. The first 2 and the last 2
            %       columns correspond to species pairs. The pair on
            %       columns 1,2 is thus matched with the pair on column
            %       3,4.
            
            SpecIDs = this.species_IDs(Sets);
            PairIDs = strcat(SpecIDs(:,1),'___',SpecIDs(:,2));
            
            SpecNames = this.IDToName(SpecIDs);
            PairNames = strcat(SpecNames(:,1),'___',SpecNames(:,2));
            
            % Load info
            NumGroups = size(groups,1);
            I = [];
            while any(any(groups));
                
                % find the species with the most matches
                if isempty(I);
                    [~,I] = max(sum(groups,1));
                end
                
                fprintf('\n%d out of %d original matches still to be processed\n',count(groups)/2,NumGroups);
                fprintf('\nFound potential alternatives for \n\n%s\n\n',PairNames{I});
                
                % Identify all the groups the species occurs in
                GroupIDx = find(groups(:,I));
                AltSpec1 = cell(numel(GroupIDx),2);
                AltSpec2 = zeros(numel(GroupIDx),3);
                for i=1:numel(GroupIDx)
                    % Identify the other species in the groups
                    SpecIDx = groups(GroupIDx(i),:);
                    SpecIDx(I) = false;
                    % Text information
                    AltSpec1{i,1} = PairIDs{SpecIDx};
                    AltSpec1{i,2} = PairNames{SpecIDx};
                    % Numeric information
                    AltSpec2(i,1) = count(groups(:,SpecIDx));
                end
                
                % Present all matches for a species
                AltProp = dataset(AltSpec1(:,1),AltSpec1(:,2),AltSpec2(:,1),'VarNames',{'ID','PairName','NumOwnMatches'});
                disp(AltProp)
                
                disp(['Please choose how to proceed:' char(10)...
                    'M: Select one of the pairs to merge with' char(10)...
                    ...%         'D: Select multiple of the pairs to merge with' char(10)... To be
                    ...%         done
                    ...%'   In this case the merge will be on single a reaction basis.' char(10) ...
                    ...%'   One reaction may correspond to several others' char(10) ...
                    'F: Inspect the alternative matches for one of the other pairs' char(10)...
                    'R: Reject (some) merges for this pair' char(10)...
                    'P: Print all reactions corresponding to one of the species char(10)' char(10)...
                    ])
                reply = COMMGEN.InputProcessing({'M','F','R','P'}); % D
                
                switch(reply)
                    case('M') % Merge one pair with one pair
                        SpecIDx = find(any(groups(GroupIDx,:),1));
                        if count(SpecIDx)==2
                            AllCombs = allcomb(Sets(SpecIDx(1),:),Sets(SpecIDx(2),:));
                            Newgroups = false(4,this.NumSpecies);
                            for i=1:4
                                Newgroups(i,AllCombs(i,:)) = true;
                            end
                            this.ProposeSpeciesMergeMultMatch(Newgroups);
                            I = [];
                            groups(GroupIDx,:) = [];
                        else
                            disp('Please enter the pair ID or enter C to cancel')
                            SpecIDx(I) = [];
                            reply = COMMGEN.InputProcessing([PairIDs(SpecIDx,1);'C']);
                            if ~strcmp(reply,'C')
                                SpecIDx = strcmp(PairIDs,reply);
                                AllCombs = allcomb(Sets(SpecIDx,:),Sets(I,:));
                                Newgroups = false(4,this.NumSpecies);
                                for i=1:4
                                    Newgroups(i,AllCombs(i,:)) = true;
                                end
                                this.ProposeSpeciesMergeMultMatch(Newgroups);
                                I = [];
                                groups(GroupIDx,:) = [];
                            end
                        end
                        clc
                    case('F') % Change focus
                        disp('Choose pair ID');
                        SpecIDx = any(groups,1);
                        reply = COMMGEN.InputProcessing(PairIDs(SpecIDx,1));
                        I = strcmp(PairIDs,reply);
                    case('R') % Reject merges
                        fprintf('Please enter the pair IDs corresponding to the to-be rejected proposals.\nFormat: ID1%sID2%s... / Name1%sName2%s...\n',this.DEL,this.DEL,this.DEL,this.DEL)
                        disp('Or enter C to cancel')
                        disp('Or enter A to reject all proposals')
                        reply = input('','s');
                        if ~strcmp(reply,'C')
                            if strcmp(reply,'A')
                                SpecIDx = find(any(groups(GroupIDx,:),1));
                                RejGroups = groups(:,I) & any(groups(:,SpecIDx),2);
                                groups(RejGroups,:) = false;
                                for i=1:numel(SpecIDx)
                                    this.RejectSpeciesMerge([I,SpecIDx(i)]);
                                end
                                I = [];
                            else
                                Names = splitstr(reply,this.DEL);
                                Found = ismember(Names,PairIDs);
                                if all(Found)
                                    SpecIDx = find(ismember(PairIDs,Names));
                                    RejGroups = groups(:,I) & any(groups(:,SpecIDx),2);
                                    groups(RejGroups,:) = false;
                                    for i=1:numel(SpecIDx)
                                        this.RejectSpeciesMerge([I,SpecIDx(i)]);
                                    end
                                    I = [];
                                else
                                    disp('The following names were not recognized. Please try again.')
                                    disp(Names(~Found))
                                end
                            end
                        end
                        clc
                    case('P') % Print all reactions for species
                        disp('Please enter the ID/name of the species (not the pair!) for which all reactions should be printed')
                        reply = COMMGEN.InputProcessing(this.SpeciesIDToName);
                        this.PrintAllRxnsForSpec(reply);
                end
                groups(sum(groups,2)==1,:)=false;
            end
        end
        %reaction groups
        function ProcessLumpedReactions(this,groups,cands)
            
            % Load data
            NumGr   = size(groups,1);
            NumCand = sum(cands,2); %Number of candidates in each group
            
            for i=1:NumGr
                % Identify true lumped reaction in the group (if any)
                clc, fprintf('Processing case %d out of the %d cases of potentially lumped reactions. \n\n',i,NumGr)
                if NumCand(i)>1
                    fprintf('Candidate lumped representations of the process:\n')
                    this.PrintEquations(groups(i,:)&cands(i,:)); % Present candidate lumped reactions
                    
                    if any(groups(i,:)&~cands(i,:))
                        fprintf('Remaining reactions in the pathway which are not candidates for the lumped reaction\n')
                        this.PrintEquations(groups(i,:)&~cands(i,:));% Present rest of the reactions
                    end
                    
                    fprintf('Enter the number corresponding to a lumped reaction (in the first reaction set) or enter ''C'' to continue\n')
                    reply = COMMGEN.InputProcessing(['C';strread(num2str(1:NumCand(i)),'%s')]);
                    if ~strcmp(reply,'C')
                        reply = str2double(reply);
                    end
                else
                    reply = 1;
                end
                if isnumeric(reply)
                    IDx = find(cands(i,:));
                    IDx = IDx(reply); % The index corresponding to the lumped reaction
                    cands(i,:)=false;
                    cands(i,IDx)=true;
                    
                    while true
                        fprintf('The identified candidate lumped reactions\n')
                        this.PrintEquations(IDx); % Present candidate lumped reactions
                        
                        fprintf('The alternative representation of the same process\n')
                        this.PrintEquations(groups(i,:)&~cands(i,:));% Present rest of the reactions
                        
                        fprintf(['Please choose how to proceed:\n'...
                            'K: Keep all reactions\n'...
                            'L: Keep only the lumped representation\n'...
                            'R: Discard the lumped representation\n'...
                            'A: Adjust any of the reactions in GPR or equation (and return here)\n'])
                        reply = COMMGEN.InputProcessing({'K','L','R','A'});
                        switch reply
                            case 'K' % Keep all reactions
                                break
                            case 'L'
                                fprintf('Should the GPR of the lumped reaction be adjusted?\n[Y/N]\n')
                                reply = COMMGEN.InputProcessing({'Y','N'});
                                RIDx = find(groups(i,:)); % Find all reactions in the group
                                IDx = find(RIDx==IDx); % Find the location of the lumped reaction in the whole set
                                if strcmp(reply,'Y')
                                    this.MergeReactions(groups(i,:),[IDx IDx 0]);
                                else
                                    this.MergeReactions(groups(i,:),[IDx IDx IDx])
                                end
                            case 'R'
                                RIDx = find(groups(i,:)); % Find all reactions in the group
                                RIDx = setdiff(RIDx,IDx); % Remove the lumped reaction itself from the list
                                for j=1:numel(RIDx)
                                    this.MergeReactions([RIDx(j) IDx],[1 1 1])
                                end
                                break
                            case 'A'
                                fprintf('Please enter the ID of the reaction you want to adjust or C to cancel\n')
                                reply = COMMGEN.InputProcessing(this.reaction_IDs(groups(i,:)));
                                RxnIDx = strcmp(this.reaction_IDs,reply);
                                this.adjustReaction(RxnIDx);
                        end
                    end
                end
            end
        end
        
        % Checking/Updating Information
        %species
        function IDx     = FreeTransport(this,CompConn)
            % Returns for every species whether or not it can freely be
            % transported between two compartments. If not provided with
            % compartments, all legal compartment connections are taken by
            % default.
            
            if ~exist('CompConn','var')
                CompConn = this.options.CompartmentConnections;
            elseif isa(CompConn,'char')
                CompConn = {CompConn};
            end
            
            IDx = false(this.NumSpecies,numel(CompConn));
            
            LogStoich = logical(this.Stoich);
            MetComp = this.MetCompartment;
            
            % For each reaction, find whether the reaction correspons to
            % two compartments
            TwoComp = cellfun(@(x) count(x=='%'),this.RxnComp)==1; %One symbol for compartment separation.
            
            % For each reaction, find whether it contains exactly two
            % metabolites
            TwoMets = sum(LogStoich,1)==2;
            
            CandRxns = find(TwoComp&TwoMets');
            for i=1:numel(CandRxns)
                RxnIDx = CandRxns(i);
                % Do the metabolites in the reaction correspond to exactly
                % one species?
                SameMet = numel(unique(this.MetToSpecies(LogStoich(:,RxnIDx))))==1;
                
                if SameMet
                    % Find compartment connection
                    comps   = unique(MetComp(LogStoich(:,RxnIDx)));
                    compstr = [comps{1} this.DEL comps{2}];
                    % Find correct species
                    SpecIDx = unique(this.MetToSpecies(LogStoich(:,RxnIDx)));
                    
                    IDx(SpecIDx,strcmp(compstr,CompConn))= true;
                end
            end
        end
        function Number  = findIdenticalRxnsAfterSpeciesMerge(this,MetIDx,S)
            % This function finds reactions which would be identical - disregarding
            % ignored species - in case two species would be the same.
            %%% Input
            % MetIDx    The metabolite indices corresponding to the species
            %           being merged. Format: n by 2 column, where n is the
            %           number of compartments in the model. A 0 means that
            %           the species is not present in that compartment. Any
            %           other value refers to the index in that
            %           compartment.
            
            % Load stoichiometric matrix
            if nargin<3
                S = logical(this.Stoich);
            elseif ~islogical(S)
                S = logical(S);
            end
            NumMets = size(S,1);
            
            % Remove ignored species
            S = this.RemIgnored(S);
            
            % Ignore Reactions in which not at least one of these species
            % is an educt or product. As we are only interested in how many
            % groups are found, not in their contents we may completely
            % remove the columns rather than setting the stoichiometric
            % coefficients to zero.
            S(:,~any(S(MetIDx,:),1)) = [];
            
            % Merge the corresponding metabolites in a new matrix
            SharedComp = find(all(MetIDx,2));
            S2 = [S; false(numel(SharedComp),size(S,2))];
            
            for i=1:numel(SharedComp)
                Rxns = any(S2(MetIDx(SharedComp(i),:),:),1);
                S2(NumMets+i,Rxns) = true;
            end
            
            S2(MetIDx,:) = [];
            
            OLDgroups = this.findDuplicateRxns(logical(S));
            NEWgroups = this.findDuplicateRxns(logical(S2));
            Number = size(NEWgroups,1) - size(OLDgroups,1);
        end
        function [Score,MaxScore] = CalculateSpeciesMergeScore(this,Pairs,S)
            % This function calculates a score representing the correlation
            % between two species.
            %%% Input
            % Pairs     matrix of n by 2, where n is the number of pairs.
            %           The columns correspond to the indices of the
            %           species making up the pair.
            %%% OPTIONAL Input
            % S         Alternative stoichiometric matrix. The indices in
            %           Pairs are now taken as the indices in the matrix
            %           rather than species indices. The number of columns
            %           should correspond to this.Stoich.
            
            % Load information
            
            if nargin==3
                assert(size(S,2)==size(this.Stoich,2),'The number of reactions doesn''t match');
                NumMets = size(S,1);
                MetComp = 'NAN';
                Comps = repmat({'NAN'},size(S,1),1);
                MetToSpec    = 1:NumMets;
                MetOrigin    = true(NumMets,1);
                NumSpec      = size(S,1);
            else
                S = this.Stoich;
                NumMets = size(S,1);
                MetComp = this.MetCompartment;
                Comps = this.CompartmentIDToName(:,1);
                MetToSpec   = this.MetToSpecies;
                MetOrigin   = this.MetOrigin;
                SpecComps   = this.SpecComps;
                NumSpec     = this.NumSpecies;
            end
            
            Sred = S;
            Sred(:,this.is_external | this.identifyRealTransport) = 0;
            
            [~,Din,Dout] = this.DegreeOfMet(S);
            RxnGeneMat   = this.rxnGeneMat;
            MetInSameRxn = this.MakeMetAssocMat(S);
            NumPairs     = size(Pairs,1);
            
            MetToComp = false(NumMets,this.NumCompartments);
            for j=1:this.NumCompartments
                MetToComp(:,j) = ismember(MetComp,Comps(j));
            end
            
            % Flatten MetToMet to correspond to the consensus model
            MetInSameRxn = any(MetInSameRxn,3);
            
            % Mapping metabolites to genes. A positive relation means that
            % the metabolite is involved in at least one reaction where the
            % gene is in the GPR.
            S2 = logical(S)*1; % Obtain a 'logical' matrix usable for matrix multiplication
            MetToGene = logical(S2*RxnGeneMat');
            
            % Translate Metabolite information to species information
            SpecDin = zeros(NumSpec,1);
            SpecDout= zeros(NumSpec,1);
            SpecToMet = false(NumSpec,NumMets);
            SpecToGene= false(NumSpec,this.NumGenes);
            for i=1:NumSpec
                MetIDx = MetToSpec==i;
                SpecDin(i)     = sum(Din(MetIDx));
                SpecDout(i)    = sum(Dout(MetIDx));
                SpecToMet(i,:) = any(MetInSameRxn(MetIDx,:),1);
                SpecToGene(i,:)= any(MetToGene(MetIDx,:),1);
            end
            
            % Pre-allocate variables
            Score = zeros(NumPairs,1);
            MaxScore = 7*ones(NumPairs,1);
            TotRxns = zeros(2,1);
            
            % Calculate the scores
            h=waitbar(0,'Calculating Score of submitted Pairs');
            
            for i=1:NumPairs
                waitbar(i/NumPairs);
                IDx    = [Pairs(i,1) Pairs(i,2)];
                MetIDx = ismember(MetToSpec,IDx);
                
                % Degree in
                CompDin = SpecDin(IDx);
                if max(CompDin)==0
                    MaxScore(i) = MaxScore(i) - 1;
                elseif max(CompDin)>0
                    Score(i) = Score(i) + log2(min(CompDin+1))/log2(max(CompDin+1));
                end
                
                % Degree out
                CompDout = SpecDout(IDx);
                if max(CompDout)==0
                    MaxScore(i) = MaxScore(i) - 1;
                elseif max(CompDout)>0
                    Score(i) = Score(i) + log2(min(CompDout+1))/log2(max(CompDout+1));
                end
                
                % Genes
                %Relative to the species with the fewest genes, what
                %fraction of the genes is shared?
                IntGenes = count(all(SpecToGene(IDx,:),1));
                TotGenes = sum(SpecToGene(IDx,:),2);
                if min(TotGenes)>0
                    Score(i) = Score(i) + IntGenes/min(TotGenes);
                else
                    MaxScore(i) = MaxScore(i)-1;
                end
                
                % Metabolites
                %Only consider metabolites which are present in any of the corresponding models
                %and could potentially be common
                Mdls = any(MetOrigin(MetIDx,:),1);
                MetsInAllModels = any(MetOrigin(:,Mdls),2);
                TotMets = count(any(SpecToMet(IDx,MetsInAllModels),1));
                IntMets = count(all(SpecToMet(IDx,MetsInAllModels),1));
                Score(i) = Score(i) + IntMets / TotMets;
                
                % Rxns
                %Relative to the species with the fewest reactions, what
                %fraction of the reactions is shared?
                if Score(i)>1 %This part takes time, no need to waste time for hopeless cases
                    if nargin==3
                        MetIDx = IDx;
                    else
                        % for every compartment, find the corresponding
                        % metabolites
                        MetIDx = zeros(this.NumCompartments,2);
                        for j=1:this.NumCompartments
                            if all(SpecComps(IDx,j),1)
                                MetIDx(j,:) = [find(MetToSpec==IDx(1)&MetToComp(:,j)) find(MetToSpec==IDx(2)&MetToComp(:,j))];
                            end
                        end
                    end
                    % Count the number of reactions for each species
                    for j=1:2
                        TotRxns(j) = count(any(S(MetToSpec==IDx(j),:),1));
                    end
                    % Count the number of shared reactions
                    MetIDx(~any(MetIDx,2),:) = [];
                    IntRxns = this.findIdenticalRxnsAfterSpeciesMerge(MetIDx,Sred);
                    Score(i) = Score(i) + IntRxns/min(TotRxns)*3;
                end
            end
            if ishandle(h)
                delete(h)
            end
        end
        function SpecCoOccur = findSpecCoOccur(this)
            % Find species which largely overlap in their reaction presence and can
            % thus be considered as a pair of metabolites. The found pairs mostly
            % correspond to redox pairs.
            %%% OUTPUT
            % SpecCoOccur   n*2 double with the indices of two species forming a pair
            %               on the different columns of the same row.
            
            % Load data & pre-allocate space
            logS = logical(this.Stoich);
            NumS = this.NumSpecies;
            NumR = this.NumReactions;
            MTS = this.MetToSpecies;
            SpecCoOccur = zeros(NumS);
            Threshold = 0.8;
            
            % Remove transport reactions from analysis
            logS(:,this.is_transport) = false;
            
            % Convert logS to species by reactions matrix
            logSpS = false(NumS,NumR);
            for i=1:NumS
                Mets = MTS ==i;
                Rxns = any(logS(Mets,:),1);
                logSpS(i,:) = Rxns;
            end
            
            % find how often species occur together
            for i=1:size(logSpS,1)
                if count(logSpS(i,:)) > 1 % Species only present in one reaction will always be an obligate pair...
                    SpecCoOccur(i,:) = sum(logSpS(:,logSpS(i,:)),2); %For each species, find how many reactions each other species shares with it
                    SpecCoOccur(i,i)=0; % Species always correlate with themselves, not interesting.
                end
            end
            
            % Weigh species co-occurence by number of reactions of the individual
            % species.
            RxnsPerSpec = sum(logSpS,2);
            SpecCoOccur = SpecCoOccur ./ repmat(RxnsPerSpec,1,this.NumSpecies); %Find fraction of reactions with the other species
            SpecCoOccur = SpecCoOccur>Threshold; %Identify species pairs which have a higher fraction of co-occurence than the threshold.
            [IA(:,1),IA(:,2)] = find(SpecCoOccur); %Find indices corresponding to species.
            IA=sort(IA,2); % Sort the rows such that a two combinations of two species has the same row
            
            % Identify species pairs
            %Find duplicate rows (these correspond to two species for which from both
            %points of view the threshold is passes.
            [~,index] = unique(IA,'rows','first');
            rep = setdiff(1:size(IA,1),index);
            SpecCoOccur = IA(rep,:);
        end
        function this    = updateRedoxPairs(this)
            % This function searches for redox pairs in the merged model
            % based on the criteria:
            % - (Nearly) always in a reaction together
            % - Not having identical chemical formulae
            %
            % The search will be compartment-specific but conclusions as
            % whether or not two species constitute a redox pair will be
            % generalized. Furthermore, transport reactions are ignored in
            % the search.
            
            % Load data & pre-allocate space
            SpecCoOccur = findSpecCoOccur(this); % How often metabolites co-occur
            
            IA = SpecCoOccur(:,1);
            IB = SpecCoOccur(:,2);
            
            % filter for pairs of m with identical chemical
            % formula
            keep = true(numel(IA));
            for i=1:numel(IA)
                % Two species having the same chemical formulae, which are
                % both not empty.
                if all(this.species{IA(i)}.formula_as_vector==this.species{IB(i)}.formula_as_vector) && any(this.species{IA(i)}.formula_as_vector)
                    keep(i) = false;
                end
            end
            IA(~keep) = [];
            IB(~keep) = [];
            
            % filter pairs for pairs already accepted or rejected
            IDx = sort([IA IB],2);
            Pairs = this.species_IDs(IDx);
            if size(Pairs)==[2 1];
                Pairs = reshape(Pairs,1,2);
            end
            PrFound = arrayfun(@(i1)all(ismember(Pairs(i1,:),[this.RedoxPairsAccepted;this.RedoxPairsRejected])),(1:size(Pairs,1))'); %Find the pairs which were already previously identified and processed
            Pairs(PrFound,:) = [];
            IA(PrFound) = [];
            IB(PrFound) = [];
            if size(Pairs,1)==0
                return % No new potential pairs identified
            end
            
            % Propose new pairs to user and ask to accept or reject them
            Present = 1:size(Pairs,1);
            rejected = [];
            while true
                fprintf('Newly found candidate redox pairs:\n')
                disp([num2cell(Present)' this.IDToName(Pairs(Present,:))])
                newRej = input('Please enter the indices of pairs which are to be rejected\nFor example: 1 or [1 2 3]\nAlternatively, enter 0 to continue\n');
                if numel(newRej)==1 && newRej==0
                    break
                end
                rejected(end+1:end+numel(newRej)) = newRej;
                Present = setdiff(Present,rejected);
                if isempty(Present)
                    break
                end
            end
            this.RedoxPairsAccepted = [this.RedoxPairsAccepted;this.species_IDs(IA(Present)),this.species_IDs(IB(Present))];
            this.RedoxPairsRejected = [this.RedoxPairsRejected;this.species_IDs(IA(rejected)),this.species_IDs(IB(rejected))];
            
            % Remove placeholders if present
            if size(this.RedoxPairsAccepted,1)>1 && strcmp('',this.RedoxPairsAccepted(1,1))
                this.RedoxPairsAccepted(1,:) = [];
            end
            if size(this.RedoxPairsRejected,1)>1 && strcmp('',this.RedoxPairsRejected(1,1))
                this.RedoxPairsRejected(1,:) = [];
            end
        end
        function Name    = IDToName(this,ID)
            % ID may be either an array of species or metabolite IDs,
            % but not a combination of both.
            if ischar(ID)
                ID = {ID};
            end
            
            if ~any(ismember(ID,[this.metabolites;this.species_IDs;this.CompartmentIDToName(:,1)]))
                Name = ID;
                return
            end
            
            SpecIDToName = this.SpeciesIDToName;
            CompIDToName = this.CompartmentIDToName;
            
            if all(~cellfun(@isempty,regexp(ID,'@','once')))
                [spec, comp] = COMMGEN.MetToSpecAndComp(ID);
                
                SpecIDToName(:,1) = regexptranslate('escape',SpecIDToName(:,1));
                CompIDToName(:,1) = regexptranslate('escape',CompIDToName(:,1));
                
                spec  = regexprep(spec,strcat('^',SpecIDToName(:,1),'$'),SpecIDToName(:,2));
                comps = regexprep(comp,strcat('^',CompIDToName(:,1),'$'),CompIDToName(:,2));
                
                Name = strcat(spec,'@',comps);
            elseif all(ismember(ID,SpecIDToName(:,1)))
                SpecIDToName(:,1) = regexptranslate('escape',SpecIDToName(:,1));
                spec  = regexprep(ID,strcat('^',SpecIDToName(:,1),'$'),SpecIDToName(:,2));
                Name = spec;
            elseif all(ismember(ID,SpecIDToName(:,2)))
                Name = ID;
            elseif all(ismember(ID,CompIDToName(:,1)))
                CompIDToName(:,1) = regexptranslate('escape',CompIDToName(:,1));
                comps = regexprep(ID,strcat('^',CompIDToName(:,1),'$'),CompIDToName(:,2));
                Name = comps;
            elseif all(ismember(ID,CompIDToName(:,2)))
                Name = ID;
            else
                error('Input not recognised.')
            end
            
        end
        function ID      = NameToID(this,Name)
            % Name may be either an array of species or metabolite names,
            % but not a combination of both.
            
            if ischar(Name)
                Name = {Name};
            end
            
            if all(ismember(Name,[this.metabolites;this.species_IDs]))
                ID = Name;
                return
            end
            
            SpecIDToName = this.SpeciesIDToName;
            CompIDToName = this.CompartmentIDToName;
            
            if all(~cellfun(@isempty,regexp(Name,'@','once')))
                [spec, comp] = COMMGEN.MetToSpecAndComp(Name);
                
                SpecIDToName(:,2) = regexptranslate('escape',SpecIDToName(:,2));
                CompIDToName(:,2) = regexptranslate('escape',CompIDToName(:,2));
                
                spec  = regexprep(spec,strcat('^',SpecIDToName(:,2),'$'),SpecIDToName(:,1));
                comps = regexprep(comp,strcat('^',CompIDToName(:,2),'$'),CompIDToName(:,1));
                
                ID = strcat(spec,'@',comps);
            elseif all(ismember(Name,SpecIDToName(:,1)))
                ID = Name;
            elseif all(ismember(Name,SpecIDToName(:,2)))
                SpecIDToName(:,2) = regexptranslate('escape',SpecIDToName(:,2));
                spec  = regexprep(Name,strcat('^',SpecIDToName(:,2),'$'),SpecIDToName(:,1));
                ID = spec;
            elseif all(ismember(Name,CompIDToName(:,1)))
                ID = Name;
            elseif all(ismember(Name,CompIDToName(:,2)))
                CompIDToName(:,2) = regexptranslate('escape',CompIDToName(:,2));
                comps = regexprep(Name,strcat('^',CompIDToName(:,2),'$'),CompIDToName(:,1));
                ID = comps;
            else
                error('Input not recognised.')
            end
            
        end
        function BCS = findBaseCorrScore(this)
            %This function determines the baseline of expected similarity of identical
            %species between the different sets of models.
            
            % Load information
            Shared = all(this.MetOrigin,2);
            S = logical(this.Stoich)*1; % Logical matrix usable for mathematical treatment.
            RGM = this.rxnGeneMat;
            NMod = this.NumModels;
            NGen = this.NumGenes;
            NMet = this.NumMetabolites;
            
            % Add information on metabolite connections
            %Identify which metabolites are connected to which other ones
            MetConn = this.MakeMetAssocMat;
            %Adjust 'score' based on number of connections of mets
            numRxnsPerMet = full(sum(S,2));
            numRxnsPerMet = repmat(numRxnsPerMet,[1,size(MetConn,2),size(MetConn,3)]);
            MetConn = MetConn ./numRxnsPerMet;
            %Metabolites which are not shared between models
            
            % Add information on gene connections
            %Identify which genes are connected to which metabolites
            MetToGene = false(NGen,NMet,NMod);
            for i=1:NMod
                rxnlist = this.RxnOrigin(:,i); %Only take reactions corresponding to one of the models
                MetToGene(:,:,i) = logical(S(:,rxnlist)*RGM(:,rxnlist)')';
            end
            %Adjust 'score' based on number of connections of genes
            numRxnsPerGene = full(sum(logical(this.rxnGeneMat),2));
            numRxnsPerGene  = repmat(numRxnsPerGene,[1,size(MetToGene,2),size(MetToGene,3)]);
            MetToGene = MetToGene ./numRxnsPerGene;
            %Discard genes which are not shared, they can't match
            GeneOrig = this.GeneOrigin;
            SharedG = all(GeneOrig,2);
            MetToGene(~SharedG,:,:) = [];
            
            MetConn = cat(1,MetConn,MetToGene);
            
            %Disregard any metabolites which are not shared, they can't match
            MetConn(~Shared,:,:) = [];
            
            %Discard metabolites which are not shared, as we're setting the baseline.
            MetConn(:,~Shared,:) = [];
            
            %If metabolites take the same place in different models, metabolites 1 +
            %numShared should be very similar.
            MPairs    = unique(sort(allcomb(1:NMod,1:NMod),2),'rows');
            % % % %             MPairs(MPairs(:,1)==MPairs(:,2),:) = [];
            numMSets  = size(MPairs,1);
            numShared = count(Shared);
            numPairs  = numShared*numMSets;
            cmsc      = zeros(numPairs*numMSets,1); %Pre-allocate
            
            for i=1:numMSets
                csc = corr([MetConn(:,:,MPairs(i,1)) MetConn(:,:,MPairs(i,2))]); % Correlation scores
                for j=1:numShared
                    cmsc((i-1)*numPairs+j) = csc(j,j+numShared);
                end
            end
            
            %Minimal correlationscore is dependent on the set percentile
            BCS = prctile(cmsc,this.options.findBaseCorrScore_quantile);
            
        end
        function FormMatch = matchSpecForm(this)
            % This function checks whether two different chemical species may be
            % considered the same based on their chemical formulae
            
            % Load information
            CEL = this.species{1}.CEL;
            S = logical(this.Stoich);
            SpecFormula  = this.SpecFormula;
            MetToSpec    = this.MetToSpecies;
            
            % Pre-allocate variables
            FormMatch = true(this.NumSpecies,this.NumSpecies);
            
            % Adjust formulae
            %Remove protons from SpecFormula to allow mode freedom
            SpecFormula(:,ismember(CEL,'H'))=0;
            % Allow matches with species with a badly defined formula
            RestGroups  = ismember(CEL,{'X','R'});
            %Chemical formulae excluding the restgroups
            SpecExclRest = SpecFormula(:,~RestGroups);
            
            % Not to be considered
            %species which were already removed
            specExist = false(this.NumSpecies,1);
            specExist(MetToSpec) = true;
            FormMatch(~specExist,:) = false;
            FormMatch(:,~specExist) = false;
            % %Protons/H2O/Biomass
            % SpecIDs = find(ismember(this.species_IDs,[this.options.IgnoredSpec 'BIOMASS']));
            % PotentialMatch(SpecIDs,:) = false;
            % PotentialMatch(:,SpecIDs) = false;
            % Species sharing a reaction
            for i=1:this.NumReactions
                MetIDx  = S(:,i);
                SpecIDx = MetToSpec(MetIDx);
                FormMatch(SpecIDx,SpecIDx)=false;
            end
            
            % Allow matching of species with badly defined chemical formulae
            %Find species with badly defined chemical formula
            NoForm = ~any(SpecFormula,2) | any(SpecFormula(:,RestGroups),2);
            for i=1:numel(this.species)
                if ~this.species{i}.formula_valid
                    NoForm(i) = true;
                end
            end
            %Remove impossible matches for species which do have a well-defined
            %chemical formula
            for i=1:this.NumSpecies
                if this.species{i}.formula_valid % Don't exclude any options based on species with a ill-defined chemical formula
                    if any(SpecFormula(i,RestGroups))
                        % Find metabolites which are not definitely smaller
                        CompareMat = repmat(SpecExclRest(i,:),this.NumSpecies,1);
                        PotMatches = ~any(CompareMat>SpecExclRest,2);
                    else
                        % Remove metabolites which don't match
                        PotMatches = ismember(SpecFormula,SpecFormula(i,:),'rows');
                    end
                    PotMatches = PotMatches | NoForm;
                    % In both directions as metabolites with a Rest-group are
                    % protected.
                    FormMatch(i,~PotMatches) = false;
                    FormMatch(~PotMatches,i) = false;
                end
            end
            
        end
        function CorrMatch = matchSpecCorr(this)
            % This function checks whether two different chemical species may be
            % considered the same based on the similarity in their position in the
            % network.
            
            % Load information
            SpcOrg = this.SpecOrigin;
            NSpc = this.NumSpecies;
            
            % Set variables
            CorrMatch = false(NSpc);
            
            % for each combination of models, identify which species corresponding to
            % exactly that combination of models may match.
            %Find base correlation score required
            BCS = this.findBaseCorrScore;
            %Find all model sets
            MSets = unique(SpcOrg(any(SpcOrg,2),:),'rows');
            %Find potential matches for each combination
            for i=1:size(MSets,1)
                for j=i:size(MSets,1)
                    NewMatches = this.findCorrMatches(MSets(i,:),MSets(j,:),BCS);
                    CorrMatch = CorrMatch | NewMatches;
                end
            end
        end
        function SpcMatch = findCorrMatches(this,MSet1,MSet2,BCS)
            % This function determines whether the similarity in network environment
            % between two species for two specific sets of models is sufficient for
            % them to be considered as identical species.
            
            % Load information
            MetOrg = this.MetOrigin;
            S = logical(this.Stoich)*1; % Logical matrix usable for mathematical treatment.
            NSpc = this.NumSpecies;
            NGen = this.NumGenes;
            NMod = this.NumModels;
            NMet = this.NumMetabolites;
            MTS = this.MetToSpecies;
            
            % Pre-allocate variable
            SpcMatch = false(NSpc);
            
            % Identify shared metabolites between the two model sets
            M1Mets  = any(MetOrg(:,MSet1),2);
            M2Mets  = any(MetOrg(:,MSet2),2);
            Shared  = M1Mets & M2Mets;
            Present = M1Mets | M2Mets;
            
            % Add information on metabolite connections
            %Identify which metabolites are connected to which other ones
            MetConn = this.MakeMetAssocMat;
            %Adjust 'score' based on number of connections of mets
            numRxnsPerMet = full(sum(S,2));
            numRxnsPerMet = repmat(numRxnsPerMet,[1,size(MetConn,2),size(MetConn,3)]);
            MetConn = MetConn ./numRxnsPerMet;
            
            % Add information on gene connections
            %Identify which genes are connected to which metabolites
            MetToGene = false(NGen,NMet,NMod);
            for i=1:NMod
                rxnlist = this.RxnOrigin(:,i); %Only take reactions corresponding to one of the models
                MetToGene(:,:,i) = logical(S(:,rxnlist)*this.rxnGeneMat(:,rxnlist)')';
            end
            %Adjust 'score' based on number of connections of genes
            numRxnsPerGene = full(sum(logical(this.rxnGeneMat),2));
            numRxnsPerGene  = repmat(numRxnsPerGene,[1,size(MetToGene,2),size(MetToGene,3)]);
            MetToGene = MetToGene ./numRxnsPerGene;
            %Discard genes which are not shared, they can't match
            GeneOrig = this.GeneOrigin;
            SharedG = all(GeneOrig,2);
            MetToGene(~SharedG,:,:) = [];
            
            MetConn = cat(1,MetConn,MetToGene);
            
            % Filter MetConn
            %Metabolites which are not shared between the models cannot add to the
            %scoring and are removed
            MetConn(~Shared,:,:) = [];
            %Discard metabolites not originating from either model set.
            MetConn(:,~Present,:) = 0; % Entire columns with a zero value will receive a correlation score of NaN. Setting columns to zero does thus not automatically correlate them.
            
            % Correlation scores
            %Calculate scores
            csc = corr([max(MetConn(:,:,MSet1),[],3) max(MetConn(:,:,MSet2),[],3)]);
            %Matches may only occur between the two different model sets
            csc(1:NMet,1:NMet) = 0;
            csc(NMet+(1:NMet),NMet+(1:NMet)) = 0;
            %Compare to threshold
            MetMatch = csc > BCS;
            %Convert metabolite pair scoring to species scoring
            [IA(:,1),IA(:,2)] = find(MetMatch);
            IA(IA>NMet) = IA(IA>NMet)-NMet;
            IA(IA(:,1)==IA(:,2),:) = [];%Species can't match themselves
            for i=1:size(IA,1)
                SpcMatch(MTS(IA(i,1)),MTS(IA(i,2))) = true; %In two locations in matrix for compatibility with findFormMatches
                SpcMatch(MTS(IA(i,2)),MTS(IA(i,1))) = true;
            end
            
        end
        function groups = matchSpecRxns(this,groups,S)
            % This function checks whether the proposed groups have any reactions which
            % would become identical if the species would be merged.
            
            % Load information
            if nargin==3
                assert(size(S,2)==size(this.Stoich,2),'The number of reactions doesn''t match');
                NumMets = size(S,1);
                MetComp = 'NAN';
                Comps = {'NaN'};
                MTS    = 1:NumMets;
            else
                S = this.Stoich;
                NumMets = size(S,1);
                MetComp = this.MetCompartment;
                Comps = this.CompartmentIDToName(:,1);
                MTS   = this.MetToSpecies;
            end
            Ncmp = numel(Comps);
            
            SpecComps = this.SpecComps;
            
            MetToComp = false(NumMets,Ncmp);
            for j=1:Ncmp;
                MetToComp(:,j) = ismember(MetComp,Comps(j));
            end
            
            % Adjust stoichiometric matrix to ignore external and transport
            % reactions (these tend to be highly similar even for
            % nonsimilar species)
            %S(:,this.is_external | this.identifyRealTransport) = 0;
            S(:,this.is_external) = 0;
            % Pre-allocate
            TotRxns = zeros(2,1);
            scores = zeros(size(groups,1),1);
            
            % Determine fraction of reactions matching
            for i=1:size(groups,1)
                if nargin==3
                    IDx = find(groups(i,:));
                    MetIDx = find(groups(i,:));
                else
                    % for every compartment, find the corresponding
                    % metabolites
                    IDx = find(groups(i,:));
                    MetIDx = zeros(Ncmp,2);
                    for j=1:Ncmp
                        if all(SpecComps(IDx,j),1)
                            MetIDx(j,:) = [find(MTS==IDx(1)&MetToComp(:,j)) find(MTS==IDx(2)&MetToComp(:,j))];
                        end
                    end
                end
                % Count the number of reactions for each species
                for j=1:2
                    TotRxns(j) = count(any(S(MTS==IDx(j),:),1));
                end
                % Count the number of shared reactions
                MetIDx(~any(MetIDx,2),:) = [];
                IntRxns = this.findIdenticalRxnsAfterSpeciesMerge(MetIDx,S);
                scores(i) = IntRxns / min(TotRxns);
            end
            groups(scores==0,:) = [];
        end
        function CorrMatch = matchSpecCorrPair(this,S,MetOrg)
            % This function checks whether two rows in the S matrix match sufficiently
            % in order for the metabolites (or species) to be considered the same. This
            % function should only be used for SimilarSpeciesPairs.
            
            % Load information
            NMet = size(S,1);
            
            % Set variables
            CorrMatch = false(NMet);
            
            % for each combination of models, identify which species corresponding to
            % exactly that combination of models may match.
            %Find base correlation score required
            BCS = this.findBaseCorrScore;
            %Find all model sets
            MSets = unique(MetOrg,'rows');
            %Find potential matches for each combination
            for i=1:size(MSets,1)
                for j=i:size(MSets,1)
                    NewMatches = this.findCorrMatchesPair(MSets(i,:),MSets(j,:),BCS,S,MetOrg);
                    CorrMatch = CorrMatch | NewMatches;
                end
            end
        end
        function Matches = findCorrMatchesPair(this,MSet1,MSet2,BCS,S,MetOrg)
            % This function determines whether the similarity in network environment
            % between two sets of two species if sufficient to consider the two pairs
            % to be the same pair. (e.g., NAD+ & NADH vs nad(+) & nadh). As this
            % function gets passed a modified S-matrix, the information on species is
            % not used. Rather, every row is interpreted as it's own species.
            
            % Load information
            NGen = this.NumGenes;
            NMod = this.NumModels;
            
            S = logical(S)*1;% Logical matrix usable for mathematical treatment.
            NMet = size(S,1);
            
            % Pre-allocate variable
            Matches = false(NMet);
            
            % Identify shared metabolites between the two model sets
            M1Mets  = any(MetOrg(:,MSet1),2);
            M2Mets  = any(MetOrg(:,MSet2),2);
            Shared  = M1Mets & M2Mets;
            Present = M1Mets | M2Mets;
            
            % Add information on metabolite connections
            %Identify which metabolites are connected to which other ones
            MetConn = this.MakeMetAssocMat(S);
            %Adjust 'score' based on number of connections of mets
            numRxnsPerMet = full(sum(S,2));
            numRxnsPerMet = repmat(numRxnsPerMet,[1,size(MetConn,2),size(MetConn,3)]);
            MetConn = MetConn ./numRxnsPerMet;
            MetConn(isnan(MetConn)) = 0;
            
            % Add information on gene connections
            %Identify which genes are connected to which metabolites
            MetToGene = false(NGen,NMet,NMod);
            for i=1:NMod
                rxnlist = this.RxnOrigin(:,i); %Only take reactions corresponding to one of the models
                MetToGene(:,:,i) = logical(S(:,rxnlist)*this.rxnGeneMat(:,rxnlist)')';
            end
            %Adjust 'score' based on number of connections of genes
            numRxnsPerGene = full(sum(logical(this.rxnGeneMat),2));
            numRxnsPerGene  = repmat(numRxnsPerGene,[1,size(MetToGene,2),size(MetToGene,3)]);
            MetToGene = MetToGene ./numRxnsPerGene;
            %Discard genes which are not shared, they can't match
            GeneOrig = this.GeneOrigin;
            SharedG = all(GeneOrig,2);
            MetToGene(~SharedG,:,:) = [];
            MetConn = cat(1,MetConn,MetToGene);
            
            % Filter MetConn
            %Metabolites which are not shared between the models cannot add to the
            %scoring and are removed
            MetConn(~Shared,:,:) = [];
            %Discard metabolites not originating from either model set.
            MetConn(:,~Present,:) = 0; % Entire columns with a zero value will receive a correlation score of NaN. Setting columns to zero does thus not automatically correlate them.
            
            % Correlation scores
            %Calculate scores
            csc = corr([MetConn(:,:,1) MetConn(:,:,2)]);
            %Matches may only occur between the two different model sets
            csc(1:NMet,1:NMet) = 0;
            csc(NMet+(1:NMet),NMet+(1:NMet)) = 0;
            %Compare to threshold
            MetMatch = csc > BCS;
            %Convert to matrix format
            [IA(:,1),IA(:,2)] = find(MetMatch);
            IA(IA>NMet) = IA(IA>NMet)-NMet;
            IA(IA(:,1)==IA(:,2),:) = [];%Species can't match themselves
            for i=1:size(IA,1)
                Matches(IA(i,1),IA(i,2)) = true; %In two locations in matrix for compatibility with rest of script.
                Matches(IA(i,2),IA(i,1)) = true;
            end
            
        end
        function identifyPolymers(this)
            %This function identifies potential polymers based on the chemical
            %formulae. This does not necessarily find all polymers!
            
            %Find all formulae
            form = cell(this.NumSpecies,1);
            for i=1:this.NumSpecies
                if ~isempty(this.species{i}.formula)
                    form{i} = this.species{i}.formula;
                end
            end
            
            %Identify formulae corresponding to a polymer
            Polymers = ~cellfun(@isempty,regexp(form,')n'));
            this.SetSpecFormula(Polymers); %empty the formulae
        end
        function SetSpecFormula(this,idx,form)
            %Adjusts the chemical sum formula of the species indicated by idx by the formula in form.
            %%% INPUT
            % idx       indices of the species for which the formula is to
            %           be changed
            % form      new formula. by default: empty
            
            % Input processing
            if islogical(idx)
                idx = find(idx);
            end
            
            if nargin<3
                form = '';
            elseif iscell(form)
                form = form{1};
            end
            
            % Change formulae
            for i=1:numel(idx)
                this.species{idx(i)}.formula = form;
                this.species{idx(i)}.chemicalSum2Vector;
                this.TrackSpec('ChangedForm',this.species{idx(i)});
            end
        end
        function FormPerRxn = findFormulaFromReactions(this,idx)
            %This function determines what the molecular formula of a
            %compound should be depending on the other molecules it shares
            %reactions with.
            %%% INPUT
            % idx   logical or linear indices corresponding to the
            %       positions of the species of interest in this.species
            
            if islogical(idx)
                idx = find(idx);
            end
            
            % Load information
            MTS = this.MetToSpecies;
            S   = this.Stoich;
            MF  = this.MetFormula;
            CEL = this.species{1}.CEL;
            
            % Pre-process information
            logS = logical(S);
            MF(ismember(MTS,idx),:) = 0; %the formula of the queried metabolites is set to unknown
            EmptyMets = ~any(MF,2);
            UnkRxns = sum(logS(EmptyMets,:),1)>1; %reactions with more than one metabolite with unknown formula
            MF = MF';
            
            % Pre-allocate variables
            FormPerRxn = cell(this.NumReactions,numel(idx));
            FormPerRxn(:) = {''};
            
            % For each species, determine the chemical formula in each
            % reaction
            for i=1:numel(idx)
                %Identify reactions in which the species is involved
                id = idx(i);
                mets = MTS==id;
                rxns = any(S(mets,:),1);
                
                %Process reactions with more than one unknown formula
                %(indeterminable)
                FormPerRxn(rxns&UnkRxns,i) = {'NA'};
                rxns(UnkRxns)=false;
                rxns = find(rxns);
                
                % Determine formula in remainder of reactions
                %Isolate stoichiometric coefficients of species of interest
                if any(rxns)
                    Ssub = S(:,rxns);
                    Sc = sum(Ssub(mets,:),1);
                    Ssub(mets,:) = 0;
                    %Find how many of each atom are missing in the reactions
                    Forms = MF * Ssub *-1;
                    %Normalize for stoichiometric coefficient of compound of
                    %interest
                    Forms = Forms ./ repmat(Sc,numel(CEL),1);
                    %find number of different formulae
                    [C,~,IB] = unique(Forms','rows');
                    for j=1:size(C,1);
                        formula = this.CELvector2formula(C(j,:),CEL);
                        FormPerRxn(rxns(IB==j),i) = {formula};
                    end
                end
            end
            % remove biomass reactions from the list
            FormPerRxn(this.is_biomass,:) = {''};
        end
        function formula = CELvector2formula(this,v,CEL)
            %Converts the vector representation of a molecular formula into
            %a more human-readable format
            %%% INPUT
            % v         vector with equal length as CEL, the coefficient on
            %           each position indicating the number of that atom is
            %           included in the molecule
            % CEL       The atoms corresponding to each position.
            
            if nargin<3
                CEL = this.species{1}.CEL;
            end
            
            formula = '';
            
            idx =  find(v); %non-empty sites
            
            for i=1:numel(idx)
                id = idx(i);
                formula = [formula CEL{id} num2str(v(id))]; %#ok<AGROW>
            end
            
        end
        %metabolites
        function MetOrigin = MetOrigin(this,S)
            % Returns for each metabolite whether or not it is associated
            % with reactions from each model
            
            % Load info
            RxnOrigin = this.RxnOrigin;
            if nargin<2
                S = logical(this.Stoich);
            end
            NumMets = size(S,1);
            
            % Pre-allocate space
            MetOrigin = false(NumMets,this.NumModels);
            
            % Find metabolite origin
            for i=1:this.NumModels
                RxnSet = RxnOrigin(:,i);
                MetSet = any(S(:,RxnSet),2);
                MetOrigin(:,i) = MetSet;
            end
        end
        function [D,Din,Dout] = DegreeOfMet(this,S)
            %%% OPTIONAL Input
            % Output
            % D:    Degree of metabolite
            % Din:  Number of incoming edges
            % Dout: Number of outgoing edges
            
            % Load info
            if nargin==1
                S = this.Stoich;
            end
            
            % Pre-allocate variables
            Din = zeros(size(S,1),1);
            Dout= zeros(size(S,1),1);
            
            % Calculate degree of each metabolite
            D = sum(logical(S),2);
            if nargout>2
                for i=1:size(S,2)
                    Rxn = this.reactions{i};
                    % Obtain directionality of the reaction
                    if strfind(Rxn.equation,'<--')
                        Dir = -1;
                    elseif strfind(Rxn.equation,'-->')
                        Dir = 1;
                    else
                        Dir = 0; % Reversible
                    end
                    
                    % Obtain indices of participating metabolites
                    educts   = S(:,i)<0;
                    products = S(:,i)>0;
                    
                    % The 'educt side' can be turned into the 'product side' of
                    % the equation.
                    if Dir >=0
                        Dout(educts) = Dout(educts)+1;
                        Din(products)= Din(products)+1;
                    end
                    
                    % The 'product side' can be turned into the 'educt side' of
                    % the equation.
                    if Dir <=0
                        Din(educts) = Din(educts)+1;
                        Dout(products)= Dout(products)+1;
                    end
                end
            end
        end
        function this = ChangeHubMets(this)
            % Present the current hub metabolite list and allow altering
            % thereof.
            
            disp('The current hub metabolites:')
            disp(this.IDToName(this.options.HubMets))
            disp(['Do you want to alter this list?' char(10)...
                'R: Remove a metabolite from the list' char(10)...
                'A: Add a metabolite to the list' char(10)...
                'S: Suggest additional Hub Metabolites based on degree' char(10)...
                'C: Cancel, leave the list as it is' char(10)...
                'N: Reset the Hub list and propose new' char(10)...
                ])
            
            reply = COMMGEN.InputProcessing({'R','A','S','C','N'});
            
            switch(reply)
                
                case('R') %Remove a metabolite from Hub Metabolites
                    disp('Enter the name of the metabolite to be removed from the list or enter C to cancel')
                    choice = this.COMMGEN.InputProcessing([this.IDToName(this.options.HubMets);'C']);
                    if ~strcmp(choice,'C')
                        this.options.HubMets(ismember(this.IDToName(this.options.HubMets),choice)) = [];
                    end
                    
                case('A') %Add a metabolite to the hub metabolites
                    disp('Enter the name of the metabolite to be added to the list or enter C to cancel')
                    choice = this.COMMGEN.InputProcessing([this.metabolites;this.IDToName(this.metabolites);'C']);
                    if ~strcmp(choice,'C')
                        choice = this.NameToID(choice);
                        if any(ismember(this.options.HubMets,choice));
                            disp('The chosen metabolite is already member of the hub metabolite list')
                        else
                            %Not yet in hub metabolites
                            this.options.HubMets(end+1) = choice;
                        end
                    end
                    
                case('S') %Suggest additional Hub Mets
                    this.SetHubMets;
                case('N')
                    this.SetHubMets(true);
            end
            
            
        end
        function MetToMet = MakeMetToMetMat(this,directed,S)
            % This function creates a matrix containing information on
            % which metabolites (on the columns) can be produced from
            % reactions in which the metabolite on the row is an educt. The
            % third dimension represents in which model(s) a herefor
            % required reaction exist.
            
            % Load data
            NumMdls = this.NumModels;
            
            if nargin < 3
                S = this.Stoich;
                if nargin < 2
                    directed = true;
                end
                %             else
                %                 if islogical(S)
                %
                %                 end
            end
            
            NumMets = size(S,1);
            
            % Create the matrix
            
            % Pre-allocate space
            MetToMet = false(NumMets,NumMets,NumMdls);
            
            % Ignore external reactions
            reactionIDx = find(~this.is_external);
            
            % Go through the reactions and map the connections between
            % educts, products and models.
            for i=1:numel(reactionIDx)
                Rxn = this.reactions{reactionIDx(i)};
                if directed
                    % Obtain directionality of the reaction
                    if strfind(Rxn.equation,'<--')
                        Dir = -1;
                    elseif strfind(Rxn.equation,'-->')
                        Dir = 1;
                    else
                        Dir = 0; % Reversible
                    end
                else
                    Dir = 0;
                end
                
                % Obtain indices of participating metabolites
                educts   = S(:,reactionIDx(i))<0;
                products = S(:,reactionIDx(i))>0;
                
                % The 'educt side' can be turned into the 'product side' of
                % the equation.
                if Dir >=0
                    MetToMet(educts,products,Rxn.Origin) = true;
                end
                
                % The 'product side' can be turned into the 'educt side' of
                % the equation.
                if Dir <=0
                    MetToMet(products,educts,Rxn.Origin) = true;
                end
            end
        end
        function MetInSameRxn = MakeMetAssocMat(this,S)
            % This function creates a matrix containing information on
            % which metabolites occur in the same reactions
            
            % Load data
            NMod = this.NumModels;
            if nargin < 2
                S = this.Stoich;
            end
            S = logical(S);
            NMet = size(S,1);
            
            % Create the matrix
            %Pre-allocate space
            MetInSameRxn = false(NMet,NMet,NMod);
            %Ignore external reactions
            reactionIDx = find(~this.is_external);
            
            % Go through the reactions and map the connections between
            % metabolites and metabolites for specific models.
            for i=1:numel(reactionIDx)
                Rxn = this.reactions{reactionIDx(i)};
                % Obtain indices of participating metabolites
                mets   = S(:,reactionIDx(i));
                MetInSameRxn(mets,mets,Rxn.Origin) = true;
            end
        end
        %reactions
        function [Balance] = IsReactionBalanced(this,RxnIDx)
            if islogical(RxnIDx)
                RxnIDx = find(RxnIDx);
            end
            Balance = zeros(numel(RxnIDx),1);
            for i=1:numel(RxnIDx)
                StoichVals = this.Stoich(:,RxnIDx(i));
                MetIDx = logical(StoichVals);
                if any(~any(this.MetChemForm(:,MetIDx),1)) || any(any(this.MetChemForm([9 12],MetIDx)))
                    % One of the metabolites does not have a chemical
                    % formula or the formula is ill-defined (R,X group)
                    Balance(i) = nan;
                elseif ~any(this.MetChemForm*StoichVals)
                    % Reaction is chemically balanced
                    Balance(i) = 1;
                end
            end
        end
        function printReactionBalance(this,RxnIDx)
            % change function to correspond to reaction equation,
            % stoichiometric matrix is not always up to date.
            
            Overview = dataset;
            
            if islogical(RxnIDx)
                RxnIDx = find(RxnIDx);
            end
            CEL = this.species{1}.CEL;
            for i=1:numel(RxnIDx)
                StoichVals = this.Stoich(:,RxnIDx(i));
                % Reaction is chemically balanced
                Balance = this.MetChemForm*StoichVals;
                Overview.(genvarname(this.reactions{RxnIDx(i)}.id)) = Balance';
            end
            
            Overview.Properties.ObsNames = CEL;
            disp(Overview)
        end
        function transport = identifyRealTransport(this)
            % this.is_transport corresponds to reactions which are multi-compartment reactions.
            % This function identifies actual transport reactions. This function
            % screens for reactions where at least one species is actually transported
            % across a membrane.
            %%% OUTPUT
            % transport     logical vector where 'true' corresponds to a transport
            %               reaction
            transport = this.is_transport;
            IDx = find(transport);
            MetToSpec = this.MetToSpecies;
            
            % Obtain logical matrix excluding to-be ignored metabolites
            S = this.Stoich;
            S = this.RemIgnored(S);
            S = logical(S);
            
            for i=1:numel(IDx)
                RxnMets  = S(:,IDx(i));
                RxnSpec  = MetToSpec(RxnMets);
                uniqSpec = unique(RxnSpec);
                if numel(uniqSpec) == numel(RxnSpec) % No species are present on both sides of the equation
                    transport(IDx(i)) = false;
                end
            end
        end
        function [dR,dM,iR,iM] = findDeadEnds(this, S)
            % MnXModelDeadEnds
            % This script uses the stoichiometric matrix to determine all "dead
            % ends" of the model structure including an optional recursive call to
            % eliminate all reactions that are never able to carry a flux because they
            % are connected to a dead-end reaction/metabolite.
            % Note, one also has to check for reversible reactions to identify all real
            % dead-ends.
            %
            %%% INPUT
            %   model           handle to model structure
            %   option          recursive call if true (default: true)
            %%% OPTIONAL INPUT
            %   S               Alternative stoichiometric matrix to be
            %                   used.
            %%% OUTPUT
            %   dR              The indices of internal dead end reactions
            %   dM              the indices of dead end metabolites
            %   iR              the indices of internal inactive reactions
            %   iM              the indices of inactive metabolites
            %
            % dead-end: Metabolite for which either no producing or consuming
            %           reactions exist and the associated reactions.
            % inactive: Metabolites / reactions which would be dead-ends if
            %           the dead-ends would - recursively be removed from the network.
            
            if nargout < 3 % Only dead-end rxns / metabolites are requested
                recursive = false;
            else
                recursive = true;
            end
            
            % Stoichiometric matrix
            if nargin<2
                S = this.Stoich; % Full stoichiometric matrix
            end
            
            % Load information
            ext = sum(logical(S),1)==1; %Reactions involving only one metabolite are external reactions
            [m,r] = size(S);
            
            % Pre-allocate variables
            iR_old = zeros(1,r);% invalid reactions
            iM_old = zeros(m,1);% invalid metabolites
            
            % Directionalities: LB + UB == 0 -> Reversible, even if both are 0
            revs = (sign(this.Ub)+sign(this.Lb))';
            bi   = revs == 0;
            
            % Reverse backward reactions
            bwi = revs==-1;
            S(:,bwi) = -1*S(:,bwi);
            
            while true
                % Metabolites must be both producable and consumable,
                % otherwise they are a dead end.
                
                % Internal reactions
                BSMi = S(:,~ext)<0;    % Consumption
                BSMe = S(:,~ext)>0;    % Production
                Mi   = sum(BSMi,2)>0;   % Are there reactions which can consume the metabolite?
                Me   = sum(BSMe,2)>0;   % Are there reactions which can produce the metabolite?
                
                % vM contains all valid internal metabolites (vM)
                vM = and(Mi, Me);
                
                % External reactions
                if any(ext)
                    BEM  = logical(S(:,ext));
                    EM   = sum(BEM,2)~=0; % Is there an external reaction for this metabolite?
                    iM = and(~vM, ~EM); % All invalid metabolites that are NOT external
                else
                    iM = ~vM; % iM contains all invalid metabolites
                end
                
                % correct entries in iM that are in at least two reactions
                % of which one is bidirectional.
                BSM = logical(S);
                i   = sum(BSM,2)>=2;                % At least two reactions
                iB  = and(sum(BSM(:,bi),2)>0,i);    % At least one bidirectional
                iM(iB) = false;
                
                % identify internal dead end reactions
                iR = sum(BSM(iM,1:end),1)>0;
                
                % Keep track of the first layer of dead-end reactions
                if ~exist('dR','var')
                    dR = iR;
                    dM = iM;
                end
                
                % if no further reactions are identified as dead ends, then stop
                if sum(iR) == 0
                    break;
                else
                    iM_old = iM | iM_old;
                    iR_old = iR | iR_old;
                    S(:,iR_old) = zeros(m,sum(iR_old)); % remove internal dead end reactions
                end
                
                if ~recursive % If not recursive
                    break;
                end
                
            end
            
            iR = iR_old;
            iM = iM_old;
        end
        function adjustReaction(this,RxnIDx)
            % This function allows to adjust the reaction GPR or equation
            
            assert((islogical(RxnIDx)&&count(RxnIDx)==1)||numel(RxnIDx)==1,'adjustReaction expects just one reaction at a time')
            
            Change = false;
            fprintf('The reaction submitted for change:\n')
            this.PrintEquations(RxnIDx)
            
            %GPR
            fprintf('Do you want to change the GPR?\n[Y/N]\n')
            reply = COMMGEN.InputProcessing({'Y','N'});
            if strcmp(reply,'Y')
                fprintf('Enter the new gene rule')
                GPR = this.MakeGeneRules;
                if ~ischar(GPR)
                    GPR = char(GPR);
                end
                this.reactions{RxnIDx}.proteins = GPR;
                Change = true;
            end
            %Equation
            fprintf('Do you want to change the reaction equation?\n[Y/N]\n')
            reply = COMMGEN.InputProcessing({'Y','N'});
            if strcmp(reply,'Y')
                equation = '';
                this.PrintEquations(RxnIDx,false)
                equation = EnterEquation(this,equation);
                this.reactions{RxnIDx}.equation = equation;
                this.reactions{RxnIDx}.getReactants;
                Change = true;
            end
            if Change
                %Track that the reaction equation and/or GPR was altered.
                this.TrackRxn('RxnChanged',this.reactions{RxnIDx}.id,this.reactions{RxnIDx})
            end
        end
        function this = MarkForRemoval(this,RxnIDx,track)
            % This function marks reactions for removal and adjusts the
            % necessary fields of 'this' such that the reaction is not
            % wrongly considered by other functions.
            if nargin<3
                track=true;
            end
            
            this.ToBeRemovedRxns(RxnIDx) = true;
            this.Stoich(:,RxnIDx) = 0;
            this.rxnGeneMat(:,RxnIDx) = 0;
            
            if track
                if islogical(RxnIDx)
                    RxnIDx = find(RxnIDx);
                end
                
                for i=1:numel(RxnIDx)
                    this = TrackRxn(this,'Removed',this.reactions{RxnIDx(i)}.id,this.reactions{RxnIDx(i)});
                end
            end
        end
        function this = ChangeCmp(this,RxnIDx,OC,TC)
            % This function changes the compartmentalization of a reaction
            %%% INPUT
            % RxnIDx    (logical) index corresponding to the reaction to be changed
            % OC        The original compartment
            % TC        The target compartment
            if islogical(RxnIDx)
                RxnIDx = find(RxnIDx);
            end
            if iscell(OC)
                OC = OC{1};
            end
            if iscell(TC)
                TC = TC{1};
            end
            
            for i=1:numel(RxnIDx)
                Rxn = this.reactions{RxnIDx(i)};
                Rxn.SrcRxns{1} = Rxn;
                origID = Rxn.id;
                Rxn.id       = [Rxn.id '_Comp_' OC '_ChangedTo_' TC];
                Rxn.equation = regexprep(Rxn.equation,{[OC ' '],[OC '$']},{[TC ' '],TC});
                if isempty(Rxn.comment)
                    Rxn.comment = ['Comp from ' OC ' to ' TC];
                else
                    Rxn.comment = [Rxn.comment ';Comp from ' OC ' to ' TC];
                end
                Rxn = Rxn.getReactants;
                
                this.reactions{RxnIDx(i)} = Rxn;
                this.TrackRxn('ChangedComp',origID,Rxn);
                disp([char(10) 'Changed the original reaction into:'])
                this.PrintEquations(RxnIDx(i));
            end
        end
        function this = addFreeTrans(this,SpecIDx,OC,TC,BaseRxn,dir)
            % This function adds a free transport reaction from compartment OC to TC
            % for specied SpecID: A@OC <==> A@TC.
            %%% INPUT
            % SpecIDx   (logical) Index of the species
            % OC        Original compartment
            % TC        Target compartment
            %%% OPTIONAL INPUT
            % origin    Origin of the reaction, by default not from any original model
            % dir       if true, reaction becomes A@OC --> A@TC.
            
            if iscell(OC), OC = OC{1}; end
            if iscell(TC), TC = TC{1}; end
            
            if nargin < 6
                dir = false;
            end
            
            SpecID = this.species{SpecIDx}.id;
            
            % Find reaction ID and check if already in model
            id = ['Free_Trans_' SpecID '_' OC this.DEL TC];
            if ismember(id,this.reaction_IDs); % Don't add reaction if already present;
                return
            end
            
            % Create new rxn
            Rxn = MergedReaction([],BaseRxn.Origin);
            Rxn.SrcRxns = {BaseRxn};
            Rxn.id       = id;
            if dir
                Rxn.equation = ['1 ' SpecID '@' OC '--> 1 ' SpecID '@' TC];
            else
                Rxn.equation = ['1 ' SpecID '@' OC ' <==> 1 ' SpecID '@' TC];
            end
            [Rxn.lb,Rxn.ub] = this.EqToBounds(Rxn.equation);
            Rxn.biomass  = 0;
            Rxn.comment = ['orig:%' num2str(Rxn.Origin) '%'];
            Rxn.external = false;
            Rxn = Rxn.getReactants;
            
            % Adding the new reaction to the model
            this.addReaction(Rxn);
            this = TrackRxn(this,'NewRxn','',Rxn);
            disp([char(10) 'Added new free transport reaction:'])
            this.PrintEquations(this.NumReactions);
        end
        function RxnToRxn = RxnToRxn(this,S)
            % This function checks which reactions are connected with each other
            % through their metabolites
            %%% OPTIONAL INPUT
            % S     Stoichiometric matrix. Default: this.Stoich. NOTE: It is
            %       recommendable to remove hub metabolites. The Ignored mets are
            %       always removed.
            
            if nargin<2
                S = this.Stoich;
                S = this.RemIgnored(S);
            end
            
            % Convert S to Sirr (Only irreversible reactions
            [Sirr,OrigIDx] = this.IrrS(S);
            
            % Split Sirr into SirrPos and SirrNeg, corresponding to educts and products
            % of reactions
            SirrPos = Sirr>0;
            SirrNeg = Sirr<0;
            
            % For each reaction, check which other reactions can utilize its' products
            RxnToRxn = false(size(S,2)); %Pre-allocate variable
            for i=1:size(Sirr,2)
                IDx = OrigIDx(any(SirrNeg(SirrPos(:,i),:),1)); %Indices of reactions which can consume a product of rxn i
                RxnToRxn(OrigIDx(i),IDx) = true;
            end
            
            % Reactions can not match themselves (reversible reactions will
            % automatically have this)
            RxnToRxn = RxnToRxn .* ~eye(size(RxnToRxn));
        end
        function this = ChangeEq(this,RxnIDx,NewEq)
            %this function is used to change the reaction equation.
            Rxn = this.reactions{RxnIDx};
            if iscell(NewEq)
                NewEq = NewEq{1};
            end
            Rxn.equation = NewEq;
            Rxn = Rxn.getReactants;
            this.reactions{RxnIDx} = Rxn;
            this.TrackRxn('EqChanged',this.reactions{RxnIDx}.id,Rxn);
        end
        function this = ReverseRxns(this,RxnIDx)
            %Reverses the reading direction of all reactions indiciated by
            %RxnIDx.
            %Parse input
            if islogical(RxnIDx)
                RxnIDx=find(RxnIDx);
            end
            %Revert reactions & Switch reaction direction prediction for
            %reaction, if available.
            for i=1:numel(RxnIDx)
                this.reactions{RxnIDx(i)} = this.reactions{RxnIDx(i)}.ReverseReaction;
                if ~isempty(this.pDir) && any(ismember(this.pDir.ids,this.reactions{RxnIDx(i)}.id))
                    this.pDir.dir(ismember(this.pDir.ids,this.reactions{RxnIDx(i)}.id)) = -1*this.pDir.dir(ismember(this.pDir.ids,this.reactions{RxnIDx(i)}.id));
                end
            end
        end
        %equations
        function [lb,ub] = EqToBounds(this,eq)
            % This function sets default upper and lower bounds depending on the arrow
            % in the equation.
            arrow = regexp(eq,'[<\->=?]{3,4}','match','once');
            
            switch arrow
                case {'<==>'}
                    lb = this.LBdef;
                    ub = this.UBdef;
                case {'<--'}
                    lb = this.LBdef;
                    ub = 0;
                case {'-->'}
                    lb = 0;
                    ub = this.UBdef;
                case {'<?>'}
                    if this.options.lbUnknownArrow
                        lb = this.options.lbUnknownArrow;
                    else
                        while ~exist('lb','var')
                            reply = input(['Arrow type <?> cannot be readily interpreted, please choose a lower bound for the reaction.\n'...
                                'Enter D to set a default for all future cases'],'s');
                            if strcmpi(reply,'D')
                                disp('Please enter the default value (<=0) or enter * to cancel');
                                reply = COMMGEN.InputProcessing('realnumber');
                                if strcmpi(reply,'*')
                                    disp('No default value set')
                                else
                                    fprintf('Default value %s set',reply)
                                    this.options.lbUnknownArrow = reply;
                                    lb = reply;
                                end
                            else
                                reply = str2double(reply);
                                if isempty(reply) || ~isnumeric(reply) || ~isreal(reply) || ~isnan(reply)
                                    disp('A real number is required')
                                else
                                    lb = reply;
                                end
                            end
                        end
                    end
                    
                    if this.options.ubUnknownArrow
                        ub = this.options.ubUnknownArrow;
                    else
                        while ~exist('ub','var')
                            reply = input(['Arrow type <?> cannot be readily interpreted, please choose an upper bound for the reaction.\n'...
                                'Enter D to set a default for all future cases'],'s');
                            if strcmpi(reply,'D')
                                disp('Please enter the default value (>=0) or enter * to cancel');
                                reply = COMMGEN.InputProcessing('realnumber');
                                if strcmpi(reply,'*')
                                    disp('No default value set')
                                else
                                    fprintf('Default value %s set',reply)
                                    this.options.ubUnknownArrow = reply;
                                    ub = reply;
                                end
                            else
                                reply = str2double(reply);
                                if isempty(reply) || ~isnumeric(reply) || ~isreal(reply) || ~isnan(reply)
                                    disp('A real number is required')
                                else
                                    ub = reply;
                                end
                            end
                        end
                    end
            end
        end
        function [equation,flag,Cancel] = MergeEquation(this,group)
            flag = '';
            Cancel = false;
            if islogical(group)
                RxnIDx = find(group);
            else
                RxnIDx = group;
            end
            Rxns = this.reactions(RxnIDx);
            
            IgnoredIDx = find(ismember(this.species_IDs,this.options.IgnoredSpec));
            NonIgnoredMets = ~ismember(this.MetToSpecies,IgnoredIDx);
            
            % Reverse reaction equation if more convenient
            for i=2:numel(RxnIDx)
                if this.Stoich(NonIgnoredMets,RxnIDx(1))'*this.Stoich(NonIgnoredMets,RxnIDx(i)) < this.Stoich(NonIgnoredMets,RxnIDx(1))'*-1*this.Stoich(NonIgnoredMets,RxnIDx(i))
                    this.ReverseRxns(RxnIDx(i));
                    Rxns{i} = this.reactions{RxnIDx(i)};
                end
            end
            
            %
            equations = cell(numel(Rxns),1);
            for i=1:numel(Rxns)
                equations{i} = regexprep(Rxns{i}.equation,'^ *','');
            end
            
            % Check if equations are identical or only differing in
            % directionality
            if numel(unique(equations))==1 % If equal
                equation = equations{1};
                return
            elseif this.options.ExactlyIdentical
                equation = '';
                Cancel = 1;
                return
            elseif numel(unique(regexprep(equations,'[ <\->=]*','')))==1 % If only differing in directionality
                if this.options.MergeArrows==0
                    if this.options.AllAuto
                        equation = '';
                        Cancel = 1;
                        return
                    end
                elseif this.options.MergeArrows==1
                    %take bidirectional reaction
                    equation = regexprep(equations(1),{'-->','<--'},{'<==>','<==>'});
                    equation = equation{1};
                    return
                elseif this.options.MergeArrows==2
                    %Determine unidirectional reactions. If multiple
                    %inconsistent ones exist, take bidirectional.
                    arrows = regexprep(equations,'[^<\->=]*([<\->=]*)[^<\->=]*','$1');
                    arrows = unique(arrows(~ismember(arrows,'<==>')));
                    if numel(arrows)==1;
                        equation = regexprep(equations{1},{'<==>'},arrows);
                        return
                    else
                        equation = regexprep(equations{1},arrows,{'<==>','<==>'});
                        return
                    end
                elseif this.options.MergeArrows==3
                    %Take predicted directionality. 
                    
                    %Check if reactions are external (external reactions
                    %won't have a reaction direction prediction). If
                    %reactions are external, assume bidirectionality
                    if all(this.is_external(RxnIDx))
                        equation = regexprep(equations{1},{'<--','-->'},{'<==>','<==>'});
                        return
                    end
                    
                    %Check if reaction directionality prediction available
                    if isempty(this.pDir)
                        this.RDPredict;
                    end
                    p_IDx = ismember(this.pDir.ids,this.reaction_IDs(RxnIDx));
                    if ~any(p_IDx)
                        this.RDPredict;
                        p_IDx = ismember(this.pDir.ids,this.reaction_IDs(RxnIDx));
                    end
                    
                    p_dir = this.pDir.dir(p_IDx);
                    p_dir = p_dir(~isnan(p_dir));
                    
                    %Confirm that predicted directionalities between the
                    %different reactions match
                    if numel(unique(p_dir))==1
                        %Select corresponding equation
                        if p_dir(1)==-1
                            equation = regexprep(equations{1},{'<==>','-->'},{'<--','<--'});
                        elseif p_dir(1)==0
                            equation = regexprep(equations{1},{'<--','-->'},{'<==>','<==>'});
                        else
                            equation = regexprep(equations{1},{'<--','<==>'},{'-->','-->'});
                        end
                        return
                    end
                elseif this.options.MergeArrows==4
                    %Take the directionality from a particular model
                    
                    %Check that there is a single reaction from the
                    %selected model. If not: continue
                    if sum(this.RxnOrigin(RxnIDx,this.options.DirectModel))==1
                        %Select that equation and return
                        idx = intersect(RxnIDx,find(this.RxnOrigin(:,this.options.DirectModel)));
                        equation = this.reactions{idx}.equation;
                        return
                    end
                end
            else %Check if only difference is in stoichiometry / ignored species
                % Count how many of the ignored metabolites occur in each
                % reaction. If there
                Red_Eq = equations;
                for i=1:numel(this.options.IgnoredSpec)
                    Counter = cell2mat(cellfun(@(x,y) length(strfind(x,y)),equations,repmat(strcat(this.options.IgnoredSpec(i),'@'),numel(equations),1),'UniformOutput',false));
                    if max(Counter)==1;
                        Red_Eq = regexprep(Red_Eq,{ ...
                            ['^[0-9]* '       this.options.IgnoredSpec{i} '@[^ ]* \+ '], ... Replace at the start
                            ['([>-]) [0-9]* ' this.options.IgnoredSpec{i} '@[^ ]* \+'],  ... Replace right after the arrow
                            [' \+ [0-9]* '     this.options.IgnoredSpec{i} '@[^ ]*$'],   ... Replace at the end
                            ['\+ [0-9]* '     this.options.IgnoredSpec{i} '@[^ ]* ']},   ... Replace anywhere else
                            {'','$1','',''});
                    end
                end
                
                if numel(unique(regexprep(Red_Eq,{' [0-9] ','^[0-9] '},{'',''})))==1 % Only stoichiometric coefficients are different
                    Balanced = this.IsReactionBalanced(RxnIDx);
                    % If all balanced reactions equations are identical, take
                    % that equation.
                    if ~any(isnan(Balanced))
                        Balanced = logical(Balanced);
                        if numel(unique(Red_Eq(Balanced)))
                            equation = equations{find(Balanced,1)};
                            return
                        end
                    end
                elseif this.options.MergeArrows>=1 && numel(unique(regexprep(Red_Eq,{' [0-9] ','^[0-9] ','[ <\->=]*'},{'','',''})))==1
                    
                    if this.options.MergeArrows==1
                        Final_Equations = regexprep(equations,{'-->','<--'},{'<==>','<==>'});
                    elseif this.options.MergeArrows==2
                        arrows = regexprep(Red_Eq,'[^<\->=]*([<\->=]*)[^<\->=]*','$1');
                        arrows = unique(arrows(~ismember(arrows,'<==>')));
                        if numel(arrows)==1;
                            Final_Equations = regexprep(equations,{'<==>'},arrows);
                        else
                            Final_Equations = regexprep(equations,{'-->','<--'},{'<==>','<==>'});
                        end
                    elseif this.options.MergeArrows==3
                        %Take predicted directionality.
                        
                        %Check if reactions are external (external reactions
                        %won' have a reaction direction prediction). If
                        %reactions are external, assume bidirectionality
                        if all(this.is_external(RxnIDx))
                            equation = regexprep(equations{1},{'<--','-->'},{'<==>','<==>'});
                            return
                        end
                        
                        %Check if reaction directionality prediction available
                        if isempty(this.pDir)
                            this.RDPredict;
                        end
                        p_IDx = ismember(this.pDir.ids,this.reaction_IDs(RxnIDx));
                        if ~any(p_IDx)
                            this.RDPredict;
                            p_IDx = ismember(this.pDir.ids,this.reaction_IDs(RxnIDx));
                        end
                        
                        p_dir = this.pDir.dir(p_IDx);
                        p_dir = p_dir(~isnan(p_dir));
                        
                        %Confirm that predicted directionalities between the
                        %different reactions match
                        if numel(unique(p_dir))==1
                            %Select corresponding equation
                            if p_dir(1)==-1
                                Final_Equations = regexprep(equations,{'<==>','-->'},{'<--','<--'});
                            elseif p_dir(1)==0
                                Final_Equations = regexprep(equations,{'<--','-->'},{'<==>','<==>'});
                            else
                                Final_Equations = regexprep(equations,{'<--','<==>'},{'-->','-->'});
                            end
                        else
                            Final_Equations = equations;
                        end
                    elseif this.options.MergeArrows==4
                        %Take the directionality from a particular model
                        
                        %Check that there is a single reaction from the
                        %selected model. If not: continue
                        if sum(this.RxnOrigin(RxnIDx,this.options.DirectModel))==1
                            %Select that equation and return
                            idx = intersect(RxnIDx,find(this.RxnOrigin(:,this.options.DirectModel)));
                            Final_Equations = repmat({this.reactions{idx}.equation},numel(equations),1);
                        else
                            Final_Equations = equations;
                        end
                    end
                    
                    Balanced = this.IsReactionBalanced(RxnIDx);
                    % If all balanced reactions equations are identical, take
                    % that equation.
                    if ~any(isnan(Balanced))
                        Balanced = logical(Balanced);
                        if numel(unique(Final_Equations(Balanced)))==1
                            equation = Final_Equations{find(Balanced,1)};
                            return
                        end
                    end
                end
            end
            
            if this.options.AllAuto
                equation = '';
                Cancel = 1;
                return
            end
            
            while true
                disp([char(10) 'Reactions equations not identical:' char(10)])
                this.PrintEquations(group)
                disp([char(10) 'How do you want to proceed?' char(10)                                        ...
                    'K: Keep exact equation from one of the reactions, including directionality' char(10)    ...
                    'C: Cancel merge' char(10)                                                               ...
                    'R: Remove one of the reactions from the list, this reaction will not be merged' char(10)...
                    '   Can only be used if there are more than 2 reactions in the current merge.' char(10)   ...
                    'S: Switch the orientation of one of the reactions (for visualisation only).' char(10)   ...
                    'A: Enter alternative reaction equation for one of the reactions' char(10)               ...
                    'H: Get more information on the individual reactions' char(10)                           ...
                    'P: Update the reaction direction prediction analysis' char(10)                          ...
                    'M: Merge two species' char(10)                                                          ...
                    '*: Enter comment for later inspection'])
                if numel(Rxns)==2
                    reply = COMMGEN.InputProcessing({'K','C','S','A','H','P','M','*'});
                else
                    reply = COMMGEN.InputProcessing({'K','C','R','S','A','H','P','M','*'});
                end
                switch reply
                    case('*')
                        comment = input('Enter comment\n','s');
                        flag = [flag comment]; %#ok<AGROW>
                        clc, disp('Comment added')
                    case('H')
                        for i=Rxns'
                            disp(char(10))
                            i{1}.printfields({'equation','lb','ub','biomass','external'},'except')
                        end
                    case('R')
                        AllInputs = ['C' cellstr(num2str((1:numel(Rxns))'))'];
                        disp([char(10) 'Choose number corresponding to to-be removed reaction' char(10)   ...
                            'Or enter C to return to the previous question' char(10)]);
                        RxnChoice = COMMGEN.InputProcessing(AllInputs);
                        if ~strcmpi('C',RxnChoice)
                            RxnChoice = uint8(str2double(RxnChoice));
                            
                            % The initially proposed merge will not
                            % happen.
                            equation='';
                            Cancel = true;
                            group(RxnIDx(RxnChoice)) =false;
                            this.MergeReactions(group);
                            return;
                        end
                    case('S')
                        AllInputs = ['C' cellstr(num2str((1:numel(Rxns))'))'];
                        disp([char(10) 'Choose number corresponding to to-be reversed reaction' char(10)   ...
                            'Or enter C to return to the previous question' char(10)]);
                        RxnChoice = COMMGEN.InputProcessing(AllInputs);
                        if ~strcmpi('C',RxnChoice)
                            RxnChoice = uint8(str2double(RxnChoice));
                            this.ReverseRxns(RxnIDx(RxnChoice))
                            Rxns{RxnChoice} = this.reactions{RxnIDx(RxnChoice)};
                        end
                    case('K')
                        AllInputs = ['C' cellstr(num2str((1:numel(Rxns))'))'];
                        disp([char(10) 'Choose number corresponding to to-be kept reaction equation' char(10)   ...
                            'Or enter C to return to the previous question' char(10)]);
                        RxnChoice = COMMGEN.InputProcessing(AllInputs);
                        if ~strcmpi('C',RxnChoice)
                            RxnChoice = str2double(RxnChoice);
                            equation = Rxns{RxnChoice}.equation;
                            return
                        end
                    case('C')
                        disp('Merge cancelled')
                        equation = '';
                        Cancel = 1;
                        return
                    case('A')
                        fprintf('Please enter the ID of the reaction you want to adjust or C to cancel\n')
                        reply = COMMGEN.InputProcessing(this.reaction_IDs(group));
                        RxnIDx = strcmp(this.reaction_IDs,reply);
                        this.adjustReaction(RxnIDx);
                        this.reactions{RxnIDx} = this.reactions{RxnIDx}.getReactants;
                        [equation,flag,Cancel] = MergeEquation(this,group);
                        return
                    case('M')
                        fprintf('After the species are merged the current method (%s) will need to be rerun (will be exited via an error).',this.CurrentFunction)
                        fprintf('Please enter the ID/name of the first species\n')
                        Spec1 = COMMGEN.InputProcessing(this.SpeciesIDToName);
                        fprintf('Please enter the ID/name of the second species\n')
                        Spec2 = COMMGEN.InputProcessing(this.SpeciesIDToName);
                        %Find species indices
                        Spec1 = this.NameToID(Spec1);
                        Spec2 = this.NameToID(Spec2);
                        IDx = ismember(this.species_IDs,[Spec1 Spec2]);
                        CheckChanged = size(this.TrackSpecies,1);
                        this.MergeTwoSpecies(IDx);
                        if CheckChanged ~= size(this.TrackSpecies,1)
                            this.UpdateAll;
                            disp('Checking for duplicate reactions after species merging.')
                            this.DuplicateRxns;
                            this.UpdateAll;
                            error('Due to changes in species, any remaining reaction groups are no longer valid. Please rerun the code.')
                        end
                    case('P')
                        this.RDPredict();
                end
            end
        end
        function equation = EnterEquation(this,equation,CheckComp)
            
            Success = true;
            NewSpecies = {};
            
            if exist('equation','var') && ~isempty(equation)
                disp([char(10) 'Your previous entry was:' char(10) equation])
            end
            
            if nargin<3
                CheckComp=true;
            end
            
            disp([char(10) 'Instructions:' char(10)...
                'Valid arrows: <--|<==>|<?>|-->' char(10)...
                'Valid metabolite format: MNXM1@MNXC3 / H+@MNXC3, either metabolite id or name, always with compartment' char(10)...
                'Use + to separate metabolites' char(10)...
                'Make sure to have spaces between all different elements of the formula (e.g., stoichiometric coefficient and metabolite id/name' char(10)])
            equation = input('Enter equation\n','s');
            equation = regexprep(equation,'  *',' '); %Remove double spaces
            
            % load lists of species and metabolites present in the model
            speciesIDs     = this.species_IDs;
            
            % Pre-allocate variables
            n = length(regexp(equation,'@','match'));
            metabo = cell(1,n);         % list of metabolites
            ridx = 1;                   % reactand index
            fled = true;                   % flag: educt side of equation
            
            % Allowed transitions
            Tr = {'Stoichiometric coefficient','Metabolite','+','Arrow'};
            Trs = [...
                0 1 0 0; ... From Stoich   to met
                0 0 1 1; ... From met      to + or arrow
                1 1 0 0; ... From +        to stoich or met
                1 1 0 0] ... From arrow    to stoich or met
                ;
            Last = 3; % First elements should be a stoichiometric coefficient or a metabolite
            
            [rargs,nrargs] = srgetargs(strtrim(equation),' ');
            % check correctness of provided equation
            NumEduct = 0;
            for zr = 1:nrargs,
                s  = strtrim(rargs{zr});
                
                if any(strcmp(s, {'<--','<==>','<?>','-->'}));
                    if Trs(Last,4)
                        Last = 4;
                        if ~fled
                            disp('Reaction formula incorrect, more than one arrow')
                            Success = false;
                        end
                        fled = false;
                    else
                        fprintf('Transition from element %s to element %s not allowed\n',Tr{Last},Tr{4})
                        Success = false;
                    end
                elseif strcmp(s, '+')
                    if Trs(Last,3)
                        Last = 3;
                    else
                        fprintf('Transition from element %s to element %s not allowed\n',Tr{Last},Tr{3})
                        Success = false;
                    end
                else
                    coeff = COMMGEN.CheckStoichCoeff(s, fled);
                    if ~isnan(coeff)
                        if Trs(Last,1)
                            Last=1;
                        else
                            fprintf('Transition from element %s to element %s not allowed\n',Tr{Last},Tr{1})
                            Success = false;
                        end
                    else
                        if Trs(Last,2)
                            sID = this.NameToID(s);
                            [spc,comp] = COMMGEN.MetToSpecAndComp(sID);
                            if ~any(strcmp(comp,this.CompartmentIDToName(:,1)))
                                [~,CompName] = COMMGEN.MetToSpecAndComp(s);
                                fprintf('Compartment %s not recognised, only the following compartments may be used:',CompName{1})
                                disp(this.CompartmentIDToName)
                                Success = false;
                            end
                            if ~any(strcmp(spc,speciesIDs)) && Success;
                                SpcName = COMMGEN.MetToSpecAndComp(s);
                                fprintf(['Proposed species %s not present in current species list\n' ...
                                    'Do you wish to add it?\n [Y/N]\n'],SpcName{1})
                                reply = COMMGEN.InputProcessing({'Y','N'});
                                if strcmp(reply,'Y')
                                    NewSpecies{end+1} = SpcName{1}; %#ok<AGROW>
                                else
                                    Success = false;
                                end
                            end
                            if ~any(strcmp(sID,this.metabolites)) && Success && CheckComp
                                [SpcName,CompName] = COMMGEN.MetToSpecAndComp(s);
                                fprintf('Species %s has not been found earlier in compartment %s\n',SpcName{1},CompName{1})
                                disp(['Are you sure you wish to assign this compartment to this species [Y/N]?' char(10)]);
                                reply = COMMGEN.InputProcessing({'Y','N'});
                                if strcmp(reply,'N')
                                    Success=false;
                                end
                            end
                            sID = this.NameToID(s);
                            metabo(ridx) = sID;
                            ridx = ridx+1;
                            Last=2;
                            if fled
                                NumEduct = NumEduct + 1;
                            end
                        else
                            fprintf('Transition from element %s to element %s not allowed\n',Tr{Last},Tr{2})
                            Success = false;
                        end
                    end
                end
            end
            
            if fled ==1
                disp('Reaction formula incorrect, no arrow found')
                Success =false;
            end
            
            comps = unique(regexprep(metabo,'.*@',''));
            if numel(comps)>1
                comps = sort(comps);
                compstr = comps{1};
                for i=2:numel(comps)
                    compstr = [compstr '%' comps{i}]; %#ok<AGROW>
                end
                if ~any(strcmp(compstr,this.options.CompartmentConnections))
                    fprintf('The compartment connection %s has not been found previously. Are you certain?\n [Y/N]\n',compstr);
                    reply = COMMGEN.InputProcessing({'Y','N'});
                    if strcmp(reply,'Y')
                        this.options.CompartmentConnections(end+1) = {compstr};
                    else
                        Success = false;
                    end
                end
            end
            
            % Check if educts and products don't occur multiple times in
            % the same reaction (same side of the arrow)
            if NumEduct >0
                educts     = metabo(1:NumEduct);
            else
                educts  = '';
            end
            
            if NumEduct < n
                products     = metabo(NumEduct+1:end);
            else
                products ='';
            end
            if Success && (numel(educts)~=numel(unique(educts)) || numel(products)~=numel(unique(products)))
                disp('One or more metabolites found multiple times at the same side of the reaction, please try again.')
                Success = false;
            end
            
            if Success
                % Put reactants in alphabetic order and add potentially missing
                % stoichiometric coefficients (assuming them to be 1).
                equation = this.SortEquation(equation);
                
                % Add new species
                for i=1:numel(NewSpecies)
                    this.addNewSpecies(NewSpecies(i));
                end
            else
                equation = this.EnterEquation(equation,CheckComp);
            end
        end
        function equation = SortEquation(this,equation)
            % Re-organize the reaction such that all stoichiometric
            % coefficients are provided and that the reactants are in
            % alphabetic order. Also remove any
            
            % Pre-process equation
            equation = regexprep(equation,' +',' '); %Remove double spaces
            
            % Pre-allocate variables
            n = length(regexp(equation,'@','match'));
            metabo = repmat({''},1,n);  % list of metabolites
            scoeff = ones(1,n);     % stoichiometric coefficients
            ridx = 1;                   % reactand index
            fled = 1;                   % flag: educt side of equation
            Remove = false(n,1);        % Whether the metabolite should be kept. Used to remove metabolites occurring on both sides of the eq.
            
            % Read the reaction
            [rargs,nrargs] = srgetargs(strtrim(equation),' ');
            for zr = 1:nrargs,
                s  = strtrim(rargs{zr});
                if any(strcmp(s, {'<--','<==>','<?>','-->'}));
                    arrow = s;
                    fled = 0;
                    NumEduct = ridx -1;
                elseif strcmp(s, '+')
                    %ignore
                else
                    coeff = COMMGEN.CheckStoichCoeff(s, fled);
                    if ~isnan(coeff)
                        scoeff(ridx) = coeff;
                    else
                        sID = this.NameToID(s);
                        if ismember(sID,metabo) % Metabolite is present at both sides of the equation
                            IDx = ismember(metabo,sID);
                            if scoeff(IDx) > scoeff(ridx) % Educt side is larger
                                scoeff(IDx) = scoeff(IDx) - scoeff(ridx);
                                Remove(ridx) = true;
                                ridx = ridx + 1;
                            elseif scoeff(IDx) > scoeff(ridx) % Product side is larger
                                scoeff(ridx) = scoeff(ridx) - scoeff(IDx);
                                Remove(IDx) = true;
                                metabo(ridx) = sID;
                                ridx = ridx + 1;
                            else % Equal stoichiometric coefficients
                                Remove([ridx find(IDx)]) = true;
                                ridx = ridx + 1;
                            end
                        else
                            metabo(ridx) = sID;
                            ridx = ridx+1;
                        end
                    end
                end
            end
            
            % If needed, remove metabolites
            if any(Remove)
                scoeff(Remove) = [];
                metabo(Remove) = [];
                NumEduct = NumEduct - count(Remove(1:NumEduct));
                n = n - count(Remove);
            end
            
            % Create educt list
            if NumEduct >0
                educts     = metabo(1:NumEduct);
                eductcoeff = scoeff(1:NumEduct);
                % Sort
                [educts,I] = sort(educts);
                eductcoeff = eductcoeff(I);
            else
                educts  = '';
            end
            
            % Create Product list
            if NumEduct < n
                products     = metabo(NumEduct+1:end);
                productcoeff = scoeff(NumEduct+1:end);
                % Sort
                [products,I] = sort(products);
                productcoeff = productcoeff(I);
            else
                products ='';
            end
            
            % Recreate the equation
            equation = '';
            % Add educts
            for i=1:NumEduct
                if isempty(equation)
                    equation = [num2str(abs(eductcoeff(i))) ' ' educts{i}];
                else
                    equation = [equation num2str(-1*eductcoeff(i)) ' ' educts{i}]; %#ok<AGROW>
                end
                if i<NumEduct
                    equation = [equation ' + ']; %#ok<AGROW>
                end
            end
            
            % Add the arrow
            if isempty(equation)
                equation = [arrow ' '];
            else
                equation = [equation ' ' arrow ' '];
            end
            
            % Add the products
            for i=1:numel(products)
                equation = [equation num2str(abs(productcoeff(i))) ' ' products{i}]; %#ok<AGROW>
                if i<n-NumEduct
                    equation = [equation ' + ']; %#ok<AGROW>
                end
            end
        end
        %genes
        function GeneOrigin = GeneOrigin(this)
            % Returns for each gene whether or not it is associated
            % with reactions from each model
            
            % Load info
            RxnOrigin = this.RxnOrigin;
            RGM = this.rxnGeneMat;
            
            % Pre-allocate space
            GeneOrigin = false(this.NumGenes,this.NumModels);
            
            % Find gene origins
            for i=1:this.NumModels
                RxnSet = RxnOrigin(:,i);
                GeneOrigin(:,i) = any(RGM(:,RxnSet),2);
            end
        end
        function GeneComp = GeneComp(this)
            % Returns for each gene whether or not it is associated
            % with each compartment
            
            % Load info
            RxnC = this.RxnComp;
            RGM  = this.rxnGeneMat;
            Cmps = this.CompartmentIDToName(:,1);
            
            % Pre-allocate space
            GeneComp = false(this.NumGenes,this.NumCompartments);
            
            % Match genes to compartments
            for i=1:this.NumReactions
                Rcomps = srgetargs(strtrim(RxnC{i}),'%');
                Rcomps = ismember(Cmps,Rcomps);
                Rgenes = RGM(:,i);
                
                GeneComp(Rgenes,Rcomps) = true;
            end
        end
        function this = ChangeGeneName(this,OrigName,TargetName)
            %The genes indicated by group are merged and will afterwards be
            %known as CollapseTo
            %%% Input
            % group         Logical index of genes to be combined
            % CollapseTo    Index of gene name to be kept
            
            %Process input
            if ~ismember(OrigName,this.genes)
                fprintf('Gene %s unknown, cannot substitute by %s',OrigName,TargetName)
                return
            end
            OrigName = regexptranslate('escape',OrigName);
            
            %find gene names
            for i=1:this.NumReactions
                this.reactions{i}.proteins = regexprep(this.reactions{i}.proteins,...
                    {['^' OrigName '([;+])'],['([;+])' OrigName   '([;+])'],['([;+])' OrigName   '$'],['^' OrigName   '$']},...
                    {[    TargetName   '$1'],['$1'     TargetName '$2'    ],['$1'     TargetName    ],     TargetName    });
            end
        end
        function [GPR,NewGenes] = MakeGeneRules(this)
            %This function allows to create new GPR rules for a reaction
            %and asserts they are correct
            GPR = COMMGEN.InputProcessing('GPR');
            GPR = COMMGEN.StandardGPR(GPR);
            if exist('this','var')
                Genes = splitstr(GPR,'[;+]');
                Genes = regexprep(Genes,{'^ *',' *$'},{'',''});
                if ~all(ismember(Genes,this.genes))
                    disp(['The following genes are not yet in the model, are you sure this GPR is correct?' char(10)])
                    disp(Genes(~ismember(Genes,this.genes)))
                    disp(['Y: Continue'     char(10) ...
                        'N: Enter new rule' char(10) ...
                        ])
                    reply = COMMGEN.InputProcessing({'Y','N'});
                    if strcmp('N',reply)
                        GPR = this.MakeGeneRules;
                    else
                        NewGenes = Genes(~ismember(Genes,this.genes));
                        this.genes = [this.genes NewGenes];
                        this.GenesFirstSeen(end+1:end+numel(NewGenes)) = this.Phase;
                        this.GenesLastSeen(end+1:end+numel(NewGenes)) = this.Phase;
                    end
                end
            end
        end
        %rejected proposals
        function this    = RejectMerge(this,group)
            % Keep track of rejections
            %%% Input
            % Rxns:         The reactions for which the merge was cancelled
            %%% Optional Input
            % flag:         If comments were entered during the merge,
            %               these can now be added to the original
            %               reactions.
            
            % If no default answer is given, prompt and allow for default answer
            if this.options.PermanentReject == 0 && ~this.options.AllAuto;
                disp('The merge between these reactions was cancelled:')
                this.PrintEquations(group)
                disp(['May this merge be proposed again later?' char(10) ...
                    'y: Yes.'                                   char(10) ...
                    'n: No.'                                    char(10) ...
                    'Y: Yes. (set as default answer (reactions and species))'           char(10) ...
                    'N: No.  (set as default answer (reactions and species))'           char(10)])
                Choice = COMMGEN.InputProcessing({'Y','N','n','y'});
                if strcmpi(Choice,'Y')
                    this.options.PermanentReject = -1;
                elseif strcmpi(Choice,'N')
                    this.options.PermanentReject = 1;
                end
            % Else, use default options
            elseif this.options.PermanentReject==1
                Choice = 'y';
            elseif this.options.PermanentReject==-1
                Choice = 'n';
            elseif this.options.AllAuto 
                fprintf('\nPlease set this.options.PermanentReject. Defaulted to not permanently rejecting cancelled merges.\n')
                Choice = 'n';
            end

            % Get GroupName
            GroupName = COMMGEN.GetGroupName(this.reactions(group));
            
            % Permanently or temporarily reject the merge.
            if strcmp(Choice,'y')
                this.RejectedMerge{end+1,1} = GroupName;
            else
                this.TempRejectedMerge{end+1,1} = GroupName;
            end
        end
        function this    = RejectSpeciesMerge(this,speciesIDx)
            % this function tracks rejected merges between species.
            %%% Input:
            %   speciesIDx  Logical or double corresponding to the indices
            %               of exactly two species;
            
            if islogical(speciesIDx)
                speciesIDx = find(speciesIDx);
            end
            
            assert(numel(speciesIDx)==2,'This function expects two species indices as input');
            
            % If no default answer is given, prompt and allow for default answer
            if this.options.PermanentReject == 0;
                disp('The merge between these spcies was cancelled:')
                this.IDToName(this.species_IDs(speciesIDx))
                disp(['May this merge be proposed again later?' char(10) ...
                    'y: Yes.'                                   char(10) ...
                    'n: No.'                                    char(10) ...
                    'Y: Yes. (set as default answer)'           char(10) ...
                    'N: No.  (set as default answer)'           char(10)])
                Choice = COMMGEN.InputProcessing({'Y','N','n','y'});
                if strcmp(Choice,'Y')
                    this.options.PermanentReject = -1;
                elseif strcmp(Choice,'N')
                    this.options.PermanentReject = 1;
                end
            end
            % Process default answers is available
            if this.options.PermanentReject ==1;
                Choice = 'n';
            elseif this.options.PermanentReject ==-1;
                Choice = 'y';
            end
            
            GroupName = COMMGEN.GetGroupName(this.species(speciesIDx));
            
            % Permanently or temporarily reject the merge.
            if strcmp(Choice,'n')
                this.RejectedMerge{end+1,1} = GroupName;
            else
                this.TempRejectedMerge{end+1,1} = GroupName;
            end
        end
        function groups  = FilterGroups(this,groups,species)
            % Filter proposed merges for reaction/species groups which were proposed and permanently rejected before
            
            if nargin<3
                species = false;
            end
            
            % Load previous rejections
            Rejected = [this.RejectedMerge;this.TempRejectedMerge];
            
            % If so far no permanent rejections exist or if 'groups' is already empty
            % return to calling function.
            if isempty(Rejected) || isempty(groups)
                return
            end
            
            % Find the name corresponding to a rejection of this group
            GroupRejectionName = cell(size(groups,1),1);
            for i=1:size(groups,1)
                if species
                    id = this.species_IDs(groups(i,:));
                else
                    Rxns = this.reactions(groups(i,:));
                    id = cell(numel(Rxns,1));
                    for j=1:numel(Rxns)
                        id{j} = Rxns{j}.id;
                    end
                end
                id = sort(id);
                Rejection = id{1};
                for j=2:count(groups(i,:))
                    Rejection = [Rejection ';' id{j}]; %#ok<AGROW>
                end
                GroupRejectionName{i} = Rejection;
            end
            
            % Remove the groups which have been rejected permanently before
            groups(ismember(GroupRejectionName,Rejected),:) = [];
            
        end
        %tracking changes
        function this    = ChangeFunction(this,FuncName)
            % This function is called when the function for condensing the
            % consensus model is changed. It ensures that the structure
            % which is passed is correct, keeps track of what is being done
            % and resets the temporarily rejected reaction merges.
            
            % Ensure passing a correct structure
            this.UpdateAll;
            
            % Reset temporarily rejected reaction merges
            if ~strcmp(this.CurrentFunction,FuncName)
                this.TempRejectedMerge = {};
            end
            % Keep track of currently running function
            this.CurrentFunction = FuncName;
        end
        function this = TrackRxn(this,Relation,OrigName,Rxn)
            % This function updates this.TrackReactions based on changes in
            % reactions.
            MergeOverview = cell(1,11);
            MergeOverview(1) = {Relation};
            MergeOverview(2) = {OrigName};
            if nargin==4 && ~isempty(Rxn)
                MergeOverview(3) = {Rxn.id};
                MergeOverview(4) = {Rxn.comment};
                MergeOverview(5) = {Rxn.equation};
                MergeOverview(6) = {Rxn.proteins};
                MergeOverview(7) = {Rxn.lb};
                MergeOverview(8) = {Rxn.ub};
                MergeOverview(11) = {Rxn.external};
            else
                MergeOverview([3:8 11]) = {''};
            end
            MergeOverview(9) = {this.Phase};
            MergeOverview(10) = {this.CurrentFunction};
            this.TrackReactions(end+1,:) = MergeOverview;
        end
        function this = TrackSpec(this,Relation,Spec,OrigID,OrigName,Comment)
            % This function updates this.TrackSpecies based on changes in
            % species.
            MergeOverview = cell(1,12);
            MergeOverview(1) = {Relation};
            
            if nargin>=4
                MergeOverview(2) = {OrigID};
            else
                MergeOverview(2) = {''};
            end
            
            if nargin>=5
                MergeOverview(3) = {OrigName};
            else
                MergeOverview(3) = {''};
            end
            
            MergeOverview(4) = {Spec.id};
            MergeOverview(5) = {Spec.name};
            if nargin==6
                MergeOverview(6) = {Comment};
            else
                MergeOverview(6) = {''};
            end
            MergeOverview(7) = {Spec.formula};
            MergeOverview(8) = {Spec.mass};
            MergeOverview(9) = {Spec.charge};
            MergeOverview(10)= {Spec.references};
            MergeOverview(11)= {this.Phase};
            MergeOverview(12)= {this.CurrentFunction};
            this.TrackSpecies(end+1,:) = MergeOverview;
        end
        function this = SummaryStatistics(this)
            % Summary Statistics
            ModelVal = 0:this.NumModels-1;
            ModelVal = 2.^ModelVal';
            
            SpecVal = this.SpecOrigin*ModelVal;
            RxnVal  = this.RxnOrigin *ModelVal;
            
            this.TrackSpecStats(end+1,:) = num2cell(histc(SpecVal,0:2^numel(this.Models)-1));
            this.TrackRxnStats(end+1,:)  = num2cell(histc(RxnVal,0:2^numel(this.Models)-1));
        end
        function SaveChoices(this,dirname)
            %%% PURPOSE
            % This function saves the changes that have been made to the
            % combined model so far as well as the current options as
            % saved under this.options. The changes are saved in three .tsv
            % files. The options are saved as a mat file. Using these
            % files, COMMGEN can rerun the entire procedure automatically.
            % Alternatively, choices can be reverted by removing the
            % corresponding lines from the tsv files.
            %%% INPUT
            %   this: Current model instance.
            %%% OPTIONAL INPUT
            %   dirname: name of the directory in which to save the changes
            %   and options file.
            %   Default value: Concatenation of the input model names,
            %   [this.ModelNames{:}]
            %%% OUTPUT
            
            %Gather to-be saved information
            TrackSpec = this.TrackSpecies;
            TrackRxns = this.TrackReactions;
            RejMerge = this.RejectedMerge;
            opts = this.options; %#ok<NASGU>
            CompIDToName = this.CompartmentIDToName; %#ok<NASGU>
            
            %Create folder
            if nargin<2
                dirname = [[this.ModelNames{:}] '_Phase' num2str(this.Phase)];
            end
            if exist(dirname,'dir')~=7
                try
                    mkdir(dirname);
                catch
                    error('Failed at creating the directory. Please check the input argument(s) for invalid characters.')
                end
            end
            
            save([dirname '/options.mat'],'opts','CompIDToName')
            
            % write to tsv files
            fid = fopen([dirname '/TrackSpec.tsv'], 'w+');
            fprintf(fid,'%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n',TrackSpec{1,1},TrackSpec{1,2},TrackSpec{1,3},TrackSpec{1,4},TrackSpec{1,5},TrackSpec{1,6},TrackSpec{1,7},TrackSpec{1,8},TrackSpec{1,9},TrackSpec{1,10},TrackSpec{1,11},TrackSpec{1,12});
            for i=2:size(TrackSpec,1)
                fprintf(fid,'%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%d\t%s\n',TrackSpec{i,1},TrackSpec{i,2},TrackSpec{i,3},TrackSpec{i,4},TrackSpec{i,5},TrackSpec{i,6},TrackSpec{i,7},TrackSpec{i,8},TrackSpec{i,9},TrackSpec{i,10},TrackSpec{i,11},TrackSpec{i,12});
            end
            fclose(fid);
            
            fid = fopen([dirname '/TrackRxns.tsv'], 'w+');
            fprintf(fid,'%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n',TrackRxns{1,1},TrackRxns{1,2},TrackRxns{1,3},TrackRxns{1,4},TrackRxns{1,5},TrackRxns{1,6},TrackRxns{1,7},TrackRxns{1,8},TrackRxns{1,9},TrackRxns{1,10},TrackRxns{1,11});
            for i=2:size(TrackRxns,1)
                fprintf(fid,'%s\t%s\t%s\t%s\t%s\t%s\t%d\t%d\t%d\t%s\t%d\n',TrackRxns{i,1},TrackRxns{i,2},TrackRxns{i,3},TrackRxns{i,4},TrackRxns{i,5},TrackRxns{i,6},TrackRxns{i,7},TrackRxns{i,8},TrackRxns{i,9},TrackRxns{i,10},TrackRxns{i,11});
            end
            fclose(fid);
            
            fid = fopen([dirname '/RejMerge.tsv'], 'w+');
            for i=1:size(RejMerge,1)
                fprintf(fid,'%s\n',RejMerge{i});
            end
            fclose(fid);
        end
        
        % Reaction direction prediction
        function this = RDPredict(this)
            %%% PURPOSE
            % This method is used to predict reaction directions based on the method
            % from Ganter et al 2014. The predicted directionalities are used to solve
            % discrepancies between reaction directions in different models. This
            % method will need to be re-run after metabolites are merged in as this
            % changes the network structure.
            %%% EXAMPLE CALL
            %   RDPredict(this);
            %%% INPUT
            %%% OPTIONAL INPUT
            %%% OUTPUT
            %   this: Updated COMMGEN model structure which includes
            %   predicted reaction directionalities for all reactions.
            
            % Combine input and database models into a single input and
            % single database model.
            
            fprintf('Performing reaction direction prediction analysis.')
            dbNames = this.options.databaseNames();
            
            %Check existence of models forming the database
            for i=1:numel(dbNames)
                if exist(dbNames{i}, 'dir')~=7
                    fprintf('\nERROR: Directory %s (database) does not exist\n\n', dbNames{i});
                    return;
                end
            end
            
            %Export COMMGEN model
            fprintf('Creating model containing query reactions')
            model_dir = 'RDPredict_input';
            db_dir = 'RD_database';
            this.writeConsensusModelToDisk(model_dir);
            
            % define input files
            %model
            FileNameModel        = [model_dir '/model.tsv'];
            FileNameCompartments = [model_dir '/compartments.tsv'];
            FileNameReactions    = [model_dir '/reactions.tsv'];
            FileNameChemcials    = [model_dir '/chemicals.tsv'];
            %database
            FileNameModelDB        = [db_dir '/model.tsv'];
            FileNameCompartmentsDB = [db_dir '/compartments.tsv'];
            FileNameReactionsDB    = [db_dir '/reactions.tsv'];
            FileNameChemicalsDB    = [db_dir '/chemicals.tsv'];
            
            % Combine models into single database model
            fprintf('Creating model containing database/reference reactions')
            
            if ~isempty(this.pDir)
                %For each database model that's part of the input models, export the
                %current COMMGEN representation. Otherwise, use original model files.
                %Check both model name and full path
                FullModelPaths = cell(1,this.NumModels);
                for i=1:this.NumModels
                    FullModelPaths{i} = regexprep(this.Models{i}.dir,'/$','');
                end
                for i=1:numel(dbNames)
                    if ismember(dbNames(i),this.ModelNames) || ismember(dbNames(i),FullModelPaths)
                        idx = ismember(this.ModelNames,dbNames(i)) | ismember(FullModelPaths,dbNames(i));
                        dbNames{i} = [db_dir '/tmp_' dbNames{i}];
                        this.writeConsensusModelToDisk(dbNames{i},this.RxnOrigin(:,idx));
                    end
                end
            end
            
            if exist(db_dir,'dir')~=7
                mkdir(db_dir);
            end
            
            %model file
            fprintf('# Database model file\n');
            fid = fopen(FileNameModelDB, 'w+');
            fprintf(fid,'ID\t%s\t\t\n',['Database_' strjoin(dbNames,'_')]);
            fclose(fid);
            
            %compartments file
            fprintf('# Database compartments file\n');
            dos(['sort -u ' strjoin(dbNames,'/compartments.tsv ') '/compartments.tsv '  ... Concatenate files
                '| awk -F\t ''{$3="";}1'' OFS="\t" ' ... Removes third column (contains original compartment name, which is irrelevant)
                '| uniq ' ... Removes duplicate entries.
                '> ' FileNameCompartmentsDB]); %Save output as compartments.tsv
            
            %reactions file
            fprintf('# Database reactions file\n');
            dos(['cat ' strjoin(dbNames,'/reactions.tsv ') '/reactions.tsv '  ... Concatenate files
                '> ' FileNameReactionsDB]); %Save output as reactions.tsv. Duplicate entries are not removed as they provide additional confirmation of the proposed reaction directionality
            
            %chemicals file
            fprintf('# Database chemicals file\n');
            dos(['sort -u ' strjoin(dbNames,'/chemicals.tsv ') '/chemicals.tsv '  ... Concatenate files
                '| awk -F\t ''{$3=$7="";}1'' OFS="\t" ' ... Removes third column (contains original metabolite name, which is irrelevant)
                '| uniq ' ... Removes duplicate entries.
                '> ' FileNameChemicalsDB]); %Save output as chemicals.tsv
            
            % Remove non-printable characters from .tsv files for
            % compatibility with SBML format and subsequent functions
            dos(['tr -d "\26" < ' FileNameModelDB ' > tmp && mv tmp ' FileNameModelDB]);
            dos(['tr -d "\26" < ' FileNameReactionsDB ' > tmp && mv tmp ' FileNameReactionsDB]);
            dos(['tr -d "\26" < ' FileNameChemicalsDB ' > tmp && mv tmp ' FileNameChemicalsDB]);
            dos(['tr -d "\26" < ' FileNameCompartmentsDB ' > tmp && mv tmp ' FileNameCompartmentsDB]);
            
            % Remove CR line terminators - if any
            dos(['tr "\r" "\n" < ' FileNameModelDB ' > tmp && mv tmp ' FileNameModelDB]);
            dos(['tr "\r" "\n" < ' FileNameReactionsDB ' > tmp && mv tmp ' FileNameReactionsDB]);
            dos(['tr "\r" "\n" < ' FileNameChemicalsDB ' > tmp && mv tmp ' FileNameChemicalsDB]);
            dos(['tr "\r" "\n" < ' FileNameCompartmentsDB ' > tmp && mv tmp ' FileNameCompartmentsDB]);
            
            % read model and database
            %read query files and construct model
            fprintf('\nReading query reactions\n')
            data  = MnXReader(FileNameModel, FileNameCompartments, FileNameReactions, FileNameChemcials);
            model = MnXModel(data);
            
            %read database files and construct model
            fprintf('\nReading database reactions\n')
            data  = MnXReader(FileNameModelDB, FileNameCompartmentsDB, FileNameReactionsDB, FileNameChemicalsDB);
            database = MnXModel(data);
            
            % remove folders
            rmdir(model_dir,'s');
            rmdir(db_dir,'s');
            
            % predict reaction directions using reaction motifs and print to screen
            if exist('model','var') && exist('database','var')
                fprintf('\n Predicting reaction directionalities\n');
                this.pDir = MnXRMPredictions_test(model, database, 'direction');
                fprintf('Reaction direction prediction successful.\n')
            else
                error('ERROR: Cannot load model or database\n');
            end
        end
        
        % Printing
        function PrintAllRxnsForSpec(this,Species,OnlyTrans)
            % Enter metabolite or species ID/Name to print all reactions
            % which are associated with the species.
            
            % Input processing
            if ~exist('OnlyTrans','var')
                OnlyTrans = false;
            end
            
            if OnlyTrans
                message = 'transport ';
            else
                message = '';
            end
            
            % Ensure cell input
            if ischar(Species)
                Species = {Species};
            end
            
            ID = Species;
            
            % Process metabolite / species information
            % Keep only species information (No effect if already just
            % species)
            if ~isempty(regexp(ID{1},'@','once'))
                ID = COMMGEN.MetToSpecAndComp(ID);
            end
            
            % Convert species Name to ID
            if ~ismember(ID,this.species_IDs)
                ID = this.NameToID(ID);
            end
            
            % Obtain species Index
            IDx = find(strcmp(ID,this.species_IDs));
            if IDx ==0
                fprintf('Species/Metabolite %s not recognised, check spelling',Species{1})
                return
            end
            
            % Obtain corresponding metabolites
            MetIDx = this.MetToSpecies == IDx;
            
            % Find and print reactions
            
            % Obtain corresponding reactions
            Rxns = any(this.Stoich(MetIDx,:),1)';
            if OnlyTrans == true
                Rxns = Rxns & this.is_transport;
            end
            RxnIDx = find(Rxns);
            
            if any(RxnIDx)
                Name = this.IDToName(ID);
                fprintf('\nPrinting reactions for %s\n\n',Name{1})
                for i=1:numel(RxnIDx)
                    this.PrintEquations(RxnIDx(i));
                end
            else
                fprintf('No %sreactions found for %s',message,Species{1})
            end
            
        end
        function PrintEquations(this,group,NoComp)
            %Prints matched reaction formulas in a way that differences and similarities
            %can be spotted easily.
            %%% INPUT
            % group     logical vector or indices of the reactions to be
            %           printed
            %%% OPTIONAL INPUT
            % NoComp    Whether the compartment information should be
            %           printed separately from the reaction equation. This
            %           can be convenient when comparing reactions from
            %           different compartments.
            
            if ~any(group)
                return
            end
            
            if islogical(group)
                RxnIDx        = find(group);
            else
                RxnIDx = unique(group);
                group = false(this.NumReactions,1);
                group(RxnIDx) = true;
            end
            NumRxns       = numel(RxnIDx);
            educts        = {};
            products      = {};
            eductStoich   = zeros(1,NumRxns);
            productStoich = zeros(1,NumRxns);
            
            RxnComp = cell(NumRxns,1);
            if nargin < 3 || NoComp
                for i=1:NumRxns
                    if numel(unique(regexp(this.reactions{RxnIDx(i)}.equation,'@[^ ]*','match'))) == 1
                        RxnComp(i) = unique(regexp(this.reactions{RxnIDx(i)}.equation,'@[^ ]*','match'));
                        NoComp = true;
                    else
                        NoComp = false;
                    end
                end
            end
            
            arrow = cell(NumRxns,1);
            RxnIDs= cell(NumRxns,1);
            Origin= cell(NumRxns,1);
            GPR   = cell(NumRxns,1);
            Balance = this.IsReactionBalanced(group);
            
            % Get reaction arrows, educts and products, IDs, origin & Gene
            % rules
            for i=1:NumRxns
                Rxn = this.reactions{RxnIDx(i)};
                arrow{i,1} = regexp(Rxn.equation,'[<\->=?]*','match','once');
                RxnIDs{i}  = Rxn.id;
                Origin{i}  = num2str(Rxn.Origin);
                GPR{i}     = Rxn.proteins;
                for j=1:numel(Rxn.reactants)
                    % Convert metabolite names to species names if compartment
                    % information is to be ignored.
                    if NoComp && ~any(cellfun(@isempty,RxnComp))
                        Reactant = char(this.MetToSpecAndComp(Rxn.reactants{j}.id));
                    else
                        Reactant = Rxn.reactants{j}.id;
                    end
                    if Rxn.reactants{j}.stoichiometry<0
                        if ~ismember(Reactant,educts)
                            educts{end+1} = Reactant; %#ok<AGROW>
                        end
                        eductStoich(strcmp(Reactant,educts),i) = -1*Rxn.reactants{j}.stoichiometry;
                    else
                        if ~ismember(Reactant,products)
                            products{end+1} = Reactant; %#ok<AGROW>
                        end
                        productStoich(strcmp(Reactant,products),i) = Rxn.reactants{j}.stoichiometry;
                    end
                end
            end
            
            % Get human readable names for metabolites
            educts   = this.IDToName(educts);
            products = this.IDToName(products);
            
            % Construct reaction formulas
            equations = cell(NumRxns+1,1);
            
            % Display reaction number (in current selection)
            if NumRxns>1
                equations(2:end) = strcat(strread(num2str(1:NumRxns),'%s'),{')  '},equations(2:end));
                N = max(cellfun(@length,equations));
                pad = repmat({sprintf('%s%ds','%',N)},NumRxns+1,1);
                equations = cellfun(@sprintf,pad,equations,'UniformOutput',false);
            end
            
            % Display reaction Origin
            NewPart = cell(NumRxns+1,1);
            NewPart{1} = 'Origin';
            NewPart(2:end) = Origin;
            N = max(cellfun(@length,NewPart));
            pad = repmat({sprintf('%s-%ds','%',N)},NumRxns+1,1);
            NewPart = cellfun(@sprintf,pad,NewPart,'UniformOutput',false);
            equations = strcat(equations,NewPart);
            
            % Display whether reaction is balanced or not
            NewPart = cell(NumRxns+1,1);
            NewPart{1} = 'Balanced';
            for i=1:NumRxns
                NewPart{i+1} = num2str(Balance(i));
            end
            N = max(cellfun(@length,NewPart));
            pad = repmat({sprintf('%s%ds','%',N)},NumRxns+1,1);
            NewPart = cellfun(@sprintf,pad,NewPart,'UniformOutput',false);
            equations = strcat(equations,{'  '},NewPart);
            
            % Display reaction identifier
            NewPart = cell(NumRxns+1,1);
            NewPart{1} = 'Reaction ID';
            NewPart(2:end) = strcat(RxnIDs,{':   '});
            N = max(cellfun(@length,NewPart));
            pad = repmat({sprintf('%s%ds','%',N)},NumRxns+1,1);
            NewPart = cellfun(@sprintf,pad,NewPart,'UniformOutput',false);
            equations = strcat(equations,{'  '},NewPart);
            
            % Display reaction compartment
            if NoComp && ~any(cellfun(@isempty,RxnComp))
                RxnComp = regexprep(RxnComp,'@','');
                RxnComp = this.IDToName(RxnComp);
                % Display compartment if this information is excluded from the
                % the actual equation
                NewPart = cell(NumRxns+1,1);
                NewPart{1} = 'Comp ';
                NewPart(2:end) = RxnComp;
                N = max(cellfun(@length,NewPart));
                pad = repmat({sprintf('%s-%ds','%',N)},NumRxns+1,1);
                NewPart = cellfun(@sprintf,pad,NewPart,'UniformOutput',false);
                equations = strcat(equations,{''},NewPart,{'  '});
            end
            
            % Educts
            for i=1:numel(educts)
                NewPart = eductStoich(i,:);
                NewPart = [{''};strread(num2str(NewPart),'%s')]; %#ok<*FPARK> % First row is a placeholder to note differences
                NewPart = regexprep(NewPart,'0',''); % Do not print 0 if metabolite is not part of reaction.
                inRxn = ~cellfun(@isempty,NewPart);
                if length(setdiff(unique(NewPart),{''}))>1
                    N = max(cellfun(@length,NewPart));
                    NewPart(1) = {repmat('*',1,N)};
                    pad = repmat({sprintf('%s-%ds','%',N)},NumRxns+1,1);
                    NewPart = cellfun(@sprintf,pad,NewPart,'UniformOutput',false);
                end
                
                % Enter metabolite name
                NewPart(inRxn) = strcat(NewPart(inRxn),{' '},educts{i});
                
                N = max(cellfun(@length,NewPart));
                if ~all(inRxn(2:end)) % Row one is placeholder
                    NewPart(1) = {repmat('*',1,N)};
                end
                pad = repmat({sprintf('%s-%ds','%',N)},NumRxns+1,1);
                NewPart = cellfun(@sprintf,pad,NewPart,'UniformOutput',false);
                equations = strcat(equations,NewPart);
                if i<numel(educts)
                    equations = strcat(equations,{' + '});
                end
            end
            
            % Arrow
            N = max(cellfun(@length,arrow));
            if length(unique(arrow))>1
                arrow = [repmat('*',1,N);arrow];
                SameDir = false;
            else
                arrow = [{''};arrow];
                SameDir = true;
            end
            pad = repmat({sprintf('%s%ds','%',N)},NumRxns+1,1);
            arrow = cellfun(@sprintf,pad,arrow,'UniformOutput',false);
            equations = strcat(equations,{' '},arrow,{' '});
            
            % Products
            for i=1:numel(products)
                NewPart = productStoich(i,:);
                NewPart = [{''};strread(num2str(NewPart),'%s')]; %#ok<*FPARK> % First row is a placeholder to note differences
                NewPart = regexprep(NewPart,'0',''); % Do not print 0 if metabolite is not part of reaction.
                inRxn = ~cellfun(@isempty,NewPart);
                if length(setdiff(unique(NewPart),{''}))>1
                    N = max(cellfun(@length,NewPart));
                    NewPart(1) = {repmat('*',1,N)};
                end
                
                % Enter metabolite name
                NewPart(inRxn) = strcat(NewPart(inRxn),{' '},products{i});
                
                N = max(cellfun(@length,NewPart));
                if ~all(inRxn(2:end)) % Row one is placeholder
                    NewPart(1) = {repmat('*',1,N)};
                end
                pad = repmat({sprintf('%s%ds','%',N)},NumRxns+1,1);
                NewPart = cellfun(@sprintf,pad,NewPart,'UniformOutput',false);
                equations = strcat(equations,NewPart);
                if i<numel(products)
                    equations = strcat(equations,{' + '});
                end
            end
            
            % Gene rules
            GPR = [{'Genes'};GPR];
            equations = strcat(equations,{' '},GPR);
            
            % Remove +s
            equations{1} = regexprep(equations{1},'+',' ');
            equations    = regexprep(equations,'  \+','   ');
            
            % Print equations
            for i=1:NumRxns+1
                disp(equations{i})
            end
            
            % Print predicted directions
            if ~SameDir
                fprintf('\nReaction directions don''t match. The predicted reaction directions based on network patterns are:\n')
                dirs = {'<--','<==>','-->'};
                for i=1:NumRxns
                    if ~isempty(this.pDir) && ismember(RxnIDs{i},this.pDir.ids);
                        dir = this.pDir.dir(ismember(this.pDir.ids,RxnIDs{i}));
                        if isnan(dir)
                            fprintf('%s: Reaction direction prediction not possible\n',RxnIDs{i})
                        else
                            fprintf('%s: %s\n',RxnIDs{i},dirs{dir+2})
                        end
                    else
                        fprintf('%s: Reaction direction prediction not available for this reaction. This can be remedied by re-running the reaction direction prediction.\n',RxnIDs{i})
                    end
                end
            end
            
        end
        function ReactionDeletionConsequence(this,group,AddIgnore)
            %This reaction displays the consequences of deleting a reaction
            %in the network-context.
            %%% Input
            %   ReactionIDx     Logical or indices corresponding to the
            %                   reactions of interest.
            %%% Optional input
            %   dR              Logical corresponding to dead end reactions
            %   iR              Logical corresponding to inactive reactions
            %   AddIgnore       Logical corresponding to other reactions
            %                   Which are also considered for removal
            
            % Input pre-processing
            Ignore = false(1,this.NumReactions);
            if nargin==3
                Ignore(AddIgnore) = true;
            end
            
            RxnIDx = find(group);
            
            % Obtain general information
            [dR,~,iR] = findDeadEnds(this);
            NumRxns = numel(RxnIDx);
            FastMet = this.FastMetLookup;
            LogStoich = logical(this.Stoich);
            
            % Pre-allocate variables
            NumNewInact  = nan(NumRxns,1);
            ToBeIgnored  = nan(NumRxns,1);
            
            % Reaction formulas and information on origin
            disp(['Reaction formulas:' char(10)])
            this.PrintEquations(group);
            
            % Gene reaction association
            disp([char(10) char(10) 'Reaction - Gene assocation' char(10)])
            GeneIDx = find(any(this.rxnGeneMat(:,RxnIDx),2));
            disp([{''} this.genes(GeneIDx)' ; this.reaction_IDs(RxnIDx), num2cell(full(this.rxnGeneMat(GeneIDx,RxnIDx)'))])
            
            % How does deletion of the reaction affect the network
            disp([char(10) 'Information on the reaction and consequences of removal' char(10) ...
                'Dead: Whether or not the reaction is a dead-end' char(10) ...
                'Inactive: Whether or not the reaction is inactive (unable to carry flux due to relying on the activity of dead-end reactions)' char(10) ...
                'NewInactive: How many reactions would become inactive due to deletion of the reaction' char(10) ...
                'AlsoMatched: How many of these are also considered for removal' char(10) ...
                ])
            isDead      = dR(RxnIDx)'; %Are the original reaction dead-ends?
            isInactive  = iR(RxnIDx)'; %Are the original reactions inactive?
            
            for i=1:numel(RxnIDx)
                % Simulating removal of the reaction and assessing network
                % consequences
                S = this.Stoich;
                S(:,RxnIDx(i)) = 0;            % Disabling the reaction
                [~,~,KOiR] = this.findDeadEnds(S);   % finding all inactive reactions
                NumNewInact(i) = sum(KOiR & ~iR);
                ToBeIgnored(i) = sum((KOiR & ~iR) & Ignore);
            end
            
            Info = dataset(isDead,isInactive,NumNewInact,ToBeIgnored, ...
                'ObsNames',this.reaction_IDs(RxnIDx), ...
                'VarNames',{'Dead','Inactive','NewInactive','AlsoMatched'});
            disp(Info)
            
            % Species information
            disp(['Species information' char(10) ...
                'Comp: The degree of the species in that compartment' char(10) ...
                'Free Transport: Whether the species can freelly diffuse between the compartments' char(10) ...
                ])
            RxnMets = this.metabolites(any(this.Stoich(:,RxnIDx),2));
            [spec,comp] = COMMGEN.MetToSpecAndComp(RxnMets);
            
            spec = unique(spec);
            [~,specIDx] = intersect(this.species_IDs,spec);
            specNames = this.IDToName(spec);
            
            comp = unique(comp);
            compNames = this.IDToName(comp);
            
            dataset(zeros(numel(spec),numel(comp)),'ObsNames',specNames)            % Degree of metabolites
            for i=1:numel(comp)
                Degree = nan(numel(spec),1);
                mets = strcat(spec,'@',comp{i});
                for j=1:numel(mets)
                    IDx = mfindstr(FastMet,mets{j});
                    if IDx==0
                        Degree(j)=0;
                    else
                        Degree(j) = sum(LogStoich(IDx,:));
                    end
                end
                MetInfo.(compNames{i}) = Degree;
            end
            % Free transport of metabolites
            FreeTrans = this.FreeTransport;
            CompConn = this.options.CompartmentConnections;
            for i=1:numel(CompConn)
                ConnComps = splitstr(CompConn{i},this.DEL);
                if all(ismember(ConnComps,comp))
                    cmpNames = this.IDToName(ConnComps);
                    MetInfo.(['FreeTrans_' cmpNames{1} '_' cmpNames{2}]) = FreeTrans(specIDx,i);
                end
            end
            disp(MetInfo)
        end
        function PrintSpeciesInformation(this,speciesIDx,merge)
            % This function prints some general information about the
            % input species. If merge==true, It also provides information
            % on how much of these characteristics are in common between
            % all species.
            if islogical(speciesIDx)
                speciesIDx = find(speciesIDx);
            end
            NumSpec = numel(speciesIDx);
            
            if nargin <3
                merge = false;
            end
            
            % Load information
            MetComp = this.MetCompartment;
            SpecComps   = this.SpecComps;
            MTS = this.MetToSpecies;
            NumMets = this.NumMetabolites;
            Comps = this.CompartmentIDToName(:,1);
            
            % Pre-process information
            MetToComp = false(NumMets,this.NumCompartments);
            for j=1:this.NumCompartments
                MetToComp(:,j) = ismember(MetComp,Comps(j));
            end
            
            % Initialize output structure
            if merge
                SpecInfo  = dataset(nan(numel(speciesIDx)+1,1),'ObsNames',[this.species_IDs(speciesIDx);'Shared'],'VarNames','tmp'); %Include empty column tmp for compatibility with earlier MATLAB versions
                NumFields = NumSpec + 1;
            else
                SpecInfo  = dataset(nan(numel(speciesIDx)),'ObsNames',this.species_IDs(speciesIDx),'VarNames','tmp'); %Include empty column tmp for compatibility with earlier MATLAB versions
                NumFields = NumSpec;
            end
            %Species Names
            Names = cell(NumFields,1);
            if merge
                Names(1:NumSpec) = this.IDToName(this.species_IDs(speciesIDx));
            else
                Names = this.IDToName(this.species_IDs(speciesIDx));
            end
            SpecInfo.Name = Names;
            %Dead-ends
            [~,dM,~,iM] = this.findDeadEnds;
            Dead     = cell(NumFields,1);
            Inactive = cell(NumFields,1);
            for i=1:NumSpec
                MetIDx = MTS==speciesIDx(i);
                if all(dM(MetIDx)) %If all corresponding mets are dead-ends
                    Dead{i} = 'Yes';
                else
                    Dead{i} = 'No';
                end
                if all(iM(MetIDx)) %If all corresponding mets are dead-ends
                    Inactive{i} = 'Yes';
                else
                    Inactive{i} = 'No';
                end
            end
            SpecInfo.DeadEnd = Dead;
            SpecInfo.Inactive= Inactive;
            %Molecular formula
            formula = cell(NumFields,1);
            for i=1:NumSpec
                formula{i} = this.species{speciesIDx(i)}.formula;
                if isempty(formula{i})
                    formula{i} = '';
                end
            end
            SpecInfo.Formula = formula;
            %Charge
            Charge = cell(NumFields,1);
            for i=1:NumSpec
                Charge{i} = this.species{speciesIDx(i)}.charge;
                if isempty(Charge{i})
                    Charge{i} = '';
                end
            end
            SpecInfo.Charge = Charge;
            %Origin
            SpecOrigin = strrep(cellstr(num2str(this.SpecOrigin)),' ','');
            ThisSpecOrigin = cell(NumFields,1);
            for i=1:NumSpec
                ThisSpecOrigin(i) = SpecOrigin(speciesIDx(i));
            end
            SpecInfo.Origin = ThisSpecOrigin;
            %Number of compartments
            NumComp = zeros(NumFields,1);
            if merge
                Shared  = this.CompartmentIDToName(:,1);
            end
            for i=1:NumSpec
                NumComp(i) = count(MTS==speciesIDx(i));
                if merge
                    SpecComp = this.MetCompartment(MTS==speciesIDx(i));
                    Shared = intersect(Shared,SpecComp);
                end
            end
            if merge
                NumComp(end) = numel(Shared);
            end
            SpecInfo.NumComp = NumComp;
            %Number of reactions
            NumRxns = zeros(NumFields,1);
            for i=1:NumSpec
                NumRxns(i) = count(any(this.Stoich(MTS==speciesIDx(i),:),1));
            end
            if merge
                % for every compartment, find the corresponding
                % metabolites
                MetIDx = zeros(this.NumCompartments,2);
                for j=1:this.NumCompartments
                    if all(SpecComps(speciesIDx,j),1)
                        MetIDx(j,:) = [find(MTS==speciesIDx(1)&MetToComp(:,j)) find(MTS==speciesIDx(2)&MetToComp(:,j))];
                    end
                end
                MetIDx(~any(MetIDx,2),:) = [];
                groups = this.findIdenticalRxnsAfterSpeciesMerge(MetIDx);
                Shared = size(groups,1);
                NumRxns(end) = Shared;
            end
            SpecInfo.NumRxns = NumRxns;
            %Number of metabolites connected to
            NumMets = zeros(NumFields,1);
            if merge
                Shared = true(this.NumMetabolites,1);
            end
            for i=1:NumSpec
                MetIDx = this.MetToSpecies==speciesIDx(i);
                Rxns = any(this.Stoich(MetIDx,:),1);
                MetConn = any(this.Stoich(:,Rxns),2);
                MetConn(MetIDx) = false; % don't count self
                NumMets(i) = count(MetConn);
                if merge
                    Shared = Shared & MetConn;
                end
            end
            if merge
                NumMets(end) = count(Shared); %
            end
            SpecInfo.NumMetConnected = NumMets;
            %Number of Genes
            NumGenes = zeros(NumFields,1);
            if merge
                Shared = true(this.NumGenes,1);
            end
            for i=1:NumSpec
                Rxns = any(this.Stoich(this.MetToSpecies==speciesIDx(i),:),1);
                NumGenes(i) = count(any(this.rxnGeneMat(:,Rxns),2));
                if merge
                    Shared = Shared & any(this.rxnGeneMat(:,Rxns),2);
                end
            end
            if merge
                NumGenes(end) = count(Shared);
            end
            SpecInfo.NumGenes = NumGenes;
            
            % Remove tmp field
            SpecInfo.tmp = [];
            
            % Print output
            disp(SpecInfo)
        end
        
        % Modify S
        function S = RemIgnored(this,S,hub)
            % Remove the ignored species from the matrix (set to 0)
            %%% OPTIONAL INPUT
            % S         Stoichiometric matrix to be used instead of
            %           this.Stoich (default)
            % hub       If true, the hub metabolites are also removed from
            %           S
            if nargin<2
                S = this.Stoich;
            end
            if nargin<3
                hub=false;
            end
            
            IgnoredIDx  = find(ismember(this.species_IDs,this.options.IgnoredSpec));
            S(ismember(this.MetToSpecies,IgnoredIDx),:) = 0;
            
            if hub
                HubMets = ismember(this.metabolites,this.options.HubMets);
                S(HubMets,:) = 0;
            end
        end
        function [Sirr,OrigIDx] = IrrS(this,S)
            %This function changes S into an irreversible S matrix, here
            %every reversible reaction is split into two and every
            %irreversible reaction occuring in the RL direction (A <- B) is
            %flipped to become (B -> A)
            %%% OPTIONAL INPUT
            % S     Stoichiometric matrix to start with. Default:
            %       this.Stoich
            if nargin<2
                S = this.Stoich;
            end
            
            LB  = this.Lb;
            UB  = this.Ub;
            rev = this.is_reversible;
            
            % Alter S to correspond to irreversible reactions only. This
            % matrix will not contain the external reactions.
            Ext = this.is_external;
            
            %Identify the number of reactions in each group
            LRRxns = LB==0 & UB>0 & ~Ext;
            NumLR  = count(LRRxns);
            RLRxns = LB<0 & UB==0 & ~Ext;
            NumRL  = count(RLRxns);
            RevRxns = rev & ~Ext;
            NumRev  = count(RevRxns);
            
            %Pre-allocate variables
            Sirr     = zeros(this.NumMetabolites,NumLR+NumRL+2*NumRev);
            [~,NumRxns] = size(Sirr);
            OrigIDx = zeros(NumRxns,1);
            
            %Reactions already in the right direction
            Start  = 1;
            End    = Start + NumLR - 1;
            Sirr(:,Start:End) = S(:,LRRxns);
            OrigIDx(Start:End) = find(LRRxns);
            
            %Reactions in the opposite direction
            Start  = Start + NumLR;
            End    = Start + NumRL-1;
            Sirr(:,Start:End) = -1*S(:,RLRxns);
            OrigIDx(Start:End) = find(RLRxns);
            
            %Reactions in both directions
            Start   = Start + NumRL;
            End     = Start + NumRev-1;
            Sirr(:,Start:End) = S(:,RevRxns);
            OrigIDx(Start:End) = find(RevRxns);
            
            Start   = Start + NumRev;
            End     = Start + NumRev-1;
            Sirr(:,Start:End) = -1*S(:,RevRxns);
            OrigIDx(Start:End) = find(RevRxns);
            
        end
        function [BigS,AllMets] = BigS(this,S)
            % Create a large S matrix where there is a slot for each metabolite in each
            % compartment
            
            % Load information
            SpecIDs = this.species_IDs;
            NumR = size(S,2);
            NumS = this.NumSpecies;
            NumC = this.NumCompartments;
            Comps = this.CompartmentIDToName(:,1);
            
            % Pre-allocate variables
            AllMets = cell(NumS*NumC,1);
            BigS = zeros(numel(AllMets),NumR);
            
            % Create BigS
            for i=1:NumC
                AllMets((i-1)*NumS+1:i*NumS) = strcat(SpecIDs,'@',Comps(i)); % Create all possible different metabolites
            end
            [~,IA] = ismember(this.metabolites,AllMets);
            BigS(IA,:) = S;
        end
        
        % Export model
        function Export(this,dir,name,KeepByCompAndModel,biomass)
            %%% PURPOSE
            % This function saves a (part of a) model. The model is saved in a tsv
            % and an SBML format. If the mechanize library is available, the model
            % will also be updated via MetaNetX.org which results in additional
            % updated tsv and SBML models.
            %%% INPUT
            %%% OPTIONAL INPUT
            %   dir: Directory the model will be saved in.
            %   Default value: ConsensusModel
            %   name: Name of the saved model.
            %   Default value: equal to the variable dir.
            %   KeepByCompAndModel: Boolean matrix where the rows correspond to the
            %   different compartments (unique(this.RxnComp)) and the columns to the
            %   models. True indicates that the combination of compartment to model
            %   should be saved. This thus allows to selectively export compartments
            %   from specific models.
            %   Default value: prompt.
            %   biomass: Index of the model from which the biomass reactions are to be
            %   kept.
            %   Default value: prompt.
            %%% OUTPUT
            
            % Input processing
            if nargin < 2
                dir = 'ConsensusModel';
            end
            
            if nargin < 3
                name = dir;
            end
            
            if nargin > 3 % Check KeepByCompAndModel
                if ~all([numel(unique(this.RxnComp)) this.NumModels] == size(KeepByCompAndModel));
                    disp('The size of KeepByCompAndModel is incorrect. KeepByCompAndModel will be recreated')
                    delete KeepByCompAndModel
                end
            end
            
            % Create KeepByCompAndModel if needed
            CompConn = unique(this.RxnComp);
            if ~exist('KeepByCompAndModel','var')
                KeepByCompAndModel = true(numel(CompConn),this.NumModels);
                while true
                    disp([{''} this.ModelNames ;CompConn num2cell(KeepByCompAndModel)]);
                    fprintf(['Overview of compartment/model combinations currently being saved.\n'...
                        'First enter a compartment ID (or ''all'') and then a model ID (or ''all'') \n' ...
                        'to no longer save that combination.\n' ...
                        'Or enter C to continue\n'])
                    reply = COMMGEN.InputProcessing([CompConn;'all';'C']);
                    if strcmp('C',reply)
                        break
                    elseif strcmp('all',reply)
                        CompRem = true(numel(CompConn),1);
                    else
                        CompRem = ismember(CompConn,reply);
                    end
                    fprintf('\nEnter model ID\n''all'' to select all\nOr C to cancel\n')
                    reply = COMMGEN.InputProcessing([this.ModelNames';'all';'C']);
                    if strcmp('C',reply)
                        disp('Cancelled')
                        ModelRem = false(this.NumModels,1);
                    elseif strcmp('all',reply)
                        ModelRem = true(this.NumModels,1);
                    else
                        ModelRem = ismember(this.ModelNames,reply);
                    end
                    
                    KeepByCompAndModel(CompRem,ModelRem) = false;
                    
                end
            end
            
            % Check / obtain biomass info
            if ~exist('biomass','var') || any(biomass > this.NumModels)
                BiomassSpec = find(strcmp(this.species_IDs,'BIOMASS'));
                BiomassMets = this.MetToSpecies == BiomassSpec;
                BiomassRxns = any(this.Stoich(BiomassMets,:),1)';
                for i=1:this.NumModels
                    fprintf('Printing all biomass-related reactions for model %s.',this.ModelNames{i});
                    this.PrintEquations(BiomassRxns & this.RxnOrigin(:,i));
                end
                fprintf('Which biomass reactions would you like to keep? (Enter model ID)\n')
                reply = COMMGEN.InputProcessing(this.ModelNames);
                biomass = find(strcmp(this.ModelNames,reply));
            end
            
            % Select reactions to keep
            KeepRxns = false(this.NumReactions,1);
            RxnOrig  = this.RxnOrigin;
            
            % Select on the basis of compartment / model
            for i=1:numel(CompConn)
                CompRxns  = ismember(this.RxnComp,CompConn(i));
                ModelRxns = any(RxnOrig(:,KeepByCompAndModel(i,:)),2);
                KeepRxns(CompRxns & ModelRxns) = true;
            end
            
            % Remove unwanted biomass reactions
            BiomassSpec = find(strcmp(this.species_IDs,'BIOMASS'));
            BiomassMets = this.MetToSpecies == BiomassSpec;
            BiomassRxns = any(this.Stoich(BiomassMets,:),1)';
            KeepRxns(BiomassRxns & ~any(RxnOrig(:,biomass),2)) = false;
            
            fprintf('Creating .tsv model representation.\n\n')
            this.writeConsensusModelToDisk(dir,KeepRxns,name);
            
            fprintf('Saving choices made during COMMGEN procedure.\n\n')
            this.SaveChoices(dir);
            
            fprintf('Converting MnXModel to SBML model\n\n')
            COMMGEN.tsvModelToSBML(dir,name);
            
            fprintf('Updating SBML model using MetaNetX\n\n')
            COMMGEN.updateSBMLWithMetaNetX(name);
        end
        function writeConsensusModelToDisk(this, directory, reactions, name)
            %%% PURPOSE
            % Writes (part of) the COMMGEN model to the tsv file model
            % representation.
            %%% EXAMPLE CALL
            %   writeConsensusModelToDisk(this, 'ConsensusModel', true(this.NumReactions,1), 'ModelName');
            %%% INPUT
            %%% OPTIONAL INPUT
            %   directory: Name of the directory to store the tsv files in.
            %   Default: ConsensusModel
            %   reactions: Indices of the reactions to be saved using a
            %   double or a logical vector. Default: all reactions saved.
            %   name: Name of the model. Default: Same value as directory.
            %%% OUTPUT
            
            % Input processing
            if nargin < 2
                directory = 'ConsensusModel';
            end
            
            if nargin < 3
                reactions = 1:this.NumReactions;
            elseif islogical(reactions)
                reactions = find(reactions);
            end
            
            if nargin < 4
                name = directory;
            end
            
            % create directory
            mkdir(directory)
            
            % create output files
            model_file     = [directory '/model.tsv'];
            reactions_file = [directory '/reactions.tsv'];
            species_file   = [directory '/chemicals.tsv'];
            comp_file      = [directory '/compartments.tsv'];
            enzymes_file   = [directory '/enzymes.tsv'];
            
            % write model.tsv file
            fprintf('# Model file\n');
            fid = fopen(model_file, 'w+');
            fprintf(fid,'ID\t%s\t\t\n',name);
            fclose(fid);
            
            % write compartments.tsv file
            fprintf('# Compartments file\n');
            fid = fopen(comp_file, 'w+');
            fprintf(fid,'BOUNDARY\tmodel boundary\tboundary\n');
            for i=1:this.NumCompartments
                fprintf(fid,'%s\t',this.CompartmentIDToName{i,1}); %MnXRef identified
                fprintf(fid,'%s\t',this.CompartmentIDToName{i,2}); %used name in COMMGEN
                fprintf(fid,'%s\n',this.options.Compartments{ismember(this.options.Compartments(:,1),this.CompartmentIDToName(i,1)),2});
            end
            fclose(fid);
            
            % write chemicals.tsv file
            fprintf('# Chemicals file\n');
            fid = fopen(species_file, 'w+');
            for j=1:this.NumSpecies
                this.species{j}.print(fid);
            end
            fclose(fid);
            
            % write reactions.tsv file
            fprintf('# Reaction file\n');
            fid = fopen(reactions_file, 'w+');
            for j=1:numel(reactions)
                this.reactions{reactions(j)}.printToFile(fid);
            end
            fclose(fid);
            
            % write enzymes.tsv file
            fprintf('# Enzymes file\n')
            fid = fopen(enzymes_file, 'w+');
            for j=1:numel(reactions)
                Rxn = this.reactions{reactions(j)};
                fprintf(fid,'%s\t%s\t%d\t%d\n',Rxn.id,Rxn.proteins,Rxn.lb,Rxn.ub);
            end
            fclose(fid);
            
            % Remove non-printable characters from .tsv files for
            % compatibility with SBML format. In particular CTRL-Z
            % characters are to be removed.
            dos(['tr -d "\26" < ' model_file ' > tmp && mv tmp ' model_file]);
            dos(['tr -d "\26" < ' reactions_file ' > tmp && mv tmp ' reactions_file]);
            dos(['tr -d "\26" < ' species_file ' > tmp && mv tmp ' species_file]);
            dos(['tr -d "\26" < ' comp_file ' > tmp && mv tmp ' comp_file]);
            dos(['tr -d "\26" < ' enzymes_file ' > tmp && mv tmp ' enzymes_file]);
            
        end
        
    end
    
    methods(Static)
        
        function tsvModelToSBML(directory,name)
            fprintf('Creating SBML file');
            system(['java -jar ' COMMGEN_location '/COMMGEN/Export/TSV2SBML.jar -P ' directory ' > ' name '.xml'])
            fprintf('SBML model created successfully.')
        end
        
        function updateSBMLWithMetaNetX(name)
            %attempt to update model using MetaNetX
            fprintf('Attempting to update model annotation based on MnXRef namespace / MetaNetX\n')
            try
                dos(['cp ' name '.xml tmp_' name '.xml'])
                tmpName = ['tmp_' name '.xml'];
                mapfile = [tmpName '.mapping'];
                %get mechanize location
                filename = [COMMGEN_location '/COMMGEN/MechLoc'];
                formatSpec = '%s%[^\n\r]';
                fileID = fopen(filename,'r');
                dataArray = textscan(fileID, formatSpec, 'Delimiter', '',  'ReturnOnError', false);
                fclose(fileID);
                MechanizeLocation = dataArray{:, 1}{1};
                
                % convert GSM
                %remove mapping file if already present
                if exist(mapfile,'file')
                    delete(mapfile)
                end
                
                try
                    %connect to MetaNetX to convert GSM to MnXRef namespace
                    dos(['perl -I ' MechanizeLocation ' ' COMMGEN_location '/COMMGEN/MetaNetXInteraction/upload_model.pl ' tmpName])
                catch
                    fprintf('Uploading of model failed. Most likely the mechanize location in %s is incorrect',filename)
                    return
                end
                delete(tmpName)
                
                %identify current files in folder. The name of the converted model
                %is currently independent of the uploaded file's name.
                files = dir;
                files = {files.name};
                
                %untar file
                dos(['tar -xvf ' tmpName '.imported.tar.gz'])
                
                %identify folder name corresponding to converted GSM
                files2 = dir;
                files2 = {files2.name};
                folder = setdiff(files2,files);
                folder = folder{1}; %convert to string
                
                %
                dirname = [regexprep(name,'\..*','') '_Updated'];
                mkdir(dirname);
                perl([COMMGEN_location '/COMMGEN/MetaNetXInteraction/m2c.pl'],folder,dirname);
                tsv2mat('-d',dirname);
                rmdir(folder,'s');
                system(['java -jar ' COMMGEN_location '/COMMGEN/Export/TSV2SBML.jar -P ' dirname ' > ' name '_Updated.xml'])
                fprintf('Model updated via MetaNetX stored in the folder %s and as %s.\n',dirname,[name '_Updated.xml']);
            catch
                fprintf('Failed at updating the model via MetaNetX.\n')
            end
        end
        
        function GroupName = GetGroupName(args)
            % This function returns a standardized name for the combination
            % of input arguments. This name will be a concatenation of
            % their original IDs, in alphabetic order.
            id = cell(numel(args,1));
            for i=1:numel(args)
                id{i} = args{i}.id;
            end
            id = sort(id);
            GroupName = id{1};
            for i=2:numel(args)
                GroupName = [GroupName ';' id{i}]; %#ok<AGROW>
            end
        end
        
        function coeff = CheckStoichCoeff(s, fled)
            co = regexp(s,'\(*(\S+)\)*','tokens');
            coeff = (1-2*fled)*str2double(co{:});
        end
        
        function reply = InputProcessing(AllInputs)
            %Function which assures that the user can only provide correct
            %input.
            
            % reply = COMMGEN.InputProcessing({'R','C','A','H','*'}); Only
            % the supplied options are allowed.
            % OR
            % reply = COMMGEN.InputProcessing('realnumber');  any real
            % number is allowed (to prevent complex numbers in lower /
            % upper bounds
            % OR
            % reply = COMMGEN.InputProcessing('GPR'); Accepts all
            % characters that could be in the GPR, but no others.
            
            if strcmp(AllInputs,'realnumber')
                reply = input('','s');
                if ~strcmp('*',reply)
                    reply = str2num(reply); %#ok<ST2NM>
                end
                while ~( ( isreal(reply) && ~isempty(reply)) || strcmp('*',reply))
                    disp('Please enter a real number or * to enter a comment')
                    reply = input('','s');
                    if ~strcmp('*',reply)
                        reply = str2num(reply); %#ok<ST2NM>
                    end
                end
            elseif strcmp(AllInputs,'GPR')
                reply = input('Entering GPR rule. Acceptable symbols: 0-9 a-z A-Z ; + ( ) _ \n Where ; corresponds to AND and + to OR.\n','s');
                reply = regexprep(reply,'''','');
                while   ~all( ismember(reply,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ+;()_'))
                    fprintf('Illegal symbols found: %s\nPlease try again.',reply(~ismember(reply,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ+;()_')))
                    reply = upper(input('','s'));
                end
            else
                reply = '';
                while ~any(strcmp(AllInputs,reply))
                    reply = input('','s');
                    reply = regexprep(reply,'''','');
                    if ~any(strcmp(AllInputs,reply))
                        disp(AllInputs)
                        disp('Input not recognized, only the above inputs are valid')
                    end
                end
            end
            
        end
        
        function stoichObj = minimizeByModel(model_db)
            % copy
            %             stoichMatrix = MnXStoich.copy(model_db.stoich);
            PosInt = model_db.startPosInt(2);
            PosExt = model_db.startPosExt(2);
            PosMue = model_db.startPosMue(2);
            PosMet = model_db.startPosMet(2);
            
            % index of internal reactions to keep
            int_keep = false(1,model_db.stoich.int);
            int_keep(PosInt:end) = true;
            
            % index of external reactions to keep
            ext_keep = false(1,model_db.stoich.ext);
            ext_keep(PosExt:end) = true;
            
            % index of biomass reactions to keep
            mue_keep = false(1,model_db.stoich.mue);
            mue_keep(PosMue:end) = true;
            
            % index of metabolites to keep
            met_keep = false(1,model_db.stoich.m);
            met_keep(1:PosMet-1) = true;
            
            stoichObj = MnXStoichConcatenate.toKeepByIndices(model_db.stoich, met_keep, int_keep, ext_keep, mue_keep);
        end
        
        function newObj = toKeepByIndices(stoich, met_keep, int_keep, ext_keep, mue_keep)
            rxnToKeep = [int_keep ext_keep mue_keep];
            
            newObj.S = stoich.S(met_keep,int_keep);
            newObj.E = stoich.E(met_keep,ext_keep);
            newObj.M = stoich.M(met_keep,mue_keep);
            
            newObj.int = sum(int_keep);
            newObj.ext = sum(ext_keep);
            newObj.mue = sum(mue_keep);
            
            newObj.cons_int = stoich.cons_int(int_keep,:);
            newObj.cons_ext = stoich.cons_ext(ext_keep,:);
            newObj.cons_mue = stoich.cons_mue(mue_keep,:);
            
            newObj.reactions_int = stoich.reactions_int(int_keep);
            newObj.reactions_ext = stoich.reactions_ext(ext_keep);
            newObj.reactions_mue = stoich.reactions_mue(mue_keep);
            
            newObj.mue_reversed = stoich.mue_reversed(mue_keep);
            
            newObj.m = sum(met_keep);
            newObj.r = newObj.int + newObj.ext + newObj.mue;
            
            newObj.FastRxnLookup = stoich.FastRxnLookup(rxnToKeep,:);
            newObj.metabolite_ids = stoich.metabolite_ids(met_keep,:);
            
            newObj.revs = stoich.revs(rxnToKeep);
            newObj.c = [];
            newObj.compartments = [];
            
            newObj.compartment_idx = stoich.compartment_idx(met_keep);
            idx = unique_no_sort(newObj.compartment_idx);
            newObj.c = length(idx);
            newObj.compartments = stoich.compartments(idx,:);
        end
        
        function newObj = toKeepByMetabolites(stoich, met_keep, int_keep)
            rxnToKeep = true(1,stoich.r);
            rxnToKeep(~int_keep) = false;
            
            newObj.S = stoich.S(met_keep,int_keep);
            newObj.E = stoich.E(met_keep,:);
            newObj.M = stoich.M(met_keep,:);
            
            newObj.int = sum(int_keep);
            newObj.ext = stoich.ext;
            newObj.mue = stoich.mue;
            
            newObj.cons_int = stoich.cons_int(int_keep,:);
            newObj.cons_ext = stoich.cons_ext;
            newObj.cons_mue = stoich.cons_mue;
            
            newObj.reactions_int = stoich.reactions_int(int_keep);
            newObj.reactions_ext = stoich.reactions_ext;
            newObj.reactions_mue = stoich.reactions_mue;
            
            newObj.mue_reversed = stoich.mue_reversed;
            newObj.m = sum(met_keep);
            newObj.r = newObj.int + newObj.ext + newObj.mue;
            
            newObj.FastRxnLookup = stoich.FastRxnLookup(rxnToKeep);
            newObj.metabolite_ids = stoich.metabolite_ids(met_keep,:);
            
            newObj.revs = stoich.revs(rxnToKeep);
            newObj.c = [];
            newObj.compartments = [];
            
            newObj.compartment_idx = stoich.compartment_idx(met_keep);
            idx = unique_no_sort(newObj.compartment_idx);
            newObj.c = length(idx);
            newObj.compartments = stoich.compartments(idx,:);
        end
        
        function [species,compartment] = MetToSpecAndComp(mets)
            if ischar(mets)
                mets = {mets};
            end
            NumMets     = numel(mets);
            species     = cell(NumMets,1);
            compartment = cell(NumMets,1);
            % Cut out the parts
            for i=1:NumMets
                delimIdx = find(mets{i} == '@');
                species{i}  = mets{i}(1:delimIdx-1);
                compartment{i} = mets{i}(delimIdx+1:end);
            end
        end
        
        function rule = StandardGPR(rule)
            %%% PURPOSE
            %   Rewrites a GPR rule into a standard format.
            %   Assumption: GPR rules are assumed to be in a disjunctive
            %   standard form and will be returned in a disjunctive
            %   standard form. (Is a standard assumption for COBRA models
            %   in Matlab).
            %%% EXAMPLE CALL
            %   rule = COMMGEN.StandardGPR(rule);
            %%% INPUT
            %   rule: Gene rules as found in COMMGEN.reactions{i}.proteins
            %%% OPTIONAL INPUT
            %%% OUTPUT
            %   rule: The GPR rule rewritten in a standard form
            
            %Remove spacing
            rule = regexprep(rule,' ','');
            
            %Cut up GPR rule in segments that satisfy the rule
            segments = regexp(rule,';','split');
            %Keep unique segments and sort
            segments = unique(segments);
            %Connect using OR clauses
            segments(1:end-1) = strcat(segments(1:end-1),';');
            rule = [segments{:}];
        end
    end
end

