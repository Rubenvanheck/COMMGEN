classdef MergeOptions
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Hidden)
        Keepcomps
    end
    
    properties (Hidden, Dependent)
        NumComp
    end
        
    properties
        %% General
        BufSize = 100000; %Size of the buffer for textscan. If textscan throws a buffer size error please increase this value.
        
        %% Reaction direction prediction
        databaseNames = {''}; %The folder names of the databases on which the reaction direction prediction method is based. Default: the input models.
        RDPredict = true; %Reaction direction prediction is on by default but can be turned off by setting this value to false.
        
        %% Compartments
        Compartments = { ...
        %MNX_ID, ...	Description                     Source
        'MNXC1','cc'; ...	cellular_component              go:0005575
        'MNXC2','e '; ...	extracellular region            go:0005576
        'MNXC3','c '; ...	cytoplasm                       go:0005737
        'MNXC4','m '; ...	mitochondrion                   go:0005739
        'MNXC5','mm'; ...	mitochondrial membrane          go:0031966
        'MNXC6','n '; ...	nucleus                         go:0005634
        'MNXC7','nm'; ...	nuclear membrane                go:0031965
        'MNXC8','h '; ...	chloroplast                     go:0009507
        'MNXC9','v '; ...	vacuole                         go:0005773
        'MNXC10','vm';...	vacuolar membrane               go:0005774
        'MNXC11','r ';...	endoplasmic reticulum           go:0005783
        'MNXC12','rm';...	endoplasmic reticulum membrane	go:0005789
        'MNXC13','x ';...	peroxisome                      go:0005777
        'MNXC14','xm';...	peroxisomal membrane            go:0005778
        'MNXC15','g ';...	Golgi apparatus                 go:0005794
        'MNXC16','gm';...	Golgi membrane                  go:0000139
        'MNXC17','l ';...	lysosome                        go:0005764
        'MNXC18','s ';...	plastid                         go:0009536
        'MNXC19','p ';...	periplasmic space               go:0042597
        'MNXC20','pm';...	plasma membrane                 go:0005886
        'MNXC21','om';...	outer membrane                  go:0019867
        'MNXC22','cw';...	cell wall                       go:0005618
        'MNXC23','y ';...	glycosome                       go:0020015
        'MNXC24','o ';...	glyoxysome                      go:0009514
        'MNXC25','i ';...	apicoplast                      go:0020011
        'MNXC26','pv';...	provirus                        go:0019038
        'MNXC27','vi';...	virion                          go:0019012
        'MNXC28','f ';...	flagellum                       go:0019861
        'MNXC29','a ';...	acidocalcisome                  go:0020022
        }
        ValidExchange = {... % By default only exchange reactions corresponding to the extracellular compartment are valid.
        'MNXC2' ,...
        }
    
        CompartmentNames =  ... % {id,name;id2,name2...}
            {'MNXC2' ,'ext';...
             'MNXC3' ,'cyt';...
             'MNXC4' ,'mit';...
             'MNXC6' ,'nuc';...
             'MNXC9' ,'vac';...
             'MNXC11','ER' ;...
             'MNXC13','prx';...
             'MNXC15','Gol';...
             'MNXC17','Lys';...
             'MNXC19','per';...
             'UNK_COMP','UNK';...
             'POOL','POOL';...
             };
         
        %% Invalid transport reactions
        CompartmentConnections  = {}; % Compartment connections which are allowed. {[id1 '%' id2,...} example: {'MNXC19%MNXC2';'MNXC19%MNXC3'}
        
        % If a reaction transports a species from x to z, where x and z are
        % not connected, we can attempt to automatically resolve this via
        % an intermediate compartment y IF the species is not yet present
        % in compartment y.
        % format for InvTransportSolution:
        % {InvCompartmentConnection,intermediatecompartment,CopyRxnTo}
        % {'id1%id3','id2','id2%id3'}
        
        % Suppose: {'x%z',y,'y%z'}
        
        % Example 1: Active transport.
        % cpd1@x + ATP@z + H2O@z --> cpd1@z + ADP@z + Pi@z + H@z
        % Becomes:
        % cpd1@x <==> cpd1@y
        % cpd1Zy + ATP@z + H2O@z --> cpd1@z + ADP@z + Pi@z + H@z
        
        % Example 2: Symport.
        % cpd1@x + H@x --> cpd1@z + H@z
        % Becomes:
        % cpd1@x <==> cpd1@y
        % cpd1@y + H@y --> cpd1@z + H@z
        
        % Example 3: Antiport.
        % cpd1@x + H@z --> cpd1@z + H@x
        % Becomes:
        % cpd1@x <==> cpd1@y
        % cpd1@y + H@z --> cpd1@z + H@y
        
        % Example 3: Free diffusion
        % cpd1@x --> cpd1@z
        % Becomes:
        % cpd1@x <==> cpd1@y
        % cpd1@y --> cpd1@z
        
        ResolveInvalidTransport = 0;  % If 1: Attempt to resolve invalid transport automatically. If 0: ask whether they should be solved automatically. If -1: Don't solve automatically;
        InvTransportSolution    = {}; % {'id1%id3','id2','id1'}. {invalid,via,free_with} Example: {'ext%cyt','per','ext'}
        
        %% Merging
        % directionality
        MergeArrows =[];% If two reactions identical in the formula except for the arrows are merged, may the arrow '<==> be assumed? 2: Yes if disagreeing in direction (<-- vs -->) but take unidirected one if other is bidirectional (<==> vs <-- becomes <--). -1: never. 0: prompt. 1: prefer bidirectional reactions. 2: prefer unidirectional reactions. 3: Use predicted directionality. 4: Use directionality from a particular model
        DirectModel =[];% Only relevant if MergeArrows==4. The model index included here will always be used to select the reaction directionality if there are no further differences between the reactions.
        % Gene rules
        GeneOR  = []; % If a merged set of reactions has different gene rules, always add them together in an OR fashion. 0: Prompt, 1: Always
        GeneORStrict = 0; % If set to 1 automatic merging will always be performed according to "strict" rules. This runs on the assumption that if a modeller chose an AND-relationship this is more likely to be correct than the OR relationship.
        % Rejected merges
        PermanentReject = 0; % 1: If rejected, never propose the same reaction/species merge again. 0: prompt and ask to choose default. -1: The same merge may always be proposed again..
        
        %% SameRxnDiffComp
        % These settings all only apply to the function SameRxnDiffComp
        SameRxnDiffComp_RemoveDead  = 0; % If the same reaction occurs in several compartments and is dead in one of them, can this reaction automatically be removed? 1: always, -1: never assume, 0:prompt.
        SameRxnDiffComp_AutoGPR     = 0; % If the GPR rules are not in agreement, may they be merged automatically? 2: don't change GPR rules of to-be-kept reaction, 1: Merge, -1: Require manual intervention.
        SameRxnDiffComp_AllActSameModel = 0; % If all reactions are active and originate from the same model, should they automatically be ignored? 1: yes, 0: promt,-1: no.
        SameRxnDiffComp_IgnoreAllInact  = 0; % If all reactions in the selection are inactive, skip the selection automatically. 1: yes, -1: no.
        
        SameRxnDiffComp_Merge % Will be automatically set.
        
        %% Properties representing a default answer for certain questions
        lbUnknownArrow
        ubUnknownArrow
        
        %% SimilarSpecies
        findSpeciesMatches_SameModel = 0; %Whether species from the same model are to be considered for matching when searching for highly similar species. -1: never. 0: prompt. 1: always.
        SimilarSpecies_OneReaction = 0; %Whether at least one reaction needs to be identical between two species for them to be considered matching.
        findBaseCorrScore_quantile = 5; %from 0 to 100. This value sets the minimal correlation score a set of two species has to have in order to be considered as similar enough to be proposed for merging. The default value is the 5th quantile of the score obtained between species of which it is known that they are identical.

        %% Metabolites
        % Hub Metabolites
        % hub metabolites are those which are associated with so many
        % reactions that they are not of interest for finding alternative
        % reactions / pathways. In HubMets, enter either the metabolite
        % name with compartment to only consider it to be a hub metabolite
        % in that compartment or enter just the metabolite name to consider
        % it as a hub metabolite in each compartment. Enter a % before and
        % after the metabolite names.
        HubMets = {};
        HubComplete = false;
        HubThreshold = 2; % The top 'x' procent of the metabolites - in order of connectivity - will be suggested as hub metabolites.
        
        % Ignored Metabolites
        % Some metabolites are often not interesting for comparing two
        % equations. For example, some models tend to exclude protons or
        % water. Alternatively if a decision is made that two chemical
        % species are the same, reactions may still not perfectly match to
        % each other afterwards due to charge differences in the original
        % compounds.
        IgnoredSpec = {'MNXM1','MNXM2'};
        
        %% Finding alternative redox pairs
        % In the lists below, add the electron donors and acceptors present
        % in the used models.
        
        RedoxPairs = {
            'MNXM6','MNXM5'         %NADP(H)
            'MNXM10','MNXM8'        %NAD(H)
            'MNXM10','MNXM91005'    %NAD(H)
            'MNXM11561','MNXM11560' %ferrodoxin (reduced form 4:2)
            'MNXM90833','MNXM2525'  %glutaredoxin reduced / glutaredoxin disulfide
            'MNXM89760','MNXM89759' %thioredoxin
            'MNXM93886','MNXM93746' %rubredoxin
            'MNXM89762','MNXM90822' %FAD(H2)
            'MNXM38','MNXM90822'    %FAD(H2)
            'MNXM208','MNXM53407'   %FMN(H2)
            'MNXM191','MNXM232'     %Ubiquinol-8 / Ubiquinone-8
            'MNXM765','MNXM601'     %pyrroloquinoline quinol(3-) / pyrroloquinoline quinone
            'MNXM91194','MNXM91192' %Ferrocytochrome-C / Ferricytochrome-C
            'MNXM25','MNXM93'       %Succinate / Fumarate
            'MNXM2819','MNXM3987'	%electron donor / acceptor
            };
        
        %% findLumpedReactions
        %
        Lumped_Threshold   = 5; %The maximum number of reactions to bo considered for lumping. The larger this number is set the longer the search for candidate lumped reactions will take.
        Lumped_MinGeneFrac = 0.3; %The minimal fraction of gene overlap between lumped and nonlumped reaction sets.

        %% Automate
        AllAuto = 0; %Whether any manual step, besides setting options, should be skipped.
        ExactlyIdentical = false; %Automatically reject any merges unless things are exactly identical. NOTE: not recommended for actual use.
    end
    
    methods
        function this = MergeOptions(InitOpts)
            if nargin==1
                for i=fields(InitOpts)'
                    this.(i{1})=InitOpts.(i{1});
                end
            end
        end
            
        function print(this)
            print(this)
        end
    end
    
end

