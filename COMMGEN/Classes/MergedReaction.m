classdef MergedReaction < MnXMetabolicReaction
    %UNTITLED6 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        SrcRxns
        comment=''
        Origin
    end
    
    properties (Dependent)
        NumSrcRxns
    end
    
    methods
        % Getter functions
        function NumSrcRxns = get.NumSrcRxns(this)
            NumSrcRxns = 0;
            if ~isempty(this.SrcRxns)
                for i=1:numel(this.SrcRxns)
                    NumSrc = NumSrcOfSrcRxn(this.SrcRxns{i});
                    if NumSrc == 0;
                        NumSrcRxns = NumSrcRxns + 1;
                    else
                        NumSrcRxns = NumSrcRxns + NumSrc;
                    end
                end
            end
        end
        % Misc
        function NumSrc = NumSrcOfSrcRxn(SrcRxn)
            NumSrc = SrcRxn.NumSrcRxns;
        end
        % Initialize
        function this = MergedReaction(reaction,Origin)
            this = this@MnXMetabolicReaction(repmat({''},14,1));
            if ~isempty(reaction)
                RxnFields = fields(reaction);
                RxnFields = setdiff(RxnFields,{'NumSrcRxns'}); % can't set dependent property like this.
                for i=1:numel(RxnFields)
                    this.(RxnFields{i}) = reaction.(RxnFields{i});
                end
                this.id = regexprep(this.id,';','__'); %Reaction IDs should not have semicolons in there as these are used to distinguish between multiple reaction IDs
            end
            if nargin==2
                this.Origin = Origin;
            end
            this.proteins = regexprep(this.proteins,{'1\*[^:]*:','^(\d)','([+;])(\d)'},{'','GENE$1','$1GENE$2'});
        end
        % Modify
        function this = ReverseReaction(this)
            [this.lb,this.ub] = deal(-1*this.ub,-1*this.lb);
            products = {};
            educts   = {};
            for i=1:numel(this.reactants)
                this.reactants{i}.stoichiometry = -1*this.reactants{i}.stoichiometry;
                if this.reactants{i}.stoichiometry < 0
                    educts(end+1) = this.reactants(i);
                else
                    products(end+1) = this.reactants(i);
                end
            end
            equation = '';
            % Reconstruct educt side of the equation
            for i=1:numel(educts)
                if i==1
                    equation = [equation num2str(-1*educts{i}.stoichiometry) ' ' educts{i}.id];
                else
                    equation = [equation ' + ' num2str(-1*educts{i}.stoichiometry) ' ' educts{i}.id];
                end
            end
            arrow = regexp(this.equation,'[<\->=?]*','match','once');
            arrow = regexprep(arrow,{'-->','<--','__>'},{'__>','-->','<--'});
            equation = [equation ' ' arrow ' '];
            for i=1:numel(products)
                if i==1
                    equation = [equation num2str(products{i}.stoichiometry) ' ' products{i}.id];
                else
                    equation = [equation ' + ' num2str(products{i}.stoichiometry) ' ' products{i}.id];
                end
            end
            this.equation = equation;
            this = getReactants(this);
        end
    end
    
end

