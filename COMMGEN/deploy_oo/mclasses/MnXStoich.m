classdef MnXStoich < handle
    %MNXSTOICH Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (GetAccess = private)
        DEL = '%';
        Sneg;
        Spos;
        is_internal
        is_external
        is_biomass
        reactions
        metabolites
    end
    
    properties (GetAccess = public)
        S
        E
        M
        int
        ext
        mue
        cons_int
        cons_ext
        cons_mue
        reactions_int
        reactions_ext
        reactions_mue
        mue_reversed;
        m
        r
        reaction_ids
        metabolite_ids;
        revs
        c
        compartments
        compartment_idx
        is_transporter
        has_gene
    end
    
    methods        
        
        function this = MnXStoich(Sneg, Spos, cons, is_internal, is_external, is_biomass, metabolites_ids, reaction_ids, has_gene)
            
            this.Sneg = Sneg;
            this.Spos = Spos;
            this.is_internal = is_internal;
            this.is_external = is_external;
            this.is_biomass  = is_biomass;          
            
            % internal reactions
            this.S = Spos(:,is_internal) + Sneg(:,is_internal);
            this.cons_int = cons(is_internal,:);
            this.reactions_int = reaction_ids(is_internal);
            
            % external reactions
            this.E = Spos(:,is_external) + Sneg(:,is_external);
            this.cons_ext = cons(is_external,:);
            this.reactions_ext = reaction_ids(is_external);
            
            % biomass reaction(s): zero or none
            nBio = sum(is_biomass);
            if nBio > 0
                this.mue_reversed  = false(1, nBio);
                this.M = Spos(:,is_biomass) + Sneg(:,is_biomass);
                this.cons_mue      = cons(is_biomass,:);
                this.reactions_mue = reaction_ids(is_biomass);
                
                % maybe reverse biomass equations (-> LP solver)
                for i=1:nBio
                    if this.cons_mue(i,1) < 0
                        this.cons_mue(i,:)    = -fliplr(this.cons_mue(i,:));
                        this.M(:,i)           = -this.M(:,i);
                        this.mue_reversed(i)  = true;
                    end
                end
            else
                this.M        = zeros(length(metabolites_ids),0);
                this.cons_mue = zeros(0,2);
            end
            
            % number of reactions
            this.int = size(this.S,2);
            this.ext = size(this.E,2);
            this.mue = size(this.M,2);
            
            % metabolite identifiers for fast look-up
            this.metabolites = metabolites_ids;
            this.m = length(metabolites_ids);
            
            m_ids = cell(this.m,1);
            for i=1:this.m
                m_ids{i} = [this.DEL metabolites_ids{i} this.DEL];
            end
            this.metabolite_ids = char(m_ids{:});
            
            % reaction identifiers for fast look-up
            this.reactions = reaction_ids;
            this.r = this.int + this.ext + this.mue;
            rxns = [this.reactions_int; this.reactions_ext; this.reactions_mue];
            r_ids = cell(this.r,1);
            for i=1:this.r
                r_ids{i} = [this.DEL rxns{i} this.DEL];
            end
            this.reaction_ids = char(r_ids{:});
            
            % reversibilities of reactions (-1): backward, (0),
            % bidirectinoal, and (1) forward
            cons = [this.cons_int; this.cons_ext; this.cons_mue];
            this.revs = (sign(cons(:,2))+sign(cons(:,1)))';
            
            % account for fixed reaction constraints
            % only forward
            tmp_pos = find(this.revs>1);
            if tmp_pos > 0
                this.revs(tmp_pos) = 1;
            end
            % only backward
            tmp_pos = find(this.revs<-1);
            if tmp_pos > 0
                this.revs(tmp_pos) = -1;
            end
            
            this = createCompartmentList(this);
            %this = resortMetabolitesAccorindCompartmentList(this);
            
            % check for transporters (reactions with more than two compartments)
            this = checkForTransporter(this);
            
            % set has-gene flag
            has_gene_int = has_gene(this.is_internal);
            has_gene_ext = has_gene(this.is_external);
            has_gene_mue = has_gene(this.is_biomass);
            this.has_gene = [has_gene_int;has_gene_ext;has_gene_mue];
        end
        
        function S = getStoich(this)
            S = [this.S this.E this.M];
        end
        
        function S = getBinaryStoich(this)
            S = logical([this.S this.E this.M]);
        end
        
        function cons = getBounds(this)
            cons = [this.cons_int; this.cons_ext; this.cons_mue];
        end
        
        function LB = getLowerBounds(this)
            cons = getBounds(this);
            LB = cons(:,1);
        end
        
        function UB = getUpperBounds(this)
            cons = getBounds(this);
            UB = cons(:,2);
        end
        
        function Mets = getMetaboliteIds(this)
            Mets = this.metabolites;
        end
        
        function Sneg = getSneg(this)
            Sneg = this.Sneg;
        end
        
        function Spos = getSpos(this)
            Spos = this.Spos;
        end
        
        function Sneg = setSneg(this, Sneg)
            this.Sneg = Sneg;
        end
        
        function Spos = setSpos(this, Spos)
            this.Spos = Spos;
        end
        
        function this = createCompartmentList(this)
            list = cell(this.m,1);
            for i=1:this.m
                id  = this.metabolite_ids(i,:);
                id  = regexprep(id,'%','');
                id  = regexprep(id,' ','');
                id = regexp(id,'@','split');
                list{i} = id{2};
            end
            [ulist, ~, J] = unique_no_sort(list);
            this.c = length(ulist);
            this.compartments = char(ulist);
            this.compartment_idx = J;
        end
        
        function this = checkForTransporter(this)
            BSM = logical([this.S this.E this.M]);
            is_trans = zeros(this.r,1);
            for i=1:this.r
                is_trans(i) = length(unique(this.compartment_idx(BSM(:,i))))>1;
            end
            this.is_transporter = is_trans;
        end
        
%         function this = resortMetabolitesAccorindCompartmentList(this)
%             [sorted, I] = sort(this.compartment_idx);
%             this.Sneg = this.Sneg(I,:);
%             this.Spos = this.Spos(I,:);
%             this.metabolites = this.metabolites(I);
%             this.S = this.S(I,:);
%             this.E = this.E(I,:);
%             this.M = this.M(I,:);
%             this.metabolite_ids = this.metabolite_ids(I,:);
%             this.compartment_idx = sorted; 
%         end
        
        
    end
    methods (Static)
        
        function newObj = copy(stoich)
            % create an empty instance of the class            
            newObj.S = stoich.S;
            newObj.E = stoich.E;
            newObj.M = stoich.M;
            newObj.int = stoich.int;
            newObj.ext = stoich.ext;
            newObj.mue = stoich.mue;
            newObj.cons_int = stoich.cons_int;
            newObj.cons_ext = stoich.cons_ext;
            newObj.cons_mue = stoich.cons_mue;
            newObj.reactions_int = stoich.reactions_int;
            newObj.reactions_ext = stoich.reactions_ext;
            newObj.reactions_mue = stoich.reactions_mue;
            newObj.mue_reversed = stoich.mue_reversed;
            newObj.m = stoich.m;
            newObj.r = stoich.r;
            newObj.reaction_ids = stoich.reaction_ids;
            newObj.metabolite_ids = stoich.metabolite_ids;
            newObj.revs = stoich.revs;
            newObj.c = stoich.c;
            newObj.compartments = stoich.compartments;
            newObj.compartment_idx = stoich.compartment_idx;
            newObj.is_transporter = stoich.is_transporter;
            newObj.has_gene = stoich.has_gene;
        end
    end
end

