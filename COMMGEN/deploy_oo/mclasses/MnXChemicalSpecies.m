classdef MnXChemicalSpecies < handle
    %MNXCHEMICALSPECIES Summary of this class goes here
    %   Detailed explanation goes here
    properties %(GetAccess = private)
        % chemical elements    
        DEL    = '%';
        CEL    = {'C','F','H','I','K','N','O','P','R','S','W','X', ...
               'Ca', 'Na', 'Sb', 'Cd', 'Fe', 'Se', 'Ag', 'Hg', 'Mg', ...
               'Ni', 'Cl', 'Mn', 'Zn', 'Co', 'Mo', 'Cr', 'As', 'Cu', ...
               'Pb', 'Br'};
        CEList
        NC 
    end
    
    properties
        id
        name
        source
        formula
        formula_as_vector
        formula_valid = false;
        mass
        charge
        references
    end
    
    methods
        
        function this = MnXChemicalSpecies(varargin)
            if nargin == 1
                arguments       = varargin{1};
                this.id         = arguments{1};
                this.name       = arguments{2};
                this.source     = arguments{3};
                this.formula    = arguments{4};
                this.mass       = arguments{5};
                this.charge     = arguments{6};
                this.references = arguments{7};
            elseif nargin == 7
                this.id         = varargin{1};
                this.name       = varargin{2};
                this.source     = varargin{3};
                this.formula    = varargin{4};
                this.mass       = varargin{5};
                this.charge     = varargin{6};
                this.references = varargin{7};
            else
                error('Wrong input format');
            end
            
            this.NC = length(this.CEL);
            this.CEList = cell(this.NC,1);
            for i=1:this.NC
                this.CEList{i} = [this.DEL this.CEL{i} this.DEL];
            end
            this.CEList = char(this.CEList);
            this = chemicalSum2Vector(this);
        end
        
        function elements = getChemicalElements(this)
            elements = this.CEL;
        end
        
        function this = chemicalSum2Vector(this)
            
            s          = this.formula;
            cel_vector = zeros(1, this.NC);
            
            % treatment "."
            s2 = [];
            if regexp(s,'\.','once') > 0
                tmp = regexp(s,'\.','split');
                for i=1:length(tmp)
                    tmp2 = regexp(tmp{i},'(^\d+)(\S+)','tokens');
                    if ~isempty(tmp2)
                        tmp2 = tmp2{:};
                        snew = repmat(tmp2{2},1,str2double(tmp2{1}));
                        s2{end+1} = snew;
                    else
                        s2{end+1} = tmp{i};
                    end
                    
                end
                s = strcat(s2{:});
            end
            
            
            % replacement
            FlagN = false;
            if FlagN
                s = regexprep(s,'\(|\)n','');
            end
            
            if regexp(s, '\)n|\.|\)m|\+', 'once') > 0
                %fprintf('warning: wrong definition of chemical formula: %s\n',s);
                this.formula_as_vector = cel_vector;
                this.formula_valid = false;
                return;
            end
            
            
            % decompose formula into pairs of chemical elements and
            % corresponding frequencies
            cc = regexp(s,'[A-Z][a-z]*\d*','match');
            
            % decompose each pair into chemical element and frequency and
            % create vector
            if ~isempty(cc)
                for i=1:length(cc)
                    tmp = regexp(cc{i},'([A-Za-z]+)(\d*)','tokens');
                    tmp = tmp{:};
                    element = tmp{1};
                    if isempty(tmp{2})
                        freq = 1;
                    else
                        freq = str2double(tmp{2});
                    end
                    idx = mfindstr(this.CEList, [ this.DEL element this.DEL]);
                    if idx > 0
                        cel_vector(idx) = cel_vector(idx) + freq;
                    else
                        fprintf('# missing element in chemical formula: %s\n',element);
                    end
                end
                this.formula_valid = true;
            end
            this.formula_as_vector = cel_vector;
        end
        
        function print(this, varargin)
            if nargin == 1
                % print species info to screen
                fprintf('%s\t', this.id);
                fprintf('%s\t', this.name);
                fprintf('%s\t', this.source);
                fprintf('%s\t', this.formula);
                fprintf('%s\t', this.mass);
                fprintf('%s\t', this.charge);
                fprintf('%s\n', this.references);
            elseif nargin == 2
                fid = varargin{1};
                % print species info to file
                fprintf(fid, '%s\t', this.id);
                fprintf(fid, '%s\t', this.name);
                fprintf(fid, '%s\t', this.source);
                fprintf(fid, '%s\t', this.formula);
                fprintf(fid, '%s\t', this.mass);
                fprintf(fid, '%s\t', this.charge);
                fprintf(fid, '%s\n', this.references);
            else
                fprintf('ERROR: wrong format\n');
            end
        end
        
    end
    
end

