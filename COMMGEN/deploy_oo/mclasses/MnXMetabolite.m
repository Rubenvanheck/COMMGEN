classdef MnXMetabolite
    %MNXMETABOLITE
    % a metabolite object after optimizing for its production
    % holds results for an FBA run
    
    properties
        id       = [];    % metabolite identifier
        pos      = 0;     % position of metabolite
        viable   = false; % model viable
        fluxes   = [];    % flux distribution
        mue_flux = 0;     % metabolite flux
    end
    
    methods
        function this = MnXMetabolite(varargin)
            if nargin == 5
                tmp = varargin{1};
                for i=1:size(tmp,1)
                    if i==1
                        this.id = deblank(tmp(i,:));
                    else
                        this.id = [this.id '; ' deblank(tmp(i,:))];
                    end
                end
                this.id = strrep(this.id,'%','');
                this.pos      = varargin{2};
                this.viable   = varargin{3};
                this.fluxes   = varargin{4};
                this.mue_flux = varargin{5};
            end
        end
    end
    
end


