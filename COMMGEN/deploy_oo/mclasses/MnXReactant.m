classdef MnXReactant < handle
    %MnXReactant Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        id
        chemical_id
        compartment
        stoichiometry
    end
    
    methods
        function this = MnXReactant(id, stoichiometry)
            this.id            = id;
            res = regexp(id, '@', 'split');
            this.chemical_id   = res{1};
            this.compartment   = res{2};
            this.stoichiometry = stoichiometry;
        end
    end
    
end

