classdef MnXRMDatabase < handle
    %MnXRMDatabase Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (GetAccess = private)
        maxRSize   = 13;  % max reaction size
        l          = 0;   % number of reaction motifs in DB
        FPrint     = 1;   % print warnings
    end
    
    properties
        FSigned= 0;   % signed/unsigned reaction motifs
        SPP     % reaction motifs
        SPC     % reaction motif counts
        SPL     % reaction motif lengths
        SPR     % reaction motif reversibility counts (bw, bi, fw)
        SPCS    % reaction motif check sums
        metabolite_ids % metabolite identifier
        jx      % java hash
    end
    
    methods
        function this = MnXRMDatabase(varargin)
            if nargin == 1
                stoich  = varargin{1};
                % determine motif count table
                this                = init(this, stoich);
                this.metabolite_ids = stoich.metabolite_ids;
            elseif nargin == 2
                stoich  = varargin{1};
                signed  = varargin{2};
                this.FSigned = signed;
                % determine motif count table
                this                = init(this, stoich);
                this.metabolite_ids = stoich.metabolite_ids;
            end
            
%             if nargin ==2
%                 db    = varargin{1};
%                 model = varargin{2};
%                 % determine motif count table
%                 this  = init(this, db.stoich);
%                 this.metabolite_ids = db.stoich.metabolite_ids;
%                 
%                 pos   = find(model.stoich.S(:,1));
%                 coef  = model.stoich.S(pos,1);
%                 [ spr motifs motifLen mCount ] = getMotifCountsForReaction(this, pos, coef, 0);
%             end
            
        end
        
        % create motif count table
        function this = init(this, stoich)
            PSet  = [];  % reaction motifs
            PL    = [];  % length of reaction motifs
            PCS   = [];  % check sum of reaction motifs
            PRev  = [];  % reversibility counts (bw, bi, fw) after standardizing the rm
            
            S = stoich.S;
            if this.FSigned > 0
                S = sign(S);
            end
            % examine every internal reaction
            for i=1:stoich.int
                % encode reaction by positions and values
                pos     = find(S(:,i));
                coef    = S(pos,i);
                len_pos = length(pos);
                
                % only consider non-empty reactions
                if len_pos > 0 && len_pos <= this.maxRSize
                    % initialize
                    [motifs, mCount] = MnXRMDatabase.determineMotifsAsBitVectors(len_pos);
                    rev = stoich.revs(i);
                    
                    % decompose each reaction into its motifs
                    for j=1:mCount
                        cIdx    = pos(motifs(j,:));
                        cStoich = coef(motifs(j,:));
                        
                        ps            = full([cIdx';cStoich']);
                        [ps,psign]    = MnXRMDatabase.fstdrx(ps); % standardize reaction motif
                        
                        % it's fastest to just append the info
                        PSet{end+1}   = ps;
                        PL(end+1)     = length(cIdx);
                        vec = zeros(1,3);
                        vec(rev*psign+2) = 1;
                        PRev(end+1,:) = vec;
                        PCS{end + 1}  = DataHash(ps); % rm check sum
                    end
                end
            end
            if ~isempty(PCS)
                % find list of unique entries via check sums
                [this.SPCS, I, J] = unique_no_sort(PCS);
                this.SPCS = char(this.SPCS);
                
                % and keep track of their characteristics
                this.SPP = PSet(I);
                this.SPL = PL(I);
                this.SPR = PRev(I,:);
                this.SPC = ones(length(I),1);
                
                % update: multiple occurrences of the same motif
                tmpJ    = J;
                tmpJ(I) = [];
                tmpJ    = unique(tmpJ);
                J(I)    = 0;
                if ~isempty(tmpJ)
                    for i=tmpJ
                        idx      = J==i;
                        this.SPR(i,:) = this.SPR(i,:) + sum(PRev(idx,:),1);
                        this.SPC(i)   = this.SPC(i) + sum(idx~=0);
                    end
                end
                this.l = length(I);
                this.jx = java.util.Hashtable(this.l);
                for i = 1:this.l
                    this.jx.put(this.SPCS(i,:), i);
                end
            else
                this.jx = java.util.Hashtable(this.l);
            end
        end
        
        % get motif counts for a reaction
        function [ spr motifs motifLen mCount ] = getMotifCountsForReaction(this, pos, coef, rev)
            
            [~, motifLen ,PCS, PSign, ~, ~, mCount, motifs] = ...
                determineMotifsForReaction(this, pos, coef, rev);
            matches = findReactionMotifsInDB(this, PCS);
            
            % check for reaction motifs and standardize for the reduced table
            if all(matches==0)
%                 if this.FPrint > 0
%                     fprintf('# Reaction has no motif in common with the db!\n');
%                 end
                spr       = [];
                motifs    = [];
                motifLen  = [];
                mCount    = [];
                return;
                
            else
                spr = getMotifCountTable(this, matches, PSign);
            end
            
        end
        
        function [PSet, PL ,PCS, PSign, POri, PRev, mCount, motifs] = ...
                determineMotifsForReaction(this, positions, stoich, varargin)
            
            % default reversibility is bidirectional
            rev = 0;
            if nargin == 4
                rev = varargin{1};
            end
            
            % initialize
            rLength = length(positions);
            [motifs, mCount] = this.determineMotifsAsBitVectors(rLength);
            
            % look-up all motifs in pattern table and create sub-table
            PSet  = cell(mCount,1);
            PL    = zeros(mCount,1);
            PCS   = cell(mCount,1);
            PSign = zeros(mCount,1);
            POri  = zeros(mCount,1); % pattern orientation (0=fwd,1=bwd);
            PRev  = zeros(mCount,3);
            
            for i=1:mCount
                
                mIdx = motifs(i,:);
                cIdx    = positions(mIdx);
                cStoich = stoich(mIdx);
                
                ps          = full([cIdx';cStoich']);
                [ps,psign]  = this.fstdrx(ps);
                PSet{i}     = ps;
                PSign(i)    = psign;
                PL(i)       = length(cIdx);
                
                POri(i)     = (psign<0); % orientation
                pos         = rev*psign+2;
                PRev(i,pos) = 1;
                PCS{i}      = DataHash(ps); % rm check sum
            end
            PCS = char(PCS);
        end
        
        function matches = findReactionMotifsInDB(this, PCS)
            % find reaction motifs in DB
            mCount  = size(PCS,1);
            matches = zeros(mCount,1);
            for i = 1:mCount
                idx = this.jx.get(PCS(i,:));
                if idx > 0
                    matches(i) = idx;
                end
%                 idx = mfindstr(this.SPCS, PCS(i,:));
%                 if idx > 0
%                     matches(i) = idx;
%                 end
            end
        end
        
        function [spr] = getMotifCountTable(this, matches, PSign)
            len = length(matches);
            spr = zeros(len,3);
            
            for i = 1:len
                if matches(i) > 0
                    if (PSign(i)>0)
                        spr(i,:) = this.SPR(matches(i),:);
                    else
                        spr(i,:) = fliplr(this.SPR(matches(i),:));
                    end
                end
            end
        end
        
    end
    
    methods (Static)
        function [motifs varargout]= determineMotifsAsBitVectors(rLength)
            % DetermineMotifs
            % Determines the motifs as bit vectors for a reaction of length rLength
            motifCounts = 0;
            for i=1:rLength
                motifCounts = motifCounts + nchoosek(rLength,i);
            end
            
            motifs = zeros(motifCounts,rLength);
            
            for i=1:motifCounts
                motifs(i,:) = dec2binVector(i,rLength);
            end
            motifs = logical(motifs);
            varargout{1} = motifCounts;
        end
        
        function [s0,varargout] = fstdrx(s0)
            % Function: Format / standardize reaction (motif) entry
            s0 = double(s0);
            l  = size(s0,2);
            
            % eliminate catalytic components
            fl = true(1,l);
            q  = zeros(1,l);
            for z = 1:l,
                if fl(z),
                    idx  = find(s0(1,:)==s0(1,z));
                    q(z) = sum(s0(2,idx));
                    fl(idx) = false;
                end
            end
            
            sidx    = find(q);
            s0(2,:) = q;
            
            if ~isempty(sidx),
                [~,i1] = sort(s0(1,sidx));
                %    s1 = 0*s0(:,sidx);
                s1(:,1:length(sidx)) = s0(:,sidx(i1));
                s0 = s1;
                
                ssign   = sign(s0(2,1));
                s0(2,:) = s0(2,:)*ssign;
            else
                s0    = [];
                ssign = 0;
            end
            
            varargout{1} = ssign; % sign for change of reaction direction
        end
        
    end 
end

