classdef MnXModelSpecification < handle
    %MNX Summary of this class goes here
    %   Detailed explanation goes here
       
    properties
        id        = [];
        name      = [];
        date      = [];
        source_id = [];
        dir       = [];
        ncbi      = 0;
        version   = '';
    end
    
    methods
        % constructor
        function this = MnXModelSpecification(varargin)
            if nargin == 1
                arguments = varargin{1};
                if length(arguments) >= 4
                    this.id        = arguments{1};
                    this.name      = arguments{2};
                    this.date      = arguments{3};
                    this.source_id = arguments{4};
                    
                    if length(arguments) >= 5
                        this.ncbi    = arguments{5};
                        if length(arguments) >= 6
                            this.version = arguments{6};
                            if length(arguments) >= 6
                                this.dir = arguments{7};
                            end
                        end
                    end
                end
            elseif nargin == 2
                arguments = varargin{1};
                if length(arguments) >= 4
                    this.id        = arguments{1};
                    this.name      = arguments{2};
                    this.date      = arguments{3};
                    this.source_id = arguments{4};
                    
                    if length(arguments) >= 5
                        this.ncbi    = arguments{5};
                        if length(arguments) >= 6
                            this.version = arguments{6};
                            if length(arguments) >= 6
                                this.dir = varargin{2};
                            end
                        end
                    end
                end
            elseif nargin >=4
                this.id   = varargin{1};
                this.name = varargin{2};
                this.date = varargin{3};
                this.source_id = varargin{4};
                if nargin >= 5
                    this.ncbi    = varargin{5};
                    if nargin >= 6
                        this.version = varargin{6};
                        if nargin >= 7
                            this.dir = varargin{7};
                        end
                    end
                end
            else
                error('Wrong input format');
            end
            
        end
    end
end
