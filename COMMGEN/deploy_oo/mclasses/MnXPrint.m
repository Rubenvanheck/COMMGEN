classdef MnXPrint
    %MNXPRINT Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Constant)
        option_biomass    = 'biomass';
        option_iko        = 'iko';
        option_bioPrec    = 'bio_precursors';
        option_fva        = 'fva';
        option_statistics = 'statistics';
        option_met        = 'met';
        option_deadends   = 'deadends';
        option_direction  = 'direction';
        option_nullspace  = 'nullspace';
        option_gapfilling = 'gapfilling';
        option_consistency= 'consistency';
        option_gko        = 'gko';
        option_gko_single = 'gko_single';
        option_gko_simple = 'gko_simple';
        option_grko_relation = 'grko_relation';
    end
    
    methods
        function this = MnXPrint(object, option, varargin)
            if nargin == 2;
                % object is either MnXModel or MnXRMPrediction
                if strcmp(this.option_biomass,option)
                    this.printBiomass(object);
                elseif strcmp(this.option_iko, option)
                    this.printiKO(object);
                elseif strcmp(this.option_bioPrec, option)
                    this.printBioPrecursors(object);
                elseif strcmp(this.option_fva, option)
                    this.printFVA(object);
                elseif strcmp(this.option_statistics, option)
                    this.printStatistics(object);
                elseif strcmp(this.option_met, option)
                    this.printiMetSingle(object);
                elseif strcmp(this.option_deadends, option)
                    this.printDeadEndReactions(object);
                    this.printDeadEndMetabolites(object);
                elseif strcmp(this.option_direction, option)
                    this.printRMDirection(object);
                elseif strcmp(this.option_nullspace, option)
                    this.printNullSpaceAnalysis(object)
                elseif strcmp(this.option_gapfilling, option)
                    this.printRMGapFilling(object);
                elseif strcmp(this.option_consistency, option)
                    this.printModelConsistency(object);
                elseif strcmp(this.option_gko, option)
                    this.printgKO(object);  % print gene knockouts (0:dead; 1:viable)
                elseif strcmp(this.option_gko_simple, option)
                    this.printgKO(object);  % print gene knockouts (0:dead; 1:viable)
                elseif strcmp(this.option_gko_single, option)
                    this.printgKOSingle(object);  % print gene knockouts (0:dead; 1:viable)
                end
            elseif nargin == 3
                file = varargin{1};
                if strcmp(this.option_consistency, option)
                    this.printModelConsistencyToFile(object,file);
                elseif strcmp(this.option_gko, option)
                    this.printgKO2File(object,file);  % print gene knockouts (0:dead; 1:viable) to file
                elseif strcmp(this.option_gko_simple, option)
                    this.printgKO2File(object,file);  % print gene knockouts (0:dead; 1:viable) to file
                elseif strcmp(this.option_gko_single, option)
                    this.printgKOSingle2File(object,file);  % print gene knockouts (0:dead; 1:viable) to file
                elseif strcmp(this.option_grko_relation, option)
                    this.printSingleGeneReactionKORelation2File(object,file);  % print gene-reaction-knockout relation to file (gene <tab> affected reactions) to file
                elseif strcmp(this.option_nullspace, option) % print nullspace analysis to file
                    this.printNullSpaceAnalysis2File(object, file);
                end
            else
                error('Wrong input format!\n');
            end
        end
    end
    
    methods (Static)
        
        function printModelConsistency(model)
            if isfield(model.consistency, 'deadends')
                fprintf('Dead-ends:\t%7.10f (%7.10f) \n', model.consistency.deadends, model.consistency.deadends_wo_ext);
            end
            if isfield(model.consistency, 'deadendsrec')
                fprintf('Rec-Dead-ends:\t%7.10f (%7.10f) \n', model.consistency.deadendsrec, model.consistency.deadendsrec_wo_ext);
            end
            if isfield(model.consistency, 'nullspace')
                fprintf('Nullspace:\t%7.10f (%7.10f)\n', model.consistency.nullspace, model.consistency.nullspace_wo_ext);
            end
            if isfield(model.consistency, 'all')
                fprintf('Nspace+Rec-DE:\t%7.10f (%7.10f)\n', model.consistency.all, model.consistency.all_wo_ext);
            end
        end
        
        function printBiomass(model)
            stoich = model.stoich;
            if ~isempty(model.fba.biomass)
                if model.fba.biomass.viable > 0
                    fprintf('# FBA: success\n');
                else
                    fprintf('# FBA: failed\n');
                end
                
                ids = [stoich.reactions_int; stoich.reactions_ext; stoich.reactions_mue];
                r = length(ids);
                for i=1:r-1
                    fprintf('%s\t%7.10f\n', ids{i}, model.fba.biomass.fluxes(i));
                end
                if model.stoich.mue_reversed > 0
                    fprintf('%s\t%7.10f\n', ids{end}, -model.fba.biomass.fluxes(end));
                else
                    fprintf('%s\t%7.10f\n', ids{end}, model.fba.biomass.fluxes(end));
                end
            end
            
        end
        
        function printiKO(model)
            stoich = model.stoich;
            if ~isempty(model.fba.iko)
                ids = [stoich.reactions_int; stoich.reactions_ext; stoich.reactions_mue];
                for i=1:length(model.fba.iko)
                    for j=1:length(model.fba.iko{i}.pos)
                        if j==1
                            tmp_id = [ids{model.fba.iko{i}.pos(j)}];
                        else
                            tmp_id = [tmp_id '; ' ids{model.fba.iko{i}.pos(j)}];
                        end
                    end
                    if model.stoich.mue_reversed > 0
                        fprintf('%s\t%7.10f\t%7.10f\t%7.10f\n', tmp_id, -model.fba.iko{i}.mue, -model.fba.biomass.mue_flux, model.fba.iko{i}.mue/model.fba.biomass.mue_flux);
                    else
                        fprintf('%s\t%7.10f\t%7.10f\t%7.10f\n', tmp_id, model.fba.iko{i}.mue, model.fba.biomass.mue_flux, model.fba.iko{i}.mue/model.fba.biomass.mue_flux);
                    end
                end
            end
        end
        
        function printBioPrecursors(model)
            if ~isempty(model.fba.bioPrecursor)
                for i=1:length(model.fba.bioPrecursor)
                    id = model.fba.bioPrecursor{i}.id;
                    id = regexprep(id, '%','');
                    fprintf('%s\t%7.10f\n',id, model.fba.bioPrecursor{i}.mue_flux);
%                     fprintf('%s\t%7.10f\t%i\n',id, model.fba.bioPrecursor{i}.mue_flux, model.fba.bioPrecursor{i}.viable);
                end
            end
        end
        
        function printiMetSingle(model)
            if ~isempty(model.fba.met)
                for i=1:length(model.fba.met)
                    id = model.fba.met{i}.id;
                    id = regexprep(id, '%','');
                    fprintf('%s\t%7.10f\n',id, model.fba.met{i}.mue_flux);
%                     fprintf('%s\t%7.10f\t%i\n',id, model.fba.imet{i}.mue_flux, model.fba.imet{i}.viable);
                end
            end
        end
        
        function printFVA(model)
            stoich = model.stoich;
            ids = [stoich.reactions_int; stoich.reactions_ext];
            cons = [stoich.cons_int; stoich.cons_ext];
            if ~isempty(model.fba.fva)
                for i=1:(stoich.int+stoich.ext)
                    if ~isempty(model.fba.fva{i})
                        % ID	LB	minLB	flux-value maxUB	UB
                        id = ids{i};
                        lb = cons(i,1);
                        ub = cons(i,2);
                        minLB = model.fba.fva{i}.fluxes(1);
                        maxLB = model.fba.fva{i}.fluxes(2);
                        flux = model.fba.biomass.fluxes(i);
                        fprintf('%s\t%7.10f\t%7.10f\t%7.10f\t%7.10f\t%7.10f\n',id, lb, minLB, flux, maxLB, ub);
                    end
                end
            end
        end
        
        function printStatistics(model)
            fprintf('Examining model: %s\n', model.id);
            fprintf('Loci:\t\t\t\t%i\n', model.locus.l);
            fprintf('Chemical species:\t\t%i\n', length(model.species));
            fprintf('Compartments:\t\t\t%i\n', model.stoich.c);
            fprintf('Metabolites x Reactions:\t%i x %i\n', model.stoich.m, model.stoich.r);
            fprintf('Metabolites x internal R:\t%i x %i\n', sum(sum(logical(model.stoich.S),2)>0), model.stoich.int);
            fprintf('Metabolites x external R:\t%i x %i\n', sum(sum(logical(model.stoich.E),2)>0), model.stoich.ext);
            fprintf('Metabolites x biomass R:\t%i x %i\n', sum(sum(logical(model.stoich.M),2)>0), model.stoich.mue);
            
            % print biomass flux if calcualted
            if ~isempty(model.fba.biomass.fluxes)
                if model.stoich.mue_reversed > 0
                    fprintf('Biomass-flux:\t\t\t%7.10f\n', -model.fba.biomass.mue_flux);
                else
                    fprintf('Biomass-flux:\t\t\t%7.10f\n', model.fba.biomass.mue_flux);
                end
            end
            
            % check reaction sizes for input reactions vs. model reactions
            rSizes = sum(logical([model.stoich.S model.stoich.E model.stoich.M]),1);
            ids    = [model.stoich.reactions_int;model.stoich.reactions_ext;model.stoich.reactions_mue];
            fprintf('# Different reaction sizes for model vs input\n');
            for i=1:model.stoich.r
                if rSizes(i) ~= model.reactions{i}.size
                    fprintf('%s (%i) : %i vs %i\n', ids{i}, i, rSizes(i), model.reactions{i}.size);
%                     fprintf('warning: %s (%i) different reaction sizes for model vs input: %i vs %i\n', ids{i}, i,rSizes(i), model.reactions{i}.size);
                end
            end
            
            % non used compartmentalized chemcial species, i.e. metabolites
            idx = find(~any([model.stoich.S model.stoich.E model.stoich.M],2));
            fprintf('# Non-used metabolites\n');
            for i=1:length(idx)
                id = deblank(model.stoich.metabolite_ids(idx(i),:));
                fprintf('%s (%i)\n', id, idx(i));
%                 fprintf('warning: Metabolite %s (%i) is not used in model.\n', id, idx(i));
            end
            
            % print wrong defined chemical formulas
            fprintf('# Wrong definition of chemical sum formula\n');
            for i=1:length(model.species)
                if model.species{i}.formula_valid == 0
                    fprintf('%s (%i):\t%s\n',model.species{i}.id, i, model.species{i}.formula);
%                     fprintf('warning: wrong definition of chemical formula (%i): %s:\t%s\n',i,model.species{i}.id, model.species{i}.formula);
                end
            end
            
            % determine reactions that are not elementally balanced
%             res = MnXModelAnalysis(model);
%             for i=1:model.stoich.int
%                 if ~res.is_balanced(i)
%                     fprintf('warning: %s (%i) is not elementally balanced\n', ids{i}, i);
%                 end
%             end
            
            fprintf('\n');
        end
        
        function printDeadEndReactions(model)
            fprintf('# "Dead-end" reactions\n');
            for i=1:model.stoich.int
                    fprintf('%s\t%i\t%i\n', model.stoich.reactions_int{i},model.structuralAnalysis.deadEnds.iR(i),model.structuralAnalysis.deadEndsRec.iR(i));
            end
            if ~isempty (model.stoich.reactions_mue)
                bio    = any(model.structuralAnalysis.deadEnds.iB);
                bioRec = any(model.structuralAnalysis.deadEndsRec.iB);
                fprintf('%s\t%i\t%i\n', model.stoich.reactions_mue{:}, bio, bioRec);
            end
        end
        
        function printDeadEndMetabolites(model)
            fprintf('# "Dead-end" metabolites\n');
            for i=1:model.stoich.m
                id = strrep(deblank(model.stoich.metabolite_ids(i,:)), '%', '');
                fprintf('%s\t%i\t%i\n', id, model.structuralAnalysis.deadEnds.iM(i),model.structuralAnalysis.deadEndsRec.iM(i));
            end
        end
        
        function printRMDirection(prediction)
            r = length(prediction.ids);
            for i=1:r
                fprintf('%s\t%i\t%i\t%7.10f\n', prediction.ids{i}, prediction.sizes(i), prediction.dir(i), prediction.ratios(i));
            end
        end
        
        function printNullSpaceAnalysis(model)
            fprintf('# Nullspace analysis\n');
            ids    = [model.stoich.reactions_int;model.stoich.reactions_ext;model.stoich.reactions_mue];
            for i=1:model.stoich.r
                fprintf('%s\t%i\t%i\t%i\n', ids{i}, ...
                    model.structuralAnalysis.correlations.zero_flux(i), ...
                    model.structuralAnalysis.correlations.grp_index(i), ...
                    model.structuralAnalysis.inconsistencies.react(i));
            end
        end
        
        function printRMGapFilling(prediction)
            r = length(prediction.ids);
            for i=1:r
                fprintf('%s\t%i\t%7.10f\n', prediction.ids{i}, prediction.sizes(i), prediction.gap(i));
            end
        end
        
        function printgKO(model)
            if model.stoich.mue_reversed > 0
                wt_mue = -model.fba.biomass.mue_flux;
            else
                wt_mue = model.fba.biomass.mue_flux;
            end
            for i=1:model.locus.l
                entry = model.fba.gko{i};
                genes = model.locus.unique_loci{entry.pos};
                if model.stoich.mue_reversed > 0
                    fprintf('%s\t%i\t%7.10f\t%7.10f\t%7.10f\n', ...
                        genes, entry.viable, -entry.mue, wt_mue, (-entry.mue)/wt_mue);
                else
                    fprintf('%s\t%i\t%7.10f\t%7.10f\t%7.10f\n', ...
                        genes, entry.viable, entry.mue, wt_mue, entry.mue/wt_mue);
                end
            end
        end
        
        function printgKOSingle(model)
            if model.stoich.mue_reversed > 0
                wt_mue = -model.fba.biomass.mue_flux;
            else
                wt_mue = model.fba.biomass.mue_flux;
            end
            l = length(model.fba.igko);
            for i=1:l
                entry = model.fba.igko{i};
                pos = model.fba.igko{i}.pos;
                genes = cell2string(model.locus.unique_loci(pos),';');
                if model.stoich.mue_reversed > 0
                    fprintf('%s\t%i\t%7.10f\t%7.10f\t%7.10f\n', ...
                        genes, entry.viable, -entry.mue, wt_mue, (-entry.mue)/wt_mue);
                else
                    fprintf('%s\t%i\t%7.10f\t%7.10f\t%7.10f\n', ...
                        genes, entry.viable, entry.mue, wt_mue, entry.mue/wt_mue);
                end
            end
        end
        
        % TO FILE ROUTINES
        % TODO: update for reversed biomass
        function printModelConsistencyToFile(model,file)
            fid = fopen(file, 'w+');
            fprintf(fid, '#Method\tScore\tScore w/o ext\tReactions\tMetabolites\tGenes\n');
            if isfield(model.consistency, 'deadends')
                fprintf(fid,'Dead-ends:\t%7.10f (%7.10f)\t%i\t%i\t%i\n', model.consistency.deadends, model.consistency.deadends_wo_ext, model.stoich.r, model.stoich.m, model.locus.l);
            end
            if isfield(model.consistency, 'deadendsrec')
                fprintf(fid,'Rec-Dead-ends:\t%7.10f (%7.10f)\t%i\t%i\t%i\n', model.consistency.deadendsrec, model.consistency.deadendsrec_wo_ext, model.stoich.r, model.stoich.m, model.locus.l);
            end
            if isfield(model.consistency, 'nullspace')
                fprintf(fid,'Nullspace:\t%7.10f (%7.10f)\t%i\t%i\t%i\n', model.consistency.nullspace, model.consistency.nullspace_wo_ext, model.stoich.r, model.stoich.m, model.locus.l);
            end
            if isfield(model.consistency, 'all')
                fprintf(fid,'Nspace+Rec-DE:\t%7.10f (%7.10f)\t%i\t%i\t%i\n', model.consistency.all, model.consistency.all_wo_ext, model.stoich.r, model.stoich.m, model.locus.l);
            end
            fclose(fid);
        end
        
        function printgKO2File(model,file)
            fid = fopen(file, 'w+');
            wt_mue = model.fba.biomass.mue_flux;
            for i=1:model.locus.l
                entry = model.fba.gko{i};
                genes = model.locus.unique_loci{entry.pos};
                fprintf(fid,'%s\t%i\t%7.10f\t%7.10f\t%7.10f\n', ...
                    genes, entry.viable, entry.mue, wt_mue, entry.mue/wt_mue);
            end
            fclose(fid);
        end
        
        function printgKOSingle2File(model,file)
            fid = fopen(file, 'w+');
            wt_mue = model.fba.biomass.mue_flux;
            l = length(model.fba.igko);
            for i=1:l
                entry = model.fba.igko{i};
                pos = model.fba.igko{i}.pos;
                genes = cell2string(model.locus.unique_loci(pos),';');
                fprintf(fid,'%s\t%i\t%7.10f\t%7.10f\t%7.10f\n', ...
                    genes, entry.viable, entry.mue, wt_mue, entry.mue/wt_mue);
            end
            fclose(fid);
        end
        
        
        function printSingleGeneReactionKORelation2File(model,file)
            fid = fopen(file, 'w+');
            % single gene knockouts for all genes (model.locus.l)
            for pos = 1:model.locus.l,
                idx = find(any(model.locus.possible_ko(:,pos),2));
                gid  = model.locus.unique_loci{pos};
                rids = ''; 
                if ~isempty(idx)
                    for i=1:length(idx)
                        tmp_id = deblank(model.stoich.reaction_ids(idx(i),:));
                        if i > 1
                            rids = [rids ';' tmp_id];
                        else
                            rids = tmp_id;
                        end
                    end
                end
                rids = strrep(rids, '%','');
                fprintf(fid, '%s\t%s\n', gid, rids);   
            end
            fclose(fid);
        end
        
        function printNullSpaceAnalysis2File(model, file)
            fid = fopen(file, 'w+');
            fprintf('# Nullspace analysis\n');
            ids    = [model.stoich.reactions_int;model.stoich.reactions_ext;model.stoich.reactions_mue];
            consistent = ~model.structuralAnalysis.correlations.zero_flux;
            consistent(model.structuralAnalysis.inconsistencies.react>0) = false;
            for i=1:model.stoich.r
                if i <= model.stoich.int
                    is_int = 1;
                    is_ext = 0;
                    is_mue = 0;
                elseif i > model.stoich.int && i <= model.stoich.int + model.stoich.ext
                    is_int = 0;
                    is_ext = 1;
                    is_mue = 0;
                else
                    is_int = 0;
                    is_ext = 0;
                    is_mue = 1;
                end
                
                fprintf(fid,'%s\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\n', ...
                    ids{i}, ...
                    is_int, is_ext, is_mue, ...
                    model.stoich.has_gene(i), ...
                    consistent(i), ...
                    model.structuralAnalysis.correlations.zero_flux(i), ...
                    model.structuralAnalysis.correlations.grp_index(i), ...
                    model.structuralAnalysis.inconsistencies.react(i));
            end
            fclose(fid);    
        end
        
    end
    
end

