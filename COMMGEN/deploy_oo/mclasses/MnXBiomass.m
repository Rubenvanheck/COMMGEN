classdef MnXBiomass
    %MNXBIOMASS 
    % a biomass object
    % holds results for an FBA run
    
    properties
        viable   = false;      % model viable
        fluxes   = [];      % flux distribution
        mue_flux = NaN;   % biomass flux
    end
    
    methods
        function this = MnXBiomass(varargin)
            if nargin == 3
                this.viable   = varargin{1};
                this.fluxes   = varargin{2};
                this.mue_flux = varargin{3};
            end
            
        end
    end
    
end

