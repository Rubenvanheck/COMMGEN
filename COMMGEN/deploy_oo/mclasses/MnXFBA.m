classdef MnXFBA
    %MNXFBA Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        biomass = []; % test for biomass
        fva     = []; % flux variability analysis
        ko      = []; % single gene knockouts
        iko     = []; % individual knockouts
        bioPrecursor = []; % biomass precursor test
        met     = []; % metabolite production test
        gko     = []; % single gene knockouts
        igko    = []; % individual knockouts
    end
    
    methods
        function this = MnXFBA()
            this.biomass = MnXBiomass();
        end
    end
    
end

