classdef MnXParseReactionEquation < handle
    %MNXPARSEREACTIONEQUATION Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (GetAccess = private)
        compartments
    end
    
    properties (GetAccess = public)
        size
    end
    
    properties
        metabolites
        coefficients
    end

    
    methods
        % constructor
        function this = MnXParseReactionEquation(compartments, equation)
            this.compartments = compartments;
            this = parseReactionEquation(this, equation);
        end
        
        function this = parseReactionEquation(this, equation)
                        
            % get the number of metabolites
            equation = regexprep(strtrim(equation),'\+\s+','');
            %n = length(srgetargs(regexprep(strtrim(regexprep(equation,'+|\(*|\)*|<--|<==>|<?>|-->|(\d\.*\d*)+','')), '(\s\s+)+',' '), ' '));
            n = length(regexp(equation,'@','match'));
            metabo = cell(1,n); % list of metabolites
            %metahs = cell(1,n); % metabolite hash
            scoeff = ones(1,n)*NaN; % stoichiometric coefficients
            
            ridx = 1;   % reactand index
            fled = 1;   % flag: educt side of equation
            
            [rargs,nrargs] = srgetargs(strtrim(equation),' ');
            % check each element of a chemical reaction
            for zr = 1:nrargs,
                s  = strtrim(rargs{zr});
                
                if ~isempty(regexp(s, '<--|<==>|<?>|-->', 'start'))
                    fled = 0;
                    
                elseif isempty(regexp(s, '+', 'start'))
                    
                    [coeff] = checkStoichCoeff(this, s, fled);
                    if ~isnan(coeff)
                        scoeff(ridx) = coeff;
                    else
                        metabo{ridx} = s;
                        ridx = ridx+1;
                    end
                end
            end
            this.metabolites = metabo;
            this.coefficients = scoeff;
            this.size = n;
        end 

        function coeff = checkStoichCoeff(this, s, fled)
            co = regexp(s,'\(*(\S+)\)*','tokens');
            coeff = (1-2*fled)*str2double(co{:});
        end
        
    end
    
end

