classdef MnXModelTransform < handle
    %MNXMODELTRANSFORM Summary of this class goes here
    %   Detailed explanation goes here
    properties (Constant)
        option_mnet = 'mnet';
        option_mat  = 'mat';
    end
    
    
    properties
%         mnet = [];
    end
    
    methods 
        function this = MnXModelTransform(model, option)
            if nargin == 2                
                if strcmp(option, this.option_mnet)
                    mnet = [];
                    stoich = model.stoich;
                    
                    mnet.S = stoich.S;
                    mnet.E = stoich.E;
                    mnet.M = stoich.M;
                    mnet.r = stoich.int;
                    mnet.e = stoich.ext;
                    mnet.m = stoich.m;
                    mnet.cons_r = stoich.cons_int;
                    mnet.cons_e = stoich.cons_ext;
                    mnet.cons_mue = stoich.cons_mue;
                    mnet.reactions = stoich.reactions_int;
                    mnet.exchanges = stoich.reactions_ext;
                    mnet.growth = stoich.reactions_mue{:};
                    if(mnet.cons_mue(1) < 0)
                        mnet.cons_mue = -fliplr(mnet.cons_mue);
                        mnet.M = -mnet.M;
                    end
                    mnet.metatbolites = stoich.metabolites;
                    outfile = [model.dir 'mnet.mat'];
                    save(outfile, 'mnet','-v7.3');                    
                elseif strcmp(option, this.option_mat)
                    outfile = [model.dir 'model.mat'];
                    save(outfile, 'model','-v7.3');
                end
            else
                error('Wrong input format');
            end
        end

    end
    
    methods (Static)
        function mnet = model2mnet(model)
            stoich = model.stoich;
            mnet = [];
            mnet.S = stoich.S;
            mnet.E = stoich.E;
            mnet.M = stoich.M;
            mnet.r = stoich.int;
            mnet.e = stoich.ext;
            mnet.m = stoich.m;
            mnet.cons_r = stoich.cons_int;
            mnet.cons_e = stoich.cons_ext;
            mnet.cons_mue = stoich.cons_mue;
            mnet.reactions = stoich.reactions_int;
            mnet.exchanges = stoich.reactions_ext;
            mnet.growth = stoich.reactions_mue{:};
            if(mnet.cons_mue(1) < 0)
                mnet.cons_mue = -fliplr(mnet.cons_mue);
                mnet.M = -mnet.M;
            end
            mnet.metatbolites = stoich.metabolites;
        end
    end
    
end

