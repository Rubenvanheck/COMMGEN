classdef MnXStoichConcatenate
    %MNXSTOICHCONCATENATE 
    % concatenates two models, where the first model is the reference model
    % for the organization of the metabolic species
    
    properties (GetAccess = private)
        DEL = '%';
        metabolites
        hidden_metabolites;
        NumMetabolites
    end
    
    properties
        id
        startPosInt
        startPosExt
        startPosMue
        startPosMet
        dir
        
        % stoich
        stoich
        
        % chemical species
        species
        metabolites2species
        
        % metabolic reactions
        reactions
        
    end
    
    methods
        function this = MnXStoichConcatenate(m1, m2)
            fprintf('# Concatenate Models\n');
            
            % get global parameters
            this.id  = [ m1.id ';' m2.id ];
            
            % internal reactions
            if any(strcmp(properties(m1), 'startPosInt'))
                this.startPosInt = m1.startPosInt;
            else
                this.startPosInt = 1;
            end
            
            if any(strcmp(properties(m2), 'startPosInt'))
                this.startPosInt = [this.startPosInt m1.stoich.int+m2.startPosInt];
            else
                this.startPosInt = [this.startPosInt m1.stoich.int+1];
            end
            
            % external reactions
            if any(strcmp(properties(m1), 'startPosExt'))
                this.startPosExt = m1.startPosExt;
            else
                this.startPosExt = 1;
            end
            
            if any(strcmp(properties(m2), 'startPosInt'))
                this.startPosExt = [this.startPosExt m1.stoich.ext+m2.startPosExt];
            else
                this.startPosExt = [this.startPosExt m1.stoich.ext+1];
            end
            
            % biomass reactions
            if any(strcmp(properties(m1), 'startPosMue'))
                this.startPosMue = m1.startPosMue;
            else
                this.startPosMue = 1;
            end
            
            if any(strcmp(properties(m2), 'startPosMue'))
                this.startPosMue = [this.startPosMue m1.stoich.muet+m2.startPosMue];
            else
                this.startPosMue = [this.startPosMue m1.stoich.mue+1];
            end
            
            % metabolites
            if any(strcmp(properties(m1), 'startPosMet'))
                this.startPosMet = m1.startPosMet;
            else
                this.startPosMet = 1;
            end
            
            if any(strcmp(properties(m2), 'startPosMet'))
                this.startPosMet = [this.startPosMet m1.stoich.m+m2.startPosMet];
            else
                this.startPosMet = [this.startPosMet m1.stoich.m+1];
            end
            
            this.dir = '/tmp/';
            
            % use internal reactions of m2: flag = false
            this = getSpecies(this, m1, m2);
            this = getMetabolites(this, m1, m2);
            
            % use a routine add reactions to append reactions from m2 to m1
            
            % create stoichiometric matrix, and resort reactions according
            % to matrix
            this     = createStoichiometricMatrix(this, m1, m2);
            this     = createSpeciesIndex(this);
            %%%%%
            %%%%% MADE_CHANGE
            %%%%%
            %this     = addReactions(this, m1, m2);
        end
        
        function this = getSpecies(this, m1, m2)
            
            % combines the list of metabolites of m1 and m2
            NumSpecies1 = length(m1.species);
            NumSpecies2 = length(m2.species);
            SpeciesList = cell(NumSpecies1 + NumSpecies2,1);
            SpeciesID   = cell(NumSpecies1 + NumSpecies2,1);
            
            % create list of species for m1
            for i=1:NumSpecies1
                SpeciesList{i} = m1.species{i};
                SpeciesID{i}   = m1.species{i}.id;
            end
            
            % get species of m2 and append to existing list
            
            for i=1:NumSpecies2
                cnt = NumSpecies1+i;
                SpeciesList{cnt} = m2.species{i};
                SpeciesID{cnt}   = m2.species{i}.id;
            end
            
            % make list unique
            if~isempty(SpeciesID)
                [~, I, ~]    = unique_no_sort(SpeciesID);
                this.species = SpeciesList(I);
            else
                this.species = [];
            end
        end
        
        
        function this = getMetabolites(this, m1, m2)
            
            % combines the list of metabolites of m1 and m2
            list = cell(0);
            cnt = 0;
            % create list of metabolites for m1
            for i=1:m1.stoich.r
                for j=1:m1.reactions{i}.size
                    cnt = cnt + 1;
                    list{cnt} = m1.reactions{i}.reactants{j}.id;
                end
            end
            % get metabolites of m2 and append to existing list
            for i=1:m2.stoich.r
                for j=1:m2.reactions{i}.size
                    cnt = cnt + 1;
                    list{cnt} = m2.reactions{i}.reactants{j}.id;
                end
            end
            
            % make list unique
            this.metabolites    = unique_no_sort(list);
            this.NumMetabolites = length(this.metabolites );
            
            % fast lookup
            this.hidden_metabolites = cell(this.NumMetabolites,1);
            for i=1:this.NumMetabolites
                this.hidden_metabolites{i} = [this.DEL this.metabolites{i} this.DEL];
            end
            this.hidden_metabolites = char(this.hidden_metabolites{:});
            
        end
        
        function this = createStoichiometricMatrix(this, m1, m2)
            % get the number of reactions
            rr   = m1.stoich.r + m2.stoich.r;
            rxns = [m1.reactions ; m2.reactions];
            Sneg = sparse(this.NumMetabolites, rr);
            Spos = sparse(this.NumMetabolites, rr);
%             Sneg = zeros(this.NumMetabolites, rr);
%             Spos = zeros(this.NumMetabolites, rr);
            
            is_external = false(rr,1);
            is_biomass  = false(rr,1);
            ids         = cell(rr,1);
            eqs         = cell(rr,1);
            cons        = zeros(rr,2);
            
            for i=1:rr
                is_external(i) = rxns{i}.external > 0;
                is_biomass(i)  = rxns{i}.biomass > 0;
                
                for j=1:rxns{i}.size
                    idx = mfindstr(this.hidden_metabolites, [this.DEL rxns{i}.reactants{j}.id this.DEL]);
                    coef = rxns{i}.reactants{j}.stoichiometry;
                    if coef < 0
                        Sneg(idx, i) = coef;
                    else
                        Spos(idx, i) = coef;
                    end
                end
                cons(i,:) = [rxns{i}.lb rxns{i}.ub];
                eqs{i}    = rxns{i}.equation;
                ids{i}    = rxns{i}.id;
            end
            
            is_internal = ~or(is_external, is_biomass);
            
            has_gene = [m1.stoich.has_gene; m2.stoich.has_gene];
            % create stoichiometric matrix
            this.stoich = MnXStoich(Sneg, Spos, cons, is_internal, is_external, is_biomass, this.metabolites, ids, has_gene);
            %%%%%
            %%%%% MADE_CHANGE
            %%%%%
            this = addReactions2(this, rxns, is_internal, is_external, is_biomass);
        end
        
        % create index for species (mapping between stoich and species)
        function this = createSpeciesIndex(this)
            this.metabolites2species = zeros(this.stoich.m,1);
            cell_mets = cell(length(this.stoich.m),1);
            mets = this.stoich.getMetaboliteIds();
            for i=1:this.stoich.m
                t = regexp(mets{i},'(\S+)@','tokens');
                cell_mets{i} = [this.DEL t{:}{:} this.DEL];
            end
            cell_mets = char(cell_mets{:});
            for i=1:length(this.species)
                idx = mfindstr(cell_mets, [this.DEL this.species{i}.id this.DEL]);
                if idx > 0
                    this.metabolites2species(idx) = i;
                end
            end
        end
        
        function this = addReactions(this, m1, m2)
            this.reactions = [m1.reactions;m2.reactions];
        end
        
        function this = addReactions2(this, rxns, is_internal, is_external, is_biomass)
            this.reactions = [rxns(is_internal);rxns(is_external);rxns(is_biomass)];
        end
        
        function this = mergeReactions(this, R1, R2)
            % R1, R2 correspond to the to-be merged reactions
            Rfields = {'id','equation','source','proteins','alternatives','compartment','ec_number','operator','lb','ub','name','references','biomass','external'};
            flag = {};
            
            for i=1:14
                if DataHash(R1.(Rfields(i)))==DataHash(R2.(Rfields(i)))
                    attr{i} = R1.(Rfields(i));
                end
            end
            
            if any(cellfun(@isempty,attr))
                %id
                if isempty(attr{1})
                    attr{1} = [R1.id ';' R2.id];
                end
                %equation
                if isempty(attr{2})
                    attr{2} = 'id'
                end
            end
            attr{1} = [R1.id ';' R2.id];
            R_fields = setdiff(fields(R1),'id');
            NoMatch = {};
            for i=R_fields'
                if DataHash(R1.(i)) == DataHash(R2.(i))
                    this.(i) = R1.(i);
                else
                    NoMatch = [NoMatch i];
                end
            end
            if ~isempty(NoMatch)
                print
                % Print already merged fields
                % Print not yet filled in fields
            end
                
            this.SrcRxns = {R1,R2};
            %print identical fields
            %print unidentical fields
            %process unidentical field - allow cancelling

        end
        
    end
    
    methods(Static)
        
        function stoichObj = minimizeByModel(model_db)
            % copy
%             stoichMatrix = MnXStoich.copy(model_db.stoich);
            PosInt = model_db.startPosInt(2);
            PosExt = model_db.startPosExt(2);
            PosMue = model_db.startPosMue(2);
            PosMet = model_db.startPosMet(2);
            
            % index of internal reactions to keep
            int_keep = false(1,model_db.stoich.int);
            int_keep(PosInt:end) = true;
            
            % index of external reactions to keep
            ext_keep = false(1,model_db.stoich.ext);
            ext_keep(PosExt:end) = true;
            
            % index of biomass reactions to keep
            mue_keep = false(1,model_db.stoich.mue);
            mue_keep(PosMue:end) = true;
            
            % index of metabolites to keep
            met_keep = false(1,model_db.stoich.m);
            met_keep(1:PosMet-1) = true;
            
            stoichObj = MnXStoichConcatenate.toKeepByIndices(model_db.stoich, met_keep, int_keep, ext_keep, mue_keep);
        end
    
        function newObj = toKeepByIndices(stoich, met_keep, int_keep, ext_keep, mue_keep)
            rxnToKeep = [int_keep ext_keep mue_keep];
            
            newObj.S = stoich.S(met_keep,int_keep);
            newObj.E = stoich.E(met_keep,ext_keep);
            newObj.M = stoich.M(met_keep,mue_keep);
            
            newObj.int = sum(int_keep);
            newObj.ext = sum(ext_keep);
            newObj.mue = sum(mue_keep);
            
            newObj.cons_int = stoich.cons_int(int_keep,:);
            newObj.cons_ext = stoich.cons_ext(ext_keep,:);
            newObj.cons_mue = stoich.cons_mue(mue_keep,:);
            
            newObj.reactions_int = stoich.reactions_int(int_keep);
            newObj.reactions_ext = stoich.reactions_ext(ext_keep);
            newObj.reactions_mue = stoich.reactions_mue(mue_keep);
            
            newObj.mue_reversed = stoich.mue_reversed(mue_keep);
            
            newObj.m = sum(met_keep);
            newObj.r = newObj.int + newObj.ext + newObj.mue;
            
            newObj.reaction_ids = stoich.reaction_ids(rxnToKeep,:);
            newObj.metabolite_ids = stoich.metabolite_ids(met_keep,:);
            
            newObj.revs = stoich.revs(rxnToKeep);
            newObj.c = [];
            newObj.compartments = [];
            
            newObj.compartment_idx = stoich.compartment_idx(met_keep);
            idx = unique_no_sort(newObj.compartment_idx);
            newObj.c = length(idx);
            newObj.compartments = stoich.compartments(idx,:);
        end
        
        function newObj = toKeepByMetabolites(stoich, met_keep, int_keep)
            rxnToKeep = true(1,stoich.r);
            rxnToKeep(~int_keep) = false;
            
            newObj.S = stoich.S(met_keep,int_keep);
            newObj.E = stoich.E(met_keep,:);
            newObj.M = stoich.M(met_keep,:);
            
            newObj.int = sum(int_keep);
            newObj.ext = stoich.ext;
            newObj.mue = stoich.mue;
            
            newObj.cons_int = stoich.cons_int(int_keep,:);
            newObj.cons_ext = stoich.cons_ext;
            newObj.cons_mue = stoich.cons_mue;
            
            newObj.reactions_int = stoich.reactions_int(int_keep);
            newObj.reactions_ext = stoich.reactions_ext;
            newObj.reactions_mue = stoich.reactions_mue;
            
            newObj.mue_reversed = stoich.mue_reversed;
            newObj.m = sum(met_keep);
            newObj.r = newObj.int + newObj.ext + newObj.mue;
            
            newObj.reaction_ids = stoich.reaction_ids(rxnToKeep);
            newObj.metabolite_ids = stoich.metabolite_ids(met_keep,:);
            
            newObj.revs = stoich.revs(rxnToKeep);
            newObj.c = [];
            newObj.compartments = [];
            
            newObj.compartment_idx = stoich.compartment_idx(met_keep);
            idx = unique_no_sort(newObj.compartment_idx);
            newObj.c = length(idx);
            newObj.compartments = stoich.compartments(idx,:);
        end
        
    end
    
end

