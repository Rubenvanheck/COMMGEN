classdef MnXLocus
    %MNXLOCUS Summary of this class goes here
    %   Detailed explanation goes here
    properties (GetAccess = private)
        DEL   = '%';
        IdOR  = ';';
        IdAND = '+';
    end
    
    properties
%         loci
        l
        unique_loci
        unique_loci_id
        ko_matrix
        ko_reaction_index
        ko_identity_matrix
        possible_ko
        possible_ko_simple
    end
    
    methods
        function this = MnXLocus(loci, unique_loci)
%             this.loci = loci;
            this.unique_loci = unique_loci;
            this.l    = length(unique_loci);
            
            this.unique_loci_id = cell(this.l,1);
            for i=1:this.l
                this.unique_loci_id{i} = [this.DEL this.unique_loci{i} this.DEL];
            end
            this.unique_loci_id = char(this.unique_loci_id{:});
            
            this = createReactionLocusKnockoutAssociation(this, loci);
        end
        
        function this = createReactionLocusKnockoutAssociation(this, loci)
            NL   = this.l;       % # unique loci
            NR   = length(loci); % # reactions
%             NR   = length(this.loci); % # reactions
            
            if NL > 0
                
                KOM = zeros(NR,NL);  % matrix for effects of knockouts
                KOR = zeros(NR,1);   % index reactions for KO-Matrix
                
                zko = 1; % counter rows KO-Matrix
                
                for z = 1:NR, % loop: all reactions
                    
                    rl = loci{z};
%                     rl = this.loci{z};
                    
                    if ~isempty(rl)
                        
                        % string formatting, default operators
%                         rl = regexprep(rl, '\d+\*','');
                        
                        % get arguments; narg = number of isoenzymes for rx.
                        [args,narg] = srgetargs(rl,this.IdOR);
                        if ~narg && ~isempty(rl),
                            args{1} = rl;
                            narg    = 1;
                        end
                        
                        for zl = 1:narg, % loop: all locus arguments
                            s = strrep(args{zl},' ','');
                            
                            [aargs,anarg] = srgetargs(s,this.IdAND); % multi-gene reactions
                            
                            if ~anarg && ~isempty(s),
                                aargs{1} = s;
                                anarg    = 1;
                            end
                            
                            for za = 1:anarg,
                                % search locus list
                                idx = mfindstr(this.unique_loci_id,[this.DEL aargs{za} this.DEL]);
                                if ~idx, % check gene list
                                    aargs{za}
                                    idx = mfindstr(this.unique_loci_id,[this.DEL aargs{za} this.DEL]);
                                    if ~idx,
                                        fprintf('Undefined locus <%s> in reaction %i!\n',aargs{za},z);
                                    end
                                end
                                
                                if idx
                                    KOM(zko,idx) = 1;
                                end
                                
                            end
                            KOR(zko) = z; % index for reaction
                            zko      = zko+1;
                        end
                    end
                end
                
                % identify loci equivalence
                KOI = zeros(NL,NL);
                for z1 = 1:NL,
                    for z2 = z1+1:NL,
                        if ((KOM(:,z1)>0)==(KOM(:,z2))>0),
                            KOI(z1,z2)=1;
                        end
                    end
                end
                
                idx = KOR>0;
                this.ko_matrix          = KOM(idx,:);
                this.ko_reaction_index  = KOR(idx,:);
                this.ko_identity_matrix = KOI;
            end
        end
    end
end

