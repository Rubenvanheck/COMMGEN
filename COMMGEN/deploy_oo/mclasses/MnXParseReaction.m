classdef MnXParseReaction < handle
    %MNXPARSEREACTION Summary of this class goes here
    %   Detailed explanation goes here
    properties (GetAccess = private)
        % fiel delimiter
        TAB = sprintf('\t');
        
        % Argument indices
        IRId     = 1;  % reaction identifier
        IREq     = 2;  % reaction equation
        IRSource = 3;  % source
        IProts   = 4;  % proteins
        IRxn     = 5;  % reaction
        IComp    = 6;  % compartment
        IEC      = 7;  % EC number
        IOp      = 8;  % operator
        ILow     = 9;  % lower_bound
        IUpp     = 10; % upper_bound
        IRName   = 11; % name
        IRefs    = 12; % Xrefs
        IMue     = 13; % entry indicating the biomass equation (0 or 1)
        IExt     = 14; % column indicating the external reactions (0 or 1)
    end
    
    properties
        id
        name
        eq_string
        eq
        cons
        dir
        external
        biomass
    end
    
    methods
        function this = MnXParseReaction(compartments, line)
            [args,~]       = srgetargs(line,this.TAB);
            this.id        = args{this.IRId};
            this.eq_string = args{this.IREq};
            this.eq        = MnXParseReactionEquation(compartments, args{this.IREq});
            this.cons      = [str2double(args{this.ILow}) str2double(args{this.IUpp})];
            this.dir       = sign(this.cons(1)) + sign(this.cons(2));
            this.name      = args{this.IRName};
            this.external  = str2double(args{this.IExt});
            this.biomass   = str2double(args{this.IMue});
        end
    end
    
end

