classdef MnXConstraints
    %MNXCONSTRAINTS Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        id
        lb
        ub
    end
    
    methods
        function this = MnXConstraints(varargin)
            if nargin == 1
                arguments = varargin{1};
                this.id = arguments{1};
                this.lb = str2double(arguments{2});
                this.ub = str2double(arguments{3});
            elseif nargin == 3
                this.id = varargin{1};
                this.lb = str2double(varargin{2});
                this.ub = str2double(varargin{3});
            else
                error('Wrong input format!\n');
            end
        end
    end
    
end

