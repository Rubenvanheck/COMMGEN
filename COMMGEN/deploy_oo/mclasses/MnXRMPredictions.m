classdef MnXRMPredictions
    %MNXRMPREDICTIONS Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (GetAccess = private)
        minimax     = 1; % use minimax estimator counts
        dir_minimax = 0; % distribute minimax estimator counts according their direction distributions
        rescale     = 1; % rescale motif counts according their size
        r                % number of predictions
        option_direction  = 'direction';
        option_gapfilling = 'gapfilling';
    end
    
    properties (Constant)
        dir_cutoffs = [
            NaN     % reaction size 1 (not to be predicted)
            1.39    % reaction size 2
            0.5
            0.75
            0.28
            0.285
            0.91
            0.105
            0.025
            0.23
            0.36
            0.3
            0.04    % reaction size 13]
            ];
    end
    
    properties
        FSigned     = 0; % signed/unsigned reaction motifs
        minRSize = 2;    % min reaction size
        maxRSize = 13;   % max reaction size    
        ids
        p_dist
        ratios
        dir
        gap
        sizes
        correl
        scale
    end
    
    methods
        function this = MnXRMPredictions(model, db, option, varargin)
            
            if strcmp(this.option_direction, option)
                if nargin > 3
                    this.maxRSize = varargin{1};
                    if nargin > 4
                        this.FSigned = varargin{2};
                        if nargin > 5
                            this.minimax = varargin{3};
                            if nargin > 6
                                this.dir_minimax = varargin{4};
                            end
                        end
                    end
                end
            
                % concatenate model and db (i.e. model is not part of the database)
                % and remove all entitites from the db-stoich, that are NOT
                % contained in the model
                this = predictReactionDirections(this, model, db);
            end
            
            if strcmp(this.option_gapfilling, option)
                if nargin > 3
                    cand = varargin{1};
                    if nargin > 4
                        this.maxRSize = varargin{2};
                        if nargin > 5
                            this.FSigned = varargin{3};
                            if nargin > 6
                                this.minimax = varargin{4};
                                if nargin > 7
                                    this.dir_minimax = varargin{5};
                                end
                            end
                        end
                    end
                end
                
                % concatenate candidate set and model (i.e. cand is not part of the database)
                % and remove all entitites from the model-stoich, that are NOT
                % contained in candidate set
                % redo for candidate set and database
                
                % scaling factor to account for different sizes of model
                % and database (i.e. ratio of internal reactions)
                this.scale = db.stoich.int/model.stoich.int;
                this = gapFilling(this, model, db, cand);
            end
            
        end
        
        function this = predictReactionDirections(this, model, db)
            % concatenate
            model_db   = MnXStoichConcatenate(model, db);
            
            % remove entries
            reduced_db = MnXStoichConcatenate.minimizeByModel(model_db);
            %             reduced_db = MnXStoichConcatenate.minimizeByModel2(model_db);
            
            % resort reduced_db metabolite entries according to model
            % metabolite order
            reduced_db = this.resortMetabolitesAccordingCompartmentList(model, reduced_db);
            
            dbRM = MnXRMDatabase(reduced_db, this.FSigned);
            
            S=model.stoich.S;
            if this.FSigned > 0
                S      = model.stoich.S;
            end
            
            this.r = model.stoich.int;
            
            this.ids    = cell(this.r,1);
            this.ratios = ones(this.r,1) * NaN;
            this.dir    = ones(this.r,1) * NaN;
            this.p_dist = cell(this.r,1);
            this.sizes  = ones(this.r,1) * NaN;
            tStart = tic;
            for i=1:this.r
%                 if mod(i,50)==0
%                     fprintf('.');
%                 end
                % reaction size
                pos           = find(S(:,i));
                coef          = S(pos,i);
                rSize         = length(pos);
                this.ids{i}   = model.stoich.reactions_int{i};
                this.sizes(i) = rSize;
                
                if rSize >= this.minRSize && rSize <= this.maxRSize
                    
                    % reduced motif count table
                    [ spr motifs motifLen mCount ] = dbRM.getMotifCountsForReaction(pos, coef, 0);
                    
                    %
                    % if, there are any counts, then continue, else not possible to
                    % predict
                    if any(any(spr)) > 0
                        % minimax for motif count
                        if this.minimax > 0
                            a = this.fMotifEstimator(spr, motifLen, mCount);
                        else
                            a = zeros(mCount, 1);
                        end
                        
                        % minimax for reaction direction
                        if this.dir_minimax > 0
                            spr = this.fMotifDirectionEstimator(spr, a);
                        else
                            % or distribute evenly
                            spr = spr + repmat(a/3,1,3);
                        end
                        
                        if this.rescale > 0
                            spr = this.rescaleMotifCounts(rSize, spr, motifLen);
                        end
                        
                        % run prediction
                        D = this.fhmmFast(rSize, spr, motifs, motifLen);
                        
                        this.p_dist{i} = D(end,:);
                        this.ratios(i) = this.negLogRatio(D, rSize);
                        if rSize >= this.minRSize && rSize <= this.maxRSize
                            this.dir(i)    = this.assignDirection(this.ratios(i), rSize);
                        else
                            this.dir(i) = -sign(this.ratios(i));
                        end
                    else
                        fprintf('# %s (%i): no motif in common with the DB\n', this.ids{i}, i);
                    end
                else
                    fprintf('# %s (%i): Reaction too large: %i reactants\n', this.ids{i}, i, rSize);
                end
            end
            toc(tStart);
        end
        
        function this = gapFilling(this, model, db, cand)
            % concatenate
            cand_model = MnXStoichConcatenate(cand, model);
            cand_db    = MnXStoichConcatenate(cand, db);
            % remove entries
            reduced_model = MnXStoichConcatenate.minimizeByModel(cand_model);
            reduced_db    = MnXStoichConcatenate.minimizeByModel(cand_db);
            
            % resort reduced_db metabolite entries according to candidate
            % metabolite order
            reduced_model = this.resortMetabolitesAccordingCompartmentList(cand, reduced_model);
            reduced_db    = this.resortMetabolitesAccordingCompartmentList(cand, reduced_db);
            
            modelRM = MnXRMDatabase(reduced_model, this.FSigned);
            dbRM    = MnXRMDatabase(reduced_db, this.FSigned);
            
            S      = cand.stoich.S;
            if this.FSigned> 0
                S = sign(S);
            end
            this.r = cand.stoich.int;
            
            this.ids    = cell(this.r,1);
            this.ratios = ones(this.r,1) * NaN;
            this.gap    = ones(this.r,1) * NaN;
            this.p_dist = cell(this.r,1);
            this.sizes  = ones(this.r,1) * NaN;
%             tStart = tic;
            
            % loop over all candidate reactions
            for i=1:this.r
                % reaction size
                pos           = find(S(:,i));
                coef          = S(pos,i);
                rSize         = length(pos);
                this.ids{i}   = cand.stoich.reactions_int{i};
                this.sizes(i) = rSize;
                
                if rSize >= this.minRSize && rSize <= this.maxRSize
                    
                    % reduced motif count table
                    [ spr, motifs, motifLen, ~] = modelRM.getMotifCountsForReaction(pos, coef, 0);
                    
                    [ dbspr, ~, ~, ~ ] = dbRM.getMotifCountsForReaction(pos, coef, 0);
                    
                    % check if there are any motifs in the model or the db
                    if isempty(spr) && isempty(dbspr)
                        continue;
                    end
                    
                    if isempty(spr)
                        continue;
                    end
                    
                    if isempty(dbspr)
                        continue;
                    end
                    
                    % check if all motifs of size one are present in the model
                    tmp_spr     = sum(spr,2);
                    all_present_model = all(tmp_spr(motifLen==1));%all(transition_counts(motifLen==1));
                    
                    if all_present_model == 0
                        continue;
                    end
                    
                    % check if all motifs of size one are present in the
                    % database
                    tmp_dbspr = sum(dbspr,2);
                    all_present_db = all(tmp_dbspr(motifLen==1));
                        
                    if all_present_db == 0
                        continue;
                    end
                    
                    % scale motif counts of db by 'dbScale'
                    dbspr = dbspr./this.scale;
                    
                    % fuse emission counts: model, non, db-scaled
                    emission_counts   = [sum(dbspr,2) zeros(length(motifLen),1) sum(spr,2)];
%                     emission_counts   = [sum(spr,2) zeros(length(motifLen),1) sum(dbspr,2)];
                    transition_counts = sum(spr,2);
                    
                    % rescale transition counts
                    if this.rescale > 0
                        transition_counts = this.rescaleMotifCounts(rSize, transition_counts, motifLen);
                    end
                    
                    % run prediction
                    D = this.fhmmFastGF(rSize, transition_counts, emission_counts, motifs, motifLen);
                    this.p_dist{i} = D(end,:);
                    this.gap(i) = this.logRatio(D, rSize);
%                     val    = this.logRatio(D, rSize);
%                     if abs(val) < 10^-15
%                         this.gap(i) = 0;
%                     else
%                         this.gap(i) = val;
%                     end
                end
            end
%             toc(tStart);
        end
    end
    
    methods (Static)
        function reduced_db = resortMetabolitesAccordingCompartmentList(model, reduced_db)
            I = zeros(reduced_db.m,1);
            for i=1:reduced_db.m
                pos = mfindstr(model.stoich.metabolite_ids, reduced_db.metabolite_ids(i,:));
                I(pos) = i;
            end
            
            reduced_db.S = reduced_db.S(I,:);
            reduced_db.E = reduced_db.E(I,:);
            reduced_db.M = reduced_db.M(I,:);
            reduced_db.metabolite_ids = reduced_db.metabolite_ids(I,:);
            reduced_db.compartment_idx = reduced_db.compartment_idx(I);
        end
        
        function a = fMotifEstimator(spr, motifLen, motifCount)
            % FMOTIFDIRECTIONESTIMATOR
            % Minimax estimator for motif given the motif count table
            %
            % Input:
            %   spr         motif count table for the considered reaction
            %   motifLen    length of motifs
            %   motifCount  the total number of motifs
            %
            % Output:
            %   spr2        updated motif count table for the considered reaction
            
            
            maxLen = max(motifLen);
            [s N pseudo]  = deal(zeros(maxLen,1));
            n      = sum(spr,2);
            a      = zeros(size(spr,1),1);
            
            warning('off', 'MATLAB:nchoosek:LargeCoefficient');
            for j = 1:maxLen
                s(j)      = sum(motifLen==j);
                N(j)      = sum(n(motifLen==j));
                pseudo(j) = sqrt(N(j))/s(j);
            end
            warning('on', 'MATLAB:nchoosek:LargeCoefficient');
            for j = 1:motifCount
                l = motifLen(j);
                if l > 0
                    a(j) = pseudo(l);
                else
                    a(j) = 0;
                end
            end
            
            %     spr2 = spr + repmat(a,1,3);
            
        end
        
        function spr3 = fMotifDirectionEstimator(spr, pseudos)
            % FMOTIFDIRECTIONESTIMATOR
            % Minimax estimator for motif direction
            %
            % Input:
            %   spr         motif count table for the considered reaction
            %   pseudos     motif pseudo counts for
            % Output:
            %   spr2        updated motif count table for the considered reaction
            
            s      = 3;
            n      = sum(spr,2);
            a      = sqrt(n)/s;
            spr2   = repmat(a,1,3) + spr;
            spr2(isnan(spr2)) = 0;
            spr2 = spr2./repmat(sum(spr2,2),1,3);
            spr2(isnan(spr2)) = 0;
            % Distribute motif estimates according their direction estimates
            spr3 = spr + spr2.*repmat(pseudos,1,3);
            % Distribute available direction estimates uniformly for zero motif
            % counts
            lp = pseudos>0;
            ls = sum(spr,2)>0;
            idx = xor(ls,lp);
            spr3(idx,:) = spr3(idx,:) + repmat(pseudos(idx)/3,1,3);
        end
                
        function D = fhmm(rSize, spr, motifs, motifLen)
            % FHMM
            % run hmm analysis
            %
            % Input:
            %   rSize    the reaction size
            %   spr      the motif count table for the considered reactions, i.e. spr
            %            basically reflects the reaction information we have
            %   motifs   the motifs (encoded binary)
            %   motifLen the length of the motifs
            %
            % Output:
            %   D        the direction distribution for [-rSize ... 0 ... rSize]
            
            mCount = 2^rSize; % including the empty motif
            cutOff = rSize + 1;
            
            % set up matrices
            % transition matrix & motif extention matrix
            direction = 1;
            [P Ext]     = MnXRMPredictions.transitionMatrix(sum(spr,2), motifs, mCount-1, motifLen, direction); % w/o self-loops
            
            % emission matrix
            Q           = MnXRMPredictions.emissionMatrix(spr, mCount-1, motifLen, rSize);
            Q(isnan(Q)) = 0;
            
            % initial state direction matrix
            H0 = zeros(mCount, 2*rSize+1);
            H0(1,rSize+1) = 1;
            
            N = rSize;
            D = zeros(N + 1,2*rSize + 1);
            D(1,:) = H0(end,:);
            P(1,1)=0;
            
            % run HMM
            for n = 1:N
                Hn = zeros(size(H0));
                % Get previous H matrix
                H_old = H0;
                % examine H entries (m = motifs)
                for m = 1:mCount
                    % check directions
                    for d=-rSize:rSize
                        extensions = find(Ext(m,:));
                        % check predecessors
                        for e = 1:length(extensions)
                            m2 = extensions(e);
                            % check old directions
                            for d2=-rSize:rSize
                                % emitted direction = new-old
                                emit = d-d2;
                                % if feasable, then
                                if(abs(emit)<=rSize)
                                    Hn(m,d+cutOff) = Hn(m,d+cutOff) + H_old(m2,d2+cutOff) * P(m,m2) * Q(Ext(m,m2),emit+cutOff);
                                end
                            end
                        end
                        % self-loop
                        Hn(m,d+cutOff) = Hn(m,d+cutOff) + H_old(m,d+cutOff) * P(m,m);
                    end
                end
                H0 = Hn;
                D(n+1,:) = Hn(end,:);
            end
        end
        
        function D = fhmmFast(rSize, spr, motifs, motifLen)
            % FHMM
            % run hmm analysis
            %
            % Input:
            %   rSize    the reaction size
            %   spr      the motif count table for the considered reactions, i.e. spr
            %            basically reflects the reaction information we have
            %   motifs   the motifs (encoded binary)
            %   motifLen the length of the motifs
            %
            % Output:
            %   D        the direction distribution for [-rSize ... 0 ... rSize]
            
            mCount = 2^rSize; % including the empty motif
            
            % transition matrix
            [P Ext]    = transitionsMatrix(sum(spr,2), double(motifs), motifLen);
            denom = sum(P,1);
            P = P./repmat(denom,size(P,1),1);
            P(:,end)   = 0;
            P(end,end) = 1;
            P(isnan(P))= 0;
            
            % emission matrix
            Q           = MnXRMPredictions.emissionMatrix(spr, mCount-1, motifLen, rSize);
            Q(isnan(Q)) = 0;
            
            % final distribution
            D = hmm(P,Ext,Q,rSize);
        end
        
        function D = fhmmFastGF(rSize, trans, emis, motifs, motifLen)
            % FHMMFASTGF
            % run hmm analysis for gap-filling
            %
            % Input:
            %   rSize    the reaction size
            %   trans    transition counts
            %   emis     emission counts
            %   motifs   the motifs (encoded binary)
            %   motifLen the length of the motifs
            %
            % Output:
            %   D        the direction distribution for [-rSize ... 0 ... rSize]
            
            mCount = 2^rSize; % including the empty motif
            
            % transition matrix
            [P Ext]    = transitionsMatrix(trans, double(motifs), motifLen);
            denom = sum(P,1);
            P = P./repmat(denom,size(P,1),1);
            P(:,end)   = 0;
            P(end,end) = 1;
            P(isnan(P))= 0;
            
            % emission matrix
            Q           = MnXRMPredictions.emissionMatrix(emis, mCount-1, motifLen, rSize);
            Q(isnan(Q)) = 0;
            
            % final distribution
            D = hmm(P,Ext,Q,rSize);
        end
        
        function [tm EM]= transitionMatrix(spr, motifs, mCount, motifLen, direction)
            % Determine transition matrix w/o self-loops
            
            TM = zeros(mCount+1);
            % Motif size matrix
            LM = zeros(mCount+1);
            
            % Extension matrix
            EM = zeros(mCount+1);
            
            for i=1:mCount+1
                for j=i+1:mCount+1
                    if i==1
                        TM(j,i) = spr(j-1,direction);
                        LM(j,i) = motifLen(j-1);
                        EM(j,i) = j;
                    else
                        if (motifLen(i-1) < motifLen(j-1))
                            from = logical(motifs(i-1,:));
                            to   = logical(motifs(j-1,:));
%                             if any(and(from,to)) && (bin2dec(num2str(and(from, to)))-bin2dec(num2str(from))==0)
%                                 idx = bin2dec(num2str(xor(from, to)));
                            if any(and(from,to)) && (MnXRMPredictions.binVector2dec(and(from, to))-MnXRMPredictions.binVector2dec(from)==0)
                               idx = MnXRMPredictions.binVector2dec(xor(from, to));
%                             if any(and(from,to)) 
%                                 val1 = binVector2dec(double(and(from, to)));
%                                 val2 = binVector2dec(double(from));
%                                 if (val1-val2==0)
%                                     idx = binVector2dec(double(xor(from, to)));
                                    TM(j,i) = spr(idx,direction);
                                    LM(j,i) = motifLen(idx);
                                    EM(j,i) = idx+1; % add one to index to account for larger matrix dimension
%                                 end
                            end
                        end
                    end
                end
            end
            
            tm = [];
            if any(sum(TM,1)) > 0
                denom = sum(TM,1);
                tm = TM./repmat(denom,mCount+1,1);
                tm(:,end)=0;
                tm(end,end)=1;
            end
            return;
        end
        
        function decimal = binVector2dec(binVec)
            % BINVECTOR2DEC
            % Converts a binary number to decimal integer.
            %
            % Input:
            %   binVec      a binary vector
            %
            % Output:
            %   decimal     decimal of the binary vector
            %
            % Usage:
            %   decimal = binVector2dec([0 0 0 1 0 1 0])
            
            len     = length(binVec);
            digits  = find(binVec == 1);
            nums    = (-1*digits) + len;
            twos    = pow2(nums);
            decimal = sum(twos);
            return;
        end
        
        function [EM]= emissionMatrix(spr, mCount, motifLen, rSize)
            % Determine emission matrix
            
            EM = zeros(mCount,2*rSize+1);
            scaled = spr./repmat(sum(spr,2),1,3);
            EM(1,rSize+1) = 1;
            for i=1:mCount
                EM(i+1,rSize - motifLen(i) + 1) = scaled(i,1);
                EM(i+1,rSize + 1) = scaled(i,2);
                EM(i+1,rSize + motifLen(i) + 1) = scaled(i,3);
            end
            return;
        end

        function ratio = logRatio(D, rSize)
            w = warning('off','MATLAB:log:logOfZero');
            ratio = log(sum(D(end,rSize+1:end))/sum(D(end,1:rSize+1)));
            if abs(ratio) < 10^-15
                ratio = 0;
            end
            warning(w);
        end

        function ratio = negLogRatio(D, rSize)
            w = warning('off','MATLAB:log:logOfZero');
            ratio = -log(sum(D(end,rSize+1:end))/sum(D(end,1:rSize+1)));
            if abs(ratio) < 10^-15
                ratio = 0;
            end
            warning(w);
        end
        
        function sprnew = rescaleMotifCounts(rSize, spr, motifLen)
            % Rescale()
            % Rescales the sum of all motifs of a given motif size to 1 (if the
            % considered motif size has any count). This is equivalent to further
            % taking the reaction size into account such that all motif counts sum up
            % to one.
            %
            % Input:
            %   rSize       the reaction size
            %   spr         the reduced motif count table
            %   motifLen    the length of the motifs in the motif count table
            %
            % Output:
            %   sprnew      the rescale motif counts
            %
            counts = zeros(rSize,1);
            for i=1:rSize
                counts(i) = sum(sum(spr(motifLen==i,:)));
            end
            counts = counts(motifLen);
            counts(counts==0)=1;
%             sprnew = spr./repmat(counts,1,3);
            sprnew = spr./repmat(counts,1,size(spr,2));
        end
        
        function direction = assignDirection(ratio, size)
            % assign direction to a log-ratio based on the correspoding
            % reaction size
            cutoff = MnXRMPredictions.dir_cutoffs(size);
            
%             if size > MnXRMPredictions.minRSize && size <= MnXRMPredictions.maxRSize
%                 cutoff = MnXRMPredictions.dir_cutoffs(size);
%             else
%                 cutoff = 0;
%             end
            
            if ratio < -cutoff
                direction = 1;
            elseif ratio > cutoff
                direction = -1;
            else
                direction = 0;
            end
            
        end
    end
    
end

