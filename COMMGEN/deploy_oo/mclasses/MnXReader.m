classdef MnXReader
    %MNXREADER Summary of this class goes here
    %   Detailed explanation goes here
    properties (GetAccess = private)
        TAB = sprintf('\t');
        SC  = '@'; % separator for compartments in metabolites
    end    
    
    properties
        specification;
        compartments;    % compartments
        NumCompartments; % # compartments
        reactions;       % reactions
        NumReactions;    % # reactions
        chemicals;       % MnXChemicalSpecies
        NumChemicals;    % # MnXChemicalSpecies
        c_default  = 'go_0005783'; % default compartment
        c_external = 'go_0005576'; % external compartment
        c_boundary = 'BOUNDARY';   % bounary compartment
    end
    
    methods
        
        function this = MnXReader(varargin)
            
            if nargin >= 1
                % read model specification
                ModelDefinitionFile = varargin{1};
                this = readModelSpecificationFile(this, ModelDefinitionFile);
                
                if nargin >= 2
                    % read compartment specification
                    CompartmentDefinitionFile = varargin{2};
                    this = readCompartmentSpecificationFile(this, CompartmentDefinitionFile);
                    
                    if nargin >= 3
                        % read metabolic reactions
                        MetabolicReactionFile = varargin{3};
                        this = readMetabolicReactions(this, MetabolicReactionFile);
                        
                        if nargin >= 4
                            % read chemical species info
                            ChemicalSpeciesFile = varargin{4};
                            this = readChemicalSpecies(this, ChemicalSpeciesFile);
                        end
                    end
                end
            end
        end
        
        % read model specification (only one line)
        function this = readModelSpecificationFile(this, file)
            fprintf('# Read model file\n');
            data = this.readFile(file);
            [args,~] = srgetargs(data{1},this.TAB);
            this.specification = MnXModelSpecification(args);
            this.specification.dir = strrep(file, 'model.tsv', '');
        end
        
        % read compartment specification
        function this = readCompartmentSpecificationFile(this, file)
            fprintf('# Read compartment file\n');
            data = this.readFile(file);
            this.NumCompartments = length(data);
            this.compartments = cell(this.NumCompartments,1);
            
            for i=1:this.NumCompartments
                this.compartments{i} = parseCompartmentsEntry(this, data{i});
                this = checkCompartmentDescription(this, this.compartments{i});
            end
        end
        
        function comp = parseCompartmentsEntry(this, entry)
            [args,~] = srgetargs(entry,this.TAB);
            comp     = MnXCompartment(args);
        end
        
        function this = checkCompartmentDescription(this, compartment)
            if strcmp(compartment,'boundary')
                this.c_boundary = compartment.id;
            end
            if strcmp(compartment.property, 'default')
                this.c_default = compartment.id;
            end
            
            if strcmp(compartment,'external')
                this.c_external = compartment.id;
            end
        end
        
        % create list of metabolic reactions (MnXMetabolicReactions)
        function this = readMetabolicReactions(this, file)
            fprintf('# Read reaction file\n');
            data = this.readFile(file);
            this.NumReactions = length(data);
            this.reactions = cell(this.NumReactions,1);
            
            for i=1:this.NumReactions
                this.reactions{i} = parseMetabolicReactionEntry(this, data{i});            
            end
            
        end
        
        function reac = parseMetabolicReactionEntry(this, entry)
            % remove boundary metabolites
            pattern = [ ' *\S+\s+\S+' this.SC this.c_boundary];
%             pattern = [ '\s*\S+\s+\S+' this.SC this.c_boundary];
            entry = regexprep(entry, pattern, '');
           	[args,~] = srgetargs(entry,this.TAB);
            % Remove the M_ and _e/_c from unrecognized metabolites from
            % cobra models.
            args(2) = regexprep(args(2),{'M_','_.@','_'},{'','@',''});
            reac     = MnXMetabolicReaction(args);
        end
        
        % create list of chemical species (MnXChemicalSpecies)
        function this = readChemicalSpecies(this, file)
            fprintf('# Read metabolite file\n');
            data   = this.readFile(file);
            this.NumChemicals = length(data);
            this.chemicals    = cell(this.NumChemicals,1);
            
            for i=1:this.NumChemicals
                this.chemicals{i} = parseChemicalSpeciesEntry(this, data{i});
            end
        end
        
        function spec = parseChemicalSpeciesEntry(this, entry)
            [args,~] = srgetargs(entry,this.TAB);
            args(1)  = regexprep(args(1),{'M_','_.$','_'},{'','',''});
            spec     = MnXChemicalSpecies(args);
        end
    end
    
    methods (Static)
        % function to read in data from a file
        function data = readFile(FileName)
            fid   = fopen(FileName, 'r');
            data = textscan(fid, '%s','delimiter','\n');
            fclose(fid);
            data = data{:};
        end
        
    end
end

