classdef MnXModel < handle
    %MNXMODEL Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (GetAccess = private)
%         Sneg
%         Spos
        DEL   = '%';
        NumMetabolites
        metabolites
        hidden_metabolites
        hidden_loci
        is_int
        is_ext
        is_mue
    end
    
    properties (Constant)
        
    end
    
    properties
        id
        dir
        
        % stoich
        stoich
        % loci
        locus
        % LP optimizations
        fba
        % chemical species
        species
        metabolites2species
        
        % metabolic reactions
        reactions % resorted entries according to stoich.reaction_ids
        
        % results for structural analysis
        structuralAnalysis
        
        % results for consistency analysis
        consistency
    end
    
    methods
        function this = MnXModel(MnXReader)
            fprintf('# Create MnXModel\n');
            
            % get global parameters
            this.id  = MnXReader.specification.id;
            this.dir = MnXReader.specification.dir;
            
            % get metabolites based on reactants
            this     = getMetabolites(this, MnXReader);
            
            % create stoichiometric matrix, and resort reactions according
            % to matrix
            this     = createStoichiometricMatrix(this, MnXReader);
            this     = sortReactions(this, MnXReader);
            
            % get loci
            this     = getLoci(this);
            
            % create object for FBA
            this.fba = MnXFBA();
            
            % get chemical species information if chemicals file was passed
            if ~isempty(MnXReader.chemicals)
                this.species = MnXReader.chemicals;
                this     = createSpeciesIndex(this);
            end
            
            % determine possible KOs
            this = possibleKOs(this);
            this = possibleKOsSimple(this);
        end
        
        % read metabolites
        function this = getMetabolites(this, MnXReader)
            list = cell(0);
            cnt = 0;
            for i=1:MnXReader.NumReactions
                for j=1:MnXReader.reactions{i}.size
                    cnt = cnt + 1;
                    list{cnt} = MnXReader.reactions{i}.reactants{j}.id;
                end
            end
            this.metabolites    = unique_no_sort(list);
            this.NumMetabolites = length(this.metabolites);
            
            this.hidden_metabolites = cell(this.NumMetabolites,1);
            for i=1:this.NumMetabolites
                this.hidden_metabolites{i} = [this.DEL this.metabolites{i} this.DEL];
            end
            this.hidden_metabolites = char(this.hidden_metabolites{:});
        end
        
        % create stoichiometric matrix
        function this = createStoichiometricMatrix(this, MnXReader)
%             Sneg = zeros(this.NumMetabolites, MnXReader.NumReactions);
%             Spos = zeros(this.NumMetabolites, MnXReader.NumReactions);
            Sneg = sparse(this.NumMetabolites, MnXReader.NumReactions);
            Spos = sparse(this.NumMetabolites, MnXReader.NumReactions);
            
            is_external = false(MnXReader.NumReactions,1);
            is_biomass  = false(MnXReader.NumReactions,1);
            ids         = cell(MnXReader.NumReactions,1);
            eqs         = cell(MnXReader.NumReactions,1);
            cons        = zeros(MnXReader.NumReactions,2);
            has_gene    = false(MnXReader.NumReactions,1);
            
            for i=1:MnXReader.NumReactions
                is_external(i) = MnXReader.reactions{i}.external > 0;
                is_biomass(i)  = MnXReader.reactions{i}.biomass > 0;
                has_gene(i)    = ~isempty(MnXReader.reactions{i}.proteins);
                for j=1:MnXReader.reactions{i}.size
                    idx  = mfindstr(this.hidden_metabolites, [this.DEL MnXReader.reactions{i}.reactants{j}.id this.DEL]);
                    coef = MnXReader.reactions{i}.reactants{j}.stoichiometry;
                    if coef < 0
                        Sneg(idx, i) = coef;
                    else 
                        Spos(idx, i) = coef;
                    end
                end
                cons(i,:) = [MnXReader.reactions{i}.lb MnXReader.reactions{i}.ub];
                eqs{i}    = MnXReader.reactions{i}.equation;
                ids{i}    = MnXReader.reactions{i}.id;
            end
            
            % TODO: HOW TO HANDLE MORE THAN 1 BIOMASS REACTION
            
            is_internal = ~or(is_external, is_biomass);
            
            this.stoich = MnXStoich(Sneg, Spos, cons, is_internal, is_external, is_biomass, this.metabolites, ids, has_gene);
%             this.stoich = MnXStoich(S, E, M, cons_int, cons_ext, cons_mue, mue_reversed, reactions_int, reactions_ext, reactions_mue, this.metabolites);
        end
        
        % get loci and loci-reaction association for internal reactions
        % only
        function this = getLoci(this)
            rr = length(this.reactions);
            list = cell(0);
            ll   = cell(rr,1);
            cnt = 0;
            for i=1:rr
                str = this.reactions{i}.proteins;
                % remove whitespace
                str = regexprep(str, '\s+', '');
                ll{i} = str;
                if ~isempty(str)
                    str = regexprep(str, '+', ';');
                    tmp = regexp(str, ';', 'split');
                    for j=1:length(tmp)
                        cnt = cnt + 1;
                        list{cnt} = tmp{j};
                    end
                end
            end
            loci          = ll;
            unique_loci   = unique_no_sort(list);
            
            this.locus = MnXLocus(loci, unique_loci);
        end
        
        % resort reactions according the ordering of the stoichiometric
        % matrix
        function this = sortReactions(this, MnXReader)
            rxns = cell(MnXReader.NumReactions,1);
            for i=1:MnXReader.NumReactions
                idx = mfindstr(this.stoich.reaction_ids, [this.DEL MnXReader.reactions{i}.id this.DEL]);
                if length(idx) == 1 && idx > 0
                    rxns{idx} = MnXReader.reactions{i};
                else
                    for j=1:length(idx)
                        rxns{idx(j)} = MnXReader.reactions{i};
                        fprintf('# Several matches for %s\n', MnXReader.reactions{i}.id);
                    end
                end
            end
            this.reactions = rxns;
        end
        
        % create index for species (mapping between stoich and species)
        function this = createSpeciesIndex(this)
            this.metabolites2species = zeros(this.stoich.m,1);
            cell_mets = cell(length(this.stoich.m),1);
            mets = this.stoich.getMetaboliteIds();
            for i=1:this.stoich.m
                t = regexp(mets{i},'(\S+)@','tokens');
                cell_mets{i} = [this.DEL t{:}{:} this.DEL];
            end
            cell_mets = char(cell_mets{:});
            for i=1:length(this.species)
                idx = mfindstr(cell_mets, [this.DEL this.species{i}.id this.DEL]);
                if idx > 0
                    this.metabolites2species(idx) = i;
                end
            end
        end
        
        % determine list of possible single-gene reaction knockouts
        function this = possibleKOs(this)
            KO      = this.locus.ko_matrix;
            rko_idx = this.locus.ko_reaction_index;
            NL      = this.locus.l;
            NR      = this.stoich.r;
            
            % define output matrix (every non-zero column allows for single-gene knockouts)
            KO_possible = false(NR, NL);
            
            for i=1:NL
                % get all reactions affected by gene i
                idx       = KO(:,i);
                rest      = KO;
                rest(:,i) = [];
                % get all reactions affected by all other genes but gene i
                rest      = sum(rest,2);
                % reduce to the non-affected reactions
                idx2 = rest == sum(KO,2);
                % get the affected reactions, i.e. entries in A
                % that are not in B using setdiff(A, B)
                KO_possible(setdiff(unique(rko_idx(idx>0)),unique(rko_idx(idx2))),i)=true;
            end
            this.locus.possible_ko = KO_possible;
        end
        
        % determine list of possible single-gene reaction knockouts
        function this = possibleKOsSimple(this)
            KO      = this.locus.ko_matrix;
            rko_idx = this.locus.ko_reaction_index;
            NL      = this.locus.l;
            NR      = this.stoich.r;
            
            % define output matrix (every non-zero column allows for single-gene knockouts)
            KO_possible = false(NR, NL);
            
            for i=1:NL
                % get all reactions affected by gene i
                idx       = KO(:,i);
                KO_possible(idx>0,i)=true;
            end
            this.locus.possible_ko_simple = KO_possible;
        end
        
        % deprecated
%         function this = possibleKOs(this)
%             % get values
%             KO      = this.locus.ko_matrix;
%             rko_idx = this.locus.ko_reaction_index;
%             NL      = this.locus.l;
%             NR      = this.stoich.r;
%             
%             % define output matrix (every non-zero column allows for single-gene knockouts)
%             KO_possible = false(NR, NL);
%             
%             % check all genes individually
%             for i=1:NL
%                 % get all reactions for a given gene
%                 idx = KO(:,i);
%                 % now, test for each reaction for isoenzymes
%                 rxns   = rko_idx(idx>0);
%                 u_rxns = unique(rxns);
%                 for j=1:length(u_rxns)
%                     idx2 = rxns == u_rxns(j);
%                     if sum(idx2) == 1
%                         % only one reaction
%                         % ko possible
% %                         fprintf('# POSSIBLE KO for reaction %i and gene %i\n', u_rxns(j), i);
%                         KO_possible(u_rxns(j), i) = true;
%                     else
%                         % more than one reaction
%                         
%                         % now test, if gene is present in all isoenzymes of a the
%                         % considered reaction
% %                         fprintf('# Checking for isoenzymes in reaction %i and for gene %i\n', u_rxns(j), i);
%                         
%                         if sum(rko_idx==u_rxns(j))==sum(idx2)
%                             %fprintf('#POSSIBLE KO: Gene %i is present in all isoenzyme-complexes for reaction %i\n', i, u_rxns(j));
%                             KO_possible(u_rxns(j), i) = true;
% %                         else
% %                             fprintf('##IMPOSSIBLE KO: Gene %i is NOT present in all isoenzyme-complexes for reaction %i\n', i, u_rxns(j));
%                         end
%                     end
%                 end
%             end
%             this.locus.possible_ko = KO_possible;
%         end
        
%         % add an artificial transport reaction to an existing model; TODO:
%         % make more portable
%         function this = addInternalReaction(this, reaction)
%             
%         end
%         
%         % add an artificial transport reaction to an existing model; TODO:
%         % make more portable
%         function this = addExternalReaction(this, reaction)
%         
%         end
    end
    
end

