classdef MnXModelStructuralAnalysis < handle
    %MNXMODELSTRUCTURALANALYSIS Summary of this class goes here
    %   Detailed explanation goes here
    %   short file -> full file in MnXModelAnalysis
    
    properties (GetAccess = private)
        option_deadendsrec  = 'deadendsrec';
        option_deadends     = 'deadends';
        option_balanced     = 'balanced';
        option_nullspace    = 'nullspace';
    end
    
    properties
        deadEnds
        deadEndsRec
        balanced_reactions
        is_balanced
        kernel
        correlations
        inconsistencies
    end
    
    methods
        function this = MnXModelStructuralAnalysis(model, varargin)%, option, varargin)
            
            if nargin == 2
                option = varargin{1};
                if strcmp(option, this.option_deadends)
                    flag = false;
                    this = MnXModelDeadEnds(this, model, flag);
                    model.structuralAnalysis.deadEnds = this.deadEnds;
                elseif strcmp(option, this.option_deadendsrec)
                    flag = true;
                    this = MnXModelDeadEnds(this, model, flag);
                    model.structuralAnalysis.deadEndsRec = this.deadEndsRec;
                elseif strcmp(option, this.option_balanced)
                    this = MnXElementalBalancedReactions(this, model);
                    model.structuralAnalysis.balanced_reactions = this.balanced_reactions;
                    model.structuralAnalysis.is_balanced = this.is_balanced;
                elseif strcmp(option, this.option_nullspace)
                    this = MnXNullSpaceAnalysis(this, model);
                    model.structuralAnalysis.kernel = this.kernel;
                    model.structuralAnalysis.correlations = this.correlations;
                    model.structuralAnalysis.inconsistencies = this.inconsistencies;
                end
            end
        end
                        
        function this = MnXModelDeadEnds(this, model, varargin)
            % MnXModelDeadEnds
            % This script uses the stoichiometric matrix to determine all "dead
            % ends" of the model structure including an optional recursive call to
            % eliminate all reactions that are never able to carry a flux because they
            % are connected to a dead-end reaction/metabolite.
            % Note, one also has to check for reversible reactions to identify all real
            % dead-ends.
            %
            % Input arguments:
            %   model           handle to model structure
            %   option          recursive call if true (default: true)
            % Output arguments:
            %   iM              the indices of dead end metabolites
            %   iR              the indices of internal dead end reactions
            %   iB              is the biomass function a dead end?
            
            
            stoich  = model.stoich;
            option  = true;
            flag    = true;
            
            S = stoich.S;
            E = stoich.E;
            M = stoich.M;
            m = stoich.m;
            r = stoich.int;
            mue = stoich.mue;
            
            iR_old = zeros(1,r);
            iM_old = zeros(m,1);
            
            % reversibilities
            cons = [stoich.cons_int; stoich.cons_mue];
            % bw: -1; bi: 0; fw: 1
            revs = (sign(cons(:,2))+sign(cons(:,1)))';
            bi   = revs == 0;
            
%             bw  = revs ==-1;
            % backward internal reactions
            bwi = revs(1:r)==-1;
            S(:,bwi) = -1*S(:,bwi);
            
            % backward external reactions
            if ~isempty(E)
                cons_e = stoich.cons_ext;
                revs_e = (sign(cons_e(:,2))+sign(cons_e(:,1)))';
                bwe = revs_e==-1;
                E(:,bwe) = -1*E(:,bwe);
            end
            
            % backward biomass reaction
            if ~isempty(M)
                cons_m = stoich.cons_mue;
                revs_m = (sign(cons_m(:,2))+sign(cons_m(:,1)))';
                bwm = revs_m==-1;
                M(:,bwm) = -1*M(:,bwm);
            end
                        
            while option
                % participation number of internal metabolites must be at least 2
                % otherwise, there will be a dead end
                if ~isempty(E)
                    BEM  = logical(E);
                    EM   = sum(BEM,2)~=0;
                    
                    BSMi = logical([S M]<0);
                    BSMe = logical([S M]>0);
                    Mi   = sum(BSMi,2)>0;
                    Me   = sum(BSMe,2)>0;
                    
                    % vM contains all valid internal metabolites (vM)
                    vM = and(Mi, Me);
                    
                    % iM contains all invalid metabolites that are NOT external
                    iM = and(~vM, ~EM);
                    
                else
                    
                    BSMi = logical([S M]<0);
                    BSMe = logical([S M]>0);
                    Mi   = sum(BSMi,2)>0;
                    Me   = sum(BSMe,2)>0;
                    
                    % vM contains all valid internal metabolites (vM)
                    vM = and(Mi, Me);
                    
                    % iM contains all invalid metabolites that are NOT external
                    iM = ~vM;
                end
                
                
                % correct entries in iM that are in at least one bidirectional and
                % one unidirectional reaction
                BSM = logical([S M]);
                i   = sum(BSM,2)>1;
                iB  = and(sum(BSM(:,bi),2)>0,i);
                iM(iB) = false;
                
                % identify internal dead end reactions
                % and check if the biomass reaction is a dead end
                if ~isempty(M)
                    iR = sum(BSM(iM,1:end-mue),1)>0;
                    iB = find(and(sum(BSM(:,(end-mue+1):end),2)>0,iM));
                else
                    iR = sum(BSM(iM,1:end),1)>0;
                    iB = [];
                end
                
                if isempty(iB)
                    iB = 0;
                end
                
                % recursive call
                if nargin == 3
                    option = varargin{1};
                    flag   = option;
                end
                
                % if no further reactions are identified as dead ends, then stop
                if sum(iR) == 0
                    option  = false;
                else
                    iM_old   = or(iM, iM_old);
                    iR_old   = or(iR, iR_old);
                    % remove internal dead end reactions
                    S(:,iR_old) = zeros(m,sum(iR_old));
                end
            end
            
            iR = iR_old;
            iM = iM_old;
            
%             if flag == 0
%                 model.structuralAnalysis.deadEnds    = MnXDeadEnds(iM, iR, iB);
%             else
%                 model.structuralAnalysis.deadEndsRec = MnXDeadEnds(iM, iR, iB);
%             end
            if flag == 0
                this.deadEnds    = MnXDeadEnds(iM, iR, iB);
            else
                this.deadEndsRec = MnXDeadEnds(iM, iR, iB);
            end
        end
       
        function this = MnXElementalBalancedReactions(this, model)
            % check all internal reactions if they are elementally balanced
            NumSpecies = length(model.species);
            NumChemEle = length(model.species{1}.getChemicalElements);
            validSpecies = false(NumSpecies,1);
            chemFormulas = zeros(NumSpecies,NumChemEle);
            for i=1:NumSpecies
                validSpecies(i)   = model.species{i}.formula_valid;
                chemFormulas(i,:) = model.species{i}.formula_as_vector;
            end
        
            S   = model.stoich.S;
            
            chemFormulasExpanded = chemFormulas(model.metabolites2species,:);
            validSpeciesExpanded = validSpecies(model.metabolites2species);
            
            this.balanced_reactions = zeros(model.stoich.int,NumChemEle);
            this.is_balanced        = false(model.stoich.int,1);
            for i=1:model.stoich.int
                pos = find(S(:,i));
                coefs = S(pos,i);
                if all(validSpeciesExpanded(pos))
                    this.balanced_reactions(i,:) = sum(chemFormulasExpanded(pos,:).*repmat(coefs,1,NumChemEle),1);
                    this.is_balanced(i) = all(this.balanced_reactions(i,:)==0);
                end
            end
        end
        
        function this = MnXModelKernel(this, model)
            % MnXModelKernel
            % computes the kernel matrix for the stoichiometric matrix
            % 
            % Input:
            %   model       model handle
            %
            % Output:
            %   kernel      kernel matrix
            
            if isfield(model.stoich,'getStoich')
                S = model.stoich.getStoich();
            else
                S = [model.stoich.S model.stoich.E model.stoich.M];
            end
            % fast but "instable" version
%             k = spnull(sparse(S));
            k = spnull(sparse(S),'r');
            % account for doubles (tests indicated that this is not necessary)
%             k = spnull(sparse(S*10000),'r');
%             % slow but "stable" version
%             [SNL,SNR] = spspaces(sparse(S'),1);
%             k = SNL{1};
%             k = k(SNL{3},:)';
%             k = full(k);
            % very slow version
%             k = null(S);
            this.kernel = MnXKernelMatrix(full(k));
        end
        
        function this = MnXModelCorrelations(this, model, varargin)
            % CORRELATIONS  Computes zero flux reactions and correlated reactions
            %               based on some model, mainly containing the stoichiometric
            %               matrix of a metabolic network.
            %
            % PARAMS    model The model for a metabolic network, where the following
            %                 members are expected:
            %                   model.S(m,r):    The stoichiometric matrix
            %                   model.E(m,e):    Stoichiometry of exchange reactions
            %                   model.M(m,1):    Biomass formation stoichiometry
            %
            %                 If the kernel member is present, and
            %                 recomputeKernel=false, it will be used for subsequent
            %                 computations (this enhances the performance drastically).
            %                 Otherwise, kernel will be calculated (and returned).
            %
            %           [tol] The tolerance used for comparison with 0. If omitted,
            %                 default (machine) tolerance is used.
            %
            % RETURNS         The model as given in the input, but the following
            %                 members will be appended (n=r+e+1):
            %                   kernel(n,k):        Basis for nullspace of the full
            %                                       stoichiometric matrix [S E M]
            %                   zero_flux(n,1):     True if the related reaction cannot
            %                                       have a non-zero flux value under
            %                                       steady-state assumption
            %                   correl_mx(n,n):     The correlation matrix, all
            %                                       reactions against all others.
            %                                       If cx = correl_mx, then
            %                                       cx(i,i)=1
            %                                       cx(i,j)=c(j,i)=0:  i not correlated
            %                                                          with j
            %                                       cx(i,j)=1/cx(j,i): i correlated
            %                                                          with j
            %                   correl_ind(1,ind)   Independent reactions, i.e. indices
            %                                       of reactions which are not
            %                                       zero-flux reactions and which do
            %                                       not correlate with any other
            %                                       reaction
            %                   correl_grp{2,grp}   Reaction correlation groups, having
            %                                       (cg = correl_grp):
            %                                       cg{1,i}(:)  Reaction indices of
            %                                                   i-th correlation group
            %                                       cg{2,i}(:)  Correlation factors of
            %                                                   i-th correlation group
            %                                       cg{2,i}(j)  Always ~= 0
            %                                       cg{2,i}(1)  Always 1
            %
            % SAMPLE USE
            %   model = correlations(model)
            %   model = correlations(model, 10^-6)
            %
            
            % stoich = model.stoich;
            if (nargin < 1 || nargin > 3)
                disp 'usage:';
                disp '  model = correlations(model [, recomputeKernel [, tol]]);';
                return;
            end;
            
            % stoichiometric matrix with exchange and biomass reactions
            if isfield(model.stoich,'getStoich')
                S = model.stoich.getStoich();
            else
                S = [model.stoich.S model.stoich.E model.stoich.M];
            end
            m = model.stoich.m; % # metabolites
            n = model.stoich.r; % # reactions
                       
            % tolerance, if none is given
            if (nargin < 3)
                tol = max(m,n)*eps(class(S))*norm(S,'inf');
            elseif (nargin == 3)
                tol = varargin{1};
            end
            
            % compute kernel matrix for model if not computed yet
            if isempty(this.kernel)
                this = MnXModelKernel(this, model);
            end
            
            % find zero flux reactions (z), and correlation
            % matrix (c)
            [z, ~, c] = cmatrix(this.kernel.K, tol); % mex-file
            
            % find correlation groups
            index = 1:n;
            grp1 = []; % the reactions which don't correlate, without 0-flux reactions
            grpi = {}; % the reaction indices of the groups
            grpf = {}; % the correlation factors
            
            grpc   = 1;  % counter groups
            grps   = []; % group size
            grpidx = zeros(n,1); % group indices for reactions
            
            while (~isempty(index))
                ci = c(index(1), :);
                nexti = find(ci(:));
                nexti = intersect(nexti, index(2:end));
                if (~isempty(nexti))
                    gidx         = [index(1), nexti];
                    grpi{end+1}  = gidx;
                    grpf{end+1}  = [1, ci(nexti)];
                    grps(grpc)   = length(gidx);
                    grpidx(gidx) = grpc;
                    grpc         = grpc+1;
                elseif (~(z(index(1))))
                    grp1(end+1) = index(1);
                end
                index = setdiff(index(2:end), nexti);
            end
            
            this.correlations = MnXCorrelationGroups(c, z, grp1, [grpi;grpf], grps, grpidx);
        end
        
        function this = MnXInconsistentCorrelationGroups(this, model, varargin)
            
            if isempty(this.correlations)
                this = MnXCorrelationGroups(this, model);
            end
            
            % calc reversibilities of reactions
            % 0: reversible 1: forward -1: back
            if nargin == 2
                revs = model.stoich.revs;
            elseif nargin == 3
                revs = varargin{1};
                % update for reactions with fixed non-zero constraints
                revs(revs>1) = 1;
                revs(revs<-1) = -1;
                if length(revs) ~= model.stoich.r
                    error(['inconsistent reversibility length, was ' ...
                        num2str(length(revs)) ...
                        'but expected ' model.stoich.r]);
                end
            end
            
            
            % check group for reversibility consistence
            inconsGrp   = false(this.correlations.NumGroups, 1);
            inconsReact = false(model.stoich.r, 1);
            inconsRev   = false(model.stoich.r, 1);
            
            for i=1:this.correlations.NumGroups
                
                inds = this.correlations.correl_grp{1, i};
                facs = this.correlations.correl_grp{2, i};
                
                rcons             = sign(revs(inds)).*sign(facs);
                flrcons           = ~(all(rcons>=0)|all(rcons<=0)); % flag inconsistent group
                inconsGrp(i)      = flrcons; % assign groups
                inconsReact(inds) = flrcons; % assing reactions
                
                idx1 = find(rcons>0);
                idx2 = find(rcons<0);
                inds = inds(:);
                if length(idx1)<length(idx2),
                    inconsRev(inds(idx1)) = true;
                else
                    inconsRev(inds(idx2)) = true;
                end
                
            end
            % TODO
%             fprintf('FIX: Changed from index to indices for "inconsRev"\n');
            this.inconsistencies = MnXInconsistentGroups(inconsRev, inconsGrp, inconsReact);
        end
        
        function this = MnXNullSpaceAnalysis(this, model)
            % calculate nullspace, zero-flux reactions based on kernel
            % matrix, correlated reaction groups, and inconsistent
            % correlation groups
            
            % first check if kernel was calculated, if so continue, else
            % calculate
            if ~isempty(this.kernel) 
                this = MnXModelKernel(this, model);
            end
            
            % determine zero-flux reactions based on kernel matrix and 
            % correlation groups
            this = MnXModelCorrelations(this, model);
            
            % determine inconsistent correlation groups
            this = MnXInconsistentCorrelationGroups(this, model);
        
        end
    end
    
end

