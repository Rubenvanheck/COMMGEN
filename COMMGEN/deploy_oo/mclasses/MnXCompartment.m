classdef MnXCompartment < handle
    %MNXCOMPARTMENT Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (GetAccess=private)
        TAB      = sprintf('\t'); % separator for files
    end
    
    properties
        id
        name
        original
        property
    end
    
    methods
        function this = MnXCompartment(varargin)
            
            if nargin == 1
                arguments = varargin{1};
                if length(arguments) == 4
                    this.id = arguments{1};
                    this.name = arguments{2};
                    this.original = arguments{3};
                    this.property = arguments{4};
                end
            elseif nargin == 4
                this.id = varargin{1};
                this.name = varargin{2};
                this.original = varargin{3};
                this.property = varargin{4};
            end
        end
    end
    
    methods (Static)
        function data = readFile(FileName)
            fid   = fopen(FileName, 'r');
            data = textscan(fid, '%s','delimiter','\n');
            fclose(fid);
            data = data{:};
        end                
    end
    
end

