classdef MnXDeadEnds
    %MNXDEADENDS Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        iM 
        iR 
        iB
    end
    
    methods
        function this = MnXDeadEnds(iM, iR, iB)
            this.iM     = iM;
            this.iR     = iR;
            this.iB     = iB;
        end
    end
    
end

