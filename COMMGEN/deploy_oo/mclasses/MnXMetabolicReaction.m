classdef MnXMetabolicReaction
    %MNXMETABOLICREACTION Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (GetAccess= private)
        %gene_pattern = '\d+\*gene_';
        %gene_pattern = '\d+\*';
    end
    
    properties
        id;             % reaction identifier
        equation;       % reaction equation
        reactants;      % reaction participants
        size;
        source;         % source
        proteins;       % proteins
        alternatives;   % reaction
        compartment;    % compartment
        ec_number;      % EC number
        operator;       % operator
        lb;             % lower_bound
        ub;             % upper_bound
        name;           % name
        references;     % Xrefs
        biomass;        % entry indicating the biomass equation (0 or 1)
        external;       % column indicating the external reactions (0 or 1)
        pathway;        % pathway
        transport;      % Whether the reaction spans compartments.
    end
    
    methods
        
        function this = MnXMetabolicReaction(varargin)
            if nargin == 1;
                arguments = varargin{1};
                if length(arguments) == 14
                    this.id = arguments{1};
                    % remove leading "+" and whitespace characters
                    str = arguments{2};
                    str = regexprep(strtrim(str),'^\+\s+','');
                    this.equation = str;
                    this.source = arguments{3};
                    str = arguments{4};
                    % remove whitespace characters
                    str = regexprep(str,'\s+','');
                    str = regexprep(strtrim(str),'\s+;$','');
                    %str = regexprep(str, this.gene_pattern, '');
                    this.proteins = regexprep(str,'-','_');
                    if ~strcmp(this.proteins,str)
                        fprintf('In reaction %s, the GPR ''%s'' was changed to ''%s'' due to removal of illegal symbols\n',this.id,str,this.proteins)
                    end
                    this.alternatives = arguments{5};
                    %this.compartment = arguments{6};
                    this.ec_number = arguments{6};
                    this.pathway = arguments{7};
                    this.operator = arguments{8};
                    this.lb = str2double(arguments{9});
                    this.ub = str2double(arguments{10});
                    this.name = arguments{11};
                    this.references = arguments{12};
                    this.biomass = str2double(arguments{13});
                    this.external = str2double(arguments{14});
                    this = getReactants(this);
                else
                    fprintf('\nERROR: Wrong format for reaction\n\t%s:\t%s\n\n', arguments{1}, arguments{2});
                    try
                        error('Wrong format');
                    catch
                        error('This was a test!')
                    end
                end
            elseif nargin == 14
                this.id = varargin{1};
                % remove leading "+" and whitespace characters
                str = varargin{2};
                str = regexprep(strtrim(str),'\+\s+','');
                this.equation = str;
                this.source = varargin{3};
                str = varargin{4};
                str = regexprep(strtrim(str),'\s+;$','');
                %str = regexprep(str, this.gene_pattern, '');
                this.proteins = str;
                this.proteins = str;
                this.alternatives = varargin{5};
                %this.compartment = varargin{6};
                this.ec_number = varargin{7};
                this.operator = varargin{8};
                this.lb = str2double(varargin{9});
                this.ub = str2double(varargin{10});
                this.name = varargin{11};
                this.references = varargin{12};
                this.biomass = str2double(varargin{13});
                this.external = str2double(varargin{14});
                this = getReactants(this);
            else
                fprintf('\nERROR: Wrong format for reaction\n\t%s:\t%s\n\n', varargin{1}, varargin{2});
                error('Wrong format');
            end
        end
               
        function this = getReactants(this)
            %% Pre-process equation
            eq = regexprep(this.equation,' +',' '); %Remove double spaces
        
            %% Pre-allocate variables
            n = length(regexp(eq,'@','match'));
            metabo = repmat({''},1,n);  % list of metabolites
            scoeff = ones(1,n)*NaN;     % stoichiometric coefficients
            ridx = 1;                   % reactand index
            fled = 1;                   % flag: educt side of equation
            Remove = false(n,1);        % Whether the metabolite should be kept. Used to remove metabolites occurring on both sides of the eq.
            
            %% Read the reaction
            [rargs,nrargs] = srgetargs(strtrim(eq),' ');
            for zr = 1:nrargs,
                s  = strtrim(rargs{zr});
                if any(strcmp(s, {'<--','<==>','<?>','-->'}));
                        arrow = s;
                        fled = 0;
                        NumEduct = ridx -1;
                elseif strcmp(s, '+')
                    %ignore
                else
                    coeff = COMMGEN.CheckStoichCoeff(s, fled);
                    if ~isnan(coeff)
                        scoeff(ridx) = coeff;
                    else
                        if ismember(s,metabo) % Metabolite is present at both sides of the equation
                            IDx = ismember(metabo,s);
                            if scoeff(IDx) > scoeff(ridx) % Educt side is larger
                                scoeff(IDx) = scoeff(IDx) - scoeff(ridx);
                                Remove(ridx) = true;
                                ridx = ridx + 1;
                            elseif scoeff(IDx) > scoeff(ridx) % Product side is larger
                                scoeff(ridx) = scoeff(ridx) - scoeff(IDx);
                                Remove(IDx) = true;
                                metabo(ridx) = sID;
                                ridx = ridx + 1;
                            else % Equal stoichiometric coefficients
                                Remove([ridx find(IDx)]) = true;
                                ridx = ridx + 1;
                            end
                        else
                            metabo{ridx} = s;
                            ridx = ridx+1;
                        end
                    end
                end
            end
            
            % If needed, remove metabolites % adjust equation
            if any(Remove)
                scoeff(Remove) = [];
                metabo(Remove) = [];
                NumEduct = NumEduct - count(Remove(1:NumEduct));
                n = n - count(Remove);
                
                %% Create educt list
                if NumEduct >0
                    educts     = metabo(1:NumEduct);
                    eductcoeff = scoeff(1:NumEduct);
                    % Sort
                    [educts,I] = sort(educts);
                    eductcoeff = eductcoeff(I);
                else
                    educts  = '';
                end
                
                %% Create Product list
                if NumEduct < n
                    products     = metabo(NumEduct+1:end);
                    productcoeff = scoeff(NumEduct+1:end);
                    % Sort
                    [products,I] = sort(products);
                    productcoeff = productcoeff(I);
                else
                    products ='';
                end
                
                %% Recreate the equation
                eq = '';
                % Add educts
                for i=1:NumEduct
                    if isempty(eq)
                        eq = [num2str(abs(eductcoeff(i))) ' ' educts{i}];
                    else
                        eq = [eq num2str(-1*eductcoeff(i)) ' ' educts{i}]; %#ok<AGROW>
                    end
                    if i<NumEduct
                        eq = [eq ' + ']; %#ok<AGROW>
                    end
                end
                
                % Add the arrow
                eq = [eq ' ' arrow ' '];
                
                % Add the products
                for i=1:numel(products)
                    eq = [eq num2str(abs(productcoeff(i))) ' ' products{i}]; %#ok<AGROW>
                    if i<n-NumEduct
                        eq = [eq ' + ']; %#ok<AGROW>
                    end
                end
                this.equation = eq;
            end
            
            this.size = n;
            this.reactants = cell(n,1);
            if this.size >0
                for i=1:this.size
                    this.reactants{i} = MnXReactant(metabo{i}, scoeff(i));
                end

                if this.external
                    this.transport = false;
                    this.compartment = 'boundary';
                else
                    comps = cell(numel(this.size),1);
                    for i=1:this.size
                        comps{i} = this.reactants{i}.compartment;
                    end
                    if numel(unique(comps))==1
                        this.compartment = comps{1};
                        this.transport = false;
                    else
                        this.transport = true;
                        comps = unique(comps);
                        compstr = comps{1};
                        for i=2:numel(comps)
                            compstr = [compstr '%' comps{i}]; %#ok<AGROW>
                        end
                        this.compartment = compstr;
                    end
                end
            end
        end
        
        function coeff = checkStoichCoeff(this, s, fled)
            co = regexp(s,'\(*(\S+)\)*','tokens');
            coeff = (1-2*fled)*str2double(co{:});
        end
        
        function print(this, varargin)
            % input: either nothing or direction (-1,0,1), lower bound and upper bound
           
           if nargin == 1
               fprintf('%s\t', this.id);
               fprintf('%s\t', this.equation);
           else
               % adapt reaction direction
               tmp_id = this.id;
               eq = this.equation;
               if varargin{1} == 0
                   tmp_id = regexprep(tmp_id, '_\S+', '_B');
                   eq = regexprep(eq, '<--|<==>|-->', '<==>');
               elseif varargin{1} == -1
                   tmp_id = regexprep(tmp_id, '_\S+', '_RL');
                   eq = regexprep(eq, '<--|<==>|-->', '<--');
               elseif varargin{1} == 1
                   tmp_id = regexprep(tmp_id, '_\S+', '_LR');
                   eq = regexprep(eq, '<--|<==>|-->', '-->');
               end
               fprintf('%s\t', tmp_id);
               fprintf('%s\t', eq);
           end
           fprintf('%s\t', this.source);
           fprintf('%s\t', this.proteins);
           fprintf('%s\t', this.alternatives);
           fprintf('%s\t', this.compartment);
           fprintf('%s\t', this.ec_number);
           fprintf('%s\t', this.operator);
           if nargin == 1
               fprintf('%i\t', this.lb);
               fprintf('%i\t', this.ub);
           else
               % adapt reaction constraints
               newlb = varargin{2};
               newub = varargin{3};
               if varargin{1} == 0
                   fprintf('%i\t', newlb);
                   fprintf('%i\t', newub);
               elseif varargin{1} == -1
                   fprintf('%i\t', newlb);
                   fprintf('%i\t', 0);
               elseif varargin{1} == 1
                   fprintf('%i\t', 0);
                   fprintf('%i\t', newub);
               end
           end
           fprintf('%s\t', this.name);
           fprintf('%s\t', this.references);
           fprintf('%i\t', this.biomass);
           fprintf('%i\n', this.external);
        end

        function printfields(this,Rfields,~)
            % Provide any value as third value if all but the supplied fields
            % should be printed           
            
           if nargin == 1
               Rfields = fields(this);
           elseif nargin >2
               Rfields = setdiff(fields(this),Rfields);
           end
           
           if ismember('id',Rfields), fprintf('id:\t\t%s\t\n', this.id); end
           if ismember('equation',Rfields),fprintf('equation:\t%s\t\n', this.equation); end
           if ismember('source',Rfields), fprintf('source:\t\t%s\t\n', this.source); end
           if ismember('proteins',Rfields), fprintf('proteins:\t%s\t\n', this.proteins); end
           if ismember('alternatives',Rfields), fprintf('alternatives:\t%s\t\n', this.alternatives); end
           if ismember('compartments',Rfields), fprintf('%compartment:\t%s\t\n', this.compartment); end
           if ismember('ec_number',Rfields), fprintf('ec_number:\t%s\t\n', this.ec_number); end
           if ismember('operator',Rfields), fprintf('operator:\t%s\t\n', this.operator); end
           if ismember('lb',Rfields), fprintf('lb:\t\t%i\t\n', this.lb); end
           if ismember('ub',Rfields), fprintf('ub:\t\t%i\t\n', this.ub); end
           if ismember('name',Rfields), fprintf('name:\t\t%s\t\n', this.name); end
           if ismember('references',Rfields), fprintf('references:\t%s\t\n', this.references); end
           if ismember('biomass',Rfields), fprintf('biomass:\t%i\t\n', this.biomass); end
           if ismember('external',Rfields), fprintf('external:\t%i\n', this.external); end
           if ismember('Origin',Rfields), fprintf('Origin:\t\t%i\n', this.Origin); end
           if ismember('NumSrcRxns',Rfields), fprintf('NumSrcRxns:\t%i\n', this.NumSrcRxns); end
           if ismember('flag',Rfields), fprintf('Flag:\t\t%s\n\n', this.flag); end
        end
        
        function printToFile(this, fid, varargin)
            if nargin == 2
                fprintf(fid, '%s\t', this.id);
                fprintf(fid, '%s\t', this.equation);
            else
                % adapt reaction id and equation direction
                tmp_id = this.id;
                eq = this.equation;
                if varargin{1} == 0
                    tmp_id = regexprep(tmp_id, '_\S+', '_B');
                    eq = regexprep(eq, '<--|<==>|-->', '<==>');
                elseif varargin{1} == -1
                    tmp_id = regexprep(tmp_id, '_\S+', '_RL');
                    eq = regexprep(eq, '<--|<==>|-->', '<--');
                elseif varargin{1} == 1
                    tmp_id = regexprep(tmp_id, '_\S+', '_LR');
                    eq = regexprep(eq, '<--|<==>|-->', '-->');
                end
                fprintf(fid, '%s\t', tmp_id);
                fprintf(fid, '%s\t', eq);
            end
            fprintf(fid, '%s\t', this.source);
            fprintf(fid, '%s\t', this.proteins);
            fprintf(fid, '%s\t', this.alternatives);
            fprintf(fid, '%s\t', this.compartment);
            fprintf(fid, '%s\t', this.ec_number);
            fprintf(fid, '%s\t', this.operator);
            if nargin == 2
                fprintf(fid, '%i\t', this.lb);
                fprintf(fid, '%i\t', this.ub);
            else
                % adapt reaction constraints
                newlb = varargin{2};
                newub = varargin{3};
                if varargin{1} == 0
                    fprintf(fid,'%i\t', newlb);
                    fprintf(fid,'%i\t', newub);
                elseif varargin{1} == -1
                    fprintf(fid,'%i\t', newlb);
                    fprintf(fid,'%i\t', 0);
                elseif varargin{1} == 1
                    fprintf(fid,'%i\t', 0);
                    fprintf(fid,'%i\t', newub);
                end
            end
            fprintf(fid, '%s\t', this.name);
            fprintf(fid, '%s\t', this.references);
            fprintf(fid, '%i\t', this.biomass);
            fprintf(fid, '%i\n', this.external);
        end
    end    
end

