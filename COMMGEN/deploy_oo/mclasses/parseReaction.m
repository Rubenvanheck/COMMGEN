function [metabo, metahs, scoeff] = parseReaction(rr)%, lb, ub)
    
    % get reversibility
    %[dir cons idxr] = getReversibility(rr, lb, ub );
    
    % decomposition of reaction equation
    [rargs,nrargs] = srgetargs(strtrim(rr),' ');
    
    n = length(srgetargs(regexprep(strtrim(regexprep(rr,'+|\(*|\)*|<--|<==>|<?>|-->|\d+','')), '(\s\s+)+',' '), ' '));
    metabo = cell(1,n); % list of metabolites
    metahs = cell(1,n); % metabolite hash
    metaid = ones(1,n)*NaN; % metabolite indices
    scoeff = ones(1,n)*NaN; % stoichiometric coefficients
    
    ridx = 1;   % reactand index
    flc  = NaN; % default compartment flag
    fled = 1;   % flag: educt side of equation
    
    % check each element of a chemical reaction
    for zr = 1:nrargs,
        s  = strtrim(rargs{zr});
        
        if ~isempty(regexp(s, '<--|<==>|<?>|-->', 'start'))
            fled = 0;
            
        elseif isempty(regexp(s, '+', 'start'))
            
            % (1) compartment definition
            flcc = false(1,length(IDComS));
            for zc = 1:length(IDComS),
                idx = regexp(s, IDComS{zc}, 'start');
                if ~isempty(idx),
                    flcc(1,zc) = true; % definition for compound
                    %                      flcc(2,zc) = (idx==1); % definition for default comp.
                end
            end
            
            
            % (2) stoichiometric coefficient
            idxs0 = regexp(s,'(', 'start');
            idxs1 = regexp(s,')', 'start');
            
            
            if ~isempty(idxs0) && ~isempty(idxs1) % stoichiometric coefficient
                
                co = str2double(s(idxs0+1:idxs1-1));
                scoeff(ridx) = (1-2*fled)*co;
                
            elseif ~isnan(str2double(s)) % stoichiometric coefficient
                
                co = str2double(s);
                scoeff(ridx) = (1-2*fled)*co;
                
            else % (3) compound % !!! check for stoichiometry notation 'x*C'
                
                compidx      = find(flcc(1,:));
                metabo{ridx} = s;
                metahs{ridx} = DataHash(s);

                if length(scoeff) < ridx % unity stoichiometry
                    scoeff(ridx) = (1-2*fled);
                end
                ridx = ridx+1;
            end
        end
    end    
end

function [dir cons idxr] = getReversibility(rr, lb, ub )
    
    IdRev          = '<==>'; % identifier reversible reactions
    IdRev2         = '<?>'; % identifier undirected reactions
    IdIrrev        = '-->'; % identifier irreversible reactions
    IdIrrevBW      = '<--'; % identifier irreversible reactions

    idxrev = regexp(rr, IdRev, 'start');
    if isempty(idxrev)
        idxrev = regexp(rr, IdRev2, 'start');
    end
    idxirr = regexp(rr, IdIrrev, 'start');
    idxirrBW = regexp(rr, IdIrrevBW, 'start');
    
    if ~isempty(idxrev),
        dir = 1;
        idxr    = idxrev;
        cons = [lb ub];
    elseif ~isempty(idxirr),
        dir = 0;
        idxr    = idxirr;
        cons = [0 ub];
    else
        dir = -1;
        idxr    = idxirrBW;
        cons = [lb 0];
    end    
end