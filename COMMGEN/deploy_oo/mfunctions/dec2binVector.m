function [bins] = dec2binVector(decimal, len)
% DEC2BINVECTOR
% Converts a decimal to a bit vector
%
% Input:
%   decimal     decimal to convert to bit vector
%   (len)       length of the bit vector
% Output:
%   bins        bit vector (of length len)

bins = false(1,len);
lim = log2(decimal);
for i = 0:lim;
    t = mod(decimal,2);
    bins(end-i) = (t~=0);
    decimal = floor(decimal/2);
end
end