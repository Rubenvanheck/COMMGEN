function str2CNFscreen(string, varargin) 
% transforms a boolean formula into
% a CNF boolean formula
% optional bracket type ('left' and 'right') and type of 'or' and 'and'
%
% default brackerts are "(" and ")
% default clause-types are "+" for "or" and
%                          "*" for "and"
%
% Input:
%   string          boolean expression
%   bracket-left    default: "("
%   bracket-right   default: ")
%   or-symbol       default: "+"
%   and-sumbol      default: "*"
%
% Usage:
%   
%   str2CNFscreen('(a+b)*(c+d)')
%   str2CNFscreen('(a;b)+(c;d)', '(', ')', ';', '+')

    l_brace = '(';
    r_brace = ')';
    
    idor  = '+';
    idand = '*';
    
    if nargin > 1,
        l_brace_new = varargin{1};
        string = strrep(string, l_brace_new, l_brace);
        if nargin > 2,
            r_brace_new = varargin{2};
            string = strrep(string, r_brace_new, r_brace);
            if nargin > 3,
                idor_new = varargin{3};
                string = strrep(string, idor_new, '_OR_');
                if nargin > 4,
                    idand_new = varargin{4};
                    string = strrep(string, idand_new, '_AND_');
                end
                string = strrep(string,'_OR_', idor);
                string = strrep(string,'_AND_', idand);
            end
        end
    end
    
    s = expand(sym(string));
    fprintf('%s\n', char(s));
end