function [Args,varargout] = srgetargs(String,DelimiterBegin,varargin)

% Function: Extraction of Arguments in command string -> List of
%           Arguments / Number of Arguments
%
% Subroutine for MKTEXMODEL.M
% 01-11-16 J. Stelling
%
% Inputs:
% String            [char array]         Evaluation string
% DelimiterBegin    [char]               Delimiter for start of argument 
% DelimiterEnd      [char]               Delimiter for end of argument 
%
% Outputs:
% Args              [cell]               List of arguments (text strings)
% NArgs (opt.)      [int]                Number of arguments
% IdxBegin (opt.)   [vector]             Index begin of arguments
% IdxEnd (opt.)     [vector]             Index end of arguments
%
% V1.1, 02-12-13: identical delimiters for begin / end
%       03-02-26: correction: indexing
% V3.2, 06-04-30: Optional number of arguments to extract
% V3.3, 07-10-23: Return single argument for non-empty string
  
  % input arguments

  NArgs = 0;
  idx1  = [];
  
  if (nargin>2),
    DelimiterEnd = varargin{1};
    if (nargin>3),
      NArgs = varargin{2};
    end
  else
    DelimiterEnd = DelimiterBegin;
  end

  % indices for position of arguments
  
  [idx0,varargout{3}] = deal(findstr(String,DelimiterBegin)); % start
  
  if ~isempty(idx0),
    
    if ~strcmp(DelimiterBegin,DelimiterEnd)
      argborders = [idx0' 0*idx0'+1];
      [idx,varargout{4}] = deal(findstr(String,DelimiterEnd)); % end

      % evaluation of closed sets 
      % correction: idx instead of idx0 !
      argborders = sortrows([argborders; idx' 0*idx'-1],1);
      argidx     = cumsum(argborders(:,2));

      bidx   = find(argidx.*argborders(:,2)==1);
      eidx   = find(argidx==0);

      % indices begin / end
      argbeg = argborders(bidx)+length(DelimiterBegin);
      argend = argborders(eidx)-length(DelimiterEnd);

    else % identical delimiters, no subgroups
      idx1   = union(idx0,[0,length(String)+1]);
      argbeg = idx1(1:end-1)+1;
      argend = idx1(2:end)-1;
      
%      argborders = [idx0(1:2:end)' idx0(2:2:end)'];
%      varargout{4} = idx0(2:2:end); 
%      idx0         = idx0(1:2:end);
%      varargout{3} = idx0;

%      argbeg = argborders(:,1)+length(DelimiterBegin);
%      argend = argborders(:,2)-length(DelimiterEnd);      
    end
    
  elseif ~isempty(String),
    argbeg = 1;
    argend = length(String);
    
  else
    [argbeg,argend] = deal([]);
  end
  
  % generate argument list
  
  if ~NArgs,
    NArgs = length(argbeg);
  end
  Args  = cell(NArgs,1);
  
  for z=1:NArgs,
    Args{z} = String(argbeg(z):argend(z));
  end
  
  varargout{1} = NArgs;
  varargout{2} = argbeg;
  varargout{3} = argend;
  varargout{4} = idx1;

  return
  