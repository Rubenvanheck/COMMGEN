function [s0,varargout] = fstdrx(s0)
            % Function: Format / standardize reaction (motif) entry
            s0 = double(s0);
            l  = size(s0,2);
            
            % eliminate catalytic components
            fl = true(1,l);
            q  = zeros(1,l);
            for z = 1:l,
                if fl(z),
                    idx  = find(s0(1,:)==s0(1,z));
                    q(z) = sum(s0(2,idx));
                    fl(idx) = false;
                end
            end
            
            sidx    = find(q);
            s0(2,:) = q;
            
            if ~isempty(sidx),
                [~,i1] = sort(s0(1,sidx));
                %    s1 = 0*s0(:,sidx);
                s1(:,1:length(sidx)) = s0(:,sidx(i1));
                s0 = s1;
                
                ssign   = sign(s0(2,1));
                s0(2,:) = s0(2,:)*ssign;
            else
                s0    = [];
                ssign = 0;
            end
            
            varargout{1} = ssign; % sign for change of reaction direction
        end