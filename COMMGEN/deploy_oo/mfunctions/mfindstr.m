% Funktion Suchen von String(s) in einer Char-Matrix:
%
%  function index = mfindstr(matrix,string)
%
% 1-1 Relation bei Angabe mehrerer Suchstrings,
% 1-n Relation bei Angabe eines Suchstrings
% Vergleich case-insensitive
%
% Ausgabe: Positionsindex Zeilen
%          optional: Positionsindex Spalten
% 
% 06-05-02: Persistent variable matrix,addtl. flag

function [idxr,varargout] = mfindstr(matrix,string,varargin)
  
  persistent r lx ly
  
  % optional arguments
  
  FlP = 0; % flag: take persistent matrix
  
  if (nargin>2),
    if strmatch(varargin{1},'persistent'),
      FlP = 1;
    end
  end

  %matrix = upper(matrix);
  %string = upper(string);

  if iscell(string),
    string = strvcat(string{:});
  end
  
  [sx,sy] = size(string);

  if ~FlP,
    [lx,ly] = size(matrix);

    matrix(:,end+1) = char(0);
    r = matrix';
%    r       = [matrix char(zeros(lx,1))]';
    r       = r(:)';

    ly = ly+1;
  end

  [idxr,idxc] = deal(zeros(max(1,sx),1));

  for z = 1:sx,
    q       = findstr(r,deblank(string(z,:)));
    if isempty(q),
      idxr(z) = 0;
    else,
      if (sx==1),
        idxr = ceil(q/ly);
        idxc = round((q/ly-floor(q/ly))*ly);
      else,
        q       = min(q);
        idxr(z) = ceil(q/ly);
        idxc(z) = round((q/ly-floor(q/ly))*ly); 
      end
    end
  end
  
  varargout{1} = idxc;
  
  return
