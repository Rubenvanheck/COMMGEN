function [model] = correlations(model, recomputeKernel, tol)
% CORRELATIONS  Computes zero flux reactions and correlated reactions
%               based on some model, mainly containing the stoichiometric
%               matrix of a metabolic network.
%
% PARAMS    model The model for a metabolic network, where the following
%                 members are expected:
%                   model.S(m,r):    The stoichiometric matrix
%                   model.E(m,e):    Stoichiometry of exchange reactions
%                   model.M(m,1):    Biomass formation stoichiometry
%
%                 The following model member might optionally be present:
%                   model.kernel(r+e+1,k):  Basis for nullspace of the full
%                                           stoichiometric matrix [S E M]
%
%                 If the kernel member is present, and 
%                 recomputeKernel=false, it will be used for subsequent 
%                 computations (this enhances the performance drastically). 
%                 Otherwise, kernel will be calculated (and returned).
%
% recomputeKernel If true or omitted or model.kernel is missing, the kernel 
%                 matrix will be (re)calculated. 
%
%           [tol] The tolerance used for comparison with 0. If omitted, 
%                 default (machine) tolerance is used.
%
% RETURNS         The model as given in the input, but the following
%                 members will be appended (n=r+e+1):                   
%                   kernel(n,k):        Basis for nullspace of the full
%                                       stoichiometric matrix [S E M]
%                   zero_flux(n,1):     True if the related reaction cannot
%                                       have a non-zero flux value under 
%                                       steady-state assumption
%                   correl_mx(n,n):     The correlation matrix, all
%                                       reactions against all others. 
%                                       If cx = correl_mx, then
%                                       cx(i,i)=1
%                                       cx(i,j)=c(j,i)=0:  i not correlated
%                                                          with j
%                                       cx(i,j)=1/cx(j,i): i correlated
%                                                          with j
%                   correl_ind(1,ind)   Independent reactions, i.e. indices
%                                       of reactions which are not 
%                                       zero-flux reactions and which do 
%                                       not correlate with any other 
%                                       reaction
%                   correl_grp{2,grp}   Reaction correlation groups, having
%                                       (cg = correl_grp):
%                                       cg{1,i}(:)  Reaction indices of
%                                                   i-th correlation group
%                                       cg{2,i}(:)  Correlation factors of
%                                                   i-th correlation group
%                                       cg{2,i}(j)  Always ~= 0
%                                       cg{2,i}(1)  Always 1
%                                       
% SAMPLE USE
%   model = correlations(model)
%   model = correlations(model, false)
%   model = correlations(model, false, 10^-6)
%


    if (nargin < 1 || nargin > 3)
        printusage;
        return;
    end;
    
    % stoichiometric matrix with exchange and biomass reactions
    stoich  = [model.S model.E model.M];
    [m,n]   = size(stoich);
    
    % recompute kernel if not deselected
    if (nargin < 2)
        recomputeKernel = true;
    end
    
    % tolerance, if none is given
    if (nargin < 3)
        tol = max(m,n)*eps(class(stoich))*norm(stoich,'inf');
    end
    
    % compute the kernel matrix
    if (~recomputeKernel && isfield(model, 'kernel'))
        k = model.kernel;
    else
        
        % standard matlab version
        %k = null(stoich);
        
        % sparse version
        % fprintf('Using spnull(sparse(stoich))\n');
        k = spnull(sparse(stoich));
        k = full(k);
        model.kernel = k;
    end
    
    % find zero flux reactions (z), binar kernel matrix (kb), and correlation
    % matrix (c)
    %[ c z ]  = cMatrix(k, n, tol); % m-file
    [z kb c] = cmatrix(k, tol);     % mex-file
    
    model.correl_mx = c;
    model.zero_flux = z;
    
    % find correlation groups
    index = 1:n;
    grp1 = []; % the reactions which don't correlate, without 0-flux reactions
    grpi = {}; % the reaction indices of the groups
    grpf = {}; % the correlation factors
    
    grpc = 1; % counter groups
    grps = []; % group size
    grpidx = zeros(n,1); % group indices for reactions

    while (~isempty(index))
        ci = c(index(1), :);
        nexti = find(ci(:));
        nexti = intersect(nexti, index(2:end));
        if (~isempty(nexti))
            gidx         = [index(1), nexti];
            grpi{end+1}  = gidx;
            grpf{end+1}  = [1, ci(nexti)];
            grps(grpc)   = length(gidx);
            grpidx(gidx) = grpc;
            grpc         = grpc+1;
        elseif (~(z(index(1))))
            grp1(end+1) = index(1);
        end
        index = setdiff(index(2:end), nexti);
    end

    model.correl_ind = grp1;
    model.correl_grp = [grpi;grpf];
    model.correl_len = grps;
    model.grp_index  = grpidx;
end

function printusage()
    disp 'usage:';
    disp '  model = correlations(model [, recomputeKernel [, tol]]);';
end

