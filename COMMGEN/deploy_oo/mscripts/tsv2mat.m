function tsv2mat(varargin)
% TSV2MAT
%
% Reads in model files, constructs MnXModel. Replaces an existing mat-file.
%
% Input:
%   -d dir      input directory
%
% Output:
%   mat-file is saved in directory <dir>
%
% Usage:
%   TSV2MAT -d <dir>

    
    %% Define input parser
    p = inputParser;
    p.addParamValue('d', [], @ischar);  % directory with input files
    
    %% parse input
    % remove "-"
    for i=1:nargin
        varargin{i} = regexprep(varargin{i}, '^-d', 'd');
    end
    
    p.parse(varargin{:});
    
    if isempty(p.Results.d)
        printusage()
        return;
    end
    
    dir = p.Results.d;
    
    if exist(dir, 'dir')~=7
        fprintf('\nERROR: Directory %s does not exist\n\n', dir);
        printusage()
        return;
    end
    
    dir = regexprep(dir,'/$','');
    
    %% define input files
    FileNameModel        = [dir '/model.tsv'];
    FileNameCompartments = [dir '/compartments.tsv'];
    FileNameReactions    = [dir '/reactions.tsv'];
    FileNameChemcials    = [dir '/chemicals.tsv'];
    FileNameMat          = [dir '/model.mat'];
    
    %% check if mat file already exists, if so, print warning and replace
    if exist(FileNameMat, 'file')
        fprintf('\n# warning: Replacing existing mat-file in directory\n\n\t%s\n\n', dir);
    end
    
    % read as data files and construct model
    data  = MnXReader(FileNameModel, FileNameCompartments, FileNameReactions, FileNameChemcials);
    model = MnXModel(data);
    
    % saves model structure
    MnXModelTransform(model,'mat');

end

function printusage()
disp '  TSV2MAT'
disp '   ';
disp '  Reads in model files, constructs MnXModel. Replaces an existing mat-file.'
disp '   ';
disp '  OPTIONS:';
disp '    -d          directory where files are located';
disp '   ';
disp '  USAGE:';
disp '    tsv2mat -d <dir>';
disp '   ';
end