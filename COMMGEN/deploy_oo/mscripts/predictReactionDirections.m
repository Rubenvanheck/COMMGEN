function predictReactionDirections(varargin)
% predictReactionDirections
% predict reaction directions of an input model by a reaction database
% using the concept of reaction motifs.
%
% Input:
%   -m  dir         input directory <dir> of model
%   -db dir         input directory <dir> of database
%
% Output:
%   to screen       RID     SIZE    DIR     RATIO
%
% Usage:
%   predictReactionDirections('-m', '<dir>', '-db', '<dir>');

    option = 'direction';
    
    p = inputParser;
    p.addParamValue('m', [], @ischar);  % directory with input files for model
    p.addParamValue('db', [], @ischar); % directory with input files for db
    
    %% parse input
    % remove "-"
    for i=1:nargin
        varargin{i} = regexprep(varargin{i}, '^-m', 'm');
        varargin{i} = regexprep(varargin{i}, '^-db', 'db');
    end
    
    p.parse(varargin{:});
    
    if isempty(p.Results.m) || isempty(p.Results.db)
        printusage()
        return;
    end
    
    model_dir    = p.Results.m;
    database_dir = p.Results.db;
    model_dir    = regexprep(model_dir, '//','/');
    database_dir = regexprep(database_dir, '//','/');
    
    if exist(model_dir, 'dir')~=7
        fprintf('\nERROR: Directory %s (model) does not exist\n\n', model_dir);
        printusage()
        return;
    end
    
    if exist(database_dir, 'dir')~=7
        fprintf('\nERROR: Directory %s (database) does not exist\n\n', database_dir);
        printusage()
        return;
    end
    
    model_dir    = regexprep(model_dir,'/$','');
    database_dir = regexprep(database_dir,'/$','');
    
    %% define input files
    % model
    FileNameModel        = [model_dir '/model.tsv'];
    FileNameCompartments = [model_dir '/compartments.tsv'];
    FileNameReactions    = [model_dir '/reactions.tsv'];
    FileNameChemcials    = [model_dir '/chemicals.tsv'];
    FileNameMat          = [model_dir '/model.mat'];
    % database
    FileNameModelDB        = [database_dir '/model.tsv'];
    FileNameCompartmentsDB = [database_dir '/compartments.tsv'];
    FileNameReactionsDB    = [database_dir '/reactions.tsv'];
    FileNameChemcialsDB    = [database_dir '/chemicals.tsv'];
    FileNameMatDB          = [database_dir '/model.mat'];
    
    %% read model and database
    if exist(FileNameMat, 'file')
        fprintf('# Loading mat-file (model)\n');
        % read as mat file
        model = load(FileNameMat);
        model = model.model;
    else
        % read as data files and construct model
        data  = MnXReader(FileNameModel, FileNameCompartments, FileNameReactions, FileNameChemcials);
        model = MnXModel(data);
    end
    
    if exist(FileNameMatDB, 'file')
        fprintf('# Loading mat-file (database)\n');
        % read as mat file
        database = load(FileNameMatDB);
        database = database.model;
    else
        % read as data files and construct model
        data  = MnXReader(FileNameModelDB, FileNameCompartmentsDB, FileNameReactionsDB, FileNameChemcialsDB);
        database = MnXModel(data);
    end
    
    %% predict reaction directions using reaction motifs and print to screen
    if exist('model','var') && exist('database','var')
        fprintf('# Reaction direction prediction using reaction motifs\n');
        pdir = MnXRMPredictions(model, database, option);
        MnXPrint(pdir, option);
    else
        error('ERROR: Cannot load model or database\n');
    end
end

function printusage()
disp '  predictReactionDirections'
disp '   ';
disp '  predict reaction directions of an input model by a reaction database'
disp '  using the concept of reaction motifs.'
disp '   ';
disp '  Input:';
disp '    -m  dir    input directory <dir> of model';
disp '    -db dir    input directory <dir> of database';
disp '   ';
disp '  Output:';
disp '    to screen   RID SIZE DIRECTION RATIO';
disp '   ';
disp '  USAGE:';
disp '    predictReactionDirections -m <dir> -db <dir>';
disp '   ';
end