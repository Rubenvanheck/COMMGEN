function [TM EM]= runTransitionMatrix(spr, motifs, mCount, motifLen)
            % Determine transition matrix w/o self-loops
            
            TM = zeros(mCount+1);
            % Motif size matrix
            LM = zeros(mCount+1);
            
            % Extension matrix
            EM = zeros(mCount+1);
            
            for i=1:mCount+1
                for j=i+1:mCount+1
                    if i==1
                        TM(j,i) = spr(j-1);
                        LM(j,i) = motifLen(j-1);
                        EM(j,i) = j;
                    else
                        if (motifLen(i-1) < motifLen(j-1))
                            from = logical(motifs(i-1,:));
                            to   = logical(motifs(j-1,:));
%                             if any(and(from,to)) && (bin2dec(num2str(and(from, to)))-bin2dec(num2str(from))==0)
%                                 idx = bin2dec(num2str(xor(from, to)));
%                             if any(and(from,to)) && (MnXRMPredictions.binVector2dec(and(from, to))-MnXRMPredictions.binVector2dec(from)==0)
%                                 idx1 = MnXRMPredictions.binVector2dec(xor(from, to))
% 
                            if any(and(from,to)) 
                                val1 = binVector2dec(double(and(from, to)));
                                val2 = binVector2dec(double(from));
                                if (val1-val2==0)
                                    idx = binVector2dec(double(xor(from, to)));
                                    TM(j,i) = spr(idx,1);
                                    LM(j,i) = motifLen(idx);
                                    EM(j,i) = idx+1; % add one to index to account for larger matrix dimension
                                end
                            end
                        end
                    end
                end
            end
            
%             tm = [];
%             if any(sum(TM,1)) > 0
%                 denom = sum(TM,1);
%                 tm = TM./repmat(denom,mCount+1,1);
%                 tm(:,end)=0;
%                 tm(end,end)=1;
%             end
            return;
        end