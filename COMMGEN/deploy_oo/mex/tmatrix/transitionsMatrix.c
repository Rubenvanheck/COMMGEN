#include "mex.h"
/*
 * cmatrix.c 
 * Mathias Ganter, 11/2011
 *
 * Determines the correlation matrix c based on the kernel matrix k
 *
 * Input:
 *  k       kernel matrix
 *  tol     tolerance
 *
 * Output:
 *  z       zero-flux vector
 *  kb      binary kernel matrix
 *  c       correlation matrix
 *
 * Usage:
 *  [z kb c] = cmatrix(k, tol);
 *
 * Compile:
 *  mex cmatrix.c
 *
 */
 

void transitionMatrix(double *tm, double *em, double *spr, double *motifs, double* motifLen, int mCount)
{
    int i,j,k,pos;
    int rLen = (int)motifLen[mCount-1], flag = 0;
    double from[rLen], to[rLen];
    char cinter[rLen+1], cxor[rLen+1], cfrom[rLen+1], cto[rLen+1];
    
    cinter[rLen] = '\0';
    cfrom[rLen]  = '\0';
    cto[rLen]    = '\0';
    cxor[rLen]   = '\0';
    
    int dim = mCount + 1;
    
    for (i=0;i<dim;i++) {
        for(j=i+1;j<dim;j++) {
            
            if (i==0) {
                tm[(j) + (i)*(dim)] = spr[j-1];
                em[(j) + (i)*(dim)] = j+1;
            } else {
                
                if(motifLen[i-1] < motifLen[j-1]) {
                    flag = 0;
                    for(k=0;k<rLen;k++){
                        
                        from[k] = motifs[(i-1)+k*mCount];
                        to[k]   = motifs[(j-1)+k*mCount];
                        
                        if (from[k] > 0.0) {
                            cfrom[k] = '1';
                        } else {
                            cfrom[k] = '0';
                        }
                        
                        if (to[k] > 0.0) {
                            cto[k] = '1';
                        } else {
                            cto[k] = '0';
                        }
                        
                        if (from[k]*to[k] > 0.0) {
                            flag = 1;
                            cinter[k] = '1';
                        } else {
                            cinter[k] = '0';
                        }
                    }
                    
                    if (flag > 0) {
                        int val1 = strtol(cinter, NULL, 2);
                        int val2 = strtol(cfrom, NULL, 2);
                        
                        if ((val1 - val2) == 0) {
                            
                            for(k=0;k<rLen;k++){
                                if ( (from[k] > 0 && to[k] == 0) || (from[k] == 0 && to[k] > 0) ){
                                    cxor[k] = '1';
                                } else {
                                    cxor[k] = '0';
                                }
                            }
                            
                            pos  = strtol(cxor,NULL,2);
                            tm[(j) + (i)*(dim)] = spr[pos-1];
                            em[(j) + (i)*(dim)] = pos+1;
                        }
                    }    
                }
            }
        }
    }
}

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] )
{
  /* k is the kernel matrix, z vector indicating zero flux reactions */
  double *tm, *em, *spr, *motifs, *motifLen;
  int mCount, mrows, mcols;
  
  /* Check for proper number of arguments. */
  if(nrhs!=3) {
    mexErrMsgTxt("Three inputs required.\n\nUsage:\n\t[tm em] = tmatrix(spr, motifs, motifLen)");
  } else if(nlhs!=2) {
    mexErrMsgTxt("Two output arguments.\n\nUsage:\n\t[tm em] = tmatrix(spr, motifs, motifLen)");
  }
  
  /*  create pointers to input matrices spr, motifs, and motifLen */
  spr      = mxGetPr(prhs[0]);
  motifs   = mxGetPr(prhs[1]);
  motifLen = mxGetPr(prhs[2]);
  
  /* get input dimensions of k */
  mCount = mxGetM(prhs[0]);
  mrows = mxGetM(prhs[1]);
  mcols = mxGetN(prhs[1]);
  
  /* Create matrices for the return arguments */
  plhs[0] = mxCreateDoubleMatrix(mCount+1,mCount+1, mxREAL);
  plhs[1] = mxCreateDoubleMatrix(mCount+1,mCount+1, mxREAL);
  
  /*  create a C pointer to a copy of the output matrix */
  tm = mxGetPr(plhs[0]);
  em = mxGetPr(plhs[1]);
  
  /* Call the cmatrix subroutine. */
  transitionMatrix(tm,em,spr,motifs,motifLen,mCount);/*, mrows, mcols);*/
  
}
