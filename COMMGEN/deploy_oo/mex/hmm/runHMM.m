function D = runHMM(P, Q, Ext, rSize)

mCount = 2^rSize;
cutOff = rSize + 1;
H0     = zeros(mCount, 2*rSize+1); 
H0(1,rSize+1) = 1;
N      = rSize;

    D      = zeros(N + 1,2*rSize + 1);
    D(1,:) = H0(end,:);

P(1,1)=0;
for n = 1:N
    Hn = zeros(size(H0));
    % Get previous H matrix
    H_old = H0;
    % examine H entries (m = motifs)
    for m = 1:mCount
        % check directions
        for d=-rSize:rSize
            extensions = find(Ext(m,:));
            % check predecessors
            for e = 1:length(extensions)
                m2 = extensions(e);
                % check old directions
                for d2=-rSize:rSize
                    % emitted direction = new-old
                    emit = d-d2;
                    % if feasable, then
                    if(abs(emit)<=rSize)
                        Hn(m,d+cutOff) = Hn(m,d+cutOff) + H_old(m2,d2+cutOff) * P(m,m2) * Q(Ext(m,m2),emit+cutOff);
                    end
                end
            end
            % self-loop
            Hn(m,d+cutOff) = Hn(m,d+cutOff) + H_old(m,d+cutOff) * P(m,m)
        end
    end
    H0 = Hn;
    D(n+1,:) = Hn(end,:);
end