#include "mex.h"
#include "math.h"
/*
 * hmm.c 
 * Mathias Ganter, 02/2012
 *
 * runs the hmm algorithm
 *
 * Input:
 *  P       transition matrix
 *  E       motif extention matrix
 *  Q       emission matrix
 *  r       reaction size
 *
 * Output:
 *  D       probability distribution matrix
 *
 * Usage:
 *  D = hmm(P, E, Q, r);
 *
 * Compile:
 *  mex hmm.c
 *
 */
 

void hmm(double *p, double *e, double *q, double *d, int r, int prows, int pcols, int erows, int ecols, int qrows, int qcols, int drows, int dcols)
{
    
    int mCount = pow(2,r);
    int xdim   = mCount;
    int ydim   = (2*r)+1;
    int i=0, j=0, k=0, m=0, pos=0, d1=0, d2=0, ext=0, m1, m2, emit;
    
    double H0[xdim][ydim], Hn[xdim][ydim], Hold[xdim][ydim];
    for(i=0;i<xdim;i++) {
        for(j=0;j<ydim;j++) {
            H0[i][j] = 0;
            Hn[i][j] = 0;
            Hold[i][j] = 0;
        }
    }
    
    H0[0][r] = 1;
    p[0]     = 0;
        
    /* run HMM */
    for (k=0; k<r; k++) {
        /* init new Hn, keep track of current H0 */
        for(i=0;i<xdim;i++) {
            for(j=0;j<ydim;j++) {
                Hn[i][j]   = 0;
                Hold[i][j] = H0[i][j];
            }
        }
        /* examine H entries (m1 = motifs) */
        for(m1=1; m1<=mCount; m1++) {
            /* check directions */
            for(d1=-r;d1<=r;d1++) {
                /* check predecessors */
                for(ext=1;ext<=erows;ext++) {
                    if (e[(m1-1)+erows*(ext-1)] > 0) {
                        for(d2=-r;d2<=r;d2++) {
                            emit = d1-d2;
                            if (abs(emit)<=r) {
                                /*Hn(m,d+cutOff) = Hn(m,d+cutOff) + H_old(m2,d2+cutOff) * P(m,m2) * Q(Ext(m,m2),emit+cutOff);
                                 *printf("%i\t%i: %1.4f\n",m1,ext,p[m1-1+prows*(ext-1)]);
                                 *printf("Ext(m1,m2): Ext(%i,%i): %i\n", m1, ext, (int)e[(m1-1)+erows*(ext-1)]); */
                                pos = (int)e[(m1-1)+erows*(ext-1)]-1;
                                /*printf("Q(Ext(m1,m2),emit+cutoff) = Q(%i,%i) = %1.4f\n", (int)e[(m1-1)+erows*(ext-1)], emit+r+1, q[pos+(emit+r)*qrows]); */
                                Hn[m1-1][d1+r] = Hn[m1-1][d1+r] + Hold[ext-1][d2+r] * p[(m1-1)+prows*(ext-1)] * q[pos+(emit+r)*qrows];
                            }
                        }
                    }
                }
                /* self loop */
                Hn[m1-1][d1+r] = Hn[m1-1][d1+r] + Hold[m1-1][d1+r] * p[(m1-1)+prows*(m1-1)];
            }
        }
        
        for(i=0;i<xdim;i++) {
            for(j=0;j<ydim;j++) {
                H0[i][j] = Hn[i][j];
            }
        }
        
        for(j=0;j<ydim;j++) {
            pos = (k+1)+drows*j;
            d[pos] = Hn[xdim-1][j];
        }
    }
}

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] )
{
  /* p is the transition matrix, e is the motif extension matrix, q is the emission matrix
   * r is the reaction size
   */
  double *p, *e, *q, *d;
  double r;
  int prows, pcols, erows, ecols, qrows, qcols, drows, dcols;
  
  /* Check for proper number of arguments. */
  if(nrhs!=4) {
    mexErrMsgTxt("Four inputs required.\n\nUsage:\n\t[D] = hmm(P, E , Q, r)");
  } else if(nlhs!=1) {
    mexErrMsgTxt("One output required.\n\nUsage:\n\t[D] = hmm(P, E , Q, r)");
  }
  
  /*  create pointers to input matrix k and tolerance tol */
  p = mxGetPr(prhs[0]);
  e = mxGetPr(prhs[1]);
  q = mxGetPr(prhs[2]);
  r = mxGetScalar(prhs[3]);
  
  /* get input dimensions of k */
  prows = mxGetM(prhs[0]);
  pcols = mxGetN(prhs[0]);
  
  erows = mxGetM(prhs[1]);
  ecols = mxGetN(prhs[1]);
  
  qrows = mxGetM(prhs[2]);
  qcols = mxGetN(prhs[2]);
  
  /* Create matrix for the output argument */
  plhs[0] = mxCreateDoubleMatrix(r+1,2*r+1, mxREAL);
  
  /*  create a C pointer to a copy of the output matrix */
  d     = mxGetPr(plhs[0]);
  drows = mxGetM(plhs[0]);
  dcols = mxGetN(plhs[0]);
  
  /* Call the cmatrix subroutine. */
  hmm(p,e,q,d,r, prows, pcols, erows, ecols, qrows, qcols, drows, dcols);
}
