function cobra = ModelToCobra(model)
%%% PURPOSE
% Converts a COMMGEN model into a COBRA model.
%%% EXAMPLE CALL
%   COBRA_model = ModelToCobra(COMMGEN_model);
%%% INPUT
%   model: COMMGEN model structure.
%%% OPTIONAL INPUT
%%% OUTPUT
%   cobra: COBRA model structure

%% Initiate
%Check for COBRA toolbox
assert(exist('initCobraToolbox.m','file')==2,'This function requires the COBRA toolbox.\n Please add the COBRA toolbox to the MATLAB path.')

%Create cobra model
cobra = createModel;
%% Add metabolites
metNames = regexprep(cellstr(model.stoich.metabolite_ids),{'%','@([^ ])*'},{'','[$1]'});
for i=1:numel(model.species)
    MetMatches = find(model.metabolites2species==i);
    metFormula = model.species{i}.formula;
    metName    = model.species{i}.name;
    
    NumMets = numel(MetMatches);
    cobra.metNames(end+1:end+NumMets,1) = {metName};
    cobra.metFormulas(end+1:end+NumMets,1) = {metFormula};
    
    for j=1:NumMets
        metID = metNames(MetMatches(j));
        cobra.mets(end+1,1) = metID;
    end
end

%% Add genes
geneIDs = regexprep(model.locus.unique_loci,'.*:','');
geneIDs = reshape(geneIDs,numel(geneIDs),1);
cobra.genes = sort(geneIDs);

%% Add reactions
for i=1:model.stoich.r
    rxn = model.reactions{i};
    id = cellstr(rxn.id);
    equation = regexprep(rxn.equation,'@([^ ])*','[$1]');
    equation = regexprep(equation,'<--','-->'); %Cobra doesn't accept reactions that are defined from right to left.
    lb = rxn.lb;
    ub = rxn.ub;
    rev = lb < 0 && ub > 0;
    grRule = regexprep(rxn.proteins,{';','\+'},{' or ',' and '});
    grRule = regexprep(grRule,'[^ ]*:','');
    cobra = addReaction(cobra,id{1},equation,[],rev,lb,ub,0,[],grRule,cobra.genes,cobra.genes,false);
    cobra.ec_number{i} = rxn.ec_number;
    cobra.source{i} = rxn.source;
    cobra.references{i} = rxn.operator;
end

%% Fix reaction directionalities for exchange reactions 
%all exchange reactions should operate in the same direction (negative flux represents addition to medium)
er = findER(cobra);
er2 = find(er);
for i=1:numel(er2)
    if sum(cobra.S(:,er2(i)))>0
        cobra.S(:,er2(i))=-1*cobra.S(:,er2(i));
    end
end

% Allow Production of all species
cobra.ub(er) = max(cobra.ub);

%% Adjust biomass reaction
% The biomass reaction is set to the boundary reaction representing biomass
% production
biomassMet  = strncmp('BIOMASS',cobra.mets,7);
biomassRxns = any(cobra.S(biomassMet,:),1);
if sum(biomassRxns&er)>=1
    cobra.c(biomassRxns&er) = 1;
else
    disp('Biomass rxn not found, thus not set!')
end

end